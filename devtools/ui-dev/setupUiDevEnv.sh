#!/bin/bash

# Obsolete! The docker-compose script uses the latest tag from the gitlab registry

# This script sets up a backend environment for developing the frontend
# It provides a full fledged backend, so it can be used for system tests as well.
# It creates new docker containers from the currently checked out sources.

echo 'building the account api docker container'
docker.exe build --file ../../src/apis/src/FinanceTracker.Apis.Account/Dockerfile --tag accountapi:latest ../../src

echo 'building the accounting entry api docker container'
docker.exe build --file ../../src/apis/src/FinanceTracker.Apis.Accounting/Dockerfile --tag accountingapi:latest ../../src

echo 'starting the test environment'
docker-compose.exe up --detach
