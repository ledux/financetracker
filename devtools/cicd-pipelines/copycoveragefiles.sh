#!/bin/bash

COVERAGE_DIR=src/apis/coverage
COVERAGE_FILE_PATTERN=src/apis/tests/**/TestResults/**/coverage.cobertura.xml

mkdir --parents $COVERAGE_DIR

for file in $COVERAGE_FILE_PATTERN; do
    echo "copy coverage file $file"
    dir=`echo $file | rev | cut -d/ -f2 | rev`
    cp $file $COVERAGE_DIR/$dir.xml
done