﻿using FinanceTracker.Apis.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.OpenTracing.Extensions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.Accounting.Hosting
{
    /// <summary>
    /// Builds the host which acts as the API and listens to http requests
    /// </summary>
    public static class ApiHostBuilder
    {
        /// <summary>
        /// Configures the settings, adds the dependencies and builds the host
        /// </summary>
        /// <returns>The fully configured host, ready to run</returns>
        public static IWebHost BuildWebHost()
        {
            return WebHost.CreateDefaultBuilder()
                .ConfigureApiSettings()
                .ConfigureServices(AddServices)
                .Configure(ConfigureApp)
                .ConfigureLogging(SetupExtensions.ConfigureLogging)
                .Build();
        }

        private static void AddServices(WebHostBuilderContext context, IServiceCollection collection)
        {
            MongoDbRepositorySettings settings = collection.ConfigureMongo(context.Configuration);

            collection
                .AddControllersAndCors()
                .AddMongo(settings)
                .AddTracing(context.Configuration)
                .AddIdCreator()
                .AddAccountingServices()
                .ConfigureKafkaEventClient(context.Configuration);
        }

        private static void ConfigureApp(WebHostBuilderContext builderContext, IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseCors("frontend-policy")
                .UseRouting()
                .UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
