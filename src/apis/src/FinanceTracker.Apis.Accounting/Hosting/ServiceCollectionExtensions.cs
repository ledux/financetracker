﻿using FinanceTracker.Apis.Accounting.Producers;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.Accounting.Hosting
{
    /// <summary>
    /// Extends the service collection
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the necessary services for the accounting api
        /// </summary>
        /// <param name="serviceCollection">The service collection to which the services are added </param>
        /// <returns>The service collection in enabel chaining </returns>
        public static IServiceCollection AddAccountingServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IAccountingService, ApiAccountingService>()
                .AddTransient<IAccountingProducer, AccountingProducer>()
                .AddSingleton<IAccountingEntryRepository, MongoAccountingRepo>();

            return serviceCollection;
        }
    }
}
