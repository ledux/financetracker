﻿using System.EventSourcing.Hosting;
using FinanceTracker.Apis.Accounting.Projections;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Common.Extensions;
using FinanceTracker.Common.KafkaHostBuilder;
using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.Accounting.Hosting
{
    /// <summary>
    /// Builds a host which has all projections to consume the accounting entries
    /// </summary>
    public static class ProjectionHostBuilder
    {
        /// <summary>
        /// Configures the settings, adds the services and builds the host
        /// </summary>
        /// <returns>A fully configured host, ready to be run</returns>
        public static IHost BuildHost()
        {
            return KafkaHostBuilder.BuildKafkaHost()
                .UseAsConsumer(projections =>
                    projections
                        .AddProjection<AccountingEntryUpdatedProjection>()
                        .AddProjection<AccountingEntryDeletedProjection>(),
                    transformers => transformers)
                .ConfigureServices(
                    (hostBuilderContext, serviceCollection) =>
                    {
                        MongoDbRepositorySettings settings =
                            serviceCollection.ConfigureMongo(hostBuilderContext.Configuration);

                        serviceCollection
                            .AddMongo(settings)
                            .AddScoped<IAccountingService, ProjectionAccountingService>()
                            .AddSingleton<IAccountingEntryRepository, MongoAccountingRepo>();
                    })
                .ConfigureLogging(SetupExtensions.ConfigureLogging)
                .Build();
        }
    }
}
