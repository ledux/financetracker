﻿namespace FinanceTracker.Apis.Accounting.Services.Models
{
    /// <summary>
    /// Represents an accounting entry which will be deleted in the api service
    /// </summary>
    public class DeletedModel
    {
        /// <summary>
        /// The id of the accounting entry
        /// </summary>
        public long AccountingEntryId { get; set; }
        /// <summary>
        /// The amount of the accounting entry
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// The id of the account to which the accounting entry belongs
        /// </summary>
        public long AccountId { get; set; }
    }
}
