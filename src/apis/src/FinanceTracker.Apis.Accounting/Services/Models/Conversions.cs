﻿using DbModel = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.Services.Models
{
    /// <summary>
    /// Converts models from service to db and back
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Copies all properties from the db model to the service model
        /// </summary>
        /// <param name="dbModel">The data of the db model</param>
        /// <returns>The service model with the same data</returns>
        public static AccountingEntry ToServiceModel(this DbModel dbModel)
        {
            return new AccountingEntry
            {
                Amount = dbModel.Amount,
                Id = dbModel.Id,
                BookingDate = dbModel.BookingDate,
                Recurring = dbModel.Recurring,
                AccountId = dbModel.AccountId,
                Description = dbModel.Description,
                CategoryId = dbModel.CategoryId,
                Tags = dbModel.Tags,
                FileId = dbModel.FileId
            };
        }

        /// <summary>
        /// Copies all properties from the service model to the db model
        /// </summary>
        /// <param name="entry">The data of the db model</param>
        /// <returns>The db model with the same data</returns>
        public static DbModel ToDbModel(this AccountingEntry entry)
        {
            return new DbModel
            {
                Amount = entry.Amount,
                Id = entry.Id,
                BookingDate = entry.BookingDate,
                Recurring = entry.Recurring,
                AccountId = entry.AccountId,
                Description = entry.Description,
                CategoryId = entry.CategoryId,
                Tags = entry.Tags,
                FileId = entry.FileId
            };
        }
    }
}
