﻿using System;
using System.Collections.Generic;
using FinanceTracker.Apis.Common.DataModels;

namespace FinanceTracker.Apis.Accounting.Services.Models
{
    /// <summary>
    /// Represents an accounting entry for transfering to the client
    /// This means money is spent.
    /// </summary>
    public class AccountingEntry : IIdModel
    {
        /// <summary>
        /// The identification of the entry
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The amount which was spent
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// The id of the account, to which this entry belongs
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// Describes the accounting entry
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// If this spending is recurring, i.e this spending occurs every month
        /// </summary>
        public bool Recurring { get; set; }
        /// <summary>
        /// When this spending took/takes place
        /// </summary>
        public DateTimeOffset BookingDate { get; set; }
        /// <summary>
        /// Tags for grouping the spending
        /// </summary>
        public IEnumerable<string> Tags { get; set; }
        /// <summary>
        /// The id of the corresponding category
        /// </summary>
        public long CategoryId { get; set; }
        /// <summary>
        /// The identification of the file, if there is any
        /// </summary>
        public long? FileId { get; set; }
    }
}
