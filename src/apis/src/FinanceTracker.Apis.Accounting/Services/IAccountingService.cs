﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services.Models;

namespace FinanceTracker.Apis.Accounting.Services
{
    /// <summary>
    /// Functionality for dealing with the accounting entry
    /// </summary>
    public interface IAccountingService
    {
        /// <summary>
        /// Functionality for creating new <see cref="AccountingEntry"/>
        /// </summary>
        /// <param name="entry">The entry, which should be created</param>
        /// <returns></returns>
        Task<AccountingEntry> CreateOrUpdate(AccountingEntry entry);

        /// <summary>
        /// Reads an accounting entry by its id
        /// </summary>
        /// <param name="id">The identification of the entry</param>
        /// <returns>Null, if there is no entry with the id, othewise the entry</returns>
        Task<AccountingEntry> GetById(long id);

        /// <summary>
        /// Reads all accounting entries at a given date
        /// </summary>
        /// <param name="date">The day, from which the accounting entries are wanted</param>
        /// <returns>At least an empty enumerable</returns>
        Task<IEnumerable<AccountingEntry>> GetByDate(DateTimeOffset date);

        /// <summary>
        /// Reads all accounting entries which belong to a certain account
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>At least an empty array</returns>
        Task<IEnumerable<AccountingEntry>> GetByAccountId(long accountId);

        /// <summary>
        /// Deletes an accounting entry
        /// </summary>
        /// <param name="id">The identification of the entry</param>
        /// <returns></returns>
        Task Delete(long id);
    }
}