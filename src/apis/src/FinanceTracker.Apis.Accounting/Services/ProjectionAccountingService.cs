﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using DbModel = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.Services
{
    /// <summary>
    /// Service for the projection host for dealing with the <see cref="AccountingEntry"/>
    /// </summary>
    public class ProjectionAccountingService : BaseAccountingService
    {
        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionAccountingService"/>
        /// </summary>
        /// <param name="entryRepository">Persists entries</param>
        /// <param name="tracer">Traces a request through the application</param>
        public ProjectionAccountingService(IAccountingEntryRepository entryRepository, ITracer tracer) 
            : base(entryRepository, tracer) { }

        /// <summary>
        /// Functionality for creating new <see cref="AccountingEntry"/>
        /// </summary>
        /// <param name="entry">The entry, which should be created</param>
        /// <returns></returns>
        public override async Task<AccountingEntry> CreateOrUpdate(AccountingEntry entry)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, entry.Id);

            DbModel dbEntry = entry.ToDbModel();
            DbModel insertedModel = await Repository.Upsert(dbEntry);

            AccountingEntry serviceModel = null;
            if (insertedModel != null)
            {
                serviceModel = insertedModel.ToServiceModel();
            }

            return serviceModel;
        }

        /// <summary>
        /// Deletes an accounting entry
        /// </summary>
        /// <param name="id">The identification of the entry</param>
        /// <returns></returns>
        public override Task Delete(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (id == 0)
            {
                tracingSpan.LogError("id is zero", TracingStatus.InvalidArgument);
                throw new ArgumentException($"The id must not be zero");
            }
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, id);

            return Repository.Delete(id);
        }
    }
}
