﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Producers;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using RepoModel = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.Services
{
    /// <summary>
    /// API Service for dealing with the <see cref="AccountingEntry"/>
    /// </summary>
    public class ApiAccountingService : BaseAccountingService
    {
        private readonly IIdCreator _idCreator;
        private readonly IAccountingProducer _accountingProducer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ApiAccountingService"/>
        /// </summary>
        /// <param name="idCreator">Creates sortable and unique ids</param>
        /// <param name="accountingProducer">Sends <see cref="AccountingEntry"/> to the event sourcing</param>
        /// <param name="entryRepository">Persists entries</param>
        /// <param name="tracer">Traces a request through the application</param>
        public ApiAccountingService(IIdCreator idCreator,
            IAccountingProducer accountingProducer,
            IAccountingEntryRepository entryRepository,
            ITracer tracer)
            : base(entryRepository, tracer)
        {
            _idCreator = idCreator ?? throw new ArgumentNullException(nameof(idCreator));
            _accountingProducer = accountingProducer ?? throw new ArgumentNullException(nameof(accountingProducer));
        }

        /// <summary>
        /// Functionality for creating new <see cref="AccountingEntry"/>
        /// </summary>
        /// <param name="entry">The entry, which should be created</param>
        /// <returns></returns>
        public override async Task<AccountingEntry> CreateOrUpdate(AccountingEntry entry)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (entry == null)
            {
                return null;
            }

            if (await Repository.GetById(entry.Id) == null)
            {
                entry.Id = _idCreator.CreateId();
                tracingSpan.LogEvent(new TracingEvent($"accounting entry id '{entry.Id}' newly created"));
            }
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountingEntryId, entry.Id },
                { TracingKeys.AccountId, entry.AccountId }
            });

            await _accountingProducer.AccountingEntryCreated(entry);

            return entry;
        }

        /// <summary>
        /// Sends the id and the amount of the deleted accounting entry to the producer.
        /// It only does that, when there is an accounting entry in the repo.
        /// </summary>
        /// <param name="id">The identification of the entry</param>
        /// <returns></returns>
        public override async Task Delete(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, id);

            RepoModel accountingEntry = await Repository.GetById(id);
            if (accountingEntry == null)
            {
                string msg = $"The id '{id}' does not have an accounting entry";
                tracingSpan.LogError(msg, TracingStatus.NotFound);
            }
            else
            {
                await _accountingProducer.AccountingEntryDeleted(new DeletedModel
                {
                    AccountingEntryId = id,
                    Amount = accountingEntry.Amount,
                    AccountId = accountingEntry.AccountId
                });
            }
        }
    }
}
