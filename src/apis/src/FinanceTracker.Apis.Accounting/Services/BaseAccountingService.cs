﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using DbModel = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.Services
{
    /// <summary>
    /// Provides functionality for the <see cref="AccountingEntry"/> which are the same for the API as well as for the projections
    /// </summary>
    public abstract class BaseAccountingService : IAccountingService
    {
        /// <summary>
        /// The persistence layer for the accounting entry
        /// </summary>
        protected readonly IAccountingEntryRepository Repository;
        /// <summary>
        /// Traces a request through the application
        /// </summary>
        protected readonly ITracer Tracer;

        /// <summary>
        /// Instantiates a new instance of the dependent 
        /// </summary>
        /// <param name="repository">The persistence layer for the account entries</param>
        /// <param name="tracer">Traces a request through the application</param>
        protected BaseAccountingService(IAccountingEntryRepository repository, ITracer tracer)
        {
            Repository = repository ?? throw new ArgumentNullException(nameof(repository));
            Tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Functionality for creating new <see cref="AccountingEntry"/>
        /// </summary>
        /// <param name="entry">The entry, which should be created</param>
        /// <returns>The created or updated entry</returns>
        public abstract Task<AccountingEntry> CreateOrUpdate(AccountingEntry entry);

        /// <summary>
        /// Deletes an accounting entry
        /// </summary>
        /// <param name="id">The identification of the entry</param>
        /// <returns></returns>
        public abstract Task Delete(long id);

        /// <summary>
        /// Reads all accounting entries at a given date
        /// </summary>
        /// <param name="date">The day, from which the accounting entries are wanted, time is ignored</param>
        /// <returns>At least an empty enumerable</returns>
        public async Task<IEnumerable<AccountingEntry>> GetByDate(DateTimeOffset date)
        {
            var dateWithoutTime = new DateTimeOffset(date.Date);
            IEnumerable<DbModel> accountingEntries = await Repository.GetByDateTimeRange(dateWithoutTime, dateWithoutTime);

            return accountingEntries.Select(e => e.ToServiceModel());
        }

        /// <summary>
        /// Reads all accounting entries which belong to a certain account
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>At least an empty array</returns>
        public async Task<IEnumerable<AccountingEntry>> GetByAccountId(long accountId)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);

            IEnumerable<DbModel> accountingEntries = await Repository.GetByAccountId(accountId).ToArrayAsync();
            tracingSpan.LogEvent($"Found {accountingEntries.Count()} entries for account id {accountId}");
            IEnumerable<AccountingEntry> serviceModels = accountingEntries.Select(e => e.ToServiceModel());

            return serviceModels;
        }

        /// <summary>
        /// Reads an accounting entry by its id
        /// </summary>
        /// <param name="id">The identification of the entry</param>
        /// <returns>Null, if there is no entry with the id, othewise the entry</returns>
        public Task<AccountingEntry> GetById(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (id == 0)
            {
                const string errorMessage = "The id must not be zero, but a positive long number";
                tracingSpan.LogError(errorMessage, TracingStatus.InvalidArgument);
                throw new ArgumentException(errorMessage);
            }

            async Task<AccountingEntry> GetAccountingEntry(long l)
            {
                DbModel dbModel = await Repository.GetById(l);

                return dbModel?.ToServiceModel();
            }
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, id);

            return GetAccountingEntry(id);
        }
    }
}
