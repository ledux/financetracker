﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories.Models;

namespace FinanceTracker.Apis.Accounting.Repositories
{
    /// <summary>
    /// Accesses the persistencs layer in order to write and read accounting entries
    /// </summary>
    public interface IAccountingEntryRepository
    {
        /// <summary>
        /// Persists a new entity
        /// </summary>
        /// <param name="entry">The data to be persisted</param>
        /// <returns></returns>
        Task<AccountingEntry> Upsert(in AccountingEntry entry);

        /// <summary>
        /// Reads an entry by its id
        /// </summary>
        /// <param name="id">The id of the requested entry</param>
        /// <returns>Null, if not found, otherwise the entry</returns>
        Task<AccountingEntry> GetById(long id);

        /// <summary>
        /// Removes an entry from the persitence
        /// </summary>
        /// <param name="id">The Id of the entry to delete</param>
        /// <returns></returns>
        Task Delete(in long id);

        /// <summary>
        /// Searches for all entries in the persitence in a certain date range (inclusive)
        /// </summary>
        /// <param name="fromDate">The first date to be included in the result</param>
        /// <param name="toDate">The last date to be included in the result</param>
        /// <returns>At least an empty enumerable</returns>
        Task<IEnumerable<AccountingEntry>> GetByDateTimeRange(DateTimeOffset fromDate, DateTimeOffset toDate);
        /// <summary>
        /// Reads all accounting entries by the account they belong to
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>At least an empty array</returns>
        Task<IEnumerable<AccountingEntry>> GetByAccountId(long accountId);
    }
}