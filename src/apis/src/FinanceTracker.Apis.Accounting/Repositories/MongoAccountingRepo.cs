﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FinanceTracker.Apis.Accounting.Repositories
{
    /// <summary>
    /// Access to the mongo db persistence layer
    /// </summary>
    public class MongoAccountingRepo: MongoDbRepository<AccountingEntry>, IAccountingEntryRepository
    {
        private readonly ITracer _tracer;

        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected override string RepoName => "Accounting";

        /// <summary>
        /// Instantiates a new instance of <see cref="MongoAccountingRepo"/>
        /// </summary>
        /// <param name="client">The mongo client which connects to the db</param>
        /// <param name="optionsAccessor">Options for configuring the client</param>
        /// <param name="tracer">Traces the request through the application</param>
        public MongoAccountingRepo(IMongoClient client,
            IOptions<MongoDbRepositorySettings> optionsAccessor,
            ITracer tracer) : base(
            client,
            optionsAccessor)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));

            CreateIndexes();
        }

        /// <summary>
        /// Adds a new document into the accounting 
        /// </summary>
        /// <param name="entry">The data to be inserted</param>
        /// <returns></returns>
        public Task<AccountingEntry> Upsert(in AccountingEntry entry)
        {
            var options = new FindOneAndReplaceOptions<AccountingEntry, AccountingEntry>
            {
                IsUpsert = true ,
                ReturnDocument = ReturnDocument.After
            };
            FilterDefinition<AccountingEntry> filter = Builders<AccountingEntry>.Filter.Eq(e => e.Id, entry.Id);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, entry.Id);
            return Collection.FindOneAndReplaceAsync(filter, entry, options);
        }

        /// <summary>
        /// Reads an entry by its id
        /// </summary>
        /// <param name="id">The id of the requested entry</param>
        /// <returns>Null, if not found, otherwise the entry</returns>
        public async Task<AccountingEntry> GetById(long id)
        {
            FilterDefinition<AccountingEntry> filter = Builders<AccountingEntry>.Filter.Eq(a => a.Id, id);

            AccountingEntry entry = (await Collection.Find(filter).ToListAsync()).SingleOrDefault();

            return entry;
        }

        /// <summary>
        /// Removes an entry from the database
        /// </summary>
        /// <param name="id">The Id of the entry to delete</param>
        /// <returns></returns>
        public Task Delete(in long id)
        {
            FilterDefinition<AccountingEntry> filter = Builders<AccountingEntry>.Filter.Eq(a => a.Id, id);
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, id );
            return Collection.DeleteOneAsync(filter);
        }

        /// <summary>
        /// Searches for all entries in the persitence in a certain date range (inclusive)
        /// </summary>
        /// <param name="fromDate">The first date to be included in the result</param>
        /// <param name="toDate">The last date to be included in the result</param>
        /// <returns>At least an empty enumerable</returns>
        public async Task<IEnumerable<AccountingEntry>> GetByDateTimeRange(DateTimeOffset fromDate,
            DateTimeOffset toDate)
        {
            FilterDefinition<AccountingEntry> fromFiler =
                Builders<AccountingEntry>.Filter.Gte(e => e.BookingDate, fromDate);
            FilterDefinition<AccountingEntry> toFilter =
                Builders<AccountingEntry>.Filter.Lte(e => e.BookingDate, toDate);
            FilterDefinition<AccountingEntry> filter = Builders<AccountingEntry>.Filter.And(fromFiler, toFilter);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.LogEvent(new TracingEvent("GetByDateTimeRange", new Dictionary<string, object>
            {
                { "FromDate", fromDate },
                { "ToDate", toDate }
            }));
            return await Collection.Find(filter).ToListAsync();
        }

        /// <summary>
        /// Reads all accounting entries by the account they belong to
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>At least an empty array</returns>
        public async Task<IEnumerable<AccountingEntry>> GetByAccountId(long accountId)
        {
            FilterDefinition<AccountingEntry> accountIdFilter = 
                Builders<AccountingEntry>.Filter.Eq( e => e.AccountId, accountId);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);
            return await Collection.FindSync(accountIdFilter).ToListAsync();
        }

        private void CreateIndexes()
        {
            IndexKeysDefinition<AccountingEntry> bookingDateIndex =
                Builders<AccountingEntry>.IndexKeys.Descending(e => e.BookingDate);
            IndexKeysDefinition<AccountingEntry> accountIdIndex =
                Builders<AccountingEntry>.IndexKeys.Ascending(e => e.AccountId);
            var bookingDateIndexModel = new CreateIndexModel<AccountingEntry>(bookingDateIndex);
            var accountIdIndexModel = new CreateIndexModel<AccountingEntry>(accountIdIndex);
            Collection.Indexes.CreateMany(new[] { bookingDateIndexModel, accountIdIndexModel });
        }
    }
}
