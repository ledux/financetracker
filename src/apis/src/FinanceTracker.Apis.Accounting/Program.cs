using System;
using System.Threading;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Hosting;
using FinanceTracker.Apis.Common.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.Accounting
{
    /// <summary>
    /// Starting point of the accounting entry application
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main method of the application
        /// </summary>
        /// <param name="args">This application does not accept any arguments</param>
        /// <returns></returns>
        public static async Task Main(string[] args)
        {
            IWebHost webHost = ApiHostBuilder.BuildWebHost();
            IHost projectionHost = ProjectionHostBuilder.BuildHost();

            await new Func<CancellationToken, Task>[]
            {
                token => webHost.RunAsync(token),
                token => projectionHost.RunAsync(token)
            }.Run();
        }
    }
}
