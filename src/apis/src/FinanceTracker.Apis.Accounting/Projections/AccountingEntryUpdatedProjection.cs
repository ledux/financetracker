﻿using System;
using System.Collections.Generic;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Accounting.Projections
{
    /// <summary>
    /// Handles the accountingEntry.updated event
    /// </summary>
    public class AccountingEntryUpdatedProjection : IProjection<AccountingEntryUpdated>
    {
        private readonly IAccountingService _accountingService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingEntryUpdatedProjection"/>
        /// </summary>
        /// <param name="accountingService"></param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingEntryUpdatedProjection(IAccountingService accountingService, ITracer tracer)
        {
            _accountingService = accountingService ?? throw new ArgumentNullException(nameof(accountingService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Inserts the accounting entry to persistence
        /// </summary>
        /// <param name="event">The event from the event sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountingEntryId, @event.Id },
                { TracingKeys.AccountId, @event.AccountId }
            });

            var accountingEntry = new AccountingEntry
            {
                Amount = @event.Amount,
                Id = @event.Id,
                BookingDate = @event.BookingDate,
                Recurring = @event.Recurring,
                AccountId = @event.AccountId,
                Description = @event.Description,
                CategoryId = @event.CategoryId,
                Tags = @event.Tags,
                FileId = @event.FileId
            };

            return _accountingService.CreateOrUpdate(accountingEntry);
        }
    }
}
