﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Accounting.Projections
{
    /// <summary>
    /// Handles the <see cref="AccountingEntryDeleted"/> event
    /// </summary>
    public class AccountingEntryDeletedProjection : IProjection<AccountingEntryDeleted>
    {
        private readonly IAccountingService _accountingService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingEntryDeletedProjection"/>
        /// </summary>
        /// <param name="accountingService">Service to handle accounting entries</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingEntryDeletedProjection(IAccountingService accountingService, ITracer tracer)
        {
            _accountingService = accountingService ?? throw new ArgumentNullException(nameof(accountingService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Listens to the <see cref="AccountingEntryDeleted"/> event and processes it
        /// </summary>
        /// <param name="event">The data from the event sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryDeleted @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, @event.Id);
            return _accountingService.Delete(@event.Id);
        }
    }
}
