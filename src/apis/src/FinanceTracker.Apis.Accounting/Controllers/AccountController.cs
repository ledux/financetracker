﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.Extensions;
using Microsoft.AspNetCore.Mvc;
using Conversions = FinanceTracker.Apis.Accounting.Controllers.Models.Conversions;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.Controllers
{

    /// <summary>
    /// Controller for the <see cref="WebModel"/> depending on an account
    /// </summary>
    [Route("v1/accounting/[controller]"), ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountingService _accountingService;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountController"/>
        /// </summary>
        /// <param name="accountingService">The service to get the <see cref="ServiceModel"/></param>
        public AccountController(IAccountingService accountingService)
        {
            _accountingService = accountingService ?? throw new ArgumentNullException(nameof(accountingService));
        }

        /// <summary>
        /// Returns all <see cref="WebModel"/> which belongs to a certain account
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>BadRequest when <paramref name="accountId"/> is zero,
        /// NotFound, if no accounting entries are found</returns>
        [HttpGet("{accountId}")]
        public async Task<IActionResult> GetByAccount(long accountId)
        {
            if (accountId == 0) 
            {
                return BadRequest($"{nameof(accountId)} cannot be zero");
            }

            IEnumerable<ServiceModel> serviceModels =
                await _accountingService.GetByAccountId(accountId).ToArrayAsync();
            if (!serviceModels.Any())
            {
                return NotFound();
            }

            IEnumerable<WebModel> accountingEntries = serviceModels.Select(Conversions.ToWebModel);


            return Ok(accountingEntries);
        }
    }
}
