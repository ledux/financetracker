﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services;
using Microsoft.AspNetCore.Mvc;
using ArgumentOutOfRangeException = System.ArgumentOutOfRangeException;
using FinanceTracker.Apis.Accounting.Controllers.Models;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinanceTracker.Apis.Accounting.Controllers
{
    /// <summary>
    /// Web api for dealing with accounting entries
    /// </summary>
    [Route("v1/[controller]")]
    [ApiController]
    public class AccountingController : ControllerBase
    {
        private readonly IAccountingService _accountingService;

        /// <summary>
        /// Instantiates a new instance of  <see cref="AccountingController"/>
        /// </summary>
        /// <param name="accountingService">The service used for dealing with the accounting entries</param>
        public AccountingController(IAccountingService accountingService)
        {
            _accountingService = accountingService ?? throw new ArgumentNullException(nameof(accountingService));
        }

        /// <summary>
        /// Returns an accounting entry by its id
        /// NotFound if there is none with the corresponding id
        /// </summary>
        /// <param name="id">The id of the requested entry</param>
        /// <returns>Not found if there is no entry with the provided id</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(long id)
        {
            ServiceModel entry = await _accountingService.GetById(id);

            if (entry != null)
            {
                return Ok(entry.ToWebModel());
            }

            return NotFound();
        }

        /// <summary>
        /// Returns the accounting entries from a given date
        /// </summary>
        /// <param name="year">The year of the date</param>
        /// <param name="month">The month of the date</param>
        /// <param name="day">The day of the date</param>
        /// <returns>Bad request, if the parameter cannot form a valid date. Otherwise the entries from that date</returns>
        [HttpGet("{year:int}-{month:int}-{day:int}")]
        public async Task<IActionResult> GetByDate(int year, int month, int day)
        {
            if (month < 1 || month > 12)
            {
                return BadRequest($"Value of month '{month}' is not in the expected range (1 - 12)");
            }

            if (day < 1 || day > 31)
            {
                return BadRequest($"Value of day '{day}' is not in the expected range (1 - 31)");
            }

            DateTimeOffset date;
            try
            {
                date = new DateTimeOffset(year, month, day, 0, 0, 0, TimeSpan.Zero);
            }
            catch (ArgumentOutOfRangeException)
            {
                return BadRequest($"Value of day '{day}' is not in the expected range (1 - 31)");
            }

            IEnumerable<ServiceModel> entries = await _accountingService.GetByDate(date);

            IEnumerable<WebModel> webModels = entries.Select(e => e.ToWebModel()).ToArray();

            return Ok(webModels);
        }

        /// <summary>
        /// Creates a new accounting entry
        /// </summary>
        /// <param name="accountingEntry">The data of the entry which should be created</param>
        /// <returns>An accepted result with the location; BadRequest, if it is malformed</returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WebModel accountingEntry)
        {
            if (accountingEntry == null)
            {
                return BadRequest();
            }

            ServiceModel created = await _accountingService.CreateOrUpdate(accountingEntry.ToServiceModel());

            return AcceptedAtAction("GetById", new { id = created.Id }, created.ToWebModel());
        }

        /// <summary>
        /// Updates an existing accounting entry. Not used for creating.
        /// </summary>
        /// <param name="id">The id of the updated entry</param>
        /// <param name="accountingEntry">The entry to update</param>
        /// <returns>NoContent, if successful. Bad request, if the entry is malformed</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] PutModel accountingEntry)
        {
            if (accountingEntry == null)
            {
                return BadRequest();
            }

            if (id == 0)
            {
                return BadRequest(new { errorMessage = "The path id cannot be zero" });
            }

            ServiceModel serviceModel = accountingEntry.ToServiceModel(id);

            await _accountingService.CreateOrUpdate(serviceModel);

            return NoContent();
        }

        /// <summary>
        /// Deletes a existing entry
        /// </summary>
        /// <param name="id">The id of the entry, which should be deleted.</param>
        /// <returns>NoContent</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            if (id == 0)
            {
                return BadRequest();
            }

            await _accountingService.Delete(id);

            return NoContent();
        }
    }
}
