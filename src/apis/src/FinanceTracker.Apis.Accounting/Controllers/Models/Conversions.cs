﻿using FinanceTracker.Apis.Common.DataModels;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.Controllers.Models
{
    /// <summary>
    /// Converts web models to service models and vice versa
    /// </summary>
    internal static class Conversions
    {
        /// <summary>
        /// Converts a service model to a web model
        /// </summary>
        /// <param name="model">The data of the service model</param>
        /// <returns>The same data in a web model</returns>
        public static WebModel ToWebModel(this ServiceModel model)
        {
            return new AccountingEntry
            {
                Id = model.Id.ToString(),
                Amount = model.Amount,
                AccountId = model.AccountId.ToString(),
                Description = model.Description,
                BookingDate = model.BookingDate,
                Recurring = model.Recurring,
                CategoryId = model.CategoryId.ToString(),
                Tags = model.Tags,
                FileId = model.FileId.ToString()
            };
        }

        /// <summary>
        /// Converts a web model into a service model
        /// </summary>
        /// <param name="model">The data in the web model</param>
        /// <returns>The same data in a service model</returns>
        public static ServiceModel ToServiceModel(this WebModel model)
        {
            var serviceModel = new ServiceModel
            {
                Amount = model.Amount,
                BookingDate = model.BookingDate,
                Recurring = model.Recurring,
                Description = model.Description,
                Tags = model.Tags,
            };

            serviceModel.AssignId(model.Id);
            serviceModel.AssignId(model.AccountId, (entry, id) => entry.AccountId = id);
            serviceModel.AssignId(model.CategoryId, (entry, id) => entry.CategoryId = id);
            serviceModel.AssignId(model.FileId, (entry, id) => entry.FileId = id);

            return serviceModel;
        }

        /// <summary>
        /// Converts a PutModel into a service model
        /// </summary>
        /// <param name="model">The data from the controller</param>
        /// <param name="entryId">The missing id in the Put model</param>
        /// <returns>The same data in a service model</returns>
        public static ServiceModel ToServiceModel(this PutModel model, long entryId)
        {
            var serviceModel = new ServiceModel
            {
                Id =  entryId,
                Description = model.Description,
                Amount = model.Amount,
                BookingDate = model.BookingDate,
                Recurring = model.Recurring,
                Tags = model.Tags
            };

            serviceModel.AssignId(model.AccountId, (entry, id) => entry.AccountId = id);
            serviceModel.AssignId(model.CategoryId, (entry, id) => entry.CategoryId = id);
            serviceModel.AssignId(model.FileId, (entry, id) => entry.FileId = id);

            return serviceModel;
        }
    }
}
