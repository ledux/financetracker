﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services.Models;

namespace FinanceTracker.Apis.Accounting.Producers
{
    /// <summary>
    /// Service to produce events with <see cref="AccountingEntry"/> data as payload
    /// </summary>
    public interface IAccountingProducer
    {
        /// <summary>
        /// Sends an created event to the event sourcing
        /// </summary>
        /// <param name="entry">The payload for the event</param>
        /// <returns>Task</returns>
        Task AccountingEntryCreated(AccountingEntry entry);

        /// <summary>
        /// Sends a deleted event to the event sourcing
        /// </summary>
        /// <param name="accountingEntry"></param>
        /// <returns></returns>
        Task AccountingEntryDeleted(DeletedModel accountingEntry);
    }
}