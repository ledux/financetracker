﻿using System;
using System.Collections.Generic;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Accounting.Producers
{
    /// <summary>
    /// Produces events concerning the accounting entries
    /// </summary>
    public class AccountingProducer : IAccountingProducer
    {
        private readonly IEventClient _eventClient;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingProducer"/>
        /// </summary>
        /// <param name="eventClient">This send events to the event sourcing</param>
        /// <param name="tracer">Follows the request</param>
        public AccountingProducer(IEventClient eventClient, ITracer tracer)
        {
            _eventClient = eventClient ?? throw new ArgumentNullException(nameof(eventClient));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends an <see cref="AccountingEntryCreated"/> event to the event sourcing
        /// </summary>
        /// <param name="entry">The data which should be sent to the event sourcing</param>
        /// <returns></returns>
        public Task AccountingEntryCreated(AccountingEntry entry)
        {
            if (entry.Id is 0L)
            {
                throw new ArgumentException("The Id of the Accounting entry must not be 0, but rather a positive long value");
            }

            var createdEvent = new AccountingEntryUpdated
            {
                Amount = entry.Amount,
                Id = entry.Id,
                BookingDate = entry.BookingDate,
                Recurring = entry.Recurring,
                AccountId = entry.AccountId,
                Description = entry.Description,
                CategoryId = entry.CategoryId,
                FileId = entry.FileId,
                Tags = entry.Tags
            };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountingEntryId, entry.Id },
                {TracingKeys.AccountId, entry.AccountId }
            }); 

            return _eventClient.Publish(createdEvent);
        }

        /// <summary>
        /// Sends a deleted event to the event sourcing
        /// </summary>
        /// <param name="accountingEntry"></param>
        /// <returns></returns>
        public Task AccountingEntryDeleted(DeletedModel accountingEntry)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, accountingEntry.AccountingEntryId);
            return _eventClient.Publish(new AccountingEntryDeleted
            {
                Id = accountingEntry.AccountingEntryId,
                Amount = accountingEntry.Amount,
                AccountId = accountingEntry.AccountId
            });
        }
    }
}