﻿using System.EventSourcing.Hosting;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Common.KafkaHostBuilder;
using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.SavingsTarget.Hosting
{
    /// <summary>
    /// Creates a host for running the projections
    /// </summary>
    public static class ProjectionHostBuilder
    {
        /// <summary>
        /// Builds and configures the host
        /// </summary>
        /// <returns>A fully configured host, ready to be run</returns>
        public static IHost Build()
        {
            return KafkaHostBuilder
                .BuildKafkaHost()
                .UseAsConsumer(
                    AddProjections,
                    builder => builder)
                .ConfigureServices(((context, collection) =>
                {
                    MongoDbRepositorySettings settings = collection.ConfigureMongo(context.Configuration);

                    collection
                        .AddMongo(settings)
                        .AddProjectionServices();
                }))
                .Build();
        }

        private static IEventSourcingBuilder<IServiceCollection> AddProjections(IEventSourcingBuilder<IServiceCollection> builder)
        {
            builder
                .AddProjection<SavingsTargetDeletedProjection>()
                .AddProjection<AccountUpdatedProjection>()
                .AddProjection<AccountingEntryUpdatedProjection>()
                .AddProjection<AccountingEntryDeletedProjection>()
                .AddProjection<SavingsTargetBalanceSetProjection>()
                .AddProjection<SavingsTargetUpdatedProjection>();

            return builder;
        }
    }
}
