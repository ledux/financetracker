﻿using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.SavingsTarget.Hosting
{
    /// <summary>
    /// Registers the necessary services to the service collection
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// All necessary services for the savings targets api will be added to the service collection
        /// </summary>
        /// <param name="collection">The collection to which the services will be added</param>
        /// <returns>The same service collection</returns>
        public static IServiceCollection AddSavingsTargetApiServices(this IServiceCollection collection)
        {
            collection
                .AddScoped<ISavingsTargetService<SavingsTargetWrite>, ApiSavingsTargetService>()
                .AddScoped<ISavingsTargetProducer, SavingsTargetProducer>()
                .AddSingleton<IFreeAssetsRepository, MongoFreeAssetsRepo>()
                .AddSingleton<ISavingsTargetRepository, MongoSavingsTargetRepo>();

            return collection;
        }

        /// <summary>
        /// All necessary services for the savings targets projection hose will be added to the service collection
        /// </summary>
        /// <param name="collection">The collection to which the services will be added</param>
        /// <returns>The same service collection</returns>
        public static IServiceCollection AddProjectionServices(this IServiceCollection collection)
        {
            collection
                .AddScoped<ISavingsTargetService<Services.Models.SavingsTarget>, ProjectionSavingsTargetService>()
                .AddScoped<IFreeAssetsService, ProjectionFreeAssetsService>()
                .AddSingleton<IFreeAssetsRepository, MongoFreeAssetsRepo>()
                .AddSingleton<ISavingsTargetRepository, MongoSavingsTargetRepo>();

            return collection;
        }
    }
}
