﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.SavingsTarget.Projections
{
    /// <summary>
    /// Listens to the account updated event and handles it
    /// </summary>
    public class AccountUpdatedProjection : IProjection<AccountUpdated>
    {
        private readonly IFreeAssetsService _freeAssetsService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of the <see cref="AccountUpdatedProjection"/>
        /// </summary>
        /// <param name="freeAssetsService">The service which handles the business logic about the free assets of an account</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountUpdatedProjection(IFreeAssetsService freeAssetsService, ITracer tracer)
        {
            _freeAssetsService = freeAssetsService ?? throw new ArgumentNullException(nameof(freeAssetsService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Creates a new entry of the free assets of an account
        /// </summary>
        /// <param name="event">The data of the updated account</param>
        /// <returns></returns>
        public Task Handle(AccountUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, @event.Id);
            return _freeAssetsService.Create(new FreeAssets { AccountId = @event.Id, Amount = @event.Balance });
        }
    }
}
