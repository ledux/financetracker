﻿using System;
using System.Collections.Generic;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.SavingsTarget.Projections
{
    /// <summary>
    /// Listens to the accounting entry updated event in the savings target application
    /// </summary>
    public class AccountingEntryUpdatedProjection : IProjection<AccountingEntryUpdated>
    {
        private readonly IFreeAssetsService _freeAssetsService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingEntryUpdatedProjection"/>
        /// </summary>
        /// <param name="freeAssetsService">Provides functionality to deal with free assets of an account</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingEntryUpdatedProjection(IFreeAssetsService freeAssetsService, ITracer tracer)
        {
            _freeAssetsService = freeAssetsService ?? throw new ArgumentNullException(nameof(freeAssetsService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Updates the free assets of the account to which the accounting entry belongs
        /// </summary>
        /// <param name="event">The data of the event</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountId, @event.AccountId },
                { TracingKeys.AccountingEntryId, @event.Id }
            });

            return _freeAssetsService.Update(@event.AccountId, @event.Amount);
        }
    }
}
