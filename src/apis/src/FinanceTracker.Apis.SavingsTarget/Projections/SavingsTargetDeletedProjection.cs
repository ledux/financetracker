﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Projections
{
    /// <summary>
    /// Listens on the deleted even of a savings target
    /// </summary>
    public class SavingsTargetDeletedProjection : IProjection<SavingsTargetDeleted>
    {
        private readonly ISavingsTargetService<ServiceModel> _savingsTargetService;
        private readonly IFreeAssetsService _freeAssetsService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="SavingsTargetDeletedProjection"/>
        /// </summary>
        /// <param name="savingsTargetService">The functionality for the savings target in the projection host</param>
        /// <param name="freeAssetsService">The functionality for the free assets</param>
        /// <param name="tracer">Traces a request through the application</param>
        public SavingsTargetDeletedProjection(ISavingsTargetService<ServiceModel> savingsTargetService,
            IFreeAssetsService freeAssetsService,
            ITracer tracer)
        {
            _savingsTargetService = savingsTargetService ?? throw new ArgumentNullException(nameof(savingsTargetService));
            _freeAssetsService = freeAssetsService ?? throw new ArgumentNullException(nameof(freeAssetsService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Updates the free assets and deletes the savings target
        /// </summary>
        /// <param name="event">The data associated with the event</param>
        /// <returns></returns>
        public async Task Handle(SavingsTargetDeleted @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, @event.SavingsTargetId);

            await _freeAssetsService.Update(new SavingsTargetBalance
            {
                SavingsTargetId = @event.SavingsTargetId, Balance = 0
            });

            await _savingsTargetService.Delete(@event.SavingsTargetId);
        }
    }
}
