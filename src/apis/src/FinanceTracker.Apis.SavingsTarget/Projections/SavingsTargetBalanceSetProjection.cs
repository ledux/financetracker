﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Projections
{
    /// <summary>
    /// Handles the <see cref="SavingsTargetBalanceSet"/> event
    /// </summary>
    public class SavingsTargetBalanceSetProjection : IProjection<SavingsTargetBalanceSet>
    {
        private readonly ISavingsTargetService<ServiceSavingsTarget> _savingsTargetService;
        private readonly IFreeAssetsService _freeAssetsService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="SavingsTargetBalanceSetProjection"/>
        /// </summary>
        /// <param name="savingsTargetService">The projection service for the savings target</param>
        /// <param name="tracer">Traces a request through the application</param>
        /// <param name="freeAssetsService">The service for dealing with the free assets</param>
        public SavingsTargetBalanceSetProjection(ISavingsTargetService<ServiceSavingsTarget> savingsTargetService,
            IFreeAssetsService freeAssetsService,
            ITracer tracer)
        {
            _savingsTargetService = savingsTargetService ?? throw new ArgumentNullException(nameof(savingsTargetService));
            _freeAssetsService = freeAssetsService ?? throw new ArgumentNullException(nameof(freeAssetsService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends the information to the projection service
        /// </summary>
        /// <param name="event">The data from the event sourcing</param>
        /// <returns></returns>
        public async Task Handle(SavingsTargetBalanceSet @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, @event.SavingsTargetId);

            await _freeAssetsService.Update(new SavingsTargetBalance
            {
                SavingsTargetId = @event.SavingsTargetId, 
                Balance = @event.Balance
            });

            await _savingsTargetService.SetBalance(new SetBalance
            {
                SavingsTargetId = @event.SavingsTargetId,
                Balance = @event.Balance
            });
        }
    }
}
