﻿using System;
using System.Collections.Generic;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Projections
{
    /// <summary>
    /// Listens to the savings target updated event
    /// </summary>
    public class SavingsTargetUpdatedProjection : IProjection<SavingsTargetUpdated>
    {
        private readonly ISavingsTargetService<ServiceModel> _savingsTargetService;
        private readonly IFreeAssetsService _freeAssetsService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="SavingsTargetUpdatedProjection"/>
        /// </summary>
        /// <param name="savingsTargetService">Business functionality for the savings target</param>
        /// <param name="freeAssetsService">Business functionality for the free assets</param>
        /// <param name="tracer">Traces a request through the application</param>
        public SavingsTargetUpdatedProjection(ISavingsTargetService<ServiceModel> savingsTargetService,
            IFreeAssetsService freeAssetsService,
            ITracer tracer)
        {
            _savingsTargetService = savingsTargetService ?? throw new ArgumentNullException(nameof(savingsTargetService));
            _freeAssetsService = freeAssetsService ?? throw new ArgumentNullException(nameof(freeAssetsService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Handles the event, sends it to the service
        /// </summary>
        /// <param name="event">The data from the event</param>
        /// <returns></returns>
        public Task Handle(SavingsTargetUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(
                new Dictionary<string, object>
                {
                    { TracingKeys.SavingsTargetId, @event.Id },
                    { TracingKeys.AccountId, @event.AccountId }
                });

            var serviceData = new ServiceModel
            {
                Id = @event.Id,
                AccountId = @event.AccountId,
                Name = @event.Name,
                Description = @event.Description,
                StartAmount = @event.StartAmount,
                TargetAmount = @event.TargetAmount,
                TargetDate = @event.TargetDate,
                Balance = @event.Balance,
                StartDate = @event.StartDate
            };

            _freeAssetsService.Update(@event.AccountId, @event.Balance * -1);

            return _savingsTargetService.CreateOrUpdate(serviceData);
        }
    }
}