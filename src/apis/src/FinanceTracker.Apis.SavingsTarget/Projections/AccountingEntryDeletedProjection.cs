﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.SavingsTarget.Projections
{
    /// <summary>
    /// Gets called when a accounting entry has to be deleted
    /// </summary>
    public class AccountingEntryDeletedProjection : IProjection<AccountingEntryDeleted>
    {
        private readonly IFreeAssetsService _freeAssetsService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingEntryDeletedProjection"/>
        /// </summary>
        /// <param name="freeAssetsService">Functionality to deal with the free assets of an account</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingEntryDeletedProjection(IFreeAssetsService freeAssetsService, ITracer tracer)
        {
            _freeAssetsService = freeAssetsService ?? throw new ArgumentNullException(nameof(freeAssetsService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Updates the free assets of an account, when an accounting entry gets deleted.
        /// </summary>
        /// <param name="event">The data of the deleted accounting entry</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryDeleted @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, @event.Id);

            return _freeAssetsService.Update(@event.AccountId, @event.Amount * -1);
        }
    }
}
