﻿using System;
using System.Collections.Generic;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Producers
{
    /// <summary>
    /// Sends event data for <see cref="ServiceModel"/>
    /// </summary>
    public class SavingsTargetProducer : ISavingsTargetProducer
    {
        private readonly IEventClient _eventClient;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="SavingsTargetProducer"/>
        /// </summary>
        /// <param name="eventClient">The client, which sends the events to the event sourcing</param>
        /// <param name="tracer">Traces a request through the application</param>
        public SavingsTargetProducer(IEventClient eventClient, ITracer tracer)
        {
            _eventClient = eventClient ?? throw new ArgumentNullException(nameof(eventClient));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends a updated event for a savings target
        /// </summary>
        /// <param name="savingsTarget">The data which was updated</param>
        /// <returns></returns>
        public Task SavingsTargetUpdated(ServiceModel savingsTarget)
        {
            if (savingsTarget.Id == 0)
            {
                throw new ArgumentException($"{nameof(savingsTarget.Id)} must not be zero");
            }

            var eventData = new SavingsTargetUpdated
            {
                Id = savingsTarget.Id,
                AccountId = savingsTarget.AccountId,
                Name = savingsTarget.Name,
                Description = savingsTarget.Description,
                Balance = savingsTarget.Balance,
                StartAmount = savingsTarget.StartAmount,
                TargetDate = savingsTarget.TargetDate,
                TargetAmount = savingsTarget.TargetAmount,
                StartDate = savingsTarget.StartDate
            };

            string operationName = this.GetMethodName();
            using ITracingSpan tracingSpan = _tracer.BuildSpan(operationName);
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.SavingsTargetId, savingsTarget.Id },
                { TracingKeys.AccountId, savingsTarget.AccountId }
            });

            return _eventClient.Publish(eventData);
        }

        /// <summary>
        /// Sends a deleted event for a savings target
        /// </summary>
        /// <param name="savingsTargetId">The id of the deleted savings target</param>
        /// <returns></returns>
        public Task SavingsTargetDeleted(long savingsTargetId)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, savingsTargetId);
            var deletedEvent = new SavingsTargetDeleted { SavingsTargetId = savingsTargetId };
            return _eventClient.Publish(deletedEvent);
        }

        /// <summary>
        /// Sends a balance set event for a savings target
        /// </summary>
        /// <param name="setBalance">The data for the event</param>
        /// <returns></returns>
        public Task SavingsTargetBalanceSet(SetBalance setBalance)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            if (setBalance == null)
            {
                tracingSpan.LogError($"The parameter {nameof(setBalance)} is null", TracingStatus.InvalidArgument);
                throw new ArgumentNullException(nameof(setBalance));
            }

            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, setBalance.SavingsTargetId);
            var savingsTargetBalanceSet = new SavingsTargetBalanceSet
            {
                SavingsTargetId = setBalance.SavingsTargetId,
                Balance = setBalance.Balance
            };

#pragma warning disable S1481 // Unused local variables should be removed; This is a discard variable, not used by design
            using ITracingSpan _ = _tracer.BuildSpan(this.GetMethodName() + " Publish");
#pragma warning restore S1481 // Unused local variables should be removed
            return _eventClient.Publish(savingsTargetBalanceSet);
        }
    }
}