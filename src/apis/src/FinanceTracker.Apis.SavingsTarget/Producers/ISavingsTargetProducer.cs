﻿using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Producers
{
    /// <summary>
    /// Produces events to the event sourcing
    /// </summary>
    public interface ISavingsTargetProducer
    {
        /// <summary>
        /// Sends a updated event for a savings target
        /// </summary>
        /// <param name="savingsTarget">The data which was updated</param>
        /// <returns></returns>
        Task SavingsTargetUpdated(ServiceSavingsTarget savingsTarget);

        /// <summary>
        /// Sends a deleted event for a savings target
        /// </summary>
        /// <param name="savingsTargetId">The id of the deleted savings target</param>
        /// <returns></returns>
        Task SavingsTargetDeleted(long savingsTargetId);

        /// <summary>
        /// Sends a balance set event for a savings target
        /// </summary>
        /// <param name="setBalance">The data for the event</param>
        /// <returns></returns>
        Task SavingsTargetBalanceSet(SetBalance setBalance);
    }
}