﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using ServiceUpdateBalance = FinanceTracker.Apis.SavingsTarget.Services.Models.SetBalance;
using RepoUpdateBalance = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SetBalance;

namespace FinanceTracker.Apis.SavingsTarget.Services
{
    /// <summary>
    /// The service functionality for the savings target in the projection host
    /// </summary>
    public class ProjectionSavingsTargetService : BaseSavingsTargetService<ServiceSavingsTarget>
    {
        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionSavingsTargetService"/>
        /// </summary>
        /// <param name="savingsTargetRepository">The persistence layer for savings targets</param>
        /// <param name="tracer">Traces a request through the application</param>
        /// <param name="freeAssetsRepository">The persistence layer of the free assets of an account</param>
        public ProjectionSavingsTargetService(ISavingsTargetRepository savingsTargetRepository,
            IFreeAssetsRepository freeAssetsRepository,
            ITracer tracer)
            : base(tracer, savingsTargetRepository, freeAssetsRepository)
        { }

        /// <summary>
        /// Creates or updates a <see cref="Models.SavingsTarget"/> wether it exists or not
        /// </summary>
        /// <param name="savingsTarget">The data to be created or updated</param>
        /// <returns>The created or updated entity</returns>
        public override async Task<ServiceSavingsTarget> CreateOrUpdate(ServiceSavingsTarget savingsTarget)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountId, savingsTarget.AccountId },
                { TracingKeys.SavingsTargetId, savingsTarget.Id }
            });

            RepoSavingsTarget dbModel = await SavingsTargetRepository.Upsert(savingsTarget.ToDbModel());

            return dbModel?.ToServiceModel();
        }

        /// <summary>
        /// Sends the id to the repo in order to remvove the entry from the persistence layer
        /// </summary>
        /// <param name="id">The id of the savings target to be deleted</param>
        /// <returns></returns>
        public override Task Delete(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, id);
            return SavingsTargetRepository.Delete(id);
        }

        /// <summary>
        /// Calls the repo to set the new value of the balance
        /// </summary>
        /// <param name="updateBalance">The information for setting the new value</param>
        /// <returns>The updated savings target</returns>
        public override Task<ServiceSavingsTarget> SetBalance(ServiceUpdateBalance updateBalance)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (updateBalance == null)
            {
                tracingSpan.LogError($"The argument {nameof(updateBalance)} cannot be null", TracingStatus.InvalidArgument);
                throw new ArgumentNullException(nameof(updateBalance));
            }

            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, updateBalance.SavingsTargetId);
            if (updateBalance.Balance < 0)
            {
                string msg = $"The balance cannot be below zero but is {updateBalance.Balance}";
                tracingSpan.LogError(msg, TracingStatus.InvalidArgument);
                throw new ArgumentException(msg);
            }

            async Task<ServiceSavingsTarget> SetSavingsTargetBalance()
            {
                RepoSavingsTarget savingsTarget = await SavingsTargetRepository.SetBalance(
                    new RepoUpdateBalance
                    {
                        SavingsTargetId = updateBalance.SavingsTargetId,
                        Balance = updateBalance.Balance
                    });

                return savingsTarget.ToServiceModel();
            }

            return SetSavingsTargetBalance();
        }
    }
}