﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Models;
using FreeAssets = FinanceTracker.Apis.SavingsTarget.Repositories.Models.FreeAssets;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Services
{
    /// <summary>
    /// The abstract class of the savings target functionality
    /// </summary>
    /// <typeparam name="TWriteModel">The write model is different between the api and the projection.
    /// This can be configured here</typeparam>
    public abstract class BaseSavingsTargetService<TWriteModel> : ISavingsTargetService<TWriteModel>
    {
        /// <summary>
        /// Traces a request through the application
        /// </summary>
        protected ITracer Tracer;
        /// <summary>
        /// The persistence layer for the savings target
        /// </summary>
        protected ISavingsTargetRepository SavingsTargetRepository;
        /// <summary>
        /// The persistence layer for the free assets
        /// </summary>
        protected readonly IFreeAssetsRepository FreeAssetsRepository;

        /// <summary>
        /// Instantiates a new instance of <see cref="BaseSavingsTargetService{TWriteModel}"/>
        /// </summary>
        /// <param name="tracer">Traces a request through the application</param>
        /// <param name="savingsTargetRepository">The persitence layer for the savings targets</param>
        /// <param name="freeAssetsRepository">The persistence layer for the free assets of an account</param>
        protected BaseSavingsTargetService(ITracer tracer,
            ISavingsTargetRepository savingsTargetRepository,
            IFreeAssetsRepository freeAssetsRepository)
        {
            Tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
            SavingsTargetRepository = savingsTargetRepository ?? throw new ArgumentNullException(nameof(savingsTargetRepository));
            FreeAssetsRepository = freeAssetsRepository ?? throw new ArgumentNullException(nameof(freeAssetsRepository));
        }

        /// <summary>
        /// Creates or updates a <see cref="SavingsTarget"/> wether it exists or nor
        /// </summary>
        /// <param name="savingsTarget">The data to be created or updated</param>
        /// <returns>The created or updated entity</returns>
        public abstract Task<ServiceModel> CreateOrUpdate(TWriteModel savingsTarget);

        /// <summary>
        /// Tries to delete a savings target from the persistence layer
        /// </summary>
        /// <param name="id">The id of the savings target to be deleted</param>
        /// <returns></returns>
        public abstract Task Delete(long id);

        /// <summary>
        /// Sets a new value to the balance of a savings target
        /// </summary>
        /// <param name="updateBalance">The information for setting the new value</param>
        /// <returns>The updated savings target</returns>
        public abstract Task<ServiceModel> SetBalance(SetBalance updateBalance);

        /// <summary>
        /// Returns an account with the associated savings target
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>Null, if there is no such account</returns>
        public Task<Account> GetByAccountId(long accountId)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (accountId == 0)
            {
                string msg = $"The argument {nameof(accountId)} cannot be zero";
                tracingSpan.LogError(msg, TracingStatus.InvalidArgument);
                throw new ArgumentException(msg);
            }
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);

            async Task<Account> GetAccountById()
            {
                Task<FreeAssets> freeAssetsTask = FreeAssetsRepository.GetByAccountId(accountId);
                Task<IEnumerable<RepoModel>> savingsTargetsTask = SavingsTargetRepository.GetByAccountId(accountId);

                FreeAssets freeAssets = await freeAssetsTask;
                Account account = null;
                if (freeAssets != null)
                {
                    IEnumerable<RepoModel> savingsTargets = await savingsTargetsTask;
                    IEnumerable<AccountSavingsTarget> accountSavingsTargets =
                        savingsTargets?.Select(s => new AccountSavingsTarget(s));
                    account = new Account
                    {
                        Id = freeAssets.AccountId,
                        FreeAssets = freeAssets.Amount,
                        SavingsTargets = accountSavingsTargets ?? Enumerable.Empty<AccountSavingsTarget>()
                    };
                }

                return account;
            }

            return GetAccountById();
        }

        /// <summary>
        /// Reads a savings target from the persistence layer and returns it
        /// </summary>
        /// <param name="id">The id of the requested savings target</param>
        /// <returns>Null, if the persistence layer returns null</returns>
        public Task<ServiceModel> GetById(long id)
        {
            if (id == 0)
            {
                throw new ArgumentException($"The parameter {nameof(id)} cannot be 0");
            }

            async Task<ServiceModel> GetData(long l)
            {
                RepoModel repoData = await SavingsTargetRepository.GetById(l);

                return repoData?.ToServiceModel();
            }

            return GetData(id);
        }
    }
}