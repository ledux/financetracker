﻿using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Services
{
    /// <summary>
    /// Provides functionaltity for dealing with the <see cref="ServiceModel"/>
    /// </summary>
    /// <typeparam name="TWriteModel">The write model is different between the api and the projection.
    /// This can be configured here</typeparam>
    public interface ISavingsTargetService<in TWriteModel>
    {
        /// <summary>
        /// Creates or updates a <see cref="ServiceModel"/> wether it exists or nor
        /// </summary>
        /// <param name="savingsTarget">The data to be created or updated</param>
        /// <returns>The created or updated entity</returns>
        Task<ServiceModel> CreateOrUpdate(TWriteModel savingsTarget);

        /// <summary>
        /// Reads a savings target from the persistence layer and returns it
        /// </summary>
        /// <param name="id">The id of the requested savings target</param>
        /// <returns>Null, if the persistence layer returns null</returns>
        public Task<ServiceModel> GetById(long id);

        /// <summary>
        /// Tries to delete a savings target from the persistence layer
        /// </summary>
        /// <param name="id">The id of the savings target to be deleted</param>
        /// <returns></returns>
        public Task Delete(long id);

        /// <summary>
        /// Returns an account with the associated savings target
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>Null, if there is no such account</returns>
        public Task<Account> GetByAccountId(long accountId);

        /// <summary>
        /// Sets a new value to the balance of a savings target
        /// </summary>
        /// <param name="updateBalance">The information for setting the new value</param>
        /// <returns>The updated savings target</returns>
        Task<ServiceModel> SetBalance(SetBalance updateBalance);
    }
}