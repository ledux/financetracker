﻿using System;

namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Represents a savings target in the services
    /// </summary>
    public class SavingsTarget
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public SavingsTarget() { }

        /// <summary>
        /// Copies the data from <paramref name="savingsTargetWrite"/> to an new instance of <see cref="SavingsTarget"/>
        /// </summary>
        /// <param name="savingsTargetWrite">The data from the write model</param>
        public SavingsTarget(SavingsTargetWrite savingsTargetWrite)
        {
            if (savingsTargetWrite == null)
            {
                throw new ArgumentNullException(nameof(savingsTargetWrite));
            }

            Id = savingsTargetWrite.Id;
            AccountId = savingsTargetWrite.AccountId;
            Name = savingsTargetWrite.Name;
            Description = savingsTargetWrite.Description;
            StartAmount = savingsTargetWrite.StartAmount;
            StartDate = savingsTargetWrite.StartDate;
            TargetDate = savingsTargetWrite.TargetDate;
            TargetAmount = savingsTargetWrite.TargetAmount;
        }

        /// <summary>
        /// The identification of the target
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The current balance of the target
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// On which account the money lies for this target
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// Upon creation, what is the inital amount of the balance. This value cannot be changed anymore
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When has the user started saving for this goal. This value cannot be changed anymore
        /// </summary>
        public DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
