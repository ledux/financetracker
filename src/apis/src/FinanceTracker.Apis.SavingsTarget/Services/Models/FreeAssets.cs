﻿namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Represents the free assets of an account in the service layer
    /// Free assets is the amount of money, which is not assigned to a savings target
    /// Free assets are tracked by account.
    /// </summary>
    public class FreeAssets
    {
        /// <summary>
        /// The identification of the account
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// The amount of money, which is not assigned to a savings target
        /// </summary>
        public decimal Amount { get; set; }
    }
}
