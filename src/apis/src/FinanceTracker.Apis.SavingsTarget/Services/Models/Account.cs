﻿using System.Collections.Generic;
using System.Linq;

namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Represents an account in the savings target application
    /// It includes the savings target of this account
    /// </summary>
    public class Account
    {
        /// <summary>
        /// The identification of the account
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The amount of money on this account which is not associated with a savings target
        /// It can be negative, but should be balanced out from one of the savings target
        /// </summary>
        public decimal FreeAssets { get; set; }
        /// <summary>
        /// The savings target of this account
        /// </summary>
        public IEnumerable<AccountSavingsTarget> SavingsTargets { get; set; } = Enumerable.Empty<AccountSavingsTarget>();
    }
}
