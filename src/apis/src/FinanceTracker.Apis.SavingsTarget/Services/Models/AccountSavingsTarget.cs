﻿using System;
using RepoModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Represents a savings target on the account.
    /// It is solely missing the account id, since this value sits on the <see cref="Account"/> itself
    /// </summary>
    public class AccountSavingsTarget
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public AccountSavingsTarget() { }

        /// <summary>
        /// Copies over all data from the repo model
        /// </summary>
        /// <param name="savingsTarget">The data from the repo</param>
        /// <exception cref="ArgumentNullException">If the repomodel <paramref name="savingsTarget"/> is null</exception>
        public AccountSavingsTarget(RepoModel savingsTarget)
        {
            if (savingsTarget == null)
            {
                throw new ArgumentNullException(nameof(savingsTarget));
            }

            Id = savingsTarget.Id;
            Name = savingsTarget.Name;
            Description = savingsTarget.Description;
            Balance = savingsTarget.Balance;
            StartAmount = savingsTarget.StartAmount;
            TargetAmount = savingsTarget.TargetAmount;
            StartDate = savingsTarget.StartDate;
            TargetDate = savingsTarget.TargetDate;
        }

        /// <summary>
        /// The identification of the target
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The current balance of the target
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// Upon creation, what is the inital amount of the balance. This value cannot be changed anymore
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When has the user started saving for this goal. This value cannot be changed anymore
        /// </summary>
        public DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
