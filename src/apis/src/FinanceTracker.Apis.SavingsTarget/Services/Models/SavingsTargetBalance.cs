﻿namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Represents the balance of an savings target
    /// </summary>
    public class SavingsTargetBalance
    {
        /// <summary>
        /// The id of the savings target to which the balance belongs
        /// </summary>
        public long SavingsTargetId { get; set; }
        /// <summary>
        /// The current balance of the savings target
        /// </summary>
        public decimal Balance { get; set; }
    }
}
