﻿using System;

namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Represents the data of a savings target, which can be modified directly by the user
    /// </summary>
    public class SavingsTargetWrite
    {
        /// <summary>
        /// The identification of the target.
        /// Cannot be modified, but is needed for identification when updating.
        /// Needs to be zero, if a new entry is created
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// On which account the money lies for this target
        /// Can only be set upon creation. Cannot be changed anymore
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// Upon creation, what is the inital amount of the balance. This value cannot be changed anymore
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When has the user started saving for this goal. This value cannot be changed anymore
        /// </summary>
        public DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
