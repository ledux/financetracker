﻿using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using DbModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Services.Models
{
    /// <summary>
    /// Converts service models to Db models an vice versa
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Converts a service model into a db model
        /// </summary>
        /// <param name="model">The data from the service model</param>
        /// <returns> The same data in a db model</returns>
        public static DbModel ToDbModel(this ServiceModel model)
        {
            return new DbModel
            {
                Id = model.Id,
                AccountId = model.AccountId,
                Name = model.Name,
                Description = model.Description,
                StartAmount = model.StartAmount,
                TargetAmount = model.TargetAmount,
                TargetDate = model.TargetDate,
                Balance = model.Balance,
                StartDate = model.StartDate,
            };
        }

        /// <summary>
        /// Converts a db model into a db model
        /// </summary>
        /// <param name="model">The data from the db</param>
        /// <returns>The same data in the service model</returns>
        public static ServiceModel ToServiceModel(this DbModel model)
        {
            return new ServiceModel
            {
                Id = model.Id,
                AccountId = model.AccountId,
                Name = model.Name,
                Description = model.Description,
                StartAmount = model.StartAmount,
                TargetAmount = model.TargetAmount,
                TargetDate = model.TargetDate,
                Balance = model.Balance,
                StartDate = model.StartDate,
            };
        }
    }
}
