﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Exceptions;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using SetBalance = FinanceTracker.Apis.SavingsTarget.Services.Models.SetBalance;

namespace FinanceTracker.Apis.SavingsTarget.Services
{
    /// <summary>
    /// The implementation of the savings target service for the api host
    /// </summary>
    public class ApiSavingsTargetService : BaseSavingsTargetService<SavingsTargetWrite>
    {
        private readonly IIdCreator _idCreator;
        private readonly ISavingsTargetProducer _savingsTargetProducer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ApiSavingsTargetService"/>
        /// </summary>
        /// <param name="idCreator">Creates unique and increasing ids</param>
        /// <param name="savingsTargetProducer">Sends events for savings targets to event sourcing</param>
        /// <param name="savingsTargetRepository">The persistence layer for the savings target</param>
        /// <param name="tracer">Traces a request through the application</param>
        /// <param name="freeAssetsRepository">The persistence layer for the free assets of an account</param>
        public ApiSavingsTargetService(IIdCreator idCreator,
            ISavingsTargetProducer savingsTargetProducer,
            ISavingsTargetRepository savingsTargetRepository,
            IFreeAssetsRepository freeAssetsRepository,
            ITracer tracer)
            : base(tracer, savingsTargetRepository, freeAssetsRepository)
        {
            _idCreator = idCreator ?? throw new ArgumentNullException(nameof(idCreator));
            _savingsTargetProducer = savingsTargetProducer ?? throw new ArgumentNullException(nameof(savingsTargetProducer));
        }

        /// <summary>
        /// Creates or updates a <see cref="SavingsTarget"/> wether it exists or nor
        /// </summary>
        /// <param name="savingsTarget">The data to be created or updated</param>
        /// <returns>The created or updated entity</returns>
        public override Task<ServiceSavingsTarget> CreateOrUpdate(SavingsTargetWrite savingsTarget)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (savingsTarget == null)
            {
                tracingSpan.LogError($"The argument {nameof(savingsTarget)} is null", TracingStatus.InvalidArgument);
                throw new ArgumentNullException(nameof(savingsTarget));
            }

            async Task<ServiceSavingsTarget> ProduceSavingsTargetUpdated(ITracingSpan span)
            {
                RepoSavingsTarget existingTarget = await SavingsTargetRepository.GetById(savingsTarget.Id);
                long accountId = existingTarget?.AccountId ?? savingsTarget.AccountId;
                if (! await AccountExists(accountId))
                {
                    throw new NotFoundException();
                }

                ServiceSavingsTarget serviceModel = existingTarget == null
                    ? CreateSavingsTargetWithNewId(savingsTarget, span)
                    : UpdateSavingsTarget(savingsTarget, span, existingTarget);

                span.SetAttributes(new Dictionary<string, object>
                {
                    { TracingKeys.SavingsTargetId, savingsTarget.Id },
                    { TracingKeys.AccountId, savingsTarget.AccountId }
                });

                await _savingsTargetProducer.SavingsTargetUpdated(serviceModel);

                return serviceModel;
            }

            return ProduceSavingsTargetUpdated(tracingSpan);
        }

        /// <summary>
        /// Sends the request to delte a savings target to the producer
        /// </summary>
        /// <param name="id">The id of the savings target to be deleted</param>
        /// <returns></returns>
        public override Task Delete(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (id == 0)
            {
                tracingSpan.LogError("The parameter is zero", TracingStatus.InvalidArgument);
                throw new ArgumentException($"The parameter {nameof(id)} cannot be 0");
            }
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, id);

            return _savingsTargetProducer.SavingsTargetDeleted(id);
        }

        /// <summary>
        /// Sends the new balance to the event sourcing
        /// </summary>
        /// <param name="updateBalance">The information for setting the new value</param>
        /// <returns>The current savings target, before the update. Null, if it does not exist</returns>
        public override Task<ServiceSavingsTarget> SetBalance(SetBalance updateBalance)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (updateBalance == null)
            {
                tracingSpan.LogError($"The parameter {nameof(updateBalance)} cannot be null");
                throw new ArgumentNullException(nameof(updateBalance));
            }

            async Task<ServiceSavingsTarget> SetSavingsTargetBalance(ITracingSpan span)
            {
                RepoSavingsTarget savingsTarget = await SavingsTargetRepository.GetById(updateBalance.SavingsTargetId);
                ServiceSavingsTarget returnValue = null;
                if (savingsTarget == null)
                {
                    string msg = $"The savings target with id '{updateBalance.SavingsTargetId}' does not exist";
                    span.LogError(msg, TracingStatus.NotFound);
                }
                else
                {
                    span.SetAttribute(TracingKeys.SavingsTargetId, updateBalance.SavingsTargetId);
                    await _savingsTargetProducer.SavingsTargetBalanceSet(updateBalance);
                    returnValue = savingsTarget.ToServiceModel();
                }

                return returnValue;
            }

            return SetSavingsTargetBalance(tracingSpan);
        }

        private async Task<bool> AccountExists(long accountId)
        {
            return (await FreeAssetsRepository.GetByAccountId(accountId)) != null;
        }

        private static ServiceSavingsTarget UpdateSavingsTarget(SavingsTargetWrite savingsTarget, ITracingSpan span,
            RepoSavingsTarget existingTarget)
        {
            span.LogEvent($"Existing savings target with id '{savingsTarget.Id}' found");
            var serviceModel = new ServiceSavingsTarget(savingsTarget)
            {
                Balance = existingTarget.Balance,
                StartDate = existingTarget.StartDate,
                StartAmount = existingTarget.StartAmount,
                AccountId = existingTarget.AccountId
            };

            return serviceModel;
        }

        private ServiceSavingsTarget CreateSavingsTargetWithNewId(SavingsTargetWrite savingsTarget, ITracingSpan span)
        {
            savingsTarget.Id = _idCreator.CreateId();
            span.LogEvent($"New id '{savingsTarget.Id}' created");
            var serviceModel = new ServiceSavingsTarget(savingsTarget) { Balance = savingsTarget.StartAmount };

            return serviceModel;
        }
    }
}