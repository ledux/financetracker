﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using DbModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.FreeAssets;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.FreeAssets;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Services
{
    /// <summary>
    /// Projection service for dealing with the free assets of an account
    /// </summary>
    public class ProjectionFreeAssetsService : IFreeAssetsService
    {
        private readonly IFreeAssetsRepository _freeAssetsRepository;
        private readonly ISavingsTargetRepository _savingsTargetRepository;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionFreeAssetsService"/>
        /// </summary>
        /// <param name="freeAssetsRepository">The persistence layer for the free assets</param>
        /// <param name="savingsTargetRepository">The persistence layer for the savings target</param>
        /// <param name="tracer">Traces a request through the application</param>
        public ProjectionFreeAssetsService(IFreeAssetsRepository freeAssetsRepository, ISavingsTargetRepository savingsTargetRepository, ITracer tracer)
        {
            _freeAssetsRepository = freeAssetsRepository ?? throw new ArgumentNullException(nameof(freeAssetsRepository));
            _savingsTargetRepository = savingsTargetRepository ?? throw new ArgumentNullException(nameof(savingsTargetRepository));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Creates a new account to track the free assets
        /// </summary>
        /// <param name="freeAssets">The new account</param>
        /// <returns>The newly created entry</returns>
        public Task<ServiceModel> Create(ServiceModel freeAssets)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            if (freeAssets == null)
            {
                tracingSpan.LogError($"The parameter {nameof(freeAssets)} cannot be null", TracingStatus.InvalidArgument);
                throw new ArgumentNullException(nameof(freeAssets));
            }

            async Task<ServiceModel> CreateFreeAssets(ITracingSpan trace)
            {
                trace.SetAttribute(TracingKeys.AccountId, freeAssets.AccountId);

                DbModel byAccountId = await _freeAssetsRepository.GetByAccountId(freeAssets.AccountId);
                if (byAccountId == null)
                {
                    trace.LogEvent(new TracingEvent($"Creating a new free asset entry"));
                    var newFreeAssets = new DbModel { AccountId = freeAssets.AccountId, Amount = freeAssets.Amount };
                    await _freeAssetsRepository.CreateEntry(newFreeAssets);
                }

                return freeAssets;
            }

            return CreateFreeAssets(tracingSpan);
        }

        /// <summary>
        /// Updates the amount of the free assets of an account
        /// </summary>
        /// <param name="accountId">The id of the account to be changed</param>
        /// <param name="amount">The amount by which the free assets should be changed</param>
        /// <returns>The updated value</returns>
        public Task<ServiceModel> Update(long accountId, decimal amount)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);

            if (accountId == 0)
            {
                string msg = $"The argument {nameof(accountId)} cannot be zero";
                tracingSpan.LogError(msg, TracingStatus.InvalidArgument);
                throw new ArgumentException(msg);
            }

            async Task<ServiceModel> UpdateFreeAssets(ITracingSpan span)
            {
                DbModel updatedFreeAssets = await _freeAssetsRepository.UpdateFreeAssets(accountId, amount);

                if (updatedFreeAssets == null)
                {
                    string msg = $"The account with id '{accountId}' cannot be updated, because it does not exist";
                    span.LogError(msg, TracingStatus.NotFound);
                    return null;
                }

                return new ServiceModel { AccountId = updatedFreeAssets.AccountId, Amount = updatedFreeAssets.Amount };
            }

            return UpdateFreeAssets(tracingSpan);
        }

        /// <summary>
        /// Updates the free assets of an account when the balance of a savings target has changed
        /// </summary>
        /// <param name="balance">The Id and the balance of an savings target</param>
        /// <returns>The changed free assets</returns>
        public async Task<ServiceModel> Update(SavingsTargetBalance balance)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, balance.SavingsTargetId);

            RepoSavingsTarget savingsTarget = await _savingsTargetRepository.GetById(balance.SavingsTargetId);
            decimal difference = savingsTarget.Balance - balance.Balance;
            DbModel freeAssets;
            if (difference != 0)
            {
                tracingSpan.LogEvent($"Update the free assets by {difference}");
                freeAssets = await _freeAssetsRepository.UpdateFreeAssets(savingsTarget.AccountId, difference);
            }
            else
            {
                tracingSpan.LogEvent("No change in the balance of the savings target");
                freeAssets = await _freeAssetsRepository.GetByAccountId(savingsTarget.AccountId);
            }

            return new ServiceModel { AccountId = freeAssets.AccountId, Amount = freeAssets.Amount };
        }
    }
}