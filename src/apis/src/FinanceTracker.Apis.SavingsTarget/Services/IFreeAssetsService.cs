﻿using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Services.Models;

namespace FinanceTracker.Apis.SavingsTarget.Services
{
    /// <summary>
    /// Provides functionality to deal with the free assets in the savings target application
    /// </summary>
    public interface IFreeAssetsService
    {
        /// <summary>
        /// Creates a new account to track the free assets
        /// </summary>
        /// <param name="freeAssets">The new account</param>
        /// <returns>The newly created entry</returns>
        Task<FreeAssets> Create(FreeAssets freeAssets);

        /// <summary>
        /// Updates the amount of the free assets of an account
        /// </summary>
        /// <param name="accountId">The id of the account to be changed</param>
        /// <param name="amount">The amount by which the free assets should be changed</param>
        /// <returns>The updated value</returns>
        Task<FreeAssets> Update(long accountId, decimal amount);

        /// <summary>
        /// Updates the free assets of an account when the balance of a savings target has changed
        /// </summary>
        /// <param name="balance">The Id and the new balance of an savings target</param>
        /// <returns>The changed free assets</returns>
        Task<FreeAssets> Update(SavingsTargetBalance balance);
    }
}