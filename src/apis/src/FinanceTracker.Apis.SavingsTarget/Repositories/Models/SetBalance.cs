﻿namespace FinanceTracker.Apis.SavingsTarget.Repositories.Models
{
    /// <summary>
    /// Represents a model for setting a new value to the balance of a savings target
    /// This is the model for data accessing layer
    /// </summary>
    public class SetBalance
    {
        /// <summary>
        /// The id of the savings target which balance should be updated
        /// </summary>
        public long SavingsTargetId { get; set; }
        /// <summary>
        /// The new balance of the savings target
        /// </summary>
        public decimal Balance { get; set; }
    }
}
