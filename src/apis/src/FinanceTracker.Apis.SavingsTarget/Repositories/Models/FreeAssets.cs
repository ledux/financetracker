﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FinanceTracker.Apis.SavingsTarget.Repositories.Models
{
    /// <summary>
    /// Represents the free assets of an account in the persistence layer
    /// Free assets is the amount of money, which is not assigned to a savings target
    /// Free assets are tracked by account.
    /// </summary>
    public class FreeAssets
    {
        /// <summary>
        /// The identification of the account
        /// </summary>
        [BsonId]
        public long AccountId { get; set; }
        /// <summary>
        /// The amount of money, which is not assigned to a savings target
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Amount { get; set; }
    }
}
