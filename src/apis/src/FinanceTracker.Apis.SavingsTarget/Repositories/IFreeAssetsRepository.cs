﻿using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories.Models;

namespace FinanceTracker.Apis.SavingsTarget.Repositories
{
    /// <summary>
    /// Persistence layer for the free assets in the savings target application
    /// </summary>
    public interface IFreeAssetsRepository
    {
        /// <summary>
        /// Creates a new entry if there is no entry yet with the same AccountId
        /// </summary>
        /// <param name="freeAssets">The data to be created</param>
        /// <returns>The newly created entry</returns>
        Task<FreeAssets> CreateEntry(FreeAssets freeAssets);

        /// <summary>
        /// Gets the free assets of an account
        /// </summary>
        /// <param name="accountId">The id of the account for which the free assets are requested</param>
        /// <returns>Null, if there is no such account, othewise the free assets of the account</returns>
        Task<FreeAssets> GetByAccountId(long accountId);

        /// <summary>
        /// Changes the free assets of an account
        /// </summary>
        /// <param name="accountId">The id of the account of which the free assets are changed</param>
        /// <param name="amount">The amount by which the free assets are changed. Can be positive or a negative number</param>
        /// <returns>The entry with the updated value</returns>
        Task<FreeAssets> UpdateFreeAssets(long accountId, decimal amount);
    }
}
