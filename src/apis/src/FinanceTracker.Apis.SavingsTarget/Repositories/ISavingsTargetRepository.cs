﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories.Models;
using DbModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Repositories
{
    /// <summary>
    /// Abstraction of the persistence layer for <see cref="DbModel"/>
    /// </summary>
    public interface ISavingsTargetRepository
    {
        /// <summary>
        /// Creates or updates a single instance of a savings target
        /// </summary>
        /// <param name="savingsTarget">The data to be persited</param>
        /// <returns>The persisted data</returns>
        Task<DbModel> Upsert(DbModel savingsTarget);

        /// <summary>
        /// Reads a savings target by its id from the persistence
        /// </summary>
        /// <param name="id">The id of the wanted entry</param>
        /// <returns>Null, if there is no entry with the given Id.</returns>
        Task<DbModel> GetById(long id);

        /// <summary>
        /// Removes an entry from the persistence
        /// </summary>
        /// <param name="id">The Id of the savings target to be removed</param>
        /// <returns>The removed entry</returns>
        Task<DbModel> Delete(long id);

        /// <summary>
        /// Searches all savings targets associated with a certain account
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>At least an empty enumerable</returns>
        Task<IEnumerable<DbModel>> GetByAccountId(long accountId);

        /// <summary>
        /// Sets the balance of a savings target
        /// </summary>
        /// <param name="updateBalance">The information for the update</param>
        /// <returns>The updated savings target</returns>
        Task<DbModel> SetBalance(SetBalance updateBalance);
    }
}