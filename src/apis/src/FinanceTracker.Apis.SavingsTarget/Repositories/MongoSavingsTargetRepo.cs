﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.Abstractions;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Repositories
{
    /// <summary>
    /// Stores <see cref="RepoSavingsTarget"/> into a mongo database
    /// </summary>
    public class MongoSavingsTargetRepo : MongoDbRepository<RepoSavingsTarget>, ISavingsTargetRepository
    {
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="MongoSavingsTargetRepo"/>
        /// </summary>
        /// <param name="client">The client, which connects to the database</param>
        /// <param name="optionsAccessor">The configuration for connecting to the database</param>
        /// <param name="tracer">Traces a request through the application</param>
        public MongoSavingsTargetRepo(IMongoClient client,
            IOptions<MongoDbRepositorySettings> optionsAccessor,
            ITracer tracer)
            : base(client, optionsAccessor)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
            IndexKeysDefinition<RepoSavingsTarget> keysDefinition = Builders<RepoSavingsTarget>.IndexKeys.Descending(s => s.AccountId);
            var indexModel = new CreateIndexModel<RepoSavingsTarget>(keysDefinition);
            Collection.Indexes.CreateOne(indexModel);
        }

        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected override string RepoName => "SavingsTarget";

        /// <summary>
        /// Creates or updates a single instance of a savings target
        /// </summary>
        /// <param name="savingsTarget">The data to be persisted</param>
        /// <returns>The persisted data</returns>
        public Task<RepoSavingsTarget> Upsert(RepoSavingsTarget savingsTarget)
        {
            var options = new FindOneAndReplaceOptions<RepoSavingsTarget, RepoSavingsTarget>
            {
                ReturnDocument = ReturnDocument.After,
                IsUpsert = true,
                BypassDocumentValidation = true
            };
            FilterDefinition<RepoSavingsTarget> filter = Builders<RepoSavingsTarget>.Filter.Eq(target => target.Id, savingsTarget.Id);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.SavingsTargetId, savingsTarget.Id },
                { TracingKeys.AccountId, savingsTarget.AccountId }
            });

            return Collection.FindOneAndReplaceAsync(filter, savingsTarget, options);
        }

        /// <summary>
        /// Reads a savings target by its id from the database
        /// </summary>
        /// <param name="id">The id of the wanted entry</param>
        /// <returns>Null, if there is no entry with the given Id.</returns>
        public Task<RepoSavingsTarget> GetById(long id)
        {
            FilterDefinition<RepoSavingsTarget> filter = Builders<RepoSavingsTarget>.Filter.Eq(st => st.Id, id);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, id);
            return Collection.FindSync(filter).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Removes an entry from the persistence
        /// </summary>
        /// <param name="id">The Id of the savings target to be removed</param>
        /// <returns>The removed entry</returns>
        public Task<RepoSavingsTarget> Delete(long id)
        {
            FilterDefinition<RepoSavingsTarget> filter = Builders<RepoSavingsTarget>.Filter.Eq(e => e.Id, id);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, id);
            return Collection.FindOneAndDeleteAsync(filter);
        }

        /// <summary>
        /// Searches all savings targets associated with a certain account
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>At least an empty enumerable</returns>
        public async Task<IEnumerable<RepoSavingsTarget>> GetByAccountId(long accountId)
        {
            FilterDefinition<RepoSavingsTarget> filter = Builders<RepoSavingsTarget>.Filter.Eq(s => s.AccountId, accountId);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);
            return await Collection.Find(filter).ToListAsync();
        }

        /// <summary>
        /// Changes the balance of a savings target
        /// </summary>
        /// <param name="updateBalance">The information for the update</param>
        /// <returns>The new balance value</returns>
        public Task<RepoSavingsTarget> SetBalance(SetBalance updateBalance)
        {
            FilterDefinition<RepoSavingsTarget> filter = Builders<RepoSavingsTarget>.Filter.Eq(s => s.Id, updateBalance.SavingsTargetId);
            UpdateDefinition<RepoSavingsTarget> update = Builders<RepoSavingsTarget>.Update.Set(s => s.Balance, updateBalance.Balance);
            var options = new FindOneAndUpdateOptions<RepoSavingsTarget, RepoSavingsTarget>
            {
                ReturnDocument = ReturnDocument.After,
                IsUpsert = false,
                BypassDocumentValidation = true,
            };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.SavingsTargetId, updateBalance.SavingsTargetId);
            return Collection.FindOneAndUpdateAsync(filter, update, options);
        }
    }
}
