﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using FreeAssets = FinanceTracker.Apis.SavingsTarget.Repositories.Models.FreeAssets;

namespace FinanceTracker.Apis.SavingsTarget.Repositories
{
    /// <summary>
    /// Stores free assets of an account into a mongo database
    /// </summary>
    public class MongoFreeAssetsRepo : MongoDbRepository<FreeAssets>, IFreeAssetsRepository
    {
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="MongoFreeAssetsRepo"/>
        /// </summary>
        /// <param name="client">The client which connects to the database</param>
        /// <param name="optionsAccessor">The options to configure the correct collection</param>
        /// <param name="tracer">Traces a request through the application</param>
        public MongoFreeAssetsRepo(IMongoClient client,
            IOptions<MongoDbRepositorySettings> optionsAccessor,
            ITracer tracer) 
            : base(client, optionsAccessor)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected override string RepoName => "FreeAssets";

        /// <summary>
        /// Creates a new entry if there is no entry yet with the same AccountId
        /// </summary>
        /// <param name="freeAssets">The data to be created</param>
        /// <returns>The newly created entry</returns>
        public async Task<FreeAssets> CreateEntry(FreeAssets freeAssets)
        {
            var options = new InsertOneOptions { BypassDocumentValidation = true };
            using (ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName()))
            {
                tracingSpan.SetAttribute(TracingKeys.AccountId, freeAssets.AccountId);
                try
                {
                    await Collection.InsertOneAsync(freeAssets, options);
                }
                catch (Exception e)
                {
                    tracingSpan.LogError(e, TracingStatus.AlreadyExists);
                }
            }

            return freeAssets;
        }

        /// <summary>
        /// Gets the free assets of an account
        /// </summary>
        /// <param name="accountId">The id of the account for which the free assets are requested</param>
        /// <returns>Null, if there is no such account, othewise the free assets of the account</returns>
        public Task<FreeAssets> GetByAccountId(long accountId)
        {
            FilterDefinition<FreeAssets> filter = Builders<FreeAssets>.Filter.Eq(f => f.AccountId, accountId);
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);
            return Collection.Find(filter).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Changes the free assets of an account
        /// </summary>
        /// <param name="accountId">The id of the account of which the free assets are changed</param>
        /// <param name="amount">The amount by which the free assets are changed. Can be positive or a negative number</param>
        /// <returns>The entry with the updated value</returns>
        public Task<FreeAssets> UpdateFreeAssets(long accountId, decimal amount)
        {
            FilterDefinition<FreeAssets> filter = Builders<FreeAssets>.Filter.Eq(f => f.AccountId, accountId);
            UpdateDefinition<FreeAssets> updateDefinition = Builders<FreeAssets>.Update.Inc(f => f.Amount, amount);
            var options = new FindOneAndUpdateOptions<FreeAssets, FreeAssets>
            {
                ReturnDocument = ReturnDocument.After,
                IsUpsert = false,
                BypassDocumentValidation = true,
            };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);
            return Collection.FindOneAndUpdateAsync(filter, updateDefinition, options);
        }
    }
}
