﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Controllers.Models;
using FinanceTracker.Apis.SavingsTarget.Exceptions;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using Microsoft.AspNetCore.Mvc;
using Account = FinanceTracker.Apis.SavingsTarget.Services.Models.Account;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using WebSetBalance = FinanceTracker.Apis.SavingsTarget.Controllers.Models.SetBalance;
using ServiceSetBalance = FinanceTracker.Apis.SavingsTarget.Services.Models.SetBalance;

namespace FinanceTracker.Apis.SavingsTarget.Controllers
{
    /// <summary>
    /// Web API for the savings target
    /// </summary>
    [Route("v1/[controller]")]
    [ApiController]
    public class SavingsTargetController : ControllerBase
    {
        private readonly ISavingsTargetService<SavingsTargetWrite> _savingsTargetService;

        /// <summary>
        /// Intstantiates a new instance of <see cref="SavingsTargetController"/>
        /// </summary>
        /// <param name="savingsTargetService">The business functionality of the savings target</param>
        public SavingsTargetController(ISavingsTargetService<SavingsTargetWrite> savingsTargetService)
        {
            _savingsTargetService = savingsTargetService ?? throw new ArgumentNullException(nameof(savingsTargetService));
        }

        /// <summary>
        /// Receives data for a savings target which should be created
        /// </summary>
        /// <param name="savingsTarget">The data, which should be created</param>
        /// <returns>202, with the location, where the data is created</returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PostModel savingsTarget)
        {
            SavingsTargetWrite serviceModel = savingsTarget.ToWriteServiceModel();
            ServiceSavingsTarget created;
            try
            {
                created = await _savingsTargetService.CreateOrUpdate(serviceModel);
            }
            catch (NotFoundException)
            {
                return BadRequest($"The account with id {savingsTarget.AccountId} is not found");
            }

            return AcceptedAtAction(nameof(GetById), new { id = created.Id }, created.ToWebModel());
        }

        /// <summary>
        /// Returns a savings target by its id
        /// </summary>
        /// <param name="id">The id of the requested savings target</param>
        /// <returns>404, when it does not exist, Bad request, when id is zero</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(long id)
        {
            if (id == 0)
            {
                return BadRequest($"The parameter {nameof(id)} cannot be zero");
            }

            ServiceSavingsTarget savingsTarget = await _savingsTargetService.GetById(id);
            if (savingsTarget == null)
            {
                return NotFound();
            }

            return Ok(savingsTarget.ToWebModel());
        }

        /// <summary>
        /// Receives data for updating a savings target
        /// </summary>
        /// <param name="savingsTargetId">The id of the savings target to be updated</param>
        /// <param name="savingsTarget">The data with which the savings target will be updated </param>
        /// <returns></returns>
        [HttpPut("{savingsTargetId}")]
        public async Task<IActionResult> Put(long savingsTargetId, [FromBody] PutModel savingsTarget)
        {
            if (savingsTargetId == 0)
            {
                return BadRequest($"The parameter {nameof(savingsTargetId)} cannot be null");
            }

            if (savingsTarget == null)
            {
                return BadRequest("The body cannot be null");
            }

            SavingsTargetWrite writeModel = savingsTarget.ToWriteModel(savingsTargetId);
            ServiceSavingsTarget updatedModel = await _savingsTargetService.CreateOrUpdate(writeModel);
            if (updatedModel == null)
            {
                return NotFound($"The model with id '{savingsTargetId}' does not exist");
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a savings target
        /// </summary>
        /// <param name="savingsTargetId">The id of the savings target to be deleted</param>
        /// <returns>BadRequest if the id is zero. NoContent otherwise</returns>
        [HttpDelete("{savingsTargetId}")]
        public async Task<IActionResult> Delete(long savingsTargetId)
        {
            if (savingsTargetId == 0)
            {
                return BadRequest($"The {nameof(savingsTargetId)} cannot be zero");
            }

            await _savingsTargetService.Delete(savingsTargetId);

            return NoContent();
        }

        /// <summary>
        /// Returns all savings target of an account
        /// </summary>
        /// <param name="accountId">The id of the account</param>
        /// <returns>Bad request if accountID is zero, NotFound if there is no account with the id</returns>
        [HttpGet("account/{accountId}")]
        public async Task<IActionResult> GetAccount(long accountId)
        {
            if (accountId == 0)
            {
                return BadRequest($"The parameter {nameof(accountId)} cannot be zero");
            }

            Account serviceAccount = await _savingsTargetService.GetByAccountId(accountId);
            if (serviceAccount == null)
            {
                return NotFound();
            }

            return Ok(serviceAccount.ToWebModel());
        }

        /// <summary>
        /// Sets the new values of the accounting of a savings target
        /// </summary>
        /// <param name="savingsTargetId">The id of the savings target</param>
        /// <param name="setBalance">The values to set the accounting</param>
        /// <returns>BadRequest if the <paramref name="savingsTargetId"/> is zero or less
        /// NotFound if no savings target with id of <paramref name="savingsTargetId"/> does exist
        /// NoContent otherwise</returns>
        [HttpPut("{savingsTargetId}/accounting")]
        public async Task<IActionResult> SetBalance(long savingsTargetId, WebSetBalance setBalance)
        {
            if (savingsTargetId < 1)
            {
                return BadRequest($"The parameter {nameof(savingsTargetId)} cannot be 0 or less");
            }

            var updateBalance = new ServiceSetBalance { SavingsTargetId = savingsTargetId, Balance = setBalance.Balance };
            ServiceSavingsTarget savingsTarget = await _savingsTargetService.SetBalance(updateBalance);
            if (savingsTarget == null)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
