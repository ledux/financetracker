﻿using System;

namespace FinanceTracker.Apis.SavingsTarget.Controllers.Models
{
    /// <summary>
    /// Represents the data, which can be sent by the client to the api in order to create a savings target
    /// </summary>
    public class PostModel
    {
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// On which account the money lies for this target
        /// Can only be set upon creation. Cannot be changed anymore
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// Upon creation, what is the inital amount of the balance. This value cannot be changed anymore
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When has the user started saving for this goal. This value cannot be changed anymore
        /// </summary>
        public DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
