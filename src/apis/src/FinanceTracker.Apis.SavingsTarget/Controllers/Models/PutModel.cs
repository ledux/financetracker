﻿using System;

namespace FinanceTracker.Apis.SavingsTarget.Controllers.Models
{
    /// <summary>
    /// Represents the data accepted by the put API.
    /// Only these values are editable for the user
    /// </summary>
    public class PutModel
    {
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
