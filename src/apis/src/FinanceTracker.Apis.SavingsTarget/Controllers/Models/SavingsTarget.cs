﻿using System;

namespace FinanceTracker.Apis.SavingsTarget.Controllers.Models
{
    /// <summary>
    /// Represents the savings target in the web layer
    /// It replaced all long values with strings (because of limitations in the javascript)
    /// </summary>
    public class SavingsTarget
    {
        /// <summary>
        /// The identification of the target
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The current balance of the target
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// On which account the money lies for this target
        /// </summary>
        public string AccountId { get; set; }
        /// <summary>
        /// Upon creation, what is the inital amount of the balance. This value cannot be changed anymore
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When has the user started saving for this goal. This value cannot be changed anymore
        /// </summary>
        public DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
