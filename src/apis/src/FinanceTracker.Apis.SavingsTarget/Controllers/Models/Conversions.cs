﻿using System.Linq;
using FinanceTracker.Apis.Common.DataModels;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using WebAccount = FinanceTracker.Apis.SavingsTarget.Controllers.Models.Account;
using ServiceAccount = FinanceTracker.Apis.SavingsTarget.Services.Models.Account;
using ServiceAccountSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.AccountSavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.Controllers.Models
{
    /// <summary>
    /// Converts controller models into service models and vice versa
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Converts a post model into a write model
        /// </summary>
        /// <param name="postModel">The data received by the controller</param>
        /// <returns>The data used by the service</returns>
        public static SavingsTargetWrite ToWriteServiceModel(this PostModel postModel)
        {
            var writeModel = new SavingsTargetWrite
            {
                Description = postModel.Description,
                Name = postModel.Name,
                StartAmount = postModel.StartAmount,
                StartDate = postModel.StartDate,
                TargetAmount = postModel.TargetAmount,
                TargetDate = postModel.TargetDate
            };

            return writeModel.AssignId(postModel.AccountId, (model, accountId) => model.AccountId = accountId);
        }

        /// <summary>
        /// Converts a put model into a write model for the service layer
        /// The missing values will be initialized with the default value
        /// </summary>
        /// <param name="putModel">The data received by the controller</param>
        /// <param name="id">The id from the url </param>
        /// <returns>The same data used by the service</returns>
        public static SavingsTargetWrite ToWriteModel(this PutModel putModel, long id)
        {
            return new SavingsTargetWrite
            {
                Id = id,
                TargetAmount = putModel.TargetAmount,
                Description = putModel.Description,
                Name = putModel.Name,
                TargetDate = putModel.TargetDate
            };
        }

        /// <summary>
        /// Converts a service model into a web model
        /// </summary>
        /// <param name="model">The data from the service</param>
        /// <returns>The data used in the controller</returns>
        public static SavingsTarget ToWebModel(this ServiceModel model)
        {
            return new SavingsTarget
            {
                Id = model.Id.ToString(),
                AccountId = model.AccountId.ToString(),
                Name = model.Name,
                Description = model.Description,
                StartAmount = model.StartAmount,
                TargetAmount = model.TargetAmount,
                TargetDate = model.TargetDate,
                Balance = model.Balance,
                StartDate = model.StartDate
            };
        }

        /// <summary>
        /// Converts an account from the service to an web model
        /// </summary>
        /// <param name="model">The data from the service</param>
        /// <returns>The same data in class used by the web layer</returns>
        public static WebAccount ToWebModel(this ServiceAccount model)
        {
            return new Account
            {
                FreeAssets = model.FreeAssets,
                SavingsTargets = model.SavingsTargets.Select(s => s.ToAccountSavingsTarget()),
                Id = model.Id.ToString()
            };
        }

        /// <summary>
        /// Converts a service model to a savings target, which is on the account
        /// </summary>
        /// <param name="model">The data from the service layer</param>
        /// <returns>The same data as a account service model</returns>
        public static AccountSavingsTarget ToAccountSavingsTarget(this ServiceAccountSavingsTarget model)
        {
            return new AccountSavingsTarget
            {
                Id = model.Id.ToString(),
                Name = model.Name,
                Description = model.Description,
                StartAmount = model.StartAmount,
                TargetAmount = model.TargetAmount,
                TargetDate = model.TargetDate,
                Balance = model.Balance,
                StartDate = model.StartDate
            };
        }
    }
}
