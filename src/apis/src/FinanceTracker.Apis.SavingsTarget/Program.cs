using System;
using System.Threading;
using System.Threading.Tasks;
using FinanceTracker.Apis.Common.Extensions;
using FinanceTracker.Apis.SavingsTarget.Hosting;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.SavingsTarget
{
    /// <summary>
    /// Starting point of the SavingsTarget application
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main method of the application
        /// </summary>
        /// <param name="args">This application does not accept any arguments</param>
        /// <returns></returns>
        public static async Task Main(string[] args)
        {
            IWebHost apiHost = ApiHostBuilder.Build();
            IHost projectionHost = ProjectionHostBuilder.Build();

            await new Func<CancellationToken, Task>[]
            {
                token => apiHost.RunAsync(token),
                token => projectionHost.RunAsync(token)
            }.Run();
        }
    }
}
