﻿using FinanceTracker.Apis.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.OpenTracing.Extensions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.Category.Hosting
{
    /// <summary>
    /// Builds the host which acts as the API and listens to http requests
    /// </summary>
    public static class ApiHostBuilder
    {
        /// <summary>
        /// Configures the settings, adds the dependencies and builds the host
        /// </summary>
        /// <returns>The fully configured host, ready to run</returns>
        public static IWebHost Build()
        {
            return WebHost.CreateDefaultBuilder()
                .ConfigureApiSettings()
                .Configure(ConfigureApp)
                .ConfigureServices(AddServices)
                .ConfigureLogging(SetupExtensions.ConfigureLogging)
                .Build();

        }

        /// <summary>
        /// Adds the necessary services to the service collection
        /// </summary>
        /// <param name="context">The context, in which the web host is built in</param>
        /// <param name="collection">The service collection, where the services should be added to</param>
        public static void AddServices(WebHostBuilderContext context, IServiceCollection collection)
        {
            MongoDbRepositorySettings settings = collection.ConfigureMongo(context.Configuration);

            collection
                .AddControllersAndCors()
                .AddMongo(settings)
                .AddTracing(context.Configuration)
                .AddIdCreator()
                .AddCategoryServices()
                .ConfigureKafkaEventClient(context.Configuration);
        }

        private static void ConfigureApp(WebHostBuilderContext builderContext, IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseCors("frontend-policy")
                .UseRouting()
                .UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

    }
}
