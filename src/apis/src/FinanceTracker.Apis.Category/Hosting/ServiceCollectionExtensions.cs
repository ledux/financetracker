﻿using FinanceTracker.Apis.Category.Producers;
using FinanceTracker.Apis.Category.Services;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.Category.Hosting
{
    /// <summary>
    /// Registers the necessary services to the service collection
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// All necessary services for the category api will be added to the service collection
        /// </summary>
        /// <param name="collection">The collection to which the services will be added</param>
        /// <returns>The same service collection</returns>
        public static IServiceCollection AddCategoryServices(this IServiceCollection collection)
        {
            collection
                .AddScoped<ICategoryService, ApiCategoryService>()
                .AddScoped<ICategoryProducer, CategoryProducer>();

            return collection;
        }
    }
}
