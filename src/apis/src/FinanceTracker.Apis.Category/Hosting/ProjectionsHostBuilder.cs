﻿using System.EventSourcing.Hosting;
using FinanceTracker.Apis.Category.Projections;
using FinanceTracker.Apis.Category.Repositories;
using FinanceTracker.Apis.Category.Services;
using FinanceTracker.Common.KafkaHostBuilder;
using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.Category.Hosting
{
    /// <summary>
    /// Creates a host for running the projections
    /// </summary>
    public static class ProjectionsHostBuilder
    {
        /// <summary>
        /// Builds and configures the host
        /// </summary>
        /// <returns>A fully configured host, ready to be run</returns>
        public static IHost Build()
        {
            return KafkaHostBuilder
                .BuildKafkaHost()
                .UseAsConsumer(
                    builder => builder.AddProjection<AccountingCategoryUpdatedProjection>(),
                    builder => builder)
                .ConfigureServices(
                    (context, collection) =>
                    {
                        MongoDbRepositorySettings settings =
                            collection.ConfigureMongo(context.Configuration);

                        collection
                            .AddMongo(settings)
                            .AddScoped<ICategoryService, ProjectionCategoryService>()
                            .AddSingleton<ICategoryRepository, MongoCategoryRepo>();

                    })
                .Build();
        }
    }
}
