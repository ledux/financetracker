﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Repositories;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using ServiceModel = FinanceTracker.Apis.Category.Services.Models.AccountingCategory;
using DbModel = FinanceTracker.Apis.Category.Repositories.Models.AccountingCategory;

namespace FinanceTracker.Apis.Category.Services
{
    /// <summary>
    /// The category service for the projection host
    /// </summary>
    public class ProjectionCategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionCategoryService"/>
        /// </summary>
        /// <param name="categoryRepository">The repository for the accounting categories</param>
        /// <param name="tracer">Traces a request through the appliacation</param>
        public ProjectionCategoryService(ICategoryRepository categoryRepository, ITracer tracer)
        {
            _categoryRepository = categoryRepository ?? throw new ArgumentNullException(nameof(categoryRepository));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Creates or updtates an existing catgory, depending if it exists already or not
        /// </summary>
        /// <param name="category">The data to be updated or created</param>
        /// <returns>The created or updated entity</returns>
        public async Task<ServiceModel> CreateOrUpdate(ServiceModel category)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.CategoryId, category.Id);

            DbModel dbModel = category.ToDbModel();
            DbModel updatedModel = await _categoryRepository.Upsert(dbModel);

            return updatedModel.ToServiceModel();
        }
    }
}
