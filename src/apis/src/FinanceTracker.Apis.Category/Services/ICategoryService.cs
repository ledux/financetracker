﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Services.Models;

namespace FinanceTracker.Apis.Category.Services
{
    /// <summary>
    /// Service for dealing with accounting categories
    /// </summary>
    public interface ICategoryService
    {
        /// <summary>
        /// Creates or updates a category
        /// </summary>
        /// <param name="category">The data, which should be created or updated</param>
        /// <returns>The created or updated data</returns>
        Task<AccountingCategory> CreateOrUpdate(AccountingCategory category);
    }
}