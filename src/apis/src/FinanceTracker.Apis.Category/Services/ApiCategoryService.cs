﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Producers;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;

namespace FinanceTracker.Apis.Category.Services
{
    /// <summary>
    /// Implementation for the api 
    /// </summary>
    public class ApiCategoryService : ICategoryService
    {
        private readonly IIdCreator _idCreator;
        private readonly ICategoryProducer _producer;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ApiCategoryService"/>
        /// </summary>
        /// <param name="idCreator">Creates unique ids</param>
        /// <param name="producer">Notifies about changes in the categories</param>
        /// <param name="tracer">Traces a request through the application</param>
        public ApiCategoryService(IIdCreator idCreator, ICategoryProducer producer, ITracer tracer)
        {
            _idCreator = idCreator ?? throw new ArgumentNullException(nameof(idCreator));
            _producer = producer ?? throw new ArgumentNullException(nameof(producer));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Notifies the event sourcing about a new or changed <see cref="AccountingCategory"/>
        /// </summary>
        /// <param name="category">The data, which should be created or updated</param>
        /// <returns>The created or updated data</returns>
        public async Task<AccountingCategory> CreateOrUpdate(AccountingCategory category)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            if (category == null)
            {
                tracingSpan.LogError("Category is null", TracingStatus.InvalidArgument);
                return null;
            }
            tracingSpan.SetAttribute(TracingKeys.CategoryId, category.Id);

            category.Id = _idCreator.CreateId();
            await _producer.CategoryUpdated(category);

            return category;
        }
    }
}
