﻿using ServiceModel = FinanceTracker.Apis.Category.Services.Models.AccountingCategory;
using DbModel = FinanceTracker.Apis.Category.Repositories.Models.AccountingCategory;

namespace FinanceTracker.Apis.Category.Services.Models
{
    /// <summary>
    /// Converts service models to db models and vice versa
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Converts a service model to a db model
        /// </summary>
        /// <param name="serviceModel">The data of the service model</param>
        /// <returns>A db model with the same data</returns>
        public static DbModel ToDbModel(this ServiceModel serviceModel)
        {
            return new DbModel
            {
                Name = serviceModel.Name,
                Id = serviceModel.Id,
                ParentId = serviceModel.Parent?.Id ?? 0
            };
        }

        /// <summary>
        /// Converts a db model to a service model. It does not instantiate the parent, even if the parent id is not 0
        /// </summary>
        /// <param name="dbModel">The data of the database</param>
        /// <returns>A service model with the same data</returns>
        public static ServiceModel ToServiceModel(this DbModel dbModel)
        {
            return new ServiceModel
            {
                Id = dbModel.Id,
                Name = dbModel.Name
            };
        }
    }
}
