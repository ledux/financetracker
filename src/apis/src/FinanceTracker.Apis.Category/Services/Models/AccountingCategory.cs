﻿namespace FinanceTracker.Apis.Category.Services.Models
{
    /// <summary>
    /// Represents a category of an expense or an income
    /// </summary>
    public class AccountingCategory
    {
        /// <summary>
        /// The identification of the category
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the category
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The parent, if any, of the category
        /// </summary>
        public AccountingCategory Parent { get; set; }
        /// <summary>
        /// How many ancestors the category has
        /// </summary>
        public byte Level { get; set; }
        /// <summary>
        /// Determines, if this category can have children. Not yet implemented
        /// </summary>
        public bool CanHaveChildren { get; set; }
    }
}
