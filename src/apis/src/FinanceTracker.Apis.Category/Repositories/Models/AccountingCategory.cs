﻿namespace FinanceTracker.Apis.Category.Repositories.Models
{
    /// <summary>
    /// Represents an accounting category in the database
    /// </summary>
    public class AccountingCategory
    {
        /// <summary>
        ///  The identification
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the category
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The identification of the parent. 0 of it does not have an ancestor
        /// </summary>
        public long ParentId { get; set; }
    }
}
