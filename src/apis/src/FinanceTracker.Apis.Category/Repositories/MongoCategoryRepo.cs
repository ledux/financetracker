﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Repositories.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.Abstractions;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FinanceTracker.Apis.Category.Repositories
{
    /// <summary>
    /// Stores categories into a mongo database
    /// </summary>
    public class MongoCategoryRepo: MongoDbRepository<AccountingCategory>, ICategoryRepository 
    {
        private readonly ITracer _tracer;

        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected override string RepoName => "Category";

        /// <summary>
        /// Instantiates a new instance of the <see cref="MongoCategoryRepo"/>
        /// </summary>
        /// <param name="client">Connects to the database</param>
        /// <param name="optionsAccessor">Holds the configuration of the database</param>
        /// <param name="tracer">Traces a request through the application</param>
        public MongoCategoryRepo(IMongoClient client,
            IOptions<MongoDbRepositorySettings> optionsAccessor,
            ITracer tracer) : base(
            client,
            optionsAccessor)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Updates a category, or creates it, when it does not exist yet.
        /// </summary>
        /// <param name="category">The data which should be created or updated</param>
        /// <returns></returns>
        public Task<AccountingCategory> Upsert(AccountingCategory category)
        {
            var options = new FindOneAndReplaceOptions<AccountingCategory, AccountingCategory>
            {
                IsUpsert = true,
                ReturnDocument = ReturnDocument.After
            };
            FilterDefinition<AccountingCategory> filter = Builders<AccountingCategory>.Filter.Eq(e => e.Id, category.Id);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.CategoryId, category.Id);

            return Collection.FindOneAndReplaceAsync(filter, category, options);
        }
    }
}
