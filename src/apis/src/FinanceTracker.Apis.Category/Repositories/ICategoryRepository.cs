﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Repositories.Models;

namespace FinanceTracker.Apis.Category.Repositories
{
    /// <summary>
    /// Abstraction for the persistence layer for the categories
    /// </summary>
    public interface ICategoryRepository
    {
        /// <summary>
        /// Updates a category, or creates it, when it does not exist yet.
        /// </summary>
        /// <param name="category">The data which should be created or updated</param>
        /// <returns></returns>
        Task<AccountingCategory> Upsert(AccountingCategory category);
    }
}