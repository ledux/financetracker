﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Services;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Category.Projections
{
    /// <summary>
    /// Handles the category updated event for the category api
    /// </summary>
    public class AccountingCategoryUpdatedProjection : IProjection<AccountingCategoryUpdated>
    {
        private readonly ICategoryService _categoryService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instancen of the <see cref="AccountingCategoryUpdatedProjection"/>
        /// </summary>
        /// <param name="categoryService">Functionality for updating or adding a new category</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingCategoryUpdatedProjection(ICategoryService categoryService, ITracer tracer)
        {
            _categoryService = categoryService ?? throw new ArgumentNullException(nameof(categoryService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends the data to the service. It creates a parent, if there is a parentId not equals 0
        /// </summary>
        /// <param name="event">The data from the event sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountingCategoryUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.CategoryId, @event.Id);
            var updatedCategory = new AccountingCategory
            {
                Id = @event.Id,
                Name = @event.Name,
                Parent = @event.ParentId == 0 ? null : new AccountingCategory { Id = @event.ParentId, Name = @event.ParentName}
            };

            return _categoryService.CreateOrUpdate(updatedCategory);
        }
    }
}
