﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Services;
using FinanceTracker.Apis.Category.Services.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinanceTracker.Apis.Category.Controllers
{
    /// <summary>
    /// Web API for the accounting categories
    /// </summary>
    [Route("v1/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        /// <summary>
        /// Instantiates a new instance of the <see cref="CategoryController"/>
        /// </summary>
        /// <param name="categoryService">The service which deals with the accounting categories</param>
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService ?? throw new ArgumentNullException(nameof(categoryService));
        }

        /// <summary>
        /// Returns all available categories
        /// </summary>
        /// <returns>All available categories</returns>
        [HttpGet]
        public IActionResult Get()
        {
            return StatusCode(501);
        }

        /// <summary>
        /// Gets a category by its id
        /// </summary>
        /// <param name="id">The id of the requested category</param>
        /// <returns>The requested category or 404 if there is none</returns>
        [HttpGet("{id}")]
        public IActionResult GetById(long id)
        {
            return StatusCode(501);
        }

        /// <summary>
        /// Creates a new category
        /// </summary>
        /// <param name="category">The data of the category to be created</param>
        /// <returns>202, if successful</returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AccountingCategory category)
        {
            AccountingCategory createdCategory = await _categoryService.CreateOrUpdate(category);

            return CreatedAtAction("GetById", new { id = createdCategory.Id }, createdCategory);
        }

        /// <summary>
        /// Updates an existing category.
        /// </summary>
        /// <param name="id">The id of the category to be updated</param>
        /// <param name="category">The updated data</param>
        /// <returns>404, if no category with the id exists</returns>
        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] AccountingCategory category)
        {
            return StatusCode(501);
        }

        /// <summary>
        /// Removes the catgory from the collection
        /// </summary>
        /// <param name="id">The id of the entry to be removed</param>
        /// <returns>204</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            return StatusCode(501);
        }
    }
}
