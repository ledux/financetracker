﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Category.Producers
{
    /// <summary>
    /// Producer for accounting categories
    /// </summary>
    public class CategoryProducer : ICategoryProducer
    {
        private readonly IEventClient _eventClient;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of the <see cref="CategoryProducer"/>
        /// </summary>
        /// <param name="eventClient">The client which sents the events to the message queue</param>
        /// <param name="tracer">Traces a request through the application</param>
        public CategoryProducer(IEventClient eventClient, ITracer tracer)
        {
            _eventClient = eventClient ?? throw new ArgumentNullException(nameof(eventClient));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Notifies that a change of a <see cref="AccountingCategory"/> occured
        /// </summary>
        /// <param name="category">The data which was changed</param>
        /// <returns></returns>
        public Task CategoryUpdated(AccountingCategory category)
        {
            if (category.Id == 0)
            {
                throw new ArgumentException("The id must be set and cannot be 0");
            }

            var updatedEvent = new AccountingCategoryUpdated
            {
                Id = category.Id,
                Name = category.Name,
                ParentName = category.Parent?.Name,
                ParentId = category.Parent?.Id ?? 0
            };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.CategoryId, category.Id);
            return _eventClient.Publish(updatedEvent);
        }
    }
}
