﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Services.Models;

namespace FinanceTracker.Apis.Category.Producers
{
    /// <summary>
    /// Produces events for the <see cref="AccountingCategory"/>
    /// </summary>
    public interface ICategoryProducer
    {
        /// <summary>
        /// Notifies that a change of a <see cref="AccountingCategory"/> occured
        /// </summary>
        /// <param name="category">The data which was changed</param>
        /// <returns></returns>
        Task CategoryUpdated(AccountingCategory category);
    }
}