﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FinanceTracker.Apis.Account.Repositories
{
    /// <summary>
    /// Persitence layer for the mongo database
    /// </summary>
    public class MongoAccountRepo : MongoDbRepository<Models.Account>, IAccountRepository 
    {
        private readonly ITracer _tracer;

        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected override string RepoName => "Account";

        /// <summary>
        /// Instantiates a new instance of <see cref="MongoAccountRepo"/>
        /// </summary>
        /// <param name="client">The client which connects to the database</param>
        /// <param name="optionsAccessor">The options for configuring the connection</param>
        /// <param name="tracer">Traces a request through the application</param>
        public MongoAccountRepo(IMongoClient client,
            IOptions<MongoDbRepositorySettings> optionsAccessor,
            ITracer tracer) : base(
            client,
            optionsAccessor)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Inserts or updates an account document in the database
        /// </summary>
        /// <param name="account">The data to be persisted</param>
        /// <returns>The updated or created document</returns>
        public Task<Models.Account> Upsert(Models.Account account)
        {
            var options = new FindOneAndReplaceOptions<Models.Account, Models.Account>
            {
                ReturnDocument = ReturnDocument.After,
                IsUpsert = true
            };

            FilterDefinition<Models.Account> filterDefinition = Builders<Models.Account>.Filter.Eq(a => a.Id, account.Id);

            return Collection.FindOneAndReplaceAsync(filterDefinition, account, options);
        }

        /// <summary>
        /// Changes the amount of an existing account
        /// </summary>
        /// <param name="id">The identification of the account</param>
        /// <param name="amount">The amount by which the balance should be changed</param>
        /// <returns>The current balance after the change</returns>
        public async Task<decimal> UpdateBalance(long id, decimal amount)
        {
            FilterDefinition<Models.Account> filterDefinition = Builders<Models.Account>.Filter.Eq(a => a.Id, id);
            UpdateDefinition<Models.Account> updateDefinition = Builders<Models.Account>.Update.Inc(a => a.Balance, amount);
            var options = new FindOneAndUpdateOptions<Models.Account, Models.Account>
                { ReturnDocument = ReturnDocument.After };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, id);
            Models.Account updatedAccount = await Collection.FindOneAndUpdateAsync(filterDefinition, updateDefinition, options);
            return updatedAccount?.Balance ?? 0;
        }

        /// <summary>
        /// Returns all accounts from the persistence layer
        /// </summary>
        /// <returns>At least an empty enumerable</returns>
        public async Task<IEnumerable<Models.Account>> GetAll()
        {
            using ITracingSpan span = _tracer.BuildSpan(this.GetMethodName());
            span.LogEvent(new TracingEvent("Reading all accounts"));
            return await Collection.FindSync(account => true).ToListAsync();
        }

        /// <summary>
        /// Reads an account entry by its id
        /// </summary>
        /// <param name="id">The identification of the wanted entry</param>
        /// <returns>The found entry or null, if there is none</returns>
        public async Task<Models.Account> GetById(long id)
        {
            FilterDefinition<Models.Account> filter = Builders<Models.Account>.Filter.Eq(a => a.Id, id);
            Models.Account account;
            using (ITracingSpan span = _tracer.BuildSpan(this.GetMethodName()))
            {
                span.SetAttribute(TracingKeys.AccountId, id);
                account = await Collection.FindSync(filter).SingleOrDefaultAsync();
            }

            return account;
        }

        /// <summary>
        /// Removes an entity by its id from the persistence layer
        /// </summary>
        /// <param name="id">The id of the entity, which should be removed</param>
        /// <returns></returns>
        public Task Delete(long id)
        {
            FilterDefinition<Models.Account> filter = Builders<Models.Account>.Filter.Eq(a => a.Id, id);
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, id);
            return Collection.DeleteOneAsync(filter);
        }
    }
}
