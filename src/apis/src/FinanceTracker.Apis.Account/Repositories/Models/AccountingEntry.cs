﻿using MongoDB.Bson.Serialization.Attributes;

namespace FinanceTracker.Apis.Account.Repositories.Models
{
    /// <summary>
    /// Represents a relation of an accounting entry to an account.
    /// Used for keeping track of which accounting entries belongs to which account.
    /// If there are any accounting entries, the account cannot be deleted.
    /// </summary>
    public class AccountingEntry
    {
        /// <summary>
        /// The id of the accounting entry. Every accounting entry has only one entry in this collection.
        /// So it can serve as the document id as well
        /// </summary>
        [BsonId]
        public long AccountingEntryId { get; set; }
        /// <summary>
        /// The id of the related account
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// The amount the balance changes with this accounting entry
        /// </summary>
        public decimal Amount { get; set; }
    }
}
