﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FinanceTracker.Apis.Account.Repositories.Models
{
    /// <summary>
    /// Represents an account in the database
    /// </summary>
    public class Account
    {
        /// <summary>
        /// The identification
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the account
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the account
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// How much money is in the account
        /// </summary>
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Balance { get; set; }
        /// <summary>
        /// Marks an account as deleted.
        /// This is done, if there are accounting entries associated with this account.
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
