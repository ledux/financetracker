﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinanceTracker.Apis.Account.Repositories
{
    /// <summary>
    /// Abstraction of the persistence layer for the <see cref="Models.Account"/>
    /// </summary>
    public interface IAccountRepository
    {
        /// <summary>
        /// Inserts or updates an account, depending if it already exists or not
        /// </summary>
        /// <param name="account">The data to be persisted</param>
        /// <returns>The persisted account</returns>
        Task<Models.Account> Upsert(Models.Account account);

        /// <summary>
        /// Changes the amount of an existing account
        /// </summary>
        /// <param name="id">The identification of the account</param>
        /// <param name="amount">The amount by which the balance should be changed</param>
        /// <returns>The current balance after the change</returns>
        Task<decimal> UpdateBalance(long id, decimal amount);

        /// <summary>
        /// Returns all accounts from the persistence layer
        /// </summary>
        /// <returns>At least an empty enumerable</returns>
        Task<IEnumerable<Models.Account>> GetAll();

        /// <summary>
        /// Reads an account entry by its id
        /// </summary>
        /// <param name="id">The identification of the wanted entry</param>
        /// <returns>The found entry or null, if there is none</returns>
        Task<Models.Account> GetById(long id);

        /// <summary>
        /// Removes an entity by its id from the persistence layer
        /// </summary>
        /// <param name="id">The id of the entity, which should be removed</param>
        /// <returns></returns>
        Task Delete(long id);
    }
}