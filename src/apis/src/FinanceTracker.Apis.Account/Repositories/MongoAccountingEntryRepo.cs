﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.Abstractions;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using DbModel = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.Repositories
{
    /// <summary>
    /// Persistence layer for the accounting entries in the account service
    /// </summary>
    public class MongoAccountingEntryRepo : MongoDbRepository<DbModel>, IAccountingEntryRepository
    {
        private readonly ITracer _tracer;
        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected override string RepoName => "AccountingEntry";

        /// <summary>
        /// Instantiates a new instance of <see cref="MongoAccountingEntryRepo"/>
        /// It creates an additional index to the accountId
        /// </summary>
        /// <param name="client">The client accessing the database</param>
        /// <param name="optionsAccessor">The options for configuring the client</param>
        /// <param name="tracer">Traces a request through the application</param>
        public MongoAccountingEntryRepo(IMongoClient client,
            IOptions<MongoDbRepositorySettings> optionsAccessor,
            ITracer tracer)
            : base(client, optionsAccessor)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));

            IndexKeysDefinition<DbModel> indexKey = Builders<DbModel>.IndexKeys.Ascending(e => e.AccountId);
            var indexModel = new CreateIndexModel<DbModel>(indexKey);
            Collection.Indexes.CreateOne(indexModel);
        }

        /// <summary>
        /// Inserts or updates an entry, depending there is already an entry with the same key
        /// </summary>
        /// <param name="data">The data, which should be inserted or updated</param>
        /// <returns>The inserted or updated data</returns>
        public Task<DbModel> Upsert(DbModel data)
        {
            FilterDefinition<DbModel> filter = Builders<DbModel>.Filter.Eq(
                e => e.AccountingEntryId,
                data.AccountingEntryId);
            var options = new FindOneAndReplaceOptions<DbModel, DbModel>
            {
                ReturnDocument = ReturnDocument.After,
                IsUpsert = true,
                BypassDocumentValidation = true
            };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountId, data.AccountId },
                { TracingKeys.AccountingEntryId, data.AccountingEntryId }
            });

            return Collection.FindOneAndReplaceAsync(filter, data, options);
        }

        /// <summary>
        /// Counts the accounting entries associated by a certain account
        /// </summary>
        /// <param name="accountId">The id of the account, for which the entries should be counted</param>
        /// <returns>The amount of accounting entries of the account. Zero, if there is no account</returns>
        public Task<long> CountEntriesByAccount(long accountId)
        {
            FilterDefinition<DbModel> filter = Builders<DbModel>.Filter.Eq(e => e.AccountId, accountId);
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, accountId);
            return Collection.CountDocumentsAsync(filter);
        }

        /// <summary>
        /// Removes an entry from the persistence layer
        /// </summary>
        /// <param name="accountingEntryId">The id of the accounting entry to be removed</param>
        /// <returns>The deleted entry</returns>
        public Task<DbModel> Delete(long accountingEntryId)
        {
            FilterDefinition<DbModel> filter = Builders<DbModel>.Filter.Eq(e => e.AccountingEntryId, accountingEntryId);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, accountingEntryId);
            return Collection.FindOneAndDeleteAsync(filter);
        }

        /// <summary>
        /// Reads an entry by its id
        /// </summary>
        /// <param name="accountingEntryId">The id of the entry</param>
        /// <returns>Null, if there is no entry with the given id</returns>
        public Task<DbModel> GetById(long accountingEntryId)
        {
            FilterDefinition<DbModel> filter = Builders<DbModel>.Filter.Eq(a => a.AccountingEntryId, accountingEntryId);

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, accountingEntryId);
            return Collection.FindSync(filter).SingleOrDefaultAsync();
        }
    }
}
