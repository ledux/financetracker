﻿using System.Threading.Tasks;
using DbModel = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.Repositories
{
    /// <summary>
    /// Persistence layer for accounting entries in the account api
    /// </summary>
    public interface IAccountingEntryRepository
    {
        /// <summary>
        /// Inserts or updates an entry, depending there is already an entry with the same key
        /// </summary>
        /// <param name="data">The data, which should be inserted or updated</param>
        /// <returns>The inserted or updated data</returns>
        Task<DbModel> Upsert(DbModel data);
        /// <summary>
        /// Counts the accounting entries associated by a certain account
        /// </summary>
        /// <param name="accountId">The id of the account, for which the entries should be counted</param>
        /// <returns>The amount of accounting entries of the account. Zero, if there is no account</returns>
        Task<long> CountEntriesByAccount(long accountId);
        /// <summary>
        /// Removes an entry from the persistence layer
        /// </summary>
        /// <param name="accountingEntryId">The id of the accounting entry to be removed</param>
        /// <returns>The deleted entry</returns>
        Task<DbModel> Delete(long accountingEntryId);

        /// <summary>
        /// Reads an entry by its id
        /// </summary>
        /// <param name="accountingEntryId">The id of the entry</param>
        /// <returns>Null, if there is no entry with the given id</returns>
        Task<DbModel> GetById(long accountingEntryId);
    }
}