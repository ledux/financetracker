using System;
using System.Threading;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Hosting;
using FinanceTracker.Apis.Common.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.Account
{
    /// <summary>
    /// Starting point of the account api
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Main method of the account api
        /// </summary>
        /// <param name="args">This application does not accept any parameters</param>
        /// <returns></returns>
        public static async Task Main(string[] args)
        {
            IWebHost apiHost = ApiHostBuilder.Build();
            IHost projectionHost = ProjectionsHostBuilder.Build();

            await new Func<CancellationToken, Task>[]
            {
                token => apiHost.RunAsync(token),
                token => projectionHost.RunAsync(token)
            }.Run();
        }
    }
}
