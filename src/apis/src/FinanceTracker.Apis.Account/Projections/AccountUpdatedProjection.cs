﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.Projections
{
    /// <summary>
    /// Handles the <see cref="AccountUpdated"/> event
    /// </summary>
    public class AccountUpdatedProjection : IProjection<AccountUpdated>
    {
        private readonly IAccountService _accountService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountUpdatedProjection"/>
        /// </summary>
        /// <param name="accountService">The business service for Accounts</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountUpdatedProjection(IAccountService accountService, ITracer tracer)
        {
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends the data to the service
        /// </summary>
        /// <param name="event">The data from the event sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, @event.Id);

            var account = new Services.Models.Account
            {
                Id = @event.Id,
                Name = @event.Name,
                Balance = @event.Balance,
                Description = @event.Description
            };

            return _accountService.CreateOrUpdate(account);
        }
    }
}
