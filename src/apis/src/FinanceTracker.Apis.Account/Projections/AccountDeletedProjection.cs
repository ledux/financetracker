﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.Projections
{
    /// <summary>
    /// Handles the <see cref="AccountDeleted"/> event
    /// </summary>
    public class AccountDeletedProjection : IProjection<AccountDeleted>
    {
        private readonly IAccountService _accountService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of the <see cref="AccountDeletedProjection"/>
        /// </summary>
        /// <param name="accountService">The business functionality for dealing with the accounts </param>
        /// <param name="tracer">Traces request through the application</param>
        public AccountDeletedProjection(IAccountService accountService, ITracer tracer)
        {
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends the id to the Delete method of the <see cref="IAccountingService"/>
        /// </summary>
        /// <param name="event">The data from the event sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountDeleted @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, @event.Id);

            return _accountService.Delete(@event.Id);
        }
    }
}
