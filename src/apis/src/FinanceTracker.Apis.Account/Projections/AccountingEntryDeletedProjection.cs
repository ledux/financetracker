﻿using System;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.Projections
{
    /// <summary>
    /// Handles the accounting entry deleted events
    /// </summary>
    public class AccountingEntryDeletedProjection : IProjection<AccountingEntryDeleted>
    {
        private readonly IAccountingEntryService _service;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingEntryDeletedProjection"/>
        /// </summary>
        /// <param name="service">The business logic for the accounting entry</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingEntryDeletedProjection(IAccountingEntryService service, ITracer tracer)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Calls the delete method of the <see cref="IAccountingEntryService"/>
        /// </summary>
        /// <param name="event">The id of the deleted accounting entry</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryDeleted @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, @event.Id);

            return _service.Delete(@event.Id);
        }
    }
}
