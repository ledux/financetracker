﻿using System;
using System.Collections.Generic;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.Projections
{
    /// <summary>
    /// Listens to the accounting entry updated event
    /// </summary>
    public class AccountingEntryUpdatedProjection : IProjection<AccountingEntryUpdated>
    {
        private readonly IAccountingEntryService _accountingEntryService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountingEntryUpdatedProjection"/>
        /// </summary>
        /// <param name="service">The business logic of the accounting entry</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountingEntryUpdatedProjection(IAccountingEntryService service, ITracer tracer)
        {
            _accountingEntryService = service ?? throw new ArgumentNullException(nameof(service));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Calls the CreateOrUpdate method of the <see cref="IAccountingEntryService"/>
        /// </summary>
        /// <param name="event">The data from the event sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountId, @event.AccountId },
                { TracingKeys.AccountingEntryId, @event.Id }
            });

            var accountingEntry = new AccountingEntry
            {
                AccountId = @event.AccountId,
                AccountingEntryId = @event.Id,
                Amount = @event.Amount
            };

            return _accountingEntryService.CreateOrUpdate(accountingEntry);
        }
    }
}
