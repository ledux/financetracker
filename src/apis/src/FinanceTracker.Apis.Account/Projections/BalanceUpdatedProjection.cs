﻿using System;
using System.Collections.Generic;
using System.EventSourcing;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.Projections
{
    /// <summary>
    /// Handles the accounting entry updated event to change the balance of an account
    /// </summary>
    public class BalanceUpdatedProjection : IProjection<AccountingEntryUpdated>
    {
        private readonly IAccountingService _accountingService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of the <see cref="BalanceUpdatedProjection"/>
        /// </summary>
        /// <param name="accountingService">The service which handles the changing of the balance</param>
        /// <param name="tracer">Traces a request through the application</param>
        public BalanceUpdatedProjection(IAccountingService accountingService, ITracer tracer)
        {
            _accountingService = accountingService ?? throw new ArgumentNullException(nameof(accountingService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Gets called, when a <see cref="AccountingEntryUpdated"/> event gets read from the event sourcing
        /// </summary>
        /// <param name="event">The data from even sourcing</param>
        /// <returns></returns>
        public Task Handle(AccountingEntryUpdated @event)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountId, @event.AccountId },
                { TracingKeys.AccountingEntryId, @event.Id }
            });

            var accountingEntry = new AccountingEntry
            {
                Amount = @event.Amount,
                AccountingEntryId = @event.Id,
                AccountId = @event.AccountId
            };

            return _accountingService.ChangeBalance(accountingEntry);
        }
    }
}
