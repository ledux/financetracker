﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.EventMessages.ErrorEvents;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.KafkaHostBuilder;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using DbAccountingEntry = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.Services.ProjectionHostServices
{
    /// <summary>
    /// Service for the accounting functionality of an account
    /// </summary>
    public class ProjectionAccountingService : IAccountingService
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountingEntryRepository _accountingEntryRepository;
        private readonly EventClientResolver _clientResolver;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionAccountingService"/>
        /// </summary>
        /// <param name="accountRepository">The persistence layer for the account</param>
        /// <param name="accountingEntryRepository">The persistence layer for the accounting entries</param>
        /// <param name="clientResolver">Resolves a <see cref="IEventClient"/> by the topic it writes to</param>
        /// <param name="tracer">Traces a request through the application</param>
        public ProjectionAccountingService(IAccountRepository accountRepository,
            IAccountingEntryRepository accountingEntryRepository,
            EventClientResolver clientResolver,
            ITracer tracer)
        {
            _accountRepository = accountRepository ?? throw new ArgumentNullException(nameof(accountRepository));
            _accountingEntryRepository = accountingEntryRepository ?? throw new ArgumentNullException(nameof(accountingEntryRepository));
            _clientResolver = clientResolver ?? throw new ArgumentNullException(nameof(clientResolver));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Changes the balance of an account
        /// </summary>
        /// <param name="accountId">The id of the account, which balance should be changed</param>
        /// <param name="amount">The amount by which the balance should be changed.</param>
        /// <returns>The new balance of the account</returns>
        public async Task<decimal> ChangeBalance(long accountId, decimal amount)
        {
            using ITracingSpan span = _tracer.BuildSpan(this.GetMethodName());
            if (accountId == 0)
            {
                span.LogError("account id is 0", TracingStatus.NotFound);
                IEventClient client = _clientResolver("system.errors");
                await client.Publish(new AccountDoesNotExist { Id = accountId });
                return 0;
            }
            span.SetAttribute(TracingKeys.AccountId, accountId);
            span.LogEvent(new TracingEvent($"changing balance by '{amount}'"));

            return await _accountRepository.UpdateBalance(accountId, amount);
        }

        /// <summary>
        /// Changes the balance of an account.
        /// If the accounting entry already exists, it only changes the difference between the existant and updated
        /// entry
        /// </summary>
        /// <param name="accountingEntry">The accounting entry, which changes the balance</param>
        /// <returns>The new balance of the account</returns>
        public async Task<decimal> ChangeBalance(AccountingEntry accountingEntry)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            DbAccountingEntry existing = await _accountingEntryRepository.GetById(accountingEntry.AccountingEntryId);
            decimal amount = accountingEntry.Amount;

            if (existing != null)
            {
                tracingSpan.LogEvent($"There is already an accounting entry with id '{accountingEntry.AccountingEntryId}'");
                amount = accountingEntry.Amount - existing.Amount;
            }

            return await ChangeBalance(accountingEntry.AccountId, amount);
        }
    }
}
