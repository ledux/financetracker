﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using AccountingEntry = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.Services.ProjectionHostServices
{
    /// <summary>
    /// Service for the accounting entries in the account projection host
    /// </summary>
    public class ProjectionAccountingEntryService : IAccountingEntryService
    {
        private readonly IAccountingEntryRepository _repository;
        private readonly IAccountingService _accountingService;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionAccountingEntryService"/>
        /// </summary>
        /// <param name="repository">The persistence layer for the accounting entries</param>
        /// <param name="accountingService">Handles accounting functionality</param>
        /// <param name="tracer">Traces a request through the application</param>
        public ProjectionAccountingEntryService(IAccountingEntryRepository repository,
            IAccountingService accountingService,
            ITracer tracer)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
            _accountingService = accountingService ?? throw new ArgumentNullException(nameof(accountingService));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Creates or updates an accounting entry
        /// </summary>
        /// <param name="accountingEntry">The data which should created or updated</param>
        /// <returns>The created or updated data</returns>
        public Task<ServiceModel> CreateOrUpdate(ServiceModel accountingEntry)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            if (accountingEntry == null)
            {
                tracingSpan.LogError($"{nameof(accountingEntry)} is null", TracingStatus.InvalidArgument);
                throw new ArgumentNullException(nameof(accountingEntry));
            }
            tracingSpan.SetAttributes(new Dictionary<string, object>
            {
                { TracingKeys.AccountId, accountingEntry.AccountId },
                { TracingKeys.AccountingEntryId, accountingEntry.AccountingEntryId }
            });

            async Task<ServiceModel> Upsert(ServiceModel accountingEntry1)
            {
                AccountingEntry dbModel = accountingEntry1.ToDbModel();
                AccountingEntry inserted = await _repository.Upsert(dbModel);

                ServiceModel serviceModel = null;
                if (inserted != null)
                {
                    serviceModel = inserted.ToServiceModel();
                }

                return serviceModel;
            }

            return Upsert(accountingEntry);
        }

        /// <summary>
        /// Removes an accounting entry from the persistence layer
        /// </summary>
        /// <param name="accountingEntryId">The id of the accounting entry</param>
        /// <returns>The deleted entry</returns>
        public async Task<ServiceModel> Delete(long accountingEntryId)
        {
            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountingEntryId, accountingEntryId);

            AccountingEntry dbEntry = await _repository.Delete(accountingEntryId);
            ServiceModel serviceEntry = null;
            if (dbEntry != null)
            {
                await _accountingService.ChangeBalance(dbEntry.AccountId, dbEntry.Amount * -1);
                serviceEntry = dbEntry.ToServiceModel();
            }

            return serviceEntry;
        }
    }
}

