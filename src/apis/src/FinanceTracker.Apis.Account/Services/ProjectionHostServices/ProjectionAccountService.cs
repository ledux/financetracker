﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using DbAccount = FinanceTracker.Apis.Account.Repositories.Models.Account;
using ServiceAccount = FinanceTracker.Apis.Account.Services.Models.Account;

namespace FinanceTracker.Apis.Account.Services.ProjectionHostServices
{
    /// <summary>
    /// Account service for the projection host
    /// </summary>
    public class ProjectionAccountService : BaseAccountService
    {
        private readonly IAccountingEntryRepository _entryRepository;

        /// <summary>
        /// Instantiates a new instance of <see cref="ProjectionAccountService"/>
        /// </summary>
        /// <param name="repository">The persistence layer for the accounts</param>
        /// <param name="tracer">Traces a request throught the application</param>
        /// <param name="entryRepository">The persistence layer for the accounting entries</param>
        public ProjectionAccountService(IAccountRepository repository, ITracer tracer, IAccountingEntryRepository entryRepository) : base(repository, tracer)
        {
            _entryRepository = entryRepository ?? throw new ArgumentNullException(nameof(entryRepository));
        }

        /// <summary>
        /// Inserts or updates an account
        /// </summary>
        /// <param name="account">The data to be updated or created</param>
        /// <returns>The updated or created account</returns>
        public override async Task<ServiceAccount> CreateOrUpdate(ServiceAccount account)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, account.Id);

            DbAccount dbModel = account.ToDbModel();
            DbAccount upsert = await AccountRepository.Upsert(dbModel);
            ServiceAccount serviceModel = null;

            if (upsert != null)
            {
                tracingSpan.LogError("Repo returned null after upsert");
                serviceModel = upsert.ToServiceModel();
            }

            return serviceModel;
        }

        /// <summary>
        /// Deletes an account, if there are no accounting entries associated with it.
        /// It sets the IsDeleted property to true, if there are any accounting entries associated with it.
        /// </summary>
        /// <param name="id">The id of the account, which should be deleted</param>
        /// <returns></returns>
        public override async Task Delete(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, id);

            long entriesByAccount = await _entryRepository.CountEntriesByAccount(id);
            if (entriesByAccount == 0)
            {
                tracingSpan.LogEvent("There are no accounting entries. Delete the account entirely");
                await AccountRepository.Delete(id);
            }
            else
            {
                tracingSpan.LogEvent($"There are {entriesByAccount} accounting entries. Only set the IsDeleted property to true");
                DbAccount account = await AccountRepository.GetById(id);
                account.IsDeleted = true;
                await AccountRepository.Upsert(account);
            }
        }
    }
}
