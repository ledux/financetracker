﻿using DbAccount = FinanceTracker.Apis.Account.Repositories.Models.Account;
using ServiceAccount = FinanceTracker.Apis.Account.Services.Models.Account;
using DbAccountingEntry = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;
using ServiceAccountingEntry = FinanceTracker.Apis.Account.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.Services.Models
{
    /// <summary>
    /// Converts service models to db models and vice versa
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Converts a service model to a db model
        /// </summary>
        /// <param name="model">The data of the service model</param>
        /// <returns>A db model with the same data</returns>
        public static DbAccount ToDbModel(this ServiceAccount model)
        {
            return new DbAccount
            {
                Id = model.Id,
                Name = model.Name,
                Balance = model.Balance,
                Description = model.Description
            };
        }

        /// <summary>
        /// Converts a service model to a db model
        /// </summary>
        /// <param name="model">The data of the service model</param>
        /// <returns>A db model with the same data</returns>
        public static DbAccountingEntry ToDbModel(this ServiceAccountingEntry model)
        {
            return new DbAccountingEntry
            {
                AccountingEntryId = model.AccountingEntryId,
                AccountId = model.AccountId,
                Amount = model.Amount
            };
        }

        /// <summary>
        /// Converts a db model to a service model. It does not instantiate the parent, even if the parent id is not 0
        /// </summary>
        /// <param name="model">The data of the database</param>
        /// <returns>A service model with the same data</returns>
        public static ServiceAccount ToServiceModel(this DbAccount model)
        {
            return new ServiceAccount
            {
                Id = model.Id,
                Name = model.Name,
                Balance = model.Balance,
                Description = model.Description
            };
        }

        /// <summary>
        /// Converts a db model to a service model.
        /// </summary>
        /// <param name="model">The data of the database</param>
        /// <returns>A service model with the same data</returns>
        public static ServiceAccountingEntry ToServiceModel(this DbAccountingEntry model)
        {
            return new AccountingEntry
            {
                AccountId = model.AccountId,
                AccountingEntryId = model.AccountingEntryId,
                Amount = model.Amount
            };
        }
    }
}
