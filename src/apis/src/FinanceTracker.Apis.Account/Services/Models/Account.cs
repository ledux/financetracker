﻿using FinanceTracker.Apis.Common.DataModels;

namespace FinanceTracker.Apis.Account.Services.Models
{
    /// <summary>
    /// Represents an (virtual) account in the services of the account api
    /// </summary>
    public class Account : IIdModel
    {
        /// <summary>
        /// The identification
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the account
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the account
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// How much money is in the account
        /// </summary>
        public decimal Balance { get; set; }
    }
}
