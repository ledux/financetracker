﻿namespace FinanceTracker.Apis.Account.Services.Models
{
    /// <summary>
    /// Represents an accounting entry in the account service
    /// </summary>
    public class AccountingEntry
    {
        /// <summary>
        /// The id of the accounting entry.
        /// </summary>
        public long AccountingEntryId { get; set; }
        /// <summary>
        /// The id of the account, to which this accounting entry belongs
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// The amount the balance changes with this accounting entry
        /// </summary>
        public decimal Amount { get; set; }
    }
}