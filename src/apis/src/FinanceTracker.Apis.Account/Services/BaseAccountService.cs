﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.Account;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;

namespace FinanceTracker.Apis.Account.Services
{
    /// <summary>
    /// Holds the base functionality for the account 
    /// </summary>
    public abstract class BaseAccountService : IAccountService
    {
        /// <summary>
        /// The persitence layer for the accounts
        /// </summary>
        protected readonly IAccountRepository AccountRepository;
        /// <summary>
        /// Tracer 
        /// </summary>
        protected readonly ITracer Tracer;

        /// <summary>
        /// Initializes a new instance of <see cref="BaseAccountService"/>
        /// </summary>
        /// <param name="accountRepository">The persistence layer for the accounts</param>
        /// <param name="tracer">Traces a request through the application</param>
        protected BaseAccountService(IAccountRepository accountRepository, ITracer tracer)
        {
            AccountRepository = accountRepository ?? throw new ArgumentNullException(nameof(accountRepository));
            Tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Creates or updates an account
        /// </summary>
        /// <param name="account">The data, which should be created</param>
        /// <returns>The newly created or updated data</returns>
        public abstract Task<ServiceModel> CreateOrUpdate(ServiceModel account);

        /// <summary>
        /// Calls the functionality to delete an account
        /// </summary>
        /// <param name="id">The id of the account, which should be deleted</param>
        /// <returns></returns>
        public abstract Task Delete(long id);

        /// <summary>
        /// Gets all accounts, which are not marked as deleted
        /// </summary>
        /// <returns>At least an empty IEnumerable</returns>
        public async Task<IEnumerable<ServiceModel>> GetAll()
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            IEnumerable<RepoModel> allAccounts = (await AccountRepository.GetAll()).Where(a => !a.IsDeleted).ToArray();

            if (!allAccounts.Any())
            {
                tracingSpan.LogEvent(new TracingEvent("No accounts found"));
                return Enumerable.Empty<ServiceModel>();
            }

            tracingSpan.LogEvent(new TracingEvent($"{allAccounts.Count()} accounts found"));
            IEnumerable<ServiceModel> serviceAccounts = allAccounts.Select(a => a.ToServiceModel());

            return serviceAccounts;
        }

        /// <summary>
        /// Reads an account by its id.
        /// If the account is marked as deleted, then it returns null
        /// </summary>
        /// <param name="id">The identification of the account</param>
        /// <returns>The found data. null, if there is no data or the account is marked as deleted</returns>
        public async Task<ServiceModel> GetById(long id)
        {
            using ITracingSpan span = Tracer.BuildSpan(this.GetMethodName());
            span.SetAttribute(TracingKeys.AccountId, id);
            RepoModel repoModel = await AccountRepository.GetById(id);
            if (repoModel?.IsDeleted ?? false)
            {
                span.LogEvent($"Requested Account with id '{repoModel.Id}  is marked as deleted");
                return null;
            }

            return repoModel?.ToServiceModel();
        }
    }
}
