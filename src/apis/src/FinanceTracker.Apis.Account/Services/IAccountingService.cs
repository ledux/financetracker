﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services.Models;

namespace FinanceTracker.Apis.Account.Services
{
    /// <summary>
    /// Interface for dealing with accounting operations on the account
    /// </summary>
    public interface IAccountingService
    {
        /// <summary>
        /// Changes the balance of an account
        /// </summary>
        /// <param name="accountId">The id of the account, which balance should be changed</param>
        /// <param name="amount">The amount by which the balance should be changed.</param>
        /// <returns>The new balance of the account</returns>
        Task<decimal> ChangeBalance(long accountId, decimal amount);

        /// <summary>
        /// Changes the balance of an account
        /// </summary>
        /// <param name="accountingEntry">The accounting entry, which changes the balance</param>
        /// <returns>The new balance of the account</returns>
        Task<decimal> ChangeBalance(AccountingEntry accountingEntry);
    }
}