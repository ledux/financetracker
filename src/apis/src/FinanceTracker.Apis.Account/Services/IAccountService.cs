﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FinanceTracker.Apis.Account.Services
{
    /// <summary>
    /// Abstraction of the account service
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Creates or updates an account
        /// </summary>
        /// <param name="account">The data, which should be created</param>
        /// <returns>The newly created or updated data</returns>
        Task<Models.Account> CreateOrUpdate(Models.Account account);

        /// <summary>
        /// Gets all accounts
        /// </summary>
        /// <returns>At least an empty IEnumerable</returns>
        Task<IEnumerable<Models.Account>> GetAll();

        /// <summary>
        /// Reads an account by its id
        /// </summary>
        /// <param name="id">The identification of the account</param>
        /// <returns>The found data or null, if there is no data</returns>
        Task<Models.Account> GetById(long id);

        /// <summary>
        /// Calls the functionality to delete an account
        /// </summary>
        /// <param name="id">The id of the account, which should be deleted</param>
        /// <returns></returns>
        Task Delete(long id);
    }
}