﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Producers;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.Account;

namespace FinanceTracker.Apis.Account.Services
{
    /// <summary>
    /// Api service for handling the account
    /// </summary>
    public class ApiAccountService : BaseAccountService
    {
        private readonly IIdCreator _idCreator;
        private readonly IAccountProducer _producer;

        /// <summary>
        /// Instantiates a new instance of the <see cref="ApiAccountService"/>
        /// </summary>
        /// <param name="repository">Persistence layer for the accounts</param>
        /// <param name="idCreator">Creates unique ids</param>
        /// <param name="producer">Produces events to the event sourcing</param>
        /// <param name="tracer">Traces request through the application</param>
        public ApiAccountService(IAccountRepository repository, IIdCreator idCreator, IAccountProducer producer,
            ITracer tracer) 
            : base(repository, tracer)
        {
            _idCreator = idCreator ?? throw new ArgumentNullException(nameof(idCreator));
            _producer = producer ?? throw new ArgumentNullException(nameof(producer));
        }

        /// <summary>
        /// Creates or updates a new account. It also sets the Id
        /// </summary>
        /// <param name="account">The account which will be created or updated</param>
        /// <returns>The created account</returns>
        public override async Task<ServiceModel> CreateOrUpdate(ServiceModel account)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            if (account == null)
            {
                tracingSpan.LogEvent(new TracingEvent("account is null"));
                return null;
            }

            RepoModel byId = await AccountRepository.GetById(account.Id);
            if (byId == null)
            {
                account.Id = _idCreator.CreateId();
                tracingSpan.LogEvent(new TracingEvent($"accountId '{account.Id}' newly created"));
            }
            else
            {
                account.Id = byId.Id;
                account.Balance = byId.Balance;
                tracingSpan.LogEvent(new TracingEvent($"Existing id '{account.Id}' used"));
            }

            tracingSpan.SetAttribute(TracingKeys.AccountId, account.Id);

            await _producer.AccountUpdated(account);

            return account;
        }

        /// <summary>
        /// Calls the producer to send an account deleted event
        /// </summary>
        /// <param name="id">The id of the account, which should be deleted</param>
        /// <returns></returns>
        public override Task Delete(long id)
        {
            using ITracingSpan tracingSpan = Tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, id);
            return _producer.AccountDeleted(id);
        }
    }
}
