﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Services.Models;

namespace FinanceTracker.Apis.Account.Services
{
    /// <summary>
    /// Service for dealing with accounting entries in the account service
    /// </summary>
    public interface IAccountingEntryService
    {
        /// <summary>
        /// Creates or updates an accounting entry
        /// </summary>
        /// <param name="accountingEntry">The data which should created or updated</param>
        /// <returns>The created or updated data</returns>
        Task<AccountingEntry> CreateOrUpdate(AccountingEntry accountingEntry);

        /// <summary>
        /// Removes an accounting entry from the persistence layer
        /// </summary>
        /// <param name="accountingEntryId">The id of the accounting entry</param>
        /// <returns>The deleted entry</returns>
        Task<AccountingEntry> Delete(long accountingEntryId);
    }
}