﻿namespace FinanceTracker.Apis.Account.Controllers.Models
{
    /// <summary>
    /// Represents the model for an account, which is used for updating an account
    /// It only has the properties, which are allowed to be changed directly
    /// </summary>
    public class PutAccount
    {
        /// <summary>
        /// The new name of the account
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The new description of the account
        /// </summary>
        public string Description { get; set; }
    }
}
