﻿using FinanceTracker.Apis.Common.DataModels;
using WebModel = FinanceTracker.Apis.Account.Controllers.Models.Account;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;

namespace FinanceTracker.Apis.Account.Controllers.Models
{
    /// <summary>
    /// Converts the web model to service models and vice versa
    /// </summary>
    public static class Conversions
    {
        /// <summary>
        /// Converts a service model into a web model
        /// </summary>
        /// <param name="model">The data of the service model</param>
        /// <returns>The same data in a web model</returns>
        public static WebModel ToWebModel(this ServiceModel model)
        {
            return new Account
            {
                Id = model.Id.ToString(),
                Name = model.Name,
                Balance = model.Balance,
                Description = model.Description
            };
        }

        /// <summary>
        /// Converts a web model into a service model
        /// </summary>
        /// <param name="model">The data of the web model</param>
        /// <returns>The same data in a service model</returns>
        public static ServiceModel ToServiceModel(this WebModel model)
        {
            var serviceModel = new ServiceModel()
            {
                Balance = model.Balance,
                Description = model.Description,
                Name = model.Name
            };

            serviceModel.AssignId(model.Id);

            return serviceModel;
        }
    }
}
