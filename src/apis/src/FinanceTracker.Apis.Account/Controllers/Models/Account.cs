﻿namespace FinanceTracker.Apis.Account.Controllers.Models
{
    /// <summary>
    /// Represents an account in the web layer 
    /// </summary>
    public class Account
    {
        /// <summary>
        /// The identification as a string representation
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The name of the account
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the account
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// How much money is in the account
        /// </summary>
        public decimal Balance { get; set; }
    }
}
