﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Controllers.Models;
using FinanceTracker.Apis.Account.Services;
using Microsoft.AspNetCore.Mvc;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;
using WebModel = FinanceTracker.Apis.Account.Controllers.Models.Account;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinanceTracker.Apis.Account.Controllers
{
    /// <summary>
    /// Web api for the accounts
    /// </summary>
    [Route("v1/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountController"/>
        /// </summary>
        /// <param name="accountService">The service for dealing with accounts in the api</param>
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
        }

        /// <summary>
        /// Returns all accounts
        /// </summary>
        /// <returns>Not found, if there are no accounts</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IEnumerable<ServiceModel> allAccounts = (await _accountService.GetAll()).ToArray();
            if (!allAccounts.Any())
            {
                return NotFound();
            }

            IEnumerable<WebModel> webModels = allAccounts.Select(m => m.ToWebModel()).ToArray();

            return Ok(webModels);
        }

        /// <summary>
        /// Gets an account by its id
        /// </summary>
        /// <param name="id">The identification number of the requested account</param>
        /// <returns>Bad Request, if id is zero, Not found, if there isn't an account with the id</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(long id)
        {
            if (id == 0)
            {
                return BadRequest("The id cannot be zero and must be a positive long value");
            }

            ServiceModel account = await _accountService.GetById(id);
            if (account == null)
            {
                return NotFound();
            }

            return Ok(account.ToWebModel());
        }

        /// <summary>
        /// Creates a new account
        /// </summary>
        /// <param name="account">The data to be created</param>
        /// <returns>Accepted at response</returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WebModel account)
        {
            ServiceModel serviceModel = account.ToServiceModel();
            ServiceModel created = await _accountService.CreateOrUpdate(serviceModel);

            return AcceptedAtAction(nameof(GetById), new { id = created.Id }, created.ToWebModel());
        }

        /// <summary>
        /// Updates an account
        /// </summary>
        /// <param name="id">The id of the account to be updated</param>
        /// <param name="account">The updated data</param>
        /// <returns>Bad request, if the ids does not match, <paramref name="id"/> is zero or
        /// <paramref name="account"/>is null</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] PutAccount account)
        {
            if (account == null)
            {
                return BadRequest("account cannot be null");
            }

            if (id == 0)
            {
                return BadRequest("The id cannot be zero");
            }

            var serviceAccount = new ServiceModel
            {
                Id = id,
                Description = account.Description,
                Name = account.Name
            };
            await _accountService.CreateOrUpdate(serviceAccount);

            return NoContent();
        }

        /// <summary>
        /// Deletes an account
        /// </summary>
        /// <param name="id">The id of the account, which should be deleted</param>
        /// <returns>BadRequest, if id is zero; NoContent otherwise</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            if (id == 0)
            {
                return BadRequest("Id cannot be zero");
            }

            await _accountService.Delete(id);
            return NoContent();
        }
    }
}
