﻿using System.EventSourcing.Hosting;
using FinanceTracker.Apis.Account.Projections;
using FinanceTracker.Common.KafkaHostBuilder;
using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.Account.Hosting
{
    /// <summary>
    /// Creates a host for running the projections
    /// </summary>
    public static class ProjectionsHostBuilder
    {
        /// <summary>
        /// Builds and configures the host
        /// </summary>
        /// <returns>A fully configured host, ready to be run</returns>
        public static IHost Build()
        {
            return KafkaHostBuilder
                .BuildKafkaHost()
                .UseAsConsumer(
                    AddProjections,
                    builder => builder)
                .ConfigureServices(
                    (context, collection) =>
                    {
                        MongoDbRepositorySettings settings =
                            collection.ConfigureMongo(context.Configuration);

                        collection
                            .AddMongo(settings)
                            .AddAccountProjectionServices();

                    })
                .Build();
        }

        private static IEventSourcingBuilder<IServiceCollection> AddProjections(IEventSourcingBuilder<IServiceCollection> builder)
        {
            return builder
                .AddProjection<BalanceUpdatedProjection>()
                .AddProjection<AccountingEntryUpdatedProjection>()
                .AddProjection<AccountingEntryDeletedProjection>()
                .AddProjection<AccountUpdatedProjection>()
                .AddProjection<AccountDeletedProjection>();
        }
    }
}
