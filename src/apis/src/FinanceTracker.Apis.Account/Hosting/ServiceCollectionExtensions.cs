﻿using FinanceTracker.Apis.Account.Producers;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Apis.Account.Services.ProjectionHostServices;
using FinanceTracker.Common.KafkaHostBuilder;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.Account.Hosting
{
    /// <summary>
    /// Extends the functionality of the <see cref="IServiceCollection"/>
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Registers all necessary services for the account api
        /// </summary>
        /// <param name="services">The servicecollection where the services are added</param>
        /// <returns>The service collection where the services are added</returns>
        public static IServiceCollection AddAccountApiServices(this IServiceCollection services)
        {
            services
                .AddScoped<IAccountService, ApiAccountService>()
                .AddScoped<IAccountProducer, AccountProducer>()
                .AddSingleton<IAccountRepository, MongoAccountRepo>();

            return services;
        }

        /// <summary>
        /// Registers all necessary services for the account projection host
        /// </summary>
        /// <param name="services">The servicecollection where the services are added</param>
        /// <returns>The service collection where the services are added</returns>
        public static IServiceCollection AddAccountProjectionServices(this IServiceCollection services)
        {
            services
                .AddEventClients()
                .AddScoped<IAccountService, ProjectionAccountService>()
                .AddScoped<IAccountingService, ProjectionAccountingService>()
                .AddScoped<IAccountingEntryService, ProjectionAccountingEntryService>()
                .AddSingleton<IAccountingEntryRepository, MongoAccountingEntryRepo>()
                .AddSingleton<IAccountRepository, MongoAccountRepo>();

            return services;
        }
    }
}
