﻿using System.Threading.Tasks;

namespace FinanceTracker.Apis.Account.Producers
{
    /// <summary>
    /// Producer which sends events of accounts to the event sourcing
    /// </summary>
    public interface IAccountProducer
    {
        /// <summary>
        /// Sends an account updated event to the event sourcing
        /// </summary>
        /// <param name="account">The data, which will be sent to the event sourcing</param>
        /// <returns></returns>
        Task AccountUpdated(Services.Models.Account account);

        /// <summary>
        /// Sends an account deleted even to the event sourcing
        /// </summary>
        /// <param name="id">The of the account which should be deleted</param>
        /// <returns></returns>
        Task AccountDeleted(long id);
    }
}