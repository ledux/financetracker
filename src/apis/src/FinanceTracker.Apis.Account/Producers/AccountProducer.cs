﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.EventMessages.Constants;
using FinanceTracker.Common.Extensions;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.Producers
{
    /// <summary>
    /// Produces events for the account
    /// </summary>
    public class AccountProducer : IAccountProducer
    {
        private readonly IEventClient _eventClient;
        private readonly ITracer _tracer;

        /// <summary>
        /// Instantiates a new instance of <see cref="AccountProducer"/>
        /// </summary>
        /// <param name="eventClient">The client which publishes events to kafka</param>
        /// <param name="tracer">Traces a request through the application</param>
        public AccountProducer(IEventClient eventClient, ITracer tracer)
        {
            _eventClient = eventClient ?? throw new ArgumentNullException(nameof(eventClient));
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Sends an account updated event to event sourcing
        /// </summary>
        /// <param name="account">The data which will be sent to event sourcing</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">If the id is not set</exception>
        public Task AccountUpdated(Services.Models.Account account)
        {
            if (account.Id == 0)
            {
                throw new ArgumentException("The id cannot be 0. It should rather be a positive number");
            }

            var updatedEvent = new AccountUpdated
            {
                Id = account.Id,
                Name = account.Name,
                Balance = account.Balance,
                Description = account.Description
            };

            using ITracingSpan tracingSpan = _tracer.BuildSpan(this.GetMethodName());
            tracingSpan.SetAttribute(TracingKeys.AccountId, account.Id);
            return _eventClient.Publish(updatedEvent);
        }

        /// <summary>
        /// Sends an account deleted even to the event sourcing
        /// </summary>
        /// <param name="id">The of the account which should be deleted</param>
        /// <returns></returns>
        public Task AccountDeleted(long id)
        {
            var deletedEvent = new AccountDeleted { Id = id };
            using ITracingSpan tracingSpan = _tracer.BuildSpan("Send account deleted event");
            tracingSpan.SetAttribute(TracingKeys.AccountId, id);

            return _eventClient.Publish(deletedEvent);
        }
    }
}
