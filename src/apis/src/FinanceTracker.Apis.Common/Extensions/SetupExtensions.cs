﻿using System;
using System.IO;
using FinanceTracker.Common.KafkaHostBuilder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace FinanceTracker.Apis.Common.Extensions
{
    /// <summary>
    /// Extension methods for the setup process
    /// </summary>
    public static class SetupExtensions
    {
        /// <summary>
        /// Reads the appsettings.json file, the environment and all environment variables and adds them to the builder
        /// </summary>
        /// <param name="builder">The builder to which the configuration will be added</param>
        /// <returns>The builder to allow chaining</returns>
        public static IWebHostBuilder ConfigureApiSettings(this IWebHostBuilder builder)
        {
            builder.ConfigureAppConfiguration((_, configurationBuilder) =>
            {
                string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                configurationBuilder
                    .SetBasePathFromEnv("CONFIG_DIR")
                    .AddJsonFile("appsettings.json", false, true)
                    .AddJsonFile($"appsettings.{environment}.json", true, true)
                    .AddEnvironmentVariables();
            });

            return builder;
        }

        /// <summary>
        /// Configures the logging capabilities of the app
        /// </summary>
        /// <param name="builder">The builder used for the logging configuration</param>
        public static void ConfigureLogging(ILoggingBuilder builder)
        {
            builder.AddConsole().AddDebug();
        }

        /// <summary>
        /// Adds the controllers and a allow all cors policy to the service collection
        /// </summary>
        /// <param name="collection">The collection where the controllers and the cors policy is added</param>
        /// <returns>The collection with the added controllers and cors policy</returns>
        public static IServiceCollection AddControllersAndCors(this IServiceCollection collection)
        {
            collection
                .AddCors(CreateCorsPolicy)
                .AddControllers();

            return collection;
        }

        /// <summary>
        /// Configures and registers kafka event client as a producer
        /// </summary>
        /// <param name="serviceCollection">Services collection container</param>
        /// <param name="configuration">Configuration read from the appsettings file</param>
        /// <returns>The service collection with the added kafka dependencies</returns>
        public static IServiceCollection ConfigureKafkaEventClient(this IServiceCollection serviceCollection,
            IConfiguration configuration)
        {
            var kafkaConfiguration = configuration.GetSection("kafka").Get<KafkaConfiguration>();

            serviceCollection
                .ConfigureKafka(kafkaConfiguration)
                .AddKafkaClient(kafkaConfiguration);

            return serviceCollection;
        }

        public static IServiceCollection ConfigureKafkaEventClient<T>(this IServiceCollection collection,
            IConfiguration configuration, Func<T, string> keySelector)
        {

            var kafkaConfiguration = configuration.GetSection("kafka").Get<KafkaConfiguration>();

            collection
                .ConfigureKafka(kafkaConfiguration)
                .AddKafkaClient(kafkaConfiguration, keySelector);

            return collection;
        }

        private static IConfigurationBuilder SetBasePathFromEnv(this IConfigurationBuilder builder, string environmentVariable)
        {
            string startupPath = Environment.GetEnvironmentVariable(environmentVariable)
                                 ?? throw new ArgumentException($"Environment variable {environmentVariable} not found.");

            startupPath = Path.IsPathRooted(startupPath)
                ? startupPath
                : Path.Combine(Directory.GetCurrentDirectory(), startupPath);

            builder.SetBasePath(startupPath);

            return builder;
        }

        private static void CreateCorsPolicy(CorsOptions options)
        {
            options.AddPolicy("frontend-policy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });
        }
    }
}
