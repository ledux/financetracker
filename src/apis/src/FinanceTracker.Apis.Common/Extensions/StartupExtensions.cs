﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FinanceTracker.Apis.Common.Extensions
{
    /// <summary>
    /// Extension methods used for starting app a console application
    /// </summary>
    public static class StartupExtensions
    {
        /// <summary>
        /// Runs all <see cref="Func{CancellationToken, Task}"/> in an <see cref="IEnumerable{T}"/> and waits until all are finished.
        /// </summary>
        /// <param name="tasks">The <see cref="IEnumerable{T}"/> with the Functions inside</param>
        /// <returns>A task for async</returns>
        public static async Task Run(this IEnumerable<Func<CancellationToken, Task>> tasks)
        {
            using var tokenSource = new CancellationTokenSource();
            // ReSharper disable once AccessToDisposedClosure
            Console.CancelKeyPress += (sender, args) => tokenSource.Cancel();
            await Task.WhenAll(tasks.Select(task => task(tokenSource.Token)).ToArray());
        }
    }
}
