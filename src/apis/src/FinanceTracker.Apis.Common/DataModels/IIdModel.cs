﻿namespace FinanceTracker.Apis.Common.DataModels
{
    /// <summary>
    /// Represents a model, which has a long value as an id
    /// </summary>
    public interface IIdModel
    {
        /// <summary>
        /// The identification of the data model
        /// </summary>
        long Id { get; set; }
    }
}