﻿using System;

namespace FinanceTracker.Apis.Common.DataModels
{
    /// <summary>
    /// Extends the functionality to data models
    /// </summary>
    public static class ModelExtensions
    {
        /// <summary>
        /// Assigns the long id from the string representation 
        /// </summary>
        /// <typeparam name="TModel">The type of the data model</typeparam>
        /// <param name="model">The instance of the data model </param>
        /// <param name="stringId">The string representation of the id</param>
        /// <param name="assignId">The action, which assigns the parsed id</param>
        /// <returns>The model, to which the id was assigned </returns>
        /// <exception cref="ArgumentException">If the <paramref name="stringId"/> cannot be parsed to a long</exception>
        public static TModel AssignId<TModel>(this TModel model, string stringId, Action<TModel, long> assignId)
        {
            if (!long.TryParse(stringId, out long longId))
            {
                if (string.IsNullOrWhiteSpace(stringId)) assignId(model, 0);

                else throw new ArgumentException($"The value '{stringId}' cannot be parsed to a Int64 value");
            }

            else assignId(model, longId);

            return model;
        }

        /// <summary>
        /// Assigns the long id from a string to the known property called Id
        /// </summary>
        /// <param name="model">The model, to which the id will be assigned to</param>
        /// <param name="stringId">The string representation of the id</param>
        /// <returns>The model, to which the id was assigned</returns>
        /// <exception cref="ArgumentException">If the <paramref name="stringId"/> cannot be parsed to a long</exception>
        public static IIdModel AssignId(this IIdModel model, string stringId)
        {
            model.AssignId(stringId, (idModel, id) => idModel.Id = id);
            return model;
        }
    }
}
