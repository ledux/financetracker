﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Assignment.Producers;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FinanceTracker.Apis.Assignment.Controllers
{
    [Route("v1/[controller]")]
    [ApiController]
    public class AssignmentController : ControllerBase
    {
        private readonly ITestParallelProducer _producer;

        public AssignmentController(ITestParallelProducer producer)
        {
            _producer = producer ?? throw new ArgumentNullException(nameof(producer));
        }
        // GET: api/<AssignmentController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<AssignmentController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            await _producer.ProduceTestData();
            return NoContent();
        }

        // POST api/<AssignmentController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<AssignmentController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<AssignmentController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
