﻿using FinanceTracker.Apis.Common.Extensions;
using FinanceTracker.Common.KafkaHostBuilder;
using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.Hosting;

namespace FinanceTracker.Apis.Assignment.Hosting
{
    /// <summary>
    /// Builds a host which has all projections to consume the accounting entries
    /// </summary>
    public static class ProjectionHostBuilder
    {
        /// <summary>
        /// Configures the settings, adds the services and builds the host
        /// </summary>
        /// <returns>A fully configured host, ready to be run</returns>
        public static IHost Build()
        {
            return KafkaHostBuilder.BuildKafkaHost()
                .UseAsConsumer(
                    projection => projection,
                    transformation => transformation)
                .ConfigureServices((context, collection) =>
                {
                    MongoDbRepositorySettings settings = collection.ConfigureMongo(context.Configuration);
                    collection.AddMongo(settings);
                })
                .ConfigureLogging(SetupExtensions.ConfigureLogging)
                .Build();
        }
    }
}
