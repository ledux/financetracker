﻿using FinanceTracker.Apis.Assignment.Producers;
using FinanceTracker.Apis.Assignment.Services.Models;
using FinanceTracker.Apis.Common.Extensions;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.MongoSetup;
using FinanceTracker.Common.Tracing.OpenTracing.Extensions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Apis.Assignment.Hosting
{
    /// <summary>
    /// Builds the api host for the assgnment API
    /// </summary>
    public static class ApiHostBuilder
    {
        /// <summary>
        /// Configures and builds the host and adds all services
        /// </summary>
        /// <returns>The fully configured host, ready to run</returns>
        public static IWebHost Build()
        {
            return WebHost.CreateDefaultBuilder()
                .ConfigureApiSettings()
                .ConfigureServices(AddServices)
                .Configure(ConfigureApp)
                .ConfigureLogging(SetupExtensions.ConfigureLogging)
                .Build();
        }

        private static void AddServices(WebHostBuilderContext context, IServiceCollection collection)
        {
            MongoDbRepositorySettings settings = collection.ConfigureMongo(context.Configuration);

            collection
                .AddControllersAndCors()
                .AddMongo(settings)
                .AddTracing(context.Configuration)
                .AddIdCreator()
                .ConfigureKafkaEventClient<TestParallelModel>(context.Configuration, model => model.Id.ToString("D6"))
                .AddScoped<ITestParallelProducer, TestParallelProducer>();
        }

        private static void ConfigureApp(WebHostBuilderContext builderContext, IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseCors("frontend-policy")
                .UseRouting()
                .UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
