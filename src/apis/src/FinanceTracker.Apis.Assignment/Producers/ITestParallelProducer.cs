﻿using System.Threading.Tasks;

namespace FinanceTracker.Apis.Assignment.Producers
{
    public interface ITestParallelProducer
    {
        Task ProduceTestData();
    }
}