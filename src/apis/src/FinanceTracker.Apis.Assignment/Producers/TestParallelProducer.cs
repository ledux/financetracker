﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Assignment.Services.Models;

namespace FinanceTracker.Apis.Assignment.Producers
{
    public class TestParallelProducer: ITestParallelProducer
    {
        private readonly IEventClient _eventClient;
        private const int Amount = 100;

        public TestParallelProducer(IEventClient eventClient)
        {
            _eventClient = eventClient ?? throw new ArgumentNullException(nameof(eventClient));
        }

        public async Task ProduceTestData()
        {
            for (var i = 0; i < Amount; i++)
            {
                var data = new TestParallelModel
                {
                    Name = $"data number {i:000000}",
                    Id = i
                };

                await _eventClient.Publish(data);
            }
        }
    }
}
