﻿namespace FinanceTracker.Apis.Common.UnitTest.DataModels.ModelExtensionsTests
{
    internal class TestDataModel
    {
        public string IdString { get; set; }
        public long IdLong { get; set; }
    }
}
