﻿using FinanceTracker.Apis.Common.DataModels;
using Xunit;

namespace FinanceTracker.Apis.Common.UnitTest.DataModels.ModelExtensionsTests
{
    public class AssignIdToIdModelTests
    {
        [Theory]
        [InlineData("1234", 1234)]
        [InlineData("0", 0)]
        [InlineData("-1234", -1234)]
        [InlineData("1234567890", 1234567890)]
        [InlineData("9876543210", 9876543210)]
        public void StringIdIsParseable_AssignsValueToIdProperty(string stringIdValue, long longIdValue)
        {
            var unitUnderTest = new TestIdModel();

            IIdModel actual = unitUnderTest.AssignId(stringIdValue);

            Assert.Equal(longIdValue, unitUnderTest.Id);
            Assert.Equal(longIdValue, actual.Id);
        }
    }
}
