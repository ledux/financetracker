﻿using FinanceTracker.Apis.Common.DataModels;

namespace FinanceTracker.Apis.Common.UnitTest.DataModels.ModelExtensionsTests
{
    internal class TestIdModel : IIdModel
    {
        public long Id { get; set; }
    }
}
