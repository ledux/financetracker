﻿using System;
using FinanceTracker.Apis.Common.DataModels;
using Xunit;

namespace FinanceTracker.Apis.Common.UnitTest.DataModels.ModelExtensionsTests
{
    public class AssignIdTests
    {
        [Theory]
        [InlineData("NotALongValue")]
        [InlineData("Another string which cannot be parsed")]
        public void IdStringIsNotALongValue__Throws(string idStringValue)
        {
            var unitUnderTest = new TestDataModel { IdString = idStringValue };

            Assert.Throws<ArgumentException>(
                () => unitUnderTest.AssignId(unitUnderTest.IdString, (model, id) => model.IdLong = id));
        }

        [Theory]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(null)]
        public void IdIsNullEmptyOrWhiteSpace__AssignsZero(string idStringValue)
        {
            var unitUnderTest = new TestDataModel { IdString = idStringValue };

            TestDataModel actual = unitUnderTest.AssignId(unitUnderTest.IdString, (model, id) => model.IdLong = id);

            Assert.Equal(0, unitUnderTest.IdLong);
            Assert.Equal(0, actual.IdLong);
        }

        [Theory]
        [InlineData("1234", 1234)]
        [InlineData("0", 0)]
        [InlineData("-1234", -1234)]
        [InlineData("1234567890", 1234567890)]
        [InlineData("9876543210", 9876543210)]
        public void IdStringRepresentsALongValue__IdIsSetToLong(string idStringValue, long idLongValue)
        {
            var unitUnderTest = new TestDataModel { IdString = idStringValue };

            TestDataModel actual = unitUnderTest.AssignId(unitUnderTest.IdString, (model, id) => model.IdLong = id);

            Assert.Equal(idLongValue, unitUnderTest.IdLong);
            Assert.Equal(idLongValue, actual.IdLong);
        }
    }
}
