﻿using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Producers;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Producers.AccountProducerTests
{
    public class AccountDeletedTests
    {
        [Fact]
        public async Task IdIsNotZero_SendsAccountDeletedEvent()
        {
            const long id = 3111235608;
            (AccountProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> eventClientMock = mockBag.GetMock<IEventClient>();

            await unitUnderTest.AccountDeleted(id);

            eventClientMock.Verify(c => c.Publish(It.Is<AccountDeleted>(a => a.Id == id)));
        }
    }
}
