﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Producers;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Producers.AccountProducerTests
{
    public class AccountUpdatedTests
    {
        private readonly long _id = 333155323;
        private readonly string _name = "Name of account";
        private readonly decimal _amount = (decimal) 3.56;
        private readonly string _description = "Description of the account";

        [Fact]
        public async Task IdIsZero_Throws()
        {
            AccountProducer unitUnderTest = new UnitUnderTestBuilder<AccountProducer>().Build();

            await Assert.ThrowsAsync<ArgumentException>(
                () => unitUnderTest.AccountUpdated(new Account.Services.Models.Account()));
        }

        [Fact]
        public void PropertiesAreSet__CallsPublishWithData()
        {
            var account = new Account.Services.Models.Account
            {
                Id = _id,
                Name = _name,
                Balance = _amount,
                Description = _description
            };

            (AccountProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> eventClientMock = mockBag.GetMock<IEventClient>();

            unitUnderTest.AccountUpdated(account);

            eventClientMock.Verify(e => e.Publish(It.Is<AccountUpdated>(a => a.Id == _id)));
            eventClientMock.Verify(e => e.Publish(It.Is<AccountUpdated>(a => a.Balance == _amount)));
            eventClientMock.Verify(e => e.Publish(It.Is<AccountUpdated>(a => a.Name == _name)));
            eventClientMock.Verify(e => e.Publish(It.Is<AccountUpdated>(a => a.Description == _description)));
        }
    }
}
