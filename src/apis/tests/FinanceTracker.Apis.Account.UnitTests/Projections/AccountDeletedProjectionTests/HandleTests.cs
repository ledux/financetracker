﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Projections;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Projections.AccountDeletedProjectionTests
{
    public class HandleTests
    {
        [Fact]
        public async Task ReceivesId_CallsDeleteOnService()
        {
            const long id = 989134124;
            (AccountDeletedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountDeletedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountService> serviceMock = mockBag.GetMock<IAccountService>();

            await unitUnderTest.Handle(new AccountDeleted { Id = id });

            serviceMock.Verify(s => s.Delete(id));
        }
    }
}
