﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Projections;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Projections.AccountingEntryDeletedProjectionTests
{
    public class HandleTests
    {
        private const long Id = 3412351234;

        [Fact]
        public async Task DataReceived_CallService()
        {
            var data = new AccountingEntryDeleted
            {
                Id = Id
            };

            (AccountingEntryDeletedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingEntryDeletedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryService> serviceMock = mockBag.GetMock<IAccountingEntryService>();

            await unitUnderTest.Handle(data);

            serviceMock.Verify(s => s.Delete(Id));
        }
    }
}
