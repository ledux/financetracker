﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Projections;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Projections.AccountUpdatedProjectionTests
{
    public class HandleTests
    {
        private readonly long _id = 99923324;
        private readonly string _name = "name";
        private readonly decimal _amount = (decimal) 254.4;
        private readonly string _description = "Description";

        [Fact]
        public async Task PropertiesSet_CallsUpsertWithData()
        {
            var accountEvent = new AccountUpdated
            {
                Id = _id,
                Name = _name,
                Balance = _amount,
                Description = _description
            };
            (AccountUpdatedProjection unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<AccountUpdatedProjection>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountService> repoMock = mockBag.GetMock<IAccountService>();

            await unitUnderTest.Handle(accountEvent);

            repoMock.Verify(r => r.CreateOrUpdate(It.Is<Account.Services.Models.Account>(a =>a.Id == _id)));
            repoMock.Verify(r => r.CreateOrUpdate(It.Is<Account.Services.Models.Account>(a =>a.Balance == _amount)));
            repoMock.Verify(r => r.CreateOrUpdate(It.Is<Account.Services.Models.Account>(a =>a.Name == _name)));
            repoMock.Verify(r => r.CreateOrUpdate(It.Is<Account.Services.Models.Account>(a =>a.Description == _description)));
        }
    }
}
