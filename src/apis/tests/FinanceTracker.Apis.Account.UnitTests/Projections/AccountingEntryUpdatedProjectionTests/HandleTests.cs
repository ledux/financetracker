﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Projections;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Projections.AccountingEntryUpdatedProjectionTests
{
    public class HandleTests
    {
        private readonly DateTimeOffset _bookingDate = DateTimeOffset.Now;
        private const decimal Amount = (decimal) 55.22;
        private const long CategoryId = 555123432;
        private const long AccountId = 989182341234;
        private const long Id = 12341234;

        [Fact]
        public async Task DateReceived_SendsDataToService()
        {
            var data = new AccountingEntryUpdated
            {
                Id = Id,
                AccountId = AccountId,
                CategoryId = CategoryId,
                BookingDate = _bookingDate
            };
            (AccountingEntryUpdatedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingEntryUpdatedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryService> serviceMock = mockBag.GetMock<IAccountingEntryService>();

            await unitUnderTest.Handle(data);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.AccountingEntryId == Id)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.AccountId == AccountId)));
        }

        [Fact]
        public async Task DataReceived__SendsTheAmountToService()
        {
            var data = new AccountingEntryUpdated
            {
                Id = Id,
                AccountId = AccountId,
                Amount = Amount
            };
            (AccountingEntryUpdatedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingEntryUpdatedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryService> serviceMock = mockBag.GetMock<IAccountingEntryService>();

            await unitUnderTest.Handle(data);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(a => a.Amount == Amount)));
        }
    }
}
