﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Projections;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Projections.BalanceUpdatedProjectionTests
{
    public class HandleTests
    {
        private const long AccountId = 6623355623;
        private const decimal Amount = (decimal) 42.6;
        private const long AccountingEntryId = 1234123;

        [Fact]
        public async Task DataReceived_CallsChangeBalanceWithAccountingEntry()
        {
            var accountingEvent = new AccountingEntryUpdated
            {
                AccountId = AccountId,
                Amount = Amount,
                Id =  AccountingEntryId
            };

            (BalanceUpdatedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<BalanceUpdatedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();

            await unitUnderTest.Handle(accountingEvent);

            serviceMock.Verify(s => s.ChangeBalance(It.Is<AccountingEntry>(a => a.AccountingEntryId == AccountingEntryId)));
            serviceMock.Verify(s => s.ChangeBalance(It.Is<AccountingEntry>(a => a.AccountId == AccountId)));
            serviceMock.Verify(s => s.ChangeBalance(It.Is<AccountingEntry>(a => a.Amount == Amount)));
        }
    }
}
