﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Controllers;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Controllers.AccountControllerTests
{
    public class DeleteTests
    {
        private const int Id = 123412;

        [Fact]
        public async Task IdIsZero_ReturnsBadRequest()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>().Build();

            IActionResult actual = await unitUnderTest.Delete(0);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero__CallsServiceWithId()
        {
            (AccountController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountController>().BuildBoth();
            Mock<IAccountService> serviceMock = mockBag.GetMock<IAccountService>();

            await unitUnderTest.Delete(Id);

            serviceMock.Verify(s => s.Delete(Id));
        }

        [Fact]
        public async Task IdIsNotZero__ReturnsNoContent()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>().Build();

            IActionResult actual = await unitUnderTest.Delete(Id);

            Assert.IsType<NoContentResult>(actual);
        }
    }
}
