﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Controllers;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Testing;
using Xunit;
using Moq;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;
using WebModel = FinanceTracker.Apis.Account.Controllers.Models.Account;

namespace FinanceTracker.Apis.Account.UnitTests.Controllers.AccountControllerTests
{
    public class PostTests
    {
        private readonly WebModel _goodData;
        private const long Id = 91234122;
        private const string Description = "Description";
        private const string Name = "Name";
        private const decimal Amount = (decimal) 562.33;

        public PostTests()
        {
            _goodData = new WebModel
            {
                Id = Id.ToString(),
                Balance = Amount,
                Name = Name,
                Description = Description
            };
        }

        [Fact]
        public async Task DataSent_CallsCreateOrUpdate()
        {
            var serviceMock = new Mock<IAccountService>();
            serviceMock.Setup(s => s.CreateOrUpdate(It.IsAny<ServiceModel>())).ReturnsAsync(new ServiceModel());
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .With<IAccountService>(serviceMock.Object)
                .Build();

            await unitUnderTest.Post(_goodData);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(s => s.Id == Id)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(s => s.Balance == Amount)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(s => s.Name == Name)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(s => s.Description == Description)));
        }
    }
}
