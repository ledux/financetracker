﻿using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Controllers;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;
using WebModel = FinanceTracker.Apis.Account.Controllers.Models.Account;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Controllers.AccountControllerTests
{
    public class GetTests
    {
        [Fact]
        public async Task ServiceReturnsEmpty_ReturnsNotFound()
        {
            var serviceMock = new Mock<IAccountService>();
            serviceMock.Setup(s => s.GetAll()).ReturnsAsync(Enumerable.Empty<ServiceModel>());
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .With<IAccountService>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.Get();

            Assert.IsType<NotFoundResult>(actual);
        }

        [Fact]
        public async Task ServiceReturnsAccounts__ReturnsOk()
        {
            var entityOne = new ServiceModel
            {
                Id = 2341234,
                Balance = (decimal) 35.5,
                Name = "Name one",
                Description = "Description one"
            };
            var entityTwo = new ServiceModel
            {
                Id = 9912341,
                Balance = (decimal) 224.3,
                Name = "Name two",
                Description = "Description two"
            };
            var entityThree = new ServiceModel
            {
                Id = 5521234,
                Balance = (decimal) 241.8,
                Name = "Name three",
                Description = "Description three"
            };

            var serviceMock = new Mock<IAccountService>();
            serviceMock.Setup(s => s.GetAll()).ReturnsAsync(new []{entityOne, entityTwo, entityThree});
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .With<IAccountService>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.Get();

            Assert.IsType<OkObjectResult>(actual);

            Assert.IsType<WebModel[]>(((OkObjectResult)actual).Value);
        }
    }
}
