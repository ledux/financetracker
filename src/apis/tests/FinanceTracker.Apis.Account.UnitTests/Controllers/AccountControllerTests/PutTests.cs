﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Controllers;
using FinanceTracker.Apis.Account.Controllers.Models;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;

namespace FinanceTracker.Apis.Account.UnitTests.Controllers.AccountControllerTests
{
    public class PutTests
    {
        private readonly PutAccount _goodData;
        private const long Id = 91234122;
        private const string Description = "Description";
        private const string Name = "Name";

        public PutTests()
        {
            _goodData = new PutAccount
            {
                Name = Name,
                Description = Description
            };
        }

        [Fact]
        public async Task AccountIsNull_ReturnsBadRequest()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>().Build();

            IActionResult actual = await unitUnderTest.Put(Id, null);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdInPathIsZero__ReturnsBadRequest()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>().Build();

            IActionResult actual = await unitUnderTest.Put(0, _goodData);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task EntityIdIsZero_IdIsNotZero_CallsServiceWithIdFromPath()
        {
            (AccountController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountController>()
                .BuildBoth();
            Mock<IAccountService> serviceMock = mockBag.GetMock<IAccountService>();

            await unitUnderTest.Put(Id, _goodData);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(a => a.Id == Id)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(a => a.Description == _goodData.Description)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(a => a.Name == _goodData.Name)));
        }

        [Fact]
        public async Task EntityIsNotNull_IdIsNotZero_ReturnsNoContent()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>().Build();

            IActionResult actual = await unitUnderTest.Put(Id, _goodData);

            Assert.IsType<NoContentResult>(actual);
        }

        [Fact]
        public async Task EntityIdIsTheSameAsIdInPath__CallsServiceWithId()
        {
            (AccountController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountController>()
                .BuildBoth();
            Mock<IAccountService> serviceMock = mockBag.GetMock<IAccountService>();

            await unitUnderTest.Put(Id, _goodData);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(a => a.Description == _goodData.Description)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(a => a.Name == _goodData.Name)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(a => a.Id == Id)));
        }
    }
}
