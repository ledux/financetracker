﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Controllers;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;
using WebModel = FinanceTracker.Apis.Account.Controllers.Models.Account;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Controllers.AccountControllerTests
{
    public class GetByIdTests
    {
        private const string Description = "Description";
        private const string Name = "Name";
        private const decimal Amount = (decimal) 552.3;
        private const long Id = 3513412;

        [Fact]
        public async Task ServiceReturnsNull_ReturnsNotFound()
        {
            var serviceMock = new Mock<IAccountService>();
            serviceMock
                .Setup(s => s.GetById(It.IsAny<long>()))
                .ReturnsAsync((ServiceModel) null);

            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .With<IAccountService>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.GetById(Id);

            Assert.IsType<NotFoundResult>(actual);
        }

        [Fact]
        public async Task ServiceReturnsAccount__ReturnsAccount()
        {
            var data = new ServiceModel
            {
                Id = Id,
                Balance = Amount,
                Name = Name,
                Description = Description
            };

            var serviceMock = new Mock<IAccountService>();
            serviceMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(data);

            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .With<IAccountService>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.GetById(Id);

            Assert.IsType<OkObjectResult>(actual);
            object account = ((OkObjectResult) actual).Value;
            Assert.IsType<WebModel>(account);
        }

        [Fact]
        public async Task IdIsZero__ReturnsBadRequest()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .Build();

            IActionResult actual = await unitUnderTest.GetById(0);

            Assert.IsType<BadRequestObjectResult>(actual);
        }
    }
}
