﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.Models;
using FinanceTracker.Apis.Account.Services.ProjectionHostServices;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ProjectionAccountingServiceTests
{
    public class ChangeBalanceTests
    {
        private const long AccountId = 2256233;
        private const decimal Amount = (decimal) 32.3;
        private const long AccountingEntryId = 4662123;

        [Fact]
        public async Task AccountExists_CallsUpdateBalance()
        {
            (ProjectionAccountingService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingService>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountRepository> repoMock = mockBag.GetMock<IAccountRepository>();

            await unitUnderTest.ChangeBalance(AccountId, Amount);

            repoMock.Verify(r => r.UpdateBalance(AccountId, Amount));
        }

        #region Overload with AccountingEntry

        [Theory]
        [InlineData(22.55)]
        [InlineData(162.99)]
        [InlineData(-22.55)]
        public async Task NoAccountingEntryInRepo__CallsUpdateBalanceWithAmount(decimal amount)
        {
            (ProjectionAccountingService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingService>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountRepository> repoMock = mockBag.GetMock<IAccountRepository>();

            var data = new AccountingEntry
            {
                AccountingEntryId = AccountingEntryId,
                AccountId = AccountId,
                Amount = amount
            };

            await unitUnderTest.ChangeBalance(data);

            repoMock.Verify(r => r.UpdateBalance(AccountId, amount));
        }

        [Theory]
        [InlineData(98.11)]
        [InlineData(198.11)]
        [InlineData(-98.11)]
        [InlineData(-198.11)]
        public async Task AccountingEntryInRepo__CallsUpdateBalanceWithDifference(decimal amount)
        {
            var accountingEntryRepoMock = new Mock<IAccountingEntryRepository>();
            var repoModel = new Repositories.Models.AccountingEntry { Amount = 100 };
            accountingEntryRepoMock
                .Setup(r => r.GetById(It.IsAny<long>()))
                .ReturnsAsync(repoModel);
            (ProjectionAccountingService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingService>()
                    .With<IAccountingEntryRepository>(accountingEntryRepoMock.Object)
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountRepository> repoMock = mockBag.GetMock<IAccountRepository>();

            var data = new AccountingEntry
            {
                AccountingEntryId = AccountingEntryId,
                AccountId = AccountId,
                Amount = amount
            };

            await unitUnderTest.ChangeBalance(data);

            repoMock.Verify(r => r.UpdateBalance(AccountId, amount - 100));
        }

        #endregion

    }
}
