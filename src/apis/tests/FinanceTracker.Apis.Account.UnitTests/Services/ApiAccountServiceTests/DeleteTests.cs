﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Producers;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ApiAccountServiceTests
{
    public class DeleteTests
    {
        [Fact]
        public async Task IdNotZero_SendsIdToProducer()
        {
            const long id = 3552123413;
            (ApiAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountProducer> producerMock = mockBag.GetMock<IAccountProducer>();

            await unitUnderTest.Delete(id);

            producerMock.Verify(p => p.AccountDeleted(id));
        }
    }
}
