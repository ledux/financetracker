﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Producers;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.Account;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ApiAccountServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly long _id = 99313471;
        private readonly decimal _amount = (decimal)22.3;
        private readonly string _description = "Description";
        private readonly string _name = "name of account";
        private readonly ServiceModel _goodAccountData;

        public CreateOrUpdateTests()
        {
            _goodAccountData = new ServiceModel
            {
                Balance = _amount,
                Description = _description,
                Name = _name,
            };
        }

        [Fact]
        public async Task AccountIsNull_ReturnsNull()
        {
            ApiAccountService unitUnderTest = new UnitUnderTestBuilder<ApiAccountService>()
                .AddTracingMocks()
                .Build();

            Account.Services.Models.Account actual = await unitUnderTest.CreateOrUpdate(null);

            Assert.Null(actual);
        }

        [Fact]
        public async Task AccountIsNotNull__IdIsSet()
        {
            var idCreatorMock = new Mock<IIdCreator>();
            idCreatorMock.Setup(i => i.CreateId()).Returns(_id);
            ApiAccountService unitUnderTest = new UnitUnderTestBuilder<ApiAccountService>()
                .With<IIdCreator>(idCreatorMock.Object)
                .AddTracingMocks()
                .Build();

            Account.Services.Models.Account actual = await unitUnderTest.CreateOrUpdate(new Account.Services.Models.Account());

            Assert.Equal(_id, actual.Id);
        }

        [Fact]
        public async Task PropertiesAreSet__SendsDataToTheProducer()
        {
            var idCreatorMock = new Mock<IIdCreator>();
            idCreatorMock.Setup(i => i.CreateId()).Returns(_id);
            (ApiAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountService>()
                .With<IIdCreator>(idCreatorMock.Object)
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountProducer> producerMock = mockBag.GetMock<IAccountProducer>();

            await unitUnderTest.CreateOrUpdate(_goodAccountData);

            producerMock.Verify(p => p.AccountUpdated(It.Is<ServiceModel>(a => a.Id == _id)));
            producerMock.Verify(p => p.AccountUpdated(It.Is<ServiceModel>(a => a.Balance == _amount)));
            producerMock.Verify(p => p.AccountUpdated(It.Is<ServiceModel>(a => a.Description == _description)));
            producerMock.Verify(p => p.AccountUpdated(It.Is<ServiceModel>(a => a.Name == _name)));
        }

        [Fact]
        public async Task RepoReturnsNotNull__SendsDataWithExistingIdToProducer()
        {
            var repoMock = new Mock<IAccountRepository>();
            const long existingId = 123412341234;
            repoMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(new RepoModel { Id = existingId });

            (ApiAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountService>()
                .AddTracingMocks()
                .With<IAccountRepository>(repoMock.Object)
                .BuildBoth();
            Mock<IAccountProducer> producerMock = mockBag.GetMock<IAccountProducer>();

            await unitUnderTest.CreateOrUpdate(_goodAccountData);

            producerMock.Verify(p => p.AccountUpdated(It.Is<ServiceModel>(a => a.Id == existingId)));
        }

        [Theory]
        [InlineData(22.55)]
        public async Task RepoReturnsNotNull__SendsTheAmountFromExistingToProducer(decimal amount)
        {
            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(new RepoModel {Balance = amount });
            (ApiAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountService>()
                .AddTracingMocks()
                .With<IAccountRepository>(repoMock.Object)
                .BuildBoth();
            Mock<IAccountProducer> producerMock = mockBag.GetMock<IAccountProducer>();

            await unitUnderTest.CreateOrUpdate(_goodAccountData);

            producerMock.Verify(p => p.AccountUpdated(It.Is<ServiceModel>(a => a.Balance == amount)));
        }
    }
}
