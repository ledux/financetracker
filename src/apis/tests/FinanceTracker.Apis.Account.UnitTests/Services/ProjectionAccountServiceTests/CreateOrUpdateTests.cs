﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.ProjectionHostServices;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ProjectionAccountServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly Account.Services.Models.Account _account;
        private readonly long _id = 3312344;
        private readonly string _name = "name";
        private readonly decimal _amount = (decimal) 35.3;
        private readonly string _description = "description";

        public CreateOrUpdateTests()
        {
            _account = new Account.Services.Models.Account
            {
                Id = _id,
                Name = _name,
                Balance = _amount,
                Description = _description
            };
        }

        [Fact]
        public async Task RepoReturnsNull_ReturnsNull()
        {
            ProjectionAccountService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountService>()
                .AddTracingMocks()
                .Build();

            Account.Services.Models.Account actual = await unitUnderTest.CreateOrUpdate(_account);

            Assert.Null(actual);
        }

        [Fact]
        public async Task PropertiesAreSet__CallsUpsertWithData()
        {
            (ProjectionAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ProjectionAccountService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountRepository> repoMock = mockBag.GetMock<IAccountRepository>();

            await unitUnderTest.CreateOrUpdate(_account);

            repoMock.Verify(r => r.Upsert(It.Is<Repositories.Models.Account>(a => a.Id == _id)));
            repoMock.Verify(r => r.Upsert(It.Is<Repositories.Models.Account>(a => a.Balance == _amount)));
            repoMock.Verify(r => r.Upsert(It.Is<Repositories.Models.Account>(a => a.Name == _name)));
            repoMock.Verify(r => r.Upsert(It.Is<Repositories.Models.Account>(a => a.Description == _description)));
        }

        [Fact]
        public async Task PropertiesAreSet__ReturnsTheDataFromRepo()
        {
            const long dbId = 33989212;
            const string dbName = "db name";
            const decimal dbAmount = (decimal) 42.56;
            const string dbDescription = "db description";

            var dbModel = new Repositories.Models.Account
            {
                Id = dbId,
                Name = dbName,
                Balance = dbAmount,
                Description = dbDescription
            };

            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.Upsert(It.IsAny<Repositories.Models.Account>())).ReturnsAsync(dbModel);

            ProjectionAccountService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountService>()
                .With<IAccountRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            Account.Services.Models.Account actual = await unitUnderTest.CreateOrUpdate(_account);

            Assert.Equal(dbId, actual.Id);
            Assert.Equal(dbName, actual.Name);
            Assert.Equal(dbAmount, actual.Balance);
            Assert.Equal(dbDescription, actual.Description);
        }
    }
}
