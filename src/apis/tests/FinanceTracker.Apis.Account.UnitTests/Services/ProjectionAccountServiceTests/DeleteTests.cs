﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.ProjectionHostServices;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.Account;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ProjectionAccountServiceTests
{
    public class DeleteTests
    {
        private const long Id = 22315123;

        [Fact]
        public async Task AccountingEntryServiceReturnsZero_CallsDeleteOnRepo()
        {
            var entryRepoMock = new Mock<IAccountingEntryRepository>();
            entryRepoMock.Setup(r => r.CountEntriesByAccount(It.IsAny<long>())).ReturnsAsync(0);
            (ProjectionAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ProjectionAccountService>()
                .AddTracingMocks()
                .With<IAccountingEntryRepository>(entryRepoMock.Object)
                .BuildBoth();
            Mock<IAccountRepository> repoMock = mockBag.GetMock<IAccountRepository>();

            await unitUnderTest.Delete(Id);

            repoMock.Verify(r => r.Delete(Id));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(3)]
        [InlineData(12134)]
        [InlineData(134)]
        public async Task AccountingEntryRepoReturnsCountBiggerThanZero__CallsUpsertWithDeletedTrue(int count)
        {
            var entryRepoMock = new Mock<IAccountingEntryRepository>();
            entryRepoMock.Setup(r => r.CountEntriesByAccount(It.IsAny<long>())).ReturnsAsync(count);
            (ProjectionAccountService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ProjectionAccountService>()
                .AddTracingMocks()
                .With<IAccountingEntryRepository>(entryRepoMock.Object)
                .BuildBoth();
            Mock<IAccountRepository> repoMock = mockBag.GetMock<IAccountRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(new RepoModel());

            await unitUnderTest.Delete(Id);

            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(a => a.IsDeleted)));
        }
    }
}
