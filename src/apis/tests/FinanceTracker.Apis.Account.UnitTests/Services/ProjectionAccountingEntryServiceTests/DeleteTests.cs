﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Apis.Account.Services.ProjectionHostServices;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using DbAccountingEntry = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;
using ServiceAccountingEntry = FinanceTracker.Apis.Account.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ProjectionAccountingEntryServiceTests
{
    public class DeleteTests
    {
        private const decimal Amount = (decimal) 55.2;
        private const long DbAccountId = 3123112344;
        private const long DbAccountingEntryId = 989812341;
        private const long Id = 12341234123;

        [Fact]
        public async Task IdIsNotZero_CallsDeleteOfRepo()
        {
            (ProjectionAccountingEntryService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            await unitUnderTest.Delete(Id);

            repoMock.Verify(r => r.Delete(Id));
        }

        [Fact]
        public async Task IdIsNotZero_RepoReturnsData_ReturnsData()
        {
            var dbData = new DbAccountingEntry
            {
                AccountingEntryId = DbAccountingEntryId,
                AccountId = DbAccountId,
            };
            var repoMock = new Mock<IAccountingEntryRepository>();
            repoMock.Setup(r => r.Delete(It.IsAny<long>())).ReturnsAsync(dbData);
            ProjectionAccountingEntryService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .With<IAccountingEntryRepository>(repoMock.Object)
                .Build();

            ServiceAccountingEntry actual = await unitUnderTest.Delete(Id);

            Assert.Equal(DbAccountId, actual.AccountId);
            Assert.Equal(DbAccountingEntryId, actual.AccountingEntryId);
        }

        [Fact]
        public async Task IdIsNotZero_RepoReturnsNull_ReturnsNull()
        {
            ProjectionAccountingEntryService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .Build();

            ServiceAccountingEntry actual = await unitUnderTest.Delete(Id);

            Assert.Null(actual);
        }

        [Fact]
        public async Task IdIsNotZero_RepoReturnsNotNull_CallsChangeBalance()
        {
            var dbData = new DbAccountingEntry
            {
                AccountingEntryId = DbAccountingEntryId,
                AccountId = DbAccountId,
                Amount = Amount
            };
            var repoMock = new Mock<IAccountingEntryRepository>();
            repoMock.Setup(r => r.Delete(It.IsAny<long>())).ReturnsAsync(dbData);
            (ProjectionAccountingEntryService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .With<IAccountingEntryRepository>(repoMock.Object)
                .BuildBoth();
            Mock<IAccountingService> accountingServiceMock = mockBag.GetMock<IAccountingService>();

            await unitUnderTest.Delete(Id);

            accountingServiceMock.Verify(s => s.ChangeBalance(DbAccountId, Amount * -1));
        }
    }
}
