﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services.ProjectionHostServices;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.AccountingEntry;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Account.UnitTests.Services.ProjectionAccountingEntryServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly ServiceModel _data;
        private readonly RepoModel _repoData;
        private const decimal Amount = (decimal) 66.22;
        private const long RepoAccountId = 8977799;
        private const long RepoAccountingEntryId = 12341234;
        private const long AccountId = 90981234;
        private const long AccountingEntryId = 32121234;

        public CreateOrUpdateTests()
        {
            _data = new ServiceModel
            {
                AccountingEntryId = AccountingEntryId,
                AccountId = AccountId,
                Amount = Amount
            };
            _repoData = new RepoModel
            {
                AccountingEntryId = RepoAccountingEntryId,
                AccountId = RepoAccountId,
                Amount = Amount
            };
        }

        [Fact]
        public async Task DataIsNull_Throws()
        {
            ProjectionAccountingEntryService unitUnderTests = new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .Build();

            await Assert.ThrowsAsync<ArgumentNullException>(() => unitUnderTests.CreateOrUpdate(null));
        }

        [Fact]
        public async Task DataIsNotNull__CallsUpsert()
        {
            (ProjectionAccountingEntryService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            await unitUnderTest.CreateOrUpdate(_data);

            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(a => a.AccountId == AccountId)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(a => a.AccountingEntryId == AccountingEntryId)));
        }

        [Fact]
        public async Task DataIsNotNull_RepoReturnsNull_ReturnsNull()
        {
            ProjectionAccountingEntryService unitUnderTests = new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .Build();

            ServiceModel actual = await unitUnderTests.CreateOrUpdate(_data);

            Assert.Null(actual);
        }

        [Fact]
        public async Task DataIsNotNull_RepoReturnsData_ReturnsData()
        {
            (ProjectionAccountingEntryService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();
            repoMock.Setup(r => r.Upsert(It.IsAny<RepoModel>())).ReturnsAsync(_repoData);

            ServiceModel actual = await unitUnderTest.CreateOrUpdate(_data);

            Assert.Equal(RepoAccountId, actual.AccountId);
            Assert.Equal(RepoAccountingEntryId, actual.AccountingEntryId);
        }

        [Fact]
        public async Task AmountIsSet__SendsAmountToRepo()
        {
            (ProjectionAccountingEntryService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingEntryService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            await unitUnderTest.CreateOrUpdate(_data);

            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(a => a.Amount == Amount)));
        }
    }
}
