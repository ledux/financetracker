﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.Account;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;

namespace FinanceTracker.Apis.Account.UnitTests.Services.BaseAccountServiceTests
{
    public class GetByIdTests
    {
        private const string Description = "Description";
        private const string Name = "Name of account";
        private const decimal Amount = (decimal) 35.1;
        private const long Id = 98123412;

        [Fact]
        public async Task RepoReturnsNull_ReturnsNull()
        {
            var repoMock = new Mock<IAccountRepository>();
            repoMock
                .Setup(r => r.GetById(It.IsAny<long>()))
                .ReturnsAsync((RepoModel) null);

            TestUnitBaseAccountService unitUnderTest = new UnitUnderTestBuilder<TestUnitBaseAccountService>()
                .With<IAccountRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            ServiceModel actual = await unitUnderTest.GetById(Id);

            Assert.Null(actual);
        }

        [Fact]
        public async Task RepoReturnsData__ReturnsSameData()
        {
            var repoData = new RepoModel
            {
                Id = Id,
                Balance = Amount,
                Name = Name,
                Description = Description
            };
            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(repoData);

            TestUnitBaseAccountService unitUnderTest = new UnitUnderTestBuilder<TestUnitBaseAccountService>()
                .With<IAccountRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            ServiceModel actual = await unitUnderTest.GetById(Id);

            Assert.Equal(Description, actual.Description);
            Assert.Equal(Name, actual.Name);
            Assert.Equal(Amount, actual.Balance);
            Assert.Equal(Id, actual.Id);
        }

        [Fact]
        public async Task RepoReturnsAccountWithIsDeletedTrue__ReturnsNull()
        {
            var repoData = new RepoModel
            {
                Id = Id,
                Balance = Amount,
                Name = Name,
                Description = Description,
                IsDeleted = true
            };
            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(repoData);

            TestUnitBaseAccountService unitUnderTest = new UnitUnderTestBuilder<TestUnitBaseAccountService>()
                .With<IAccountRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            ServiceModel actual = await unitUnderTest.GetById(Id);

            Assert.Null(actual);
        }
    }
}
