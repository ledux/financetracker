﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Apis.Account.Services;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Account.UnitTests.Services.BaseAccountServiceTests
{
    internal class TestUnitBaseAccountService : BaseAccountService
    {
        public TestUnitBaseAccountService(IAccountRepository accountRepository, ITracer tracer) 
            : base(accountRepository, tracer) { }

        public override Task<Account.Services.Models.Account> CreateOrUpdate(Account.Services.Models.Account account)
        {
            throw new NotSupportedException("This class is for unit testing purposes only");
        }

        public override Task Delete(long id)
        {
            throw new NotSupportedException("This class is for unit testing purposes only");
        }
    }
}
