﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Account.Repositories;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using RepoModel = FinanceTracker.Apis.Account.Repositories.Models.Account;
using ServiceModel = FinanceTracker.Apis.Account.Services.Models.Account;

namespace FinanceTracker.Apis.Account.UnitTests.Services.BaseAccountServiceTests
{
    public class GetAllTests
    {
        [Fact]
        public async Task RepoReturnsEmpty__ReturnsEmpty()
        {
            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.GetAll()).ReturnsAsync(Enumerable.Empty<RepoModel>());

            TestUnitBaseAccountService unitUnderTest = new UnitUnderTestBuilder<TestUnitBaseAccountService>()
                .With<IAccountRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            IEnumerable<ServiceModel> actual = await unitUnderTest.GetAll();

            Assert.NotNull(actual);
            Assert.Empty(actual);
        }

        [Fact]
        public async Task RepoReturnsData__ReturnsSameData()
        {
            var entityOne = new RepoModel
            {
                Id = 2341234,
                Balance = (decimal) 35.5,
                Name = "Name one",
                Description = "Description one"
            };
            var entityTwo = new RepoModel
            {
                Id = 9912341,
                Balance = (decimal) 224.3,
                Name = "Name two",
                Description = "Description two"
            };
            var entityThree = new RepoModel
            {
                Id = 5521234,
                Balance = (decimal) 241.8,
                Name = "Name three",
                Description = "Description three"
            };

            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.GetAll()).ReturnsAsync(new List<RepoModel>{entityOne, entityTwo, entityThree});

            TestUnitBaseAccountService unitUnderTest = new UnitUnderTestBuilder<TestUnitBaseAccountService>()
                .With<IAccountRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            IEnumerable<ServiceModel> actual = await unitUnderTest.GetAll();

            Assert.NotNull(actual);
            Assert.Collection(actual, 
                account =>
                {
                    Assert.Equal(entityOne.Id, account.Id);
                    Assert.Equal(entityOne.Balance, account.Balance);
                    Assert.Equal(entityOne.Name, account.Name);
                    Assert.Equal(entityOne.Description, account.Description);
                },
                account =>
                {
                    Assert.Equal(entityTwo.Id, account.Id);
                    Assert.Equal(entityTwo.Balance, account.Balance);
                    Assert.Equal(entityTwo.Name, account.Name);
                    Assert.Equal(entityTwo.Description, account.Description);
                },
                account =>
                {
                    Assert.Equal(entityThree.Id, account.Id);
                    Assert.Equal(entityThree.Balance, account.Balance);
                    Assert.Equal(entityThree.Name, account.Name);
                    Assert.Equal(entityThree.Description, account.Description);
                }
                );
        }

        [Fact]
        public async Task IncludeDeletedFalse__AccountsWithDeletedTrueAreFilteredOut()
        {
            const int idOfDeleted = 2341234;
            var entityOne = new RepoModel
            {
                Id = idOfDeleted,
                Balance = (decimal) 35.5,
                Name = "Deleted one",
                Description = "Description one",
                IsDeleted = true
            };
            var entityTwo = new RepoModel
            {
                Id = 9912341,
                Balance = (decimal) 224.3,
                Name = "Name two",
                Description = "Description two"
            };
            var entityThree = new RepoModel
            {
                Id = 5521234,
                Balance = (decimal) 241.8,
                Name = "Name three",
                Description = "Description three"
            };

            var repoMock = new Mock<IAccountRepository>();
            repoMock.Setup(r => r.GetAll()).ReturnsAsync(new List<RepoModel>{entityOne, entityTwo, entityThree});

            TestUnitBaseAccountService unitUnderTest = new UnitUnderTestBuilder<TestUnitBaseAccountService>()
                .AddTracingMocks()
                .With<IAccountRepository>(repoMock.Object)
                .Build();

            IEnumerable<ServiceModel> actual = await unitUnderTest.GetAll();

            Assert.All(actual, account => Assert.NotEqual(idOfDeleted, account.Id));
        }
    }
}
