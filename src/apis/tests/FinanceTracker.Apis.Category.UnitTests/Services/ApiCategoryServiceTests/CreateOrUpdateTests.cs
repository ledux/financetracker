﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Producers;
using FinanceTracker.Apis.Category.Services;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Category.UnitTests.Services.ApiCategoryServiceTests
{
    public class CreateOrUpdateTests
    {
        [Fact]
        public async Task CategoryIsNull_ReturnsNull()
        {
            ApiCategoryService unitUnderTest = new UnitUnderTestBuilder<ApiCategoryService>()
                .AddTracingMocks()
                .Build();

            AccountingCategory actual = await unitUnderTest.CreateOrUpdate(null);

            Assert.Null(actual);
        }

        [Fact]
        public async Task CategoryIsNotNull__ReturnValueHasIdSet()
        {
            const long id = 129389482;
            var idCreatorMock = new Mock<IIdCreator>();
            idCreatorMock.Setup(c => c.CreateId()).Returns(id);
            ApiCategoryService unitUnderTest = new UnitUnderTestBuilder<ApiCategoryService>()
                .With<IIdCreator>(idCreatorMock.Object)
                .AddTracingMocks()
                .Build();

            AccountingCategory actual = await unitUnderTest.CreateOrUpdate(new AccountingCategory());

            Assert.Equal(id, actual.Id);
        }

        [Fact]
        public async Task PropertiesAreSet__CallsCategoryUpdatedWithTheData()
        {
            var category = new AccountingCategory { Name = "name" };
            (ApiCategoryService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiCategoryService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<ICategoryProducer> producerMock = mockBag.GetMock<ICategoryProducer>();

            await unitUnderTest.CreateOrUpdate(category);

            producerMock.Verify(p => p.CategoryUpdated(category));
        }
    }
}
