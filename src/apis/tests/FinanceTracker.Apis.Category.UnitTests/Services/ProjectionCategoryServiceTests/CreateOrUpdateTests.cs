﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Repositories;
using FinanceTracker.Apis.Category.Services;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using DbModel = FinanceTracker.Apis.Category.Repositories.Models.AccountingCategory;

namespace FinanceTracker.Apis.Category.UnitTests.Services.ProjectionCategoryServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly long _id = 33999134;
        private readonly string _name  = "name";
        private readonly long _parentId = 93939912344;
        private readonly string _parentName = "parent name";

        [Fact]
        public async Task ParentIsNull_ParentIdIs0()
        {
            var repoMock = new Mock<ICategoryRepository>();
            repoMock.Setup(r => r.Upsert(It.IsAny<DbModel>())).ReturnsAsync(new DbModel { Id = _id, Name = _name });
            ProjectionCategoryService unitUnderTest = new UnitUnderTestBuilder<ProjectionCategoryService>()
                .With<ICategoryRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();
            var category = new AccountingCategory
            {
                Id = _id,
                Name = _name
            };

            await unitUnderTest.CreateOrUpdate(category);

            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(c => c.ParentId == 0)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(c => c.Id == _id)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(c => c.Name == _name)));
        }

        [Fact]
        public async Task ParentIsNotNull__ParentIdIsIdOfParent()
        {
            var repoMock = new Mock<ICategoryRepository>();
            repoMock.Setup(r => r.Upsert(It.IsAny<DbModel>())).ReturnsAsync(new DbModel { Id = _id, Name = _name });
            ProjectionCategoryService unitUnderTest = new UnitUnderTestBuilder<ProjectionCategoryService>()
                .With<ICategoryRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();
            var category = new AccountingCategory
            {
                Id = _id,
                Name = _name,
                Parent = new AccountingCategory
                {
                    Id = _parentId,
                    Name = _parentName
                }
            };

            await unitUnderTest.CreateOrUpdate(category);

            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(c => c.ParentId == _parentId)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(c => c.Id == _id)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(c => c.Name == _name)));
            
        }
    }
}
