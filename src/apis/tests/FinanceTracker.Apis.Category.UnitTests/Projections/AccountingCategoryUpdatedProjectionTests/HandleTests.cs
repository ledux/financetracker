﻿
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Projections;
using FinanceTracker.Apis.Category.Services;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Category.UnitTests.Projections.AccountingCategoryUpdatedProjectionTests

{
    public class HandleTests
    {
        private readonly long _id = 3352123;
        private readonly string _name = "name";
        private readonly string _parentName = "parent name";
        private readonly long _parentId =299898131;

        public HandleTests()
        {
        }

        [Fact]
        public async Task ParentIdIs0_ParentObjectIsNull()
        {
            (AccountingCategoryUpdatedProjection unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<AccountingCategoryUpdatedProjection>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<ICategoryService> serviceMock = mockBag.GetMock<ICategoryService>();
            var updatedEvent = new AccountingCategoryUpdated
            {
                Id = _id,
                Name = _name,
            };

            await unitUnderTest.Handle(updatedEvent);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Parent == null)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Id == _id)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Name == _name)));
        }

        [Fact]
        public async Task ParentIdIsNot0__ParentIsNotNullHasValues()
        {
            (AccountingCategoryUpdatedProjection unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<AccountingCategoryUpdatedProjection>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<ICategoryService> serviceMock = mockBag.GetMock<ICategoryService>();
            var updatedEvent = new AccountingCategoryUpdated
            {
                Id = _id,
                Name = _name,
                ParentName = _parentName,
                ParentId = _parentId
            };

            await unitUnderTest.Handle(updatedEvent);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Parent.Name == _parentName)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Parent.Id == _parentId)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Id == _id)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingCategory>(c => c.Name == _name)));
            
        }
    }
}
