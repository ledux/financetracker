﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Category.Producers;
using FinanceTracker.Apis.Category.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Category.UnitTests.Producers.CategoryProducerTests
{
    public class CategoryUpdatedTests
    {
        private readonly long _id = 939895134;
        private readonly string _name = "category name";
        private readonly long _parentId = 99923221144;
        private readonly string _parentName = "name of the parent";

        [Fact]
        public async Task IdIsZero_Throws()
        {
            CategoryProducer unitUnderTest = new UnitUnderTestBuilder<CategoryProducer>().Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.CategoryUpdated(new AccountingCategory()));
        }

        [Fact]
        public async Task PropertiesAreSet_NoParent_SendsNoParentDataToEventClient()
        {
            var category = new AccountingCategory
            {
                Id = _id,
                Name = _name,
            };

            (CategoryProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<CategoryProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> eventClientMock = mockBag.GetMock<IEventClient>();

            await unitUnderTest.CategoryUpdated(category);

            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.Id == _id)));
            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.Name == _name)));
            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.ParentId == 0)));
            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.ParentName == null)));
        }

        [Fact]
        public async Task ParentIsNotNull__SendsTheParentDataToEventClient()
        {
            var category = new AccountingCategory
            {
                Id = _id,
                Name = _name,
                Parent = new AccountingCategory
                {
                    Id = _parentId,
                    Name = _parentName
                }
            };
            
            (CategoryProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<CategoryProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> eventClientMock = mockBag.GetMock<IEventClient>();

            await unitUnderTest.CategoryUpdated(category);

            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.Id == _id)));
            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.Name == _name)));
            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.ParentId ==_parentId)));
            eventClientMock.Verify(client => client.Publish(It.Is<AccountingCategoryUpdated>(c => c.ParentName == _parentName)));
        }
    }
}
