﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Controllers;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.UnitTests.Extensions;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Controllers.AccountControllerTests
{
    public class GetByAccountIdTests
    {
        private const long AccountId = 35511344;

        [Fact]
        public async Task AccountIdIsZero_ReturnsBadRequest()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>().Build();

            IActionResult actual = await unitUnderTest.GetByAccount(0);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task AccountIdIsNotZero__CallsServiceWithSameId()
        {
            (AccountController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountController>()
                .BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();

            await unitUnderTest.GetByAccount(AccountId);

            serviceMock.Verify(s => s.GetByAccountId(AccountId));
        }

        [Fact]
        public async Task ServiceReturnsEmpty__ReturnsNotFound()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .AddAccountingService(0)
                .Build();

            IActionResult actual = await unitUnderTest.GetByAccount(AccountId);

            Assert.IsType<NotFoundResult>(actual);
        }

        [Fact]
        public async Task ServiceReturnsData__ReturnsSameData()
        {
            AccountController unitUnderTest = new UnitUnderTestBuilder<AccountController>()
                .AddAccountingService(5)
                .Build();

            IActionResult actual = await unitUnderTest.GetByAccount(AccountId);

            Assert.IsType<OkObjectResult>(actual);
            var data = ((OkObjectResult) actual).Value as IEnumerable<WebModel>;

            Assert.Collection(data ?? throw new InvalidOperationException(), 
                entry => Assert.Equal("0", entry.Id),
                entry => Assert.Equal("1", entry.Id),
                entry => Assert.Equal("2", entry.Id),
                entry => Assert.Equal("3", entry.Id),
                entry => Assert.Equal("4", entry.Id)
                );

        }
    }
}
