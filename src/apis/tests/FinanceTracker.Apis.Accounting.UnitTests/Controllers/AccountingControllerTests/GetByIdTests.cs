﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Controllers;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Controllers.AccountingControllerTests
{
    public class GetByIdTests
    {
        private readonly long _accountId = 33989214 ;
        private readonly int _amount = 42;
        private readonly DateTime _bookingDate = new DateTime(2020, 8, 20);
        private readonly long _id = 33123443;
        private readonly long _categoryId = 22351234;

        [Fact]
        public async Task ServiceReturnsNull_ReturnsNotFound()
        {
            var serviceMock = new Mock<IAccountingService>();
            serviceMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync((AccountingEntry) null);
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .With<IAccountingService>(serviceMock.Object)
                .Build();

            IActionResult actionResult = await unitUnderTest.GetById(33333);

            Assert.IsType<NotFoundResult>(actionResult);
        }

        [Fact]
        public async Task ServiceReturnsEntry__ReturnsOkWithData()
        {
            var entry = new AccountingEntry
            {
                Id = _id,
                Amount = _amount,
                AccountId = _accountId,
                BookingDate = _bookingDate,
                CategoryId = _categoryId,
            };

            var serviceMock = new Mock<IAccountingService>();
            serviceMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(entry);
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .With<IAccountingService>(serviceMock.Object)
                .Build();

            IActionResult actionResult = await unitUnderTest.GetById(33333);

            Assert.IsType<OkObjectResult>(actionResult);
            var webModel = (actionResult as OkObjectResult)?.Value as WebModel;
            Assert.NotNull(webModel);
            Assert.Equal(entry.Amount, webModel.Amount);
            Assert.Equal(entry.Id.ToString(), webModel.Id);
            Assert.Equal(entry.AccountId.ToString(), webModel.AccountId);
            Assert.Equal(entry.BookingDate, webModel.BookingDate);
            Assert.Equal(entry.CategoryId.ToString(), webModel.CategoryId);
        }
    }
}
