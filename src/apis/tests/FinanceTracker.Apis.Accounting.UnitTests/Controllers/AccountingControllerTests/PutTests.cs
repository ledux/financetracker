﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Controllers;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.PutModel;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Controllers.AccountingControllerTests
{
    public class PutTests
    {
        private readonly long _id = 6562341432;

        [Fact]
        public async Task EntityIsNull_ReturnsBadRequest()
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actionResult = await unitUnderTest.Put(_id, null);

            Assert.IsType<BadRequestResult>(actionResult);
        }

        [Fact]
        public async Task IdIsZero__ReturnsBadRequest()
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actionResult = await unitUnderTest.Put(0, new WebModel());

            Assert.IsType<BadRequestObjectResult>(actionResult);
            var badRequest = actionResult as BadRequestObjectResult;
            Assert.NotNull(badRequest?.Value);
            object value = badRequest.Value;
            Assert.NotNull(value?.GetType().GetProperty("errorMessage")?.GetValue(value));
        }

        [Fact]
        public async Task EntityIdIsZero_IdIsNotZero_CallsServiceWithIdFromPath()
        {
            var entry = new WebModel();
            (AccountingController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingController>().BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();

            await unitUnderTest.Put(_id, entry);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModel>(e => e.Id == _id)));
        }

        [Fact]
        public async Task EntityIsNotNull_IdIsNotZero_ReturnsNoContent()
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actionResult = await unitUnderTest.Put(_id, new WebModel());

            Assert.IsType<NoContentResult>(actionResult);
        }
    }
}
