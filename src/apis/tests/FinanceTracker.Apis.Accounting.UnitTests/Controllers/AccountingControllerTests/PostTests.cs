﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Controllers;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Controllers.AccountingControllerTests
{
    public class PostTests
    {
        private readonly long _id = 66123421234;

        [Fact]
        public async Task EntityIsNotNull_ReturnsAccepted()
        {
            var serviceMock = new Mock<IAccountingService>();
            serviceMock.Setup(s => s.CreateOrUpdate(It.IsAny<ServiceModel>()))
                .ReturnsAsync(new ServiceModel { Id = _id });
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .With<IAccountingService>(serviceMock.Object)
                .Build();

            IActionResult actionResult = await unitUnderTest.Post(new WebModel());

            Assert.IsType<AcceptedAtActionResult>(actionResult);
        }

        [Fact]
        public async Task ServiceReturnsEntityWithId__ReturnsLocationHeader()
        {
            var serviceMock = new Mock<IAccountingService>();
            serviceMock.Setup(s => s.CreateOrUpdate(It.IsAny<ServiceModel>()))
                .ReturnsAsync(new ServiceModel() { Id = _id });
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .With<IAccountingService>(serviceMock.Object)
                .Build();

            var actionResult = await unitUnderTest.Post(new WebModel()) as AcceptedAtActionResult;

            Assert.True(actionResult?.RouteValues.Contains(new KeyValuePair<string, object>("id", _id)));
        }

        [Fact]
        public async Task EntityIsNull__ReturnsBadRequest()
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .Build();

            IActionResult actionResult = await unitUnderTest.Post(null);

            Assert.IsType<BadRequestResult>(actionResult);
        }
    }
}
