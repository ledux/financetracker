﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Controllers;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Controllers.AccountingControllerTests
{
    public class DeleteTests
    {
        private readonly long _id = 113911234;

        [Fact]
        public async Task IdIsZero_ReturnsBadRequest()
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actionResult = await unitUnderTest.Delete(0);

            Assert.IsType<BadRequestResult>(actionResult);
        }

        [Fact]
        public async Task IdIsPositive__CallsServiceWithId()
        {
            (AccountingController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingController>().BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();

            IActionResult actionResult = await unitUnderTest.Delete(_id);

            Assert.IsType<NoContentResult>(actionResult);
            serviceMock.Verify(s => s.Delete(_id));
        }
    }
}
