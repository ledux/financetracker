﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Controllers;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.UnitTests.Extensions;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;
using WebModel = FinanceTracker.Apis.Accounting.Controllers.Models.AccountingEntry;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Controllers.AccountingControllerTests
{
    public class GetByDateTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(13)]
        [InlineData(20)]
        public async Task MonthIsOutOfRange_ReturnsBadRequest(int month)
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actual = await unitUnderTest.GetByDate(2020, month, 1);

            Assert.IsType<BadRequestObjectResult>(actual);
            Assert.Contains("month", ((BadRequestObjectResult) actual).Value.ToString());
            Assert.Contains(month.ToString(), ((BadRequestObjectResult) actual).Value.ToString());
        }

        [Theory]
        [InlineData(0)]
        [InlineData(32)]
        [InlineData(33)]
        public async Task DayIsOutOfRange__ReturnsBadRequest(int day)
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actual = await unitUnderTest.GetByDate(2020, 5, day);

            Assert.IsType<BadRequestObjectResult>(actual);
            Assert.Contains("day", ((BadRequestObjectResult) actual).Value.ToString());
            Assert.Contains(day.ToString(), ((BadRequestObjectResult) actual).Value.ToString());
        }

        [Theory]
        [InlineData(2020, 1, 1)]
        [InlineData(2020, 1, 31)]
        [InlineData(2020, 2, 29)]
        [InlineData(2019, 2, 28)]
        [InlineData(2020, 3, 31)]
        public async Task ParamsFormValidDate__CallsGetByDateWithTheDateTime(int  year, int month, int day)
        {
            (AccountingController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingController>()
                .BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();

            await unitUnderTest.GetByDate(year, month, day);

            serviceMock.Verify(s => s.GetByDate(It.Is<DateTimeOffset>(d => d.Year ==  year)));
            serviceMock.Verify(s => s.GetByDate(It.Is<DateTimeOffset>(d => d.Month ==  month)));
            serviceMock.Verify(s => s.GetByDate(It.Is<DateTimeOffset>(d => d.Day ==  day)));
        }

        [Fact]
        public async Task ParamsFormAValidDate__ReturnsTheReceivedEntriesAsOkay()
        {
            const short amount = 5;
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .AddAccountingService(amount)
                .Build();

            IActionResult actual = await unitUnderTest.GetByDate(2020, 5, 12);

            Assert.IsType<OkObjectResult>(actual);
            var data = ((OkObjectResult) actual).Value as IEnumerable<WebModel>;
            Assert.NotEmpty(data ?? throw new InvalidOperationException());
            Assert.Equal(amount, data.Count());
        }

        [Fact]
        public async Task ParamsFormAValidDate_ServiceReturnsEmpty_ReturnsEmpty()
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>()
                .AddAccountingService(0)
                .Build();

            IActionResult actual = await unitUnderTest.GetByDate(2020, 5, 12);

            Assert.IsType<OkObjectResult>(actual);
            var data = ((OkObjectResult) actual).Value as IEnumerable<WebModel>;
            Assert.Empty(data ?? throw new InvalidOperationException());
        }

        [Theory]
        [InlineData(2020, 1, 32)]
        [InlineData(2020, 2, 30)]
        [InlineData(2019, 2, 29)]
        [InlineData(2020, 3, 32)]
        [InlineData(2020, 4, 31)]
        [InlineData(2020, 5, 32)]
        [InlineData(2020, 6, 31)]
        [InlineData(2020, 7, 32)]
        [InlineData(2020, 8, 32)]
        [InlineData(2020, 9, 31)]
        [InlineData(2020, 10, 32)]
        [InlineData(2020, 11, 31)]
        [InlineData(2020, 12, 32)]
        public async Task DayIsTooBigForMonth__ReturnsBadRequest(int year, int month, int day)
        {
            AccountingController unitUnderTest = new UnitUnderTestBuilder<AccountingController>().Build();

            IActionResult actual = await unitUnderTest.GetByDate(year, month, day);

            Assert.IsType<BadRequestObjectResult>(actual);
        }
    }
}
