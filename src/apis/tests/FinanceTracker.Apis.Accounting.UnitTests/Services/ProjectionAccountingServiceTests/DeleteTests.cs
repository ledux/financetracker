﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.ProjectionAccountingServiceTests
{
    public class DeleteTests
    {
        [Fact]
        public async Task IdIsZero_Throws()
        {
            ProjectionAccountingService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountingService>()
                .AddTracingMocks()
                .Build();
            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.Delete(0));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(88134)]
        [InlineData(3212)]
        public async Task IdIsNotZero__CallsDeleteOnRepo(long id)
        {
            (ProjectionAccountingService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionAccountingService>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            await unitUnderTest.Delete(id);

            repoMock.Verify(r => r.Delete(id));
        }
    }
}
