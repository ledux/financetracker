﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Moq;
using Xunit;
using DbModel = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.ProjectionAccountingServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly long _accountId = 33989214 ;
        private readonly int _amount = 42;
        private readonly DateTime _bookingDate = new DateTime(2020, 8, 20);
        private readonly bool _recurring = false;
        private readonly long _id = 299389433;
        private readonly long _categoryId = 333335563;
        private readonly AccountingEntry _serviceModel;

        public CreateOrUpdateTests()
        {
            _serviceModel = new AccountingEntry
            {
                Amount = _amount,
                Id = _id,
                BookingDate = _bookingDate,
                Recurring = _recurring,
                AccountId = _accountId,
                CategoryId = _categoryId,
            };
        }

        [Fact]
        public async Task RepoReturnsNull__ReturnsNull()
        {
            ProjectionAccountingService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountingService>()
                .AddTracingMocks()
                .Build();

            AccountingEntry actual = await unitUnderTest.CreateOrUpdate(_serviceModel);

            Assert.Null(actual);
        }

        [Fact(Skip = "Weird Moq error")]
        public async Task PropertiesAreSet_CallsUpsertWithData()
        {
            var repoMock = new Mock<IAccountingEntryRepository>();
            ProjectionAccountingService unitUnderTest = new UnitUnderTestBuilder<ProjectionAccountingService>()
                .With<IAccountingEntryRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            await unitUnderTest.CreateOrUpdate(_serviceModel);

            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(e => e.Recurring == _serviceModel.Recurring)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(e => e.Amount == _serviceModel.Amount)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(e => e.Id == _serviceModel.Id)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(e => e.BookingDate == _serviceModel.BookingDate)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(e => e.AccountId == _serviceModel.AccountId)));
            repoMock.Verify(r => r.Upsert(It.Is<DbModel>(e => e.CategoryId == _serviceModel.CategoryId)));
        }
    }
}
