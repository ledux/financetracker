﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.Tracing.Abstractions;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.BaseAccountingServiceTests
{
    internal class UnitUnderTest : BaseAccountingService
    {
        public UnitUnderTest(IAccountingEntryRepository repository, ITracer tracer) : base(repository, tracer) { }

        public override Task<AccountingEntry> CreateOrUpdate(AccountingEntry entry)
        {
            throw new NotSupportedException("This class is only for testing the non abstract methods");
        }

        public override Task Delete(long id)
        {
            throw new NotSupportedException("This class is only for testing the non abstract methods");
        }
    }
}
