﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Repositories.Models;
using FinanceTracker.Apis.Accounting.UnitTests.Extensions;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using ServiceModel = FinanceTracker.Apis.Accounting.Services.Models.AccountingEntry;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.BaseAccountingServiceTests
{
    public class GetByAccountIdTests
    {
        private const long AccountId = 1234512345;

        [Fact]
        public async Task RepoReturnsEmpty_ReturnsEmpty()
        {
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddAccountingRepo(Enumerable.Empty<AccountingEntry>())
                .AddTracingMocks()
                .Build();

            IEnumerable<ServiceModel> actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.Empty(actual);
        }

        [Fact]
        public async Task RepoReturnsList__ReturnsSameData()
        {
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddAccountingRepo(5)
                .AddTracingMocks()
                .Build();

            IEnumerable<ServiceModel> actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.Equal(5, actual.Count());
        }

        [Fact]
        public async Task AccountIdNotZero__CallsRepoWithAccountId()
        {
            (UnitUnderTest unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            await unitUnderTest.GetByAccountId(AccountId);

            repoMock.Verify(r => r.GetByAccountId(AccountId));
        }

        [Fact]
        public async Task RepoReturnsData__ReturnsSameData()
        {
            AccountingEntry[] repoData = CreateDbData().ToArray();
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddAccountingRepo(repoData)
                .AddTracingMocks()
                .Build();

            IEnumerable<ServiceModel> actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.Collection(actual, 
                entry => Assert.Equal(repoData[0].Amount, entry.Amount),
                entry => Assert.Equal(repoData[1].Amount, entry.Amount),
                entry => Assert.Equal(repoData[2].Amount, entry.Amount),
                entry => Assert.Equal(repoData[3].Amount, entry.Amount),
                entry => Assert.Equal(repoData[4].Amount, entry.Amount)
                );
        }

        private static IEnumerable<AccountingEntry> CreateDbData(int count = 5)
        {
            var random =new Random();
            for (var i = 0; i < count; i++)
            {
                yield return new AccountingEntry
                {
                    Id = i,
                    Amount = (decimal) (random.NextDouble() * 100),
                };
            }
        }
    }
}
