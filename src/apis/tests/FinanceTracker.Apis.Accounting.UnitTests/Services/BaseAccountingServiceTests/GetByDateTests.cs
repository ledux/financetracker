﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Repositories.Models;
using FinanceTracker.Apis.Accounting.UnitTests.Extensions;
using FinanceTracker.Common.Testing;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.BaseAccountingServiceTests
{
    public class GetByDateTests
    {
        private static readonly DateTimeOffset BookingDate = new DateTimeOffset(DateTime.Today);
        private const decimal Amount = (decimal) 2231.3;
        private const long Id = 221234161324;

        [Fact]
        public async Task DateIsValid_CallsGetByDateRangeWithTwoIdenticalValues()
        {
            (UnitUnderTest unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<UnitUnderTest>().BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            var date = new DateTimeOffset(new DateTime(2020, 12, 12));
            await unitUnderTest.GetByDate(date);

            repoMock.Verify(r => r.GetByDateTimeRange(date, date));
        }

        [Fact]
        public async Task DateIsValid_RepoReturnsOneEntry_ReturnsSameValueAsReceivedFromRepo()
        {
            var entry = new AccountingEntry
            {
                Id = Id,
                Amount = Amount,
                BookingDate = BookingDate
            };

            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddAccountingRepo(new[] { entry })
                .Build();

            IEnumerable<Accounting.Services.Models.AccountingEntry> actual = await unitUnderTest.GetByDate(BookingDate);

            Assert.NotNull(actual);
            IEnumerable<Accounting.Services.Models.AccountingEntry> entries = 
                actual as Accounting.Services.Models.AccountingEntry[] 
                ?? actual.ToArray();
            Assert.Single(entries);
            Assert.Equal(Id, entries.First().Id);
            Assert.Equal(Amount, entries.First().Amount);
            Assert.Equal(BookingDate, entries.First().BookingDate);
        }

        [Fact]
        public async Task DateTimeHasTime__CallsGetByDateTimeRangeWithoutTime()
        {
            var dateTime = new DateTimeOffset(2020, 5, 12, 3, 5, 15, TimeSpan.Zero);
            (UnitUnderTest unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<UnitUnderTest>().BuildBoth();
            Mock<IAccountingEntryRepository> repoMock = mockBag.GetMock<IAccountingEntryRepository>();

            await unitUnderTest.GetByDate(dateTime);

            repoMock
                .Verify(r => 
                    r.GetByDateTimeRange(
                        It.Is<DateTimeOffset>(d => d.Hour == 0),
                        It.Is<DateTimeOffset>(d => d.Hour == 0)));
            repoMock
                .Verify(r => 
                    r.GetByDateTimeRange(
                        It.Is<DateTimeOffset>(d => d.Minute == 0),
                        It.Is<DateTimeOffset>(d => d.Minute == 0)));
            repoMock
                .Verify(r => 
                    r.GetByDateTimeRange(
                        It.Is<DateTimeOffset>(d => d.Second == 0),
                        It.Is<DateTimeOffset>(d => d.Second == 0)));
        }
    }
}
