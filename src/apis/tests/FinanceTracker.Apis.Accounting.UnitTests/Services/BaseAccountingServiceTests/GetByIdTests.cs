﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using DbModel = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.BaseAccountingServiceTests
{
    public class GetByIdTests
    {
        private readonly long _id = 2234412;
        private readonly decimal _amount = (decimal) 35.2;
        private readonly long _accountId = 8891235;
        private readonly DateTimeOffset _bookingDate = new DateTimeOffset(2020, 8, 21, 0, 0, 0, TimeSpan.Zero);
        private readonly bool _recurring = true;
        private readonly long _categoryId = 338001234;

        [Fact]
        public async Task RepoReturnsNull_ReturnsNull()
        {
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .Build();
            const long id = 6623134;

            AccountingEntry actual = await unitUnderTest.GetById(id);

            Assert.Null(actual);
        }

        [Fact]
        public async Task RepoReturnsEntry__ReturnsEntryWithData()
        {
            var entry = new DbModel
            {
                Id = _id,
                Amount = _amount,
                AccountId = _accountId,
                BookingDate = _bookingDate,
                Recurring = _recurring,
                CategoryId = _categoryId,
            };
            var repoMock = new Mock<IAccountingEntryRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(entry);
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .With<IAccountingEntryRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            AccountingEntry actual = await unitUnderTest.GetById(33111552);

            Assert.Equal(_id, actual.Id);
            Assert.Equal(_amount, actual.Amount);
            Assert.Equal(_accountId, actual.AccountId);
            Assert.Equal(_categoryId, actual.CategoryId);
            Assert.Equal(_recurring, actual.Recurring);
            Assert.Equal(_bookingDate, actual.BookingDate);
        }

        [Fact]
        public async Task IdIsZero__Throws()
        {
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.GetById(0));
        }
    }
}
