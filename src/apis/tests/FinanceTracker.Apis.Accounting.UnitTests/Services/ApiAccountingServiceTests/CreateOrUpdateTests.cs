﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Producers;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Moq;
using Xunit;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.ApiAccountingServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly long _accountId = 33989214 ;
        private readonly int _amount = 42;
        private readonly DateTime _bookingDate = new DateTime(2020, 8, 20);
        private readonly bool _recurring = false;
        private readonly long _idOfExistingEntry = 123412341234;
        private const long CreatedId = 9238911235;

        [Fact]
        public async Task EntryIsNull_ReturnsNull()
        {
            ApiAccountingService unitUnderTest = new UnitUnderTestBuilder<ApiAccountingService>().Build();

            AccountingEntry accountingEntry = await unitUnderTest.CreateOrUpdate(null);

            Assert.Null(accountingEntry);
        }

        [Fact]
        public async Task EntryIsNotNull_ReturnsEntryWithId()
        {
            var idCreatorMock = new Mock<IIdCreator>();
            idCreatorMock.Setup(idc => idc.CreateId()).Returns(CreatedId);
            ApiAccountingService unitUnderTest = new UnitUnderTestBuilder<ApiAccountingService>()
                .With<IIdCreator>(idCreatorMock.Object)
                .AddTracingMocks()
                .Build();

            AccountingEntry actual = await unitUnderTest.CreateOrUpdate(new AccountingEntry());

            Assert.Equal(CreatedId, actual.Id);
        }

        [Fact]
        public async Task EntryIsNotNull_PropertiesAreSet_SendsTheDataToProducer()
        {
            var producerMock = new Mock<IAccountingProducer>();
            ApiAccountingService unitUnderTest = new UnitUnderTestBuilder<ApiAccountingService>()
                .With<IAccountingProducer>(producerMock.Object)
                .AddTracingMocks()
                .Build();
            var accountingEntry = new AccountingEntry
            {
                AccountId = _accountId,
                Amount = _amount,
                BookingDate = _bookingDate,
                Recurring = _recurring
            };

            await unitUnderTest.CreateOrUpdate(accountingEntry);

            producerMock.Verify(p =>p.AccountingEntryCreated(accountingEntry));
        }

        [Fact]
        public async Task GetByIdReturnsEntry_IdIsNotZero_UsesGivenIdToSendToTheProducer()
        {
            var idCreatorMock = new Mock<IIdCreator>();
            idCreatorMock.Setup(idc => idc.CreateId()).Returns(CreatedId);
            var repoMock = new Mock<IAccountingEntryRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(new Repositories.Models.AccountingEntry());
            (ApiAccountingService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountingService>()
                .With<IIdCreator>(idCreatorMock.Object)
                .With<IAccountingEntryRepository>(repoMock.Object)
                .AddTracingMocks()
                .BuildBoth();

            Mock<IAccountingProducer> producerMock = mockBag.GetMock<IAccountingProducer>();
            var accountingEntry = new AccountingEntry
            {
                Id = _idOfExistingEntry,
                AccountId = _accountId,
                Amount = _amount,
                BookingDate = _bookingDate,
                Recurring = _recurring
            };

            await unitUnderTest.CreateOrUpdate(accountingEntry);

            producerMock.Verify(p => p.AccountingEntryCreated(It.Is<AccountingEntry>(e => e.Id == _idOfExistingEntry)));
        }
    }
}
