﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Producers;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;
using AccountingEntry = FinanceTracker.Apis.Accounting.Repositories.Models.AccountingEntry;

namespace FinanceTracker.Apis.Accounting.UnitTests.Services.ApiAccountingServiceTests
{
    public class DeleteTests
    {
        private const long Id = 3235123451;

        [Fact]
        public async Task IdIsSet_RepoReturnsNull_DoesNotSendsAccountingEntryDeletedEvent()
        {
            (ApiAccountingService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountingService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingProducer> producerMock = mockBag.GetMock<IAccountingProducer>();

            await unitUnderTest.Delete(Id);

            producerMock.Verify(p => p.AccountingEntryDeleted(It.IsAny<DeletedModel>()), Times.Never);
        }

        [Fact]
        public async Task IdIsSet_ReadsEntryFromRepo_()
        {
            var repoMock = new Mock<IAccountingEntryRepository>();
            const decimal amount = 2342;
            var repoData = new AccountingEntry{ Amount = amount};
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(repoData);
            ApiAccountingService unitUnderTest = new UnitUnderTestBuilder<ApiAccountingService>()
                .With<IAccountingEntryRepository>(repoMock.Object)
                .AddTracingMocks()
                .Build();

            await unitUnderTest.Delete(Id);

            repoMock.Verify(r => r.GetById(Id));
        }

        [Fact]
        public async Task IdIsSet_RepoReturnsAccountingEntry_SendsDataProducer()
        {
            var repoMock = new Mock<IAccountingEntryRepository>();
            const decimal amount = 2342;
            const long accountId = 988872;
            var repoData = new AccountingEntry{ Amount = amount, Id = Id, AccountId = accountId };
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(repoData);
            (ApiAccountingService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiAccountingService>()
                .With<IAccountingEntryRepository>(repoMock.Object)
                .AddTracingMocks()
                .BuildBoth();
            Mock<IAccountingProducer> producerMock = mockBag.GetMock<IAccountingProducer>();

            await unitUnderTest.Delete(Id);

            producerMock.Verify(p => p.AccountingEntryDeleted(It.Is<DeletedModel>(m => m.Amount == amount)));
            producerMock.Verify(p => p.AccountingEntryDeleted(It.Is<DeletedModel>(m => m.AccountingEntryId == Id)));
            producerMock.Verify(p => p.AccountingEntryDeleted(It.Is<DeletedModel>(m => m.AccountId == accountId)));
        }
    }
}
