﻿using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Projections;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Projections.AccountingEntryDeletedProjectionTests
{
    public class HandleTests
    {
        private const long Id = 335135234;

        [Fact]
        public async Task PropertiesAreSet_CallsDelete()
        {
            var deletedEvent = new AccountingEntryDeleted
            {
                Id = Id
            };

            (AccountingEntryDeletedProjection unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<AccountingEntryDeletedProjection>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();

            await unitUnderTest.Handle(deletedEvent);

            serviceMock.Verify(s => s.Delete(Id));
        }
    }
}
