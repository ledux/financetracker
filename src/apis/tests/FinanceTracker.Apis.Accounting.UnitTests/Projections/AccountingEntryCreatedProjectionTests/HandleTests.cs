﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Projections;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Projections.AccountingEntryCreatedProjectionTests
{
    public class HandleTests
    {
        private readonly long _accountId = 33989214;
        private readonly int _amount = 42;
        private readonly DateTime _bookingDate = new DateTime(2020, 8, 20);
        private readonly bool _recurring = false;
        private readonly long _id = 999313344;

        [Fact]
        public async Task PropertiesAreSet_CallsInsertEntry()
        {
            (AccountingEntryUpdatedProjection unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<AccountingEntryUpdatedProjection>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<IAccountingService> serviceMock = mockBag.GetMock<IAccountingService>();
            var entry = new AccountingEntryUpdated
            {
                Amount = _amount,
                Id = _id,
                BookingDate = _bookingDate,
                Recurring = _recurring,
                AccountId = _accountId,
                CategoryId = _accountId,
            };

            await unitUnderTest.Handle(entry);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.Recurring == entry.Recurring)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.Amount == entry.Amount)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.AccountId == entry.AccountId)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.BookingDate == entry.BookingDate)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<AccountingEntry>(e => e.Id == entry.Id)));
        }
    }
}
