﻿using System;
using System.Collections.Generic;
using System.Linq;
using FinanceTracker.Apis.Accounting.Repositories;
using FinanceTracker.Apis.Accounting.Repositories.Models;
using FinanceTracker.Common.Testing;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Extensions
{
    /// <summary>
    /// Extends the <see cref="IUnitUnderTestBuilder{T}"/> to add a <see cref="IAccountingEntryRepository"/>
    /// </summary>
    internal static class AccountingRepoMockExtensions
    {
        /// <summary>
        /// Adds an accounting service, which returns the given amount of <see cref="AccountingEntry"/> from the methods
        /// </summary>
        /// <typeparam name="T">The type of the unit under test</typeparam>
        /// <param name="builder">The builder, to which the mock is added </param>
        /// <param name="quantity">The amount of entries the methods should return</param>
        /// <returns>The builder with the added mock</returns>
        public static IUnitUnderTestBuilder<T> AddAccountingRepo<T>(this IUnitUnderTestBuilder<T> builder,
            int quantity) where T : class
        {
            var repoMock = new Mock<IAccountingEntryRepository>();
            repoMock
                .Setup(r => r.GetByDateTimeRange(It.IsAny<DateTimeOffset>(), It.IsAny<DateTimeOffset>()))
                .ReturnsAsync(CreateDbModels(quantity));

            repoMock.Setup(r => r.GetByAccountId(It.IsAny<long>())).ReturnsAsync(CreateDbModels(quantity));

            builder.With<IAccountingEntryRepository>(repoMock.Object);

            return builder;
        }

        /// <summary>
        /// Adds an accounting service, which returns the given <see cref="AccountingEntry"/> from the methods
        /// </summary>
        /// <typeparam name="T">The type of the unit under test</typeparam>
        /// <param name="builder">The builder, to which the mock is added </param>
        /// <param name="entries">The entries the methods should return</param>
        /// <returns>The builder with the added mock</returns>
        public static IUnitUnderTestBuilder<T> AddAccountingRepo<T>(this IUnitUnderTestBuilder<T> builder,
            IEnumerable<AccountingEntry> entries) where T : class
        {
            var repoMock = new Mock<IAccountingEntryRepository>();
            IEnumerable<AccountingEntry> accountingEntries = entries as AccountingEntry[] ?? entries.ToArray();
            repoMock
                .Setup(r => r.GetByDateTimeRange(It.IsAny<DateTimeOffset>(), It.IsAny<DateTimeOffset>()))
                .ReturnsAsync(accountingEntries);

            repoMock.Setup(r => r.GetByAccountId(It.IsAny<long>())).ReturnsAsync(accountingEntries);

            builder.With<IAccountingEntryRepository>(repoMock.Object);

            return builder;
        }

        private static IEnumerable<AccountingEntry> CreateDbModels(int quantity)
        {
            var entries = new List<AccountingEntry>();
            for (var i = 0; i < quantity; i++)
            {
                entries.Add(new AccountingEntry { Id = i });
            }

            return entries;
        }
    }
}
