﻿using System;
using System.Collections.Generic;
using FinanceTracker.Apis.Accounting.Services;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.Testing;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Extensions
{
    internal static class AccountingServiceMockExtensions
    {
        /// <summary>
        /// Adds an accounting service, which returns the given amount of <see cref="AccountingEntry"/> from the methods
        /// </summary>
        /// <typeparam name="T">The type of the unit under test</typeparam>
        /// <param name="builder">The builder, to which the mock is added </param>
        /// <param name="quantity">The amount of entries the methods should return</param>
        /// <returns>The builder with the added mock</returns>
        public static IUnitUnderTestBuilder<T> AddAccountingService<T>(this IUnitUnderTestBuilder<T> builder, int quantity) where T : class
        {
            var serviceMock = new Mock<IAccountingService>();
            serviceMock
                .Setup(s => s.GetByDate(It.IsAny<DateTimeOffset>()))
                .ReturnsAsync(CreateAccountingEntries(quantity));

            serviceMock.Setup(s => s.GetByAccountId(It.IsAny<long>())).ReturnsAsync(CreateAccountingEntries(quantity));

            builder.With<IAccountingService>(serviceMock.Object);

            return builder;
        }

        private static IEnumerable<AccountingEntry> CreateAccountingEntries(int quantity)
        {
            var entries = new List<AccountingEntry>();
            for (var i = 0; i < quantity; i++)
            {
                entries.Add(new AccountingEntry { Id = i });
            }

            return entries;
        }
    }
}
