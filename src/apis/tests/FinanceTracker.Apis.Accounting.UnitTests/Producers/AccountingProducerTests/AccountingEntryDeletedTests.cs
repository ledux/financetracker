﻿using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Producers;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Producers.AccountingProducerTests
{
    public class AccountingEntryDeletedTests
    {
        private const long Id = 9912341;

        [Fact]
        public async Task IdIsSet_ProducesDeletedEventWithSameId()
        {
            (AccountingProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> clientMock = mockBag.GetMock<IEventClient>();

            await unitUnderTest.AccountingEntryDeleted(new DeletedModel { AccountingEntryId = Id });

            clientMock.Verify(c => c.Publish(It.Is<AccountingEntryDeleted>(e => e.Id == Id)));
        }

        [Fact]
        public async Task ModelIsNotNull_ProducesEventWithSameData()
        {
            (AccountingProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> clientMock = mockBag.GetMock<IEventClient>();
            const decimal amount = 334;
            const long accountId = 772342;
            var accountingEntry = new DeletedModel { AccountingEntryId = Id, Amount = amount, AccountId = accountId};

            await unitUnderTest.AccountingEntryDeleted(accountingEntry);

            clientMock.Verify(c => c.Publish(It.Is<AccountingEntryDeleted>(e => e.Amount == amount)));
            clientMock.Verify(c => c.Publish(It.Is<AccountingEntryDeleted>(e => e.AccountId == accountId)));
        }
    }
}
