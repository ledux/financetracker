﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.Accounting.Producers;
using FinanceTracker.Apis.Accounting.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.Accounting.UnitTests.Producers.AccountingProducerTests
{
    public class AccountingEntryCreatedTests
    {
        private readonly DateTime _bookingDate = new DateTime(2020, 8, 15);
        private readonly bool _recurring = false;
        private readonly long _accountId = 288322443;
        private readonly long _categoryId =  28986562;
        private const decimal Amount = (decimal) 23.5;
        private const long Id = 928398982;

        [Fact]
        public async Task IdIsZero_Throws()
        {
            AccountingProducer unitUnderTest = new UnitUnderTestBuilder<AccountingProducer>().Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.AccountingEntryCreated(new AccountingEntry()));
        }

        [Fact]
        public async Task PropertiesAreSet__CallsPublishWithData()
        {
            (AccountingProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<AccountingProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> eventClientMock = mockBag.GetMock<IEventClient>();
            var accountingEntry = new AccountingEntry
            {
                Id = Id,
                BookingDate = _bookingDate,
                Amount = Amount,
                Recurring = _recurring,
                AccountId = _accountId,
                CategoryId = _categoryId,
            };

            await unitUnderTest.AccountingEntryCreated(accountingEntry);

            eventClientMock.Verify(c => c.Publish(It.Is<AccountingEntryUpdated>(e => e.Recurring == _recurring)));
            eventClientMock.Verify(c => c.Publish(It.Is<AccountingEntryUpdated>(e => e.BookingDate == _bookingDate)));
            eventClientMock.Verify(c => c.Publish(It.Is<AccountingEntryUpdated>(e => e.Amount == Amount)));
            eventClientMock.Verify(c => c.Publish(It.Is<AccountingEntryUpdated>(e => e.Id == Id)));
            eventClientMock.Verify(c => c.Publish(It.Is<AccountingEntryUpdated>(e => e.CategoryId == _categoryId)));
            eventClientMock.Verify(c => c.Publish(It.Is<AccountingEntryUpdated>(e => e.AccountId == _accountId)));
        }
    }
}
