﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FinanceTracker.Common.MongoSetup.UnitTests.MongoDbRepositoryBaseTests
{
    internal class UnitUnderTest : MongoDbRepositoryBase
    {
        public UnitUnderTest(IMongoClient client, IOptions<MongoDbRepositorySettings> optionsAccessor) : base(client, optionsAccessor)
        {
        }

        protected override string RepoName => "TestRepo";
    }
}
