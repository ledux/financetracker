﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Moq;
using Xunit;

namespace FinanceTracker.Common.MongoSetup.UnitTests.MongoDbRepositoryBaseTests
{
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
    public class CtorTests
    {
        private readonly IMongoClient _mockClient;
        private const string DatabaseName = "TestDatabaseName";

        public CtorTests()
        {
            var clientMock = new Mock<IMongoClient>();
            _mockClient = clientMock.Object;

        }

        [Fact]
        public void OptionsAccessorIsNull__Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new UnitUnderTest(_mockClient, null));
        }

        [Fact]
        public void OptionsAccessorNotNull_CollectionsReturnsNull_Throws()
        {
            var settings = new MongoDbRepositorySettings
            {
                Collections = new List<MongoDbCollectionSettings>
                {
                    new MongoDbCollectionSettings
                    {
                        RepoName = "RepoOne"
                    },
                    new MongoDbCollectionSettings
                    {
                        RepoName = "RepoTwo"
                    }
                }
            };

            var optionsMock = new Mock<IOptions<MongoDbRepositorySettings>>();
            optionsMock.Setup(o => o.Value).Returns(settings);

            Assert.Throws<MongoConfigurationException>(() => new UnitUnderTest(_mockClient, optionsMock.Object));
        }

        [Fact]
        public void OptionsAccessorIsNotNull_ConfigurationFound_CallsGetDatabase()
        {
            var settings = new MongoDbRepositorySettings
            {
                Collections = new List<MongoDbCollectionSettings>
                {
                    new MongoDbCollectionSettings
                    {
                        RepoName = "RepoOne"
                    },
                    new MongoDbCollectionSettings
                    {
                        RepoName = "TestRepo",
                        Database = DatabaseName
                    }
                }
            };
            var optionsMock = new Mock<IOptions<MongoDbRepositorySettings>>();
            optionsMock.Setup(o => o.Value).Returns(settings);
            var clientMock = new Mock<IMongoClient>();

            new UnitUnderTest(clientMock.Object, optionsMock.Object);

            clientMock.Verify(c => c.GetDatabase(DatabaseName, null));
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void OptionsAccessorIsNotNull_DatabaseNameIsNullOrEmpty_Throws(string databaseName)
        {
            var settings = new MongoDbRepositorySettings
            {
                Collections = new List<MongoDbCollectionSettings>
                {
                    new MongoDbCollectionSettings
                    {
                        RepoName = "RepoOne"
                    },
                    new MongoDbCollectionSettings
                    {
                        RepoName = "TestRepo",
                        Database = databaseName
                    }
                }
            };
            var optionsMock = new Mock<IOptions<MongoDbRepositorySettings>>();
            optionsMock.Setup(o => o.Value).Returns(settings);
            var clientMock = new Mock<IMongoClient>();

            Assert.Throws<MongoConfigurationException>(() => new UnitUnderTest(clientMock.Object, optionsMock.Object) );
        }

        [Theory]
        [InlineData("testrepo")]
        [InlineData("testRepo")]
        [InlineData("tEstrepo")]
        [InlineData("tesTrepo")]
        [InlineData("tesTrePo")]
        [InlineData("TESTREPO")]
        public void OptionsAccessorIsNotNull_DatabaseNameIsSet_CasingDoesNotMatter(string repoName)
        {
            var settings = new MongoDbRepositorySettings
            {
                Collections = new List<MongoDbCollectionSettings>
                {
                    new MongoDbCollectionSettings
                    {
                        RepoName = "RepoOne"
                    },
                    new MongoDbCollectionSettings
                    {
                        RepoName = repoName,
                        Database = DatabaseName
                    }
                }
            };
            var optionsMock = new Mock<IOptions<MongoDbRepositorySettings>>();
            optionsMock.Setup(o => o.Value).Returns(settings);
            var clientMock = new Mock<IMongoClient>();

            new UnitUnderTest(clientMock.Object, optionsMock.Object);

            clientMock.Verify(c => c.GetDatabase(DatabaseName, null));
        }
    }
}
