﻿using System;
using System.Collections.Generic;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Common.Testing.FakeItEasy;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Extensions
{
    internal static class SavingsTargetRepoExtensions
    {
        internal static IUnitUnderTestBuilder<T> AddSavingsTargetRepo<T>(this IUnitUnderTestBuilder<T> builder, int count) where T : class
        {
            var savingsTargetRepository = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => savingsTargetRepository.GetByAccountId(A<long>._)).Returns(CreateTestData(count));
            builder.With<ISavingsTargetRepository>(savingsTargetRepository);
            return builder;
        }

        private static IEnumerable<Repositories.Models.SavingsTarget> CreateTestData(int count)
        {
            var random = new Random();
            for (var i = 0; i < count; i++)
            {
                yield return new Repositories.Models.SavingsTarget
                {
                    Id = i,
                    AccountId = 123412,
                    Name = $"Name {i}",
                    Description = $"Description {i}",
                    TargetAmount = random.Next(100, 1000),
                    TargetDate = new DateTime(DateTime.Today.Year +1, 10, 1),
                    StartAmount = random.Next(0, 90),
                    Balance = random.Next(150, 900),
                    StartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month > 1 ? DateTime.Today.Month - 1 : 1, 1),
                };
            }
        }
    }
}
