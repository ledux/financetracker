﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Apis.SavingsTarget.UnitTests.Extensions;
using FinanceTracker.Common.Testing.FakeItEasy;
using Xunit;
using DbFreeAssets = FinanceTracker.Apis.SavingsTarget.Repositories.Models.FreeAssets;
using DbSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.BaseSavingsTargetServiceTests
{
    public class GetByAccountIdTests
    {
        private const long AccountId = 90878732;
        [Fact]
        public async Task IdIsZero_Throws()
        {
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>().Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.GetByAccountId(0));
        }

        [Fact]
        public async Task IdIsNotZero__CallsGetByIdOnFreeAssetsRepo()
        {
            var builder = new UnitUnderTestBuilder<UnitUnderTest>();
            var fakeRepo = builder.GetMock<IFreeAssetsRepository>();
            UnitUnderTest unitUnderTest = builder.Build();

            await unitUnderTest.GetByAccountId(AccountId);

            A.CallTo(() => fakeRepo.GetByAccountId(AccountId)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task IdIsNotZero__CallsGetByAccountIdOnSavingsTargetRepo()
        {
            var builder = new UnitUnderTestBuilder<UnitUnderTest>();
            var fakeRepo = builder.GetMock<ISavingsTargetRepository>();
            UnitUnderTest unitUnderTest = builder.Build();

            await unitUnderTest.GetByAccountId(AccountId);

            A.CallTo(() => fakeRepo.GetByAccountId(AccountId)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task IdIsNotZero_FreeAssetsRepoReturnNull_ReturnsNull()
        {
            var builder = new UnitUnderTestBuilder<UnitUnderTest>();
            var fakeRepo = builder.GetMock<IFreeAssetsRepository>();
            A.CallTo(() => fakeRepo.GetByAccountId(A<long>._)).Returns((DbFreeAssets) null);
            UnitUnderTest unitUnderTest = builder.Build();

            Account actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.Null(actual);
        }

        [Fact]
        public async Task IdIsNotZero_FreeAssetsReturnsData_SavingsTargetReturnsNull_ReturnsEmptySavingsTargets()
        {
            var fakeFreeAssetsRepo = A.Fake<IFreeAssetsRepository>();
            var fakeSavingsTargetRepo = A.Fake<ISavingsTargetRepository>();
            var freeAssets = new DbFreeAssets { AccountId = AccountId};
            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(A<long>._)).Returns(freeAssets);
            A.CallTo(() => fakeSavingsTargetRepo.GetByAccountId(A<long>._))
                .Returns((IEnumerable<DbSavingsTarget>) null);
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .With<IFreeAssetsRepository>(fakeFreeAssetsRepo)
                .With<ISavingsTargetRepository>(fakeSavingsTargetRepo)
                .Build();

            Account actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.NotNull(actual);
            Assert.Empty(actual.SavingsTargets);
        }

        [Fact]
        public async Task IdIsNotZero_FreeAssetsReturnsData_SavingsTargetReturnsEmpty_ReturnsEmpytSavingsTarget()
        {
            var fakeFreeAssetsRepo = A.Fake<IFreeAssetsRepository>();
            var fakeSavingsTargetRepo = A.Fake<ISavingsTargetRepository>();
            var freeAssets = new DbFreeAssets { AccountId = AccountId};
            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(A<long>._)).Returns(freeAssets);
            A.CallTo(() => fakeSavingsTargetRepo.GetByAccountId(A<long>._))
                .Returns(Enumerable.Empty<DbSavingsTarget>());
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .With<IFreeAssetsRepository>(fakeFreeAssetsRepo)
                .With<ISavingsTargetRepository>(fakeSavingsTargetRepo)
                .Build();

            Account actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.NotNull(actual);
            Assert.Empty(actual.SavingsTargets);
        }

        [Fact]
        public async Task IdIsNotZero_FreeAssetsReturnsData_ReturnsIdAndFreeAssets()
        {
            var fakeFreeAssetsRepo = A.Fake<IFreeAssetsRepository>();
            const decimal amount = (decimal) 12342.22;
            var freeAssets = new DbFreeAssets { AccountId = AccountId, Amount = amount};
            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(A<long>._)).Returns(freeAssets);
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .With<IFreeAssetsRepository>(fakeFreeAssetsRepo)
                .Build();

            Account actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.Equal(AccountId, actual.Id);
            Assert.Equal(amount, actual.FreeAssets);
        }

        [Fact]
        public async Task IdIsNotZero_FreeAssetsReturnsData_SavingsTargetReturnsData_SavingsTargetAreReturned()
        {
            var fakeFreeAssetsRepo = A.Fake<IFreeAssetsRepository>();
            const decimal amount = (decimal) 12342.22;
            var freeAssets = new DbFreeAssets { AccountId = AccountId, Amount = amount};
            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(A<long>._)).Returns(freeAssets);
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .With<IFreeAssetsRepository>(fakeFreeAssetsRepo)
                .AddSavingsTargetRepo(5)
                .Build();

            Account actual = await unitUnderTest.GetByAccountId(AccountId);

            Assert.NotEmpty(actual.SavingsTargets);
            Assert.Equal(5, actual.SavingsTargets.Count());
        }
    }
}
