﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Tracing.Abstractions;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.BaseSavingsTargetServiceTests
{
    internal class UnitUnderTest : BaseSavingsTargetService<ServiceModel>
    {
        public UnitUnderTest(ITracer tracer,
            ISavingsTargetRepository savingsTargetRepository,
            IFreeAssetsRepository freeAssetsRepository) 
            : base(tracer, savingsTargetRepository, freeAssetsRepository) { }

        public override Task<ServiceModel> CreateOrUpdate(ServiceModel savingsTarget)
        {
            throw new NotSupportedException("This class is only used for testing the non abstract method in an abstract class");
        }

        public override Task Delete(long id)
        {
            throw new NotSupportedException("This class is only used for testing the non abstract method in an abstract class");
        }

        public override Task<ServiceModel> SetBalance(SetBalance updateBalance)
        {
            throw new NotSupportedException("This class is only used for testing the non abstract method in an abstract class");
        }
    }
}
