﻿using System;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Common.Tracing.Abstractions;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.BaseSavingsTargetServiceTests
{
    public class CtorTests
    {
        [Fact]
        public void SavingsTargetRepositoryIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                () => new UnitUnderTest(A.Dummy<ITracer>(), null, A.Dummy<IFreeAssetsRepository>()));
        }

        [Fact]
        public void FreeAssetsRepositoryIsNull__Throws()
        {
            Assert.Throws<ArgumentNullException>(
                () => new UnitUnderTest(A.Dummy<ITracer>(), A.Dummy<ISavingsTargetRepository>(), null));
        }
    }
}
