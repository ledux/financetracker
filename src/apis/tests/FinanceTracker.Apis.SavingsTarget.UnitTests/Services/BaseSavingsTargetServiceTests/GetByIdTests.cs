﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.BaseSavingsTargetServiceTests
{
    public class GetByIdTests
    {
        private readonly RepoModel _repoData;
        private const long Id = 123421233;

        public GetByIdTests()
        {
            _repoData = new RepoModel
            {
                Id = 12341234,
                AccountId = 098989883,
                Name = "Name of target",
                Description = "Description of target",
                StartAmount = 200,
                TargetAmount = 2000,
                TargetDate = new DateTime(2022, 1, 1),
                Balance = 400,
                StartDate = new DateTime(2020, 8, 1)
            };
        }

        [Fact]
        public async Task RepoReturnsNull_ReturnsNull()
        {
            var repoMock = new Mock<ISavingsTargetRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync((RepoModel) null);
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .With<ISavingsTargetRepository>(repoMock.Object)
                .Build();

            ServiceModel actual = await unitUnderTest.GetById(Id);

            Assert.Null(actual);
        }

        [Fact]
        public async Task IdIsZero__Throws()
        {
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.GetById(0));
        }

        [Fact]
        public async Task IdIsNotZero__CallsRepoWithSameId()
        {
            var repoMock = new Mock<ISavingsTargetRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(_repoData);
            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .With<ISavingsTargetRepository>(repoMock.Object)
                .Build();

            await unitUnderTest.GetById(Id);

            repoMock.Verify(r => r.GetById(Id));
        }

        [Fact]
        public async Task RepoReturnsData__ReturnsSameData()
        {
            var repoMock = new Mock<ISavingsTargetRepository>();
            repoMock.Setup(r => r.GetById(It.IsAny<long>())).ReturnsAsync(_repoData);

            UnitUnderTest unitUnderTest = new UnitUnderTestBuilder<UnitUnderTest>()
                .AddTracingMocks()
                .With<ISavingsTargetRepository>(repoMock.Object)
                .Build();

            ServiceModel actual = await unitUnderTest.GetById(Id);

            Assert.Equal(_repoData.Id, actual.Id);
            Assert.Equal(_repoData.AccountId, actual.AccountId);
            Assert.Equal(_repoData.Name, actual.Name);
            Assert.Equal(_repoData.Description, actual.Description);
            Assert.Equal(_repoData.StartAmount, actual.StartAmount);
            Assert.Equal(_repoData.TargetAmount, actual.TargetAmount);
            Assert.Equal(_repoData.TargetDate, actual.TargetDate);
            Assert.Equal(_repoData.Balance, actual.Balance);
            Assert.Equal(_repoData.StartDate, actual.StartDate);
        }
    }
}
