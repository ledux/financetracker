﻿using System;
using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing.FakeItEasy;
using Xunit;
using ServiceUpdateBalance = FinanceTracker.Apis.SavingsTarget.Services.Models.SetBalance;
using RepoUpdateBalance = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SetBalance;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ProjectionSavingsTargetServiceTests
{
    public class SetBalanceTests
    {
        private const long SavingsTargetId = 1852342;

        [Theory]
        [InlineData(-33)]
        [InlineData(-0.001)]
        [InlineData(-1)]
        public async Task BalanceIsNegative_Throws(decimal balance)
        {
            var update = new ServiceUpdateBalance { SavingsTargetId = SavingsTargetId, Balance = balance };

            ProjectionSavingsTargetService unitUnderTest = 
                new UnitUnderTestBuilder<ProjectionSavingsTargetService>().Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.SetBalance(update));
        }

        [Fact]
        public async Task BalanceIsNull__Throws()
        {
            ProjectionSavingsTargetService unitUnderTest = 
                new UnitUnderTestBuilder<ProjectionSavingsTargetService>().Build();

            await Assert.ThrowsAsync<ArgumentNullException>(() => unitUnderTest.SetBalance(null));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(10)]
        [InlineData(1234.33)]
        public async Task BalanceIsNotNull__CallsRepoWithSameValues(decimal balance)
        {
            var update = new ServiceUpdateBalance { SavingsTargetId = SavingsTargetId, Balance = balance };
            var builder = new UnitUnderTestBuilder<ProjectionSavingsTargetService>();
            var fakeRepo = builder.GetMock<ISavingsTargetRepository>();
            ProjectionSavingsTargetService unitUnderTest = builder.Build();

            await unitUnderTest.SetBalance(update);

            A.CallTo(() => fakeRepo.SetBalance(A<RepoUpdateBalance>.That.Matches(u => u.SavingsTargetId == update.SavingsTargetId)))
                .MustHaveHappenedOnceExactly();
            A.CallTo(() => fakeRepo.SetBalance(A<RepoUpdateBalance>.That.Matches(u => u.Balance == update.Balance)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task BalanceIsNotNull_RepoReturnsValue_ReturnsSameValue()
        {
            const decimal balance = 1234443;
            var update = new ServiceUpdateBalance { SavingsTargetId = SavingsTargetId, Balance = (decimal) 1234.33 };
            var repoData = new RepoSavingsTarget { Description = "Description", Name = "Name", Balance = balance };
            var fakeRepo = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeRepo.SetBalance(A<RepoUpdateBalance>._)).Returns(repoData);
            ProjectionSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ProjectionSavingsTargetService>()
                .With<ISavingsTargetRepository>(fakeRepo)
                .Build();

            ServiceSavingsTarget actual = await unitUnderTest.SetBalance(update);

            Assert.Equal(repoData.Balance, actual.Balance);
            Assert.Equal(repoData.Description, actual.Description);
            Assert.Equal(repoData.Name, actual.Name);
        }
    }
}
