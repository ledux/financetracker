﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ProjectionSavingsTargetServiceTests
{
    public class CreateOrUpdateTests
    {
        private readonly ServiceModel _data;
        private readonly RepoModel _repoData;

        public CreateOrUpdateTests()
        {
            _data = new ServiceModel
            {
                Id = 12341234,
                AccountId = 0989898989,
                Name = "Name",
                Description = "Description",
                StartAmount = 22,
                TargetAmount = 20000,
                TargetDate = new DateTime(2021, 5, 1),
                Balance = 1000,
                StartDate = new DateTime(2020, 9, 1)
            };
            _repoData = new RepoModel
            {
                Id = 34322,
                AccountId = 999999,
                Name = "db Name",
                Description = "db Description",
                StartAmount = 222,
                TargetAmount = 50000,
                TargetDate = new DateTime(2022, 5, 1),
                Balance = 6000,
                StartDate = new DateTime(2019, 9, 1)
            };
        }

        [Fact]
        public async Task SavingsTargetNotNull_SendsDataToRepo()
        {
            (ProjectionSavingsTargetService unitUnderTest, MockBag mockBag) =
                new UnitUnderTestBuilder<ProjectionSavingsTargetService>()
                    .AddTracingMocks()
                    .BuildBoth();
            Mock<ISavingsTargetRepository> repoMock = mockBag.GetMock<ISavingsTargetRepository>();
            repoMock.Setup(r => r.Upsert(It.IsAny<RepoModel>())).ReturnsAsync(new RepoModel());

            await unitUnderTest.CreateOrUpdate(_data);

            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.Id == _data.Id)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.Name == _data.Name)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.Description == _data.Description)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.Balance == _data.Balance)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.AccountId == _data.AccountId)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.StartAmount == _data.StartAmount)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.TargetAmount == _data.TargetAmount)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.StartDate == _data.StartDate)));
            repoMock.Verify(r => r.Upsert(It.Is<RepoModel>(m => m.TargetDate == _data.TargetDate)));
        }

        [Fact]
        public async Task RepoReturnsValue__ReturnsTheSameValue()
        {
            var repoMock = new Mock<ISavingsTargetRepository>();
            repoMock.Setup(r => r.Upsert(It.IsAny<RepoModel>())).ReturnsAsync(_repoData);
            ProjectionSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ProjectionSavingsTargetService>()
                .AddTracingMocks()
                .With<ISavingsTargetRepository>(repoMock.Object)
                .Build();

            ServiceModel actual = await unitUnderTest.CreateOrUpdate(_data);

            Assert.Equal(_repoData.Id, actual.Id);
            Assert.Equal(_repoData.Name, actual.Name);
            Assert.Equal(_repoData.Description, actual.Description);
            Assert.Equal(_repoData.Balance, actual.Balance);
            Assert.Equal(_repoData.AccountId, actual.AccountId);
            Assert.Equal(_repoData.StartAmount, actual.StartAmount);
            Assert.Equal(_repoData.TargetAmount, actual.TargetAmount);
            Assert.Equal(_repoData.StartDate, actual.StartDate);
            Assert.Equal(_repoData.TargetDate, actual.TargetDate);
        }

        [Fact]
        public async Task RepoReturnsNull_ReturnsNull()
        {
            ProjectionSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ProjectionSavingsTargetService>()
                .AddTracingMocks()
                .Build();

            ServiceModel actual = await unitUnderTest.CreateOrUpdate(_data);

            Assert.Null(actual);
        }
    }
}
