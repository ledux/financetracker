﻿using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ProjectionSavingsTargetServiceTests
{
    public class DeleteTest
    {
        private const long Id = 5553234;

        [Fact]
        public async Task IdNotZero_CallsTheRepoWithTheSameId()
        {
            (ProjectionSavingsTargetService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ProjectionSavingsTargetService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<ISavingsTargetRepository> repoMock = mockBag.GetMock<ISavingsTargetRepository>();

            await unitUnderTest.Delete(Id);

            repoMock.Verify(r => r.Delete(Id));
        }
    }
}
