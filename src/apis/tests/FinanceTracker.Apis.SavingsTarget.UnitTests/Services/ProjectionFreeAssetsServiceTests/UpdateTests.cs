﻿using System;
using System.Threading.Tasks;
using AutoBogus;
using Bogus;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Testing.FakeItEasy;
using DbModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.FreeAssets;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.FreeAssets;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ProjectionFreeAssetsServiceTests
{
    public class UpdateTests
    {
        #region Update by accountId and amount
        [Fact]
        public async Task AccountIdIsZero__Throws()
        {
            ProjectionFreeAssetsService unitUnderTest = new UnitUnderTestBuilder<ProjectionFreeAssetsService>().Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.Update(0, (decimal) 13342.22));
        }

        [Fact]
        public async Task AccountIdIsNotZero__CallsUpdateFreeAssetsWithAccountIdAndAmount()
        {
            var builder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var fakeRepo = builder.GetMock<IFreeAssetsRepository>();
            ProjectionFreeAssetsService unitUnderTest = builder.Build();

            const decimal amount = (decimal) 332.23;
            const long accountId = 2345523;
            await unitUnderTest.Update(accountId, amount);

            A.CallTo(() => fakeRepo.UpdateFreeAssets(accountId, amount)).MustHaveHappened();
        }

        [Fact]
        public async Task AccountIdIsNotZero_RepoReturnsValue_ReturnsSameValues()
        {
            const long accountId = 2345523;
            const decimal amount = (decimal) 332.23;
            const decimal freeAssets = (decimal) 99999.22;
            var builder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var fakeRepo = builder.GetMock<IFreeAssetsRepository>();
            var repoData = new DbModel { AccountId = accountId, Amount = freeAssets };
            A.CallTo(() => fakeRepo.UpdateFreeAssets(A<long>._, A<decimal>._)).Returns(repoData);
            ProjectionFreeAssetsService unitUnderTest = builder.Build();

            ServiceModel updated = await unitUnderTest.Update(accountId, amount);

            Assert.Equal(accountId, updated.AccountId);
            Assert.Equal(freeAssets, updated.Amount);
        }

        [Fact]
        public async Task AccountIdIsNotZero_RepoReturnsNull_ReturnsNull()
        {
            const long accountId = 2345523;
            const decimal amount = (decimal) 332.23;
            var builder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var fakeRepo = builder.GetMock<IFreeAssetsRepository>();
            A.CallTo(() => fakeRepo.UpdateFreeAssets(A<long>._, A<decimal>._)).Returns((DbModel) null);
            ProjectionFreeAssetsService unitUnderTest = builder.Build();

            ServiceModel actual = await unitUnderTest.Update(accountId, amount);

            Assert.Null(actual);
        }
        #endregion

        #region Update by SavingsTargetBalance
        [Fact]
        public async Task SavingsTargetBalanceIsNotNull__CallsGetById()
        {
            var savingsTargetBalance = AutoFaker.Generate<SavingsTargetBalance>();
            var builder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var fakeRepo = builder.GetMock<ISavingsTargetRepository>();
            ProjectionFreeAssetsService unitUnderTest = builder.Build();

            await unitUnderTest.Update(savingsTargetBalance);

            A.CallTo(() => fakeRepo.GetById(savingsTargetBalance.SavingsTargetId)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task RepoReturnsDifferentBalanceThanInParameter__CallsUpdateFreeAssetsWithTheDifference()
        {
            SavingsTargetBalance savingsTargetBalance = new AutoFaker<SavingsTargetBalance>()
                .RuleFor(s => s.Balance, faker => faker.Random.Decimal(10, 40))
                .Generate();
            RepoSavingsTarget repoData = new AutoFaker<RepoSavingsTarget>()
                .RuleFor(s => s.Balance, faker => faker.Random.Decimal(50, 200))
                .Generate();
            decimal difference = repoData.Balance - savingsTargetBalance.Balance;
            var fakeSavingsTargetRepo = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeSavingsTargetRepo.GetById(A<long>._)).Returns(repoData);
            IUnitUnderTestBuilder<ProjectionFreeAssetsService> builder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>()
                .With(fakeSavingsTargetRepo);
            var fakeFreeAssetsRepo = builder.GetMock<IFreeAssetsRepository>();
            ProjectionFreeAssetsService unitUnderTest = builder.Build();

            await unitUnderTest.Update(savingsTargetBalance);

            A.CallTo(() => fakeFreeAssetsRepo.UpdateFreeAssets(repoData.AccountId, difference))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task RepoReturnsSameBalanceThanInParameter__DoesNotCallUpdateFreeAssets()
        {
            long balance = new Faker().Random.Long(10, 100);
            SavingsTargetBalance savingsTargetBalance = new AutoFaker<SavingsTargetBalance>()
                .RuleFor(s => s.Balance, balance)
                .Generate();
            RepoSavingsTarget repoData = new AutoFaker<RepoSavingsTarget>()
                .RuleFor(s => s.Balance, balance)
                .Generate();
            var fakeSavingsTargetRepo = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeSavingsTargetRepo.GetById(A<long>._)).Returns(repoData);
            IUnitUnderTestBuilder<ProjectionFreeAssetsService> builder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>()
                .With(fakeSavingsTargetRepo);
            var fakeFreeAssetsRepo = builder.GetMock<IFreeAssetsRepository>();
            ProjectionFreeAssetsService unitUnderTest = builder.Build();

            await unitUnderTest.Update(savingsTargetBalance);

            A.CallTo(() => fakeFreeAssetsRepo.UpdateFreeAssets(A<long>._, A<decimal>._)).MustNotHaveHappened();
        }

        [Fact]
        public async Task RepoReturnsDifferentBalanceThanInParameter__ReturnsUpdatedFreeAssets()
        {
            SavingsTargetBalance savingsTargetBalance = new AutoFaker<SavingsTargetBalance>()
                .RuleFor(s => s.Balance, faker => faker.Random.Decimal(10, 40))
                .Generate();
            RepoSavingsTarget repoData = new AutoFaker<RepoSavingsTarget>()
                .RuleFor(s => s.Balance, faker => faker.Random.Decimal(50, 200))
                .Generate();
            var updatedFreeAssets = AutoFaker.Generate<DbModel>();

            var fakeSavingsTargetRepo = A.Fake<ISavingsTargetRepository>();
            var fakeFreeAssetsRepo = A.Fake<IFreeAssetsRepository>();
            A.CallTo(() => fakeSavingsTargetRepo.GetById(A<long>._)).Returns(repoData);
            A.CallTo(() => fakeFreeAssetsRepo.UpdateFreeAssets(A<long>._, A<decimal>._)).Returns(updatedFreeAssets);
            ProjectionFreeAssetsService unitUnderTest = new UnitUnderTestBuilder<ProjectionFreeAssetsService>()
                .With(fakeSavingsTargetRepo)
                .With(fakeFreeAssetsRepo)
                .Build();

            FreeAssets actual = await unitUnderTest.Update(savingsTargetBalance);

            Assert.Equal(updatedFreeAssets.AccountId, actual.AccountId);
            Assert.Equal(updatedFreeAssets.Amount, actual.Amount);
        }

        [Fact]
        public async Task RepoReturnsSameBalanceAsInParameter__ReturnsValueFromGetByAccountId()
        {
            long balance = new Faker().Random.Long(10, 100);
            SavingsTargetBalance savingsTargetBalance = new AutoFaker<SavingsTargetBalance>()
                .RuleFor(s => s.Balance, balance)
                .Generate();
            RepoSavingsTarget repoData = new AutoFaker<RepoSavingsTarget>()
                .RuleFor(s => s.Balance, balance)
                .Generate();
            var actualFreeAssets = AutoFaker.Generate<DbModel>();

            var fakeSavingsTargetRepo = A.Fake<ISavingsTargetRepository>();
            var fakeFreeAssetsRepo = A.Fake<IFreeAssetsRepository>();
            A.CallTo(() => fakeSavingsTargetRepo.GetById(A<long>._)).Returns(repoData);
            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(A<long>._)).Returns(actualFreeAssets);

            ProjectionFreeAssetsService unitUnderTest = new UnitUnderTestBuilder<ProjectionFreeAssetsService>()
                .With(fakeSavingsTargetRepo)
                .With(fakeFreeAssetsRepo)
                .Build();

            FreeAssets actual = await unitUnderTest.Update(savingsTargetBalance);

            Assert.Equal(actualFreeAssets.AccountId, actual.AccountId);
            Assert.Equal(actualFreeAssets.Amount, actual.Amount);
        }
        #endregion
    }
}
