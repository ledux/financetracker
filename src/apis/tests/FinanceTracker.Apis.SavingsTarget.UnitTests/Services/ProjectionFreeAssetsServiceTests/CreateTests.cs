﻿using System;
using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing.FakeItEasy;
using DbModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.FreeAssets;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.FreeAssets;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ProjectionFreeAssetsServiceTests
{
    public class CreateTests
    {
        [Fact]
        public async Task FreeAssetsIsNull_Throws()
        {
            ProjectionFreeAssetsService unitUnderTest = new UnitUnderTestBuilder<ProjectionFreeAssetsService>()
                .Build();
            await Assert.ThrowsAsync<ArgumentNullException>(() => unitUnderTest.Create(null));
        }

        [Fact]
        public async Task FreeAssetsIsNotNull__CallsGetByAccountIdWithAccountId()
        {
            var unitUnderTestBuilder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var repoFake = unitUnderTestBuilder.GetMock<IFreeAssetsRepository>();
            ProjectionFreeAssetsService unitUnderTest = unitUnderTestBuilder.Build();
            var freeAsset = new ServiceModel { AccountId = 1234123, Amount = 333 };

            await unitUnderTest.Create(freeAsset);

            A.CallTo(() => repoFake.GetByAccountId(freeAsset.AccountId)).MustHaveHappened();
        }

        [Fact]
        public async Task FreeAssetIsNotNull_RepoReturnsNull_CallCreateWithSameValues()
        {
            var unitUnderTestBuilder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var repoFake = unitUnderTestBuilder.GetMock<IFreeAssetsRepository>();
            A.CallTo(() => repoFake.GetByAccountId(A<long>._)).Returns((DbModel) null);
            ProjectionFreeAssetsService unitUnderTest = unitUnderTestBuilder.Build();
            var freeAsset = new ServiceModel { AccountId = 1234123, Amount = 333 };
            
            await unitUnderTest.Create(freeAsset);

            A.CallTo(() => repoFake.CreateEntry(A<DbModel>.That.Matches(f => f.AccountId == freeAsset.AccountId)))
                .MustHaveHappened();
            A.CallTo(() => repoFake.CreateEntry(A<DbModel>.That.Matches(f => f.Amount == freeAsset.Amount)))
                .MustHaveHappened();
        }

        [Fact]
        public async Task FreeAssetIsNotNull_RepoReturnsValue_DoesNotCallCreate()
        {
            var unitUnderTestBuilder = new UnitUnderTestBuilder<ProjectionFreeAssetsService>();
            var repoFake = unitUnderTestBuilder.GetMock<IFreeAssetsRepository>();
            A.CallTo(() => repoFake.GetByAccountId(A<long>._)).Returns(new DbModel());
            ProjectionFreeAssetsService unitUnderTest = unitUnderTestBuilder.Build();
            var freeAsset = new ServiceModel { AccountId = 1234123, Amount = 333 };
            
            await unitUnderTest.Create(freeAsset);

            A.CallTo(() => repoFake.CreateEntry(A<DbModel>._)).MustNotHaveHappened();
        }
    }
}
