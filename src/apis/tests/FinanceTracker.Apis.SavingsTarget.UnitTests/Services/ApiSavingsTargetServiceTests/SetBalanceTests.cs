﻿using System;
using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Testing.FakeItEasy;
using RepoSavingsTarget = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ApiSavingsTargetServiceTests
{
    public class SetBalanceTests
    {
        [Fact]
        public async Task SetBalanceIsNull_Throws()
        {
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>().Build();

            await Assert.ThrowsAsync<ArgumentNullException>(() => unitUnderTest.SetBalance(null));
        }

        [Fact]
        public async Task SetBalanceIsNotNull__SendsDataToTheProducer()
        {
            var data = new SetBalance { SavingsTargetId = 1234, Balance = (decimal) 33.2 };
            var builder = new UnitUnderTestBuilder<ApiSavingsTargetService>();
            var fakeProducer = builder.GetMock<ISavingsTargetProducer>();
            ApiSavingsTargetService unitUnderTest = builder.Build();

            await unitUnderTest.SetBalance(data);

            A.CallTo(
                    () => fakeProducer.SavingsTargetBalanceSet(
                        A<SetBalance>.That.Matches(s => s.SavingsTargetId == data.SavingsTargetId)))
                .MustHaveHappenedOnceExactly();
            A.CallTo(
                    () => fakeProducer.SavingsTargetBalanceSet(
                        A<SetBalance>.That.Matches(s => s.Balance == data.Balance)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task SetBalanceIsNotNull_RepoReturns_ReturnsNull()
        {
            var data = new SetBalance { SavingsTargetId = 1234, Balance = (decimal) 33.2 };
            var fakeRepo = A.Fake<ISavingsTargetRepository>();
            var repoData = new RepoSavingsTarget { Id = 23423, Name = "name", Description = "description" };
            A.CallTo(() => fakeRepo.GetById(A<long>._)).Returns(repoData);
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With<ISavingsTargetRepository>(fakeRepo)
                .Build();

            ServiceSavingsTarget actual = await unitUnderTest.SetBalance(data);

            Assert.Equal(repoData.Id, actual.Id);
            Assert.Equal(repoData.Name, actual.Name);
            Assert.Equal(repoData.Description, actual.Description);
        }

        [Fact]
        public async Task RepoReturnsNull__ReturnsNull()
        {
            var data = new SetBalance { SavingsTargetId = 1234, Balance = (decimal) 33.2 };
            var fakeRepo = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeRepo.GetById(A<long>._)).Returns((RepoSavingsTarget) null);
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With<ISavingsTargetRepository>(fakeRepo)
                .Build();

            ServiceSavingsTarget actual = await unitUnderTest.SetBalance(data);

            Assert.Null(actual);
        }

        [Fact]
        public void SetBalanceIsNotNull__CallsGetByIdWithTheSameId()
        {
            var data = new SetBalance { SavingsTargetId = 1234, Balance = (decimal) 33.2 };
            var builder = new UnitUnderTestBuilder<ApiSavingsTargetService>();
            var fakeRepo = builder.GetMock<ISavingsTargetRepository>();
            ApiSavingsTargetService unitUnderTest = builder.Build();

            unitUnderTest.SetBalance(data);

            A.CallTo(() => fakeRepo.GetById(data.SavingsTargetId)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task RepoReturnsNull__DoesNotCallTheProducer()
        {
            var data = new SetBalance { SavingsTargetId = 1234, Balance = (decimal) 33.2 };
            var fakeRepo = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeRepo.GetById(A<long>._)).Returns((RepoSavingsTarget) null);
            IUnitUnderTestBuilder<ApiSavingsTargetService> builder = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With<ISavingsTargetRepository>(fakeRepo);
            var fakeProducer = builder.GetMock<ISavingsTargetProducer>();
            ApiSavingsTargetService unitUnderTest = builder.Build();

            await unitUnderTest.SetBalance(data);

            A.CallTo(() => fakeProducer.SavingsTargetBalanceSet(A<SetBalance>._)).MustNotHaveHappened();
        }
    }
}
