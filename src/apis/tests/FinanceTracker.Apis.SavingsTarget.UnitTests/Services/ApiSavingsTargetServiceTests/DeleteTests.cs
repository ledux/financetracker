﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ApiSavingsTargetServiceTests
{
    public class DeleteTests
    {
        private const long Id = 6663534;

        [Fact]
        public async Task IdIsZero_Throws()
        {
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .AddTracingMocks()
                .Build();

            await Assert.ThrowsAsync<ArgumentException>(() => unitUnderTest.Delete(0));
        }

        [Fact]
        public async Task IdIsNotZero__CallsTheProducerWithTheSameId()
        {
            (ApiSavingsTargetService unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<ISavingsTargetProducer> producerMock = mockBag.GetMock<ISavingsTargetProducer>();

            await unitUnderTest.Delete(Id);

            producerMock.Verify(p => p.SavingsTargetDeleted(Id));
        }
    }
}
