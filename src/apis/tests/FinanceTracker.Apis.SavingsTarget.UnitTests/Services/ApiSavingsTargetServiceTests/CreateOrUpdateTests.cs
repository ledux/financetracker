﻿using System;
using System.Threading.Tasks;
using AutoBogus;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Exceptions;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Apis.SavingsTarget.Repositories;
using FinanceTracker.Apis.SavingsTarget.Repositories.Models;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.IdCreator;
using FinanceTracker.Common.Testing.Extensions;
using FinanceTracker.Common.Testing.FakeItEasy;
using MoqBuilder =  FinanceTracker.Common.Testing;
using ServiceModelWrite = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTargetWrite;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using RepoModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.ApiSavingsTargetServiceTests
{
    public class CreateOrUpdateTests
    {
        [Fact]
        public async Task SavingsTargetIsNull_Throws()
        {
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>().Build();

            await Assert.ThrowsAsync<ArgumentNullException>(() => unitUnderTest.CreateOrUpdate(null));
        }

        [Fact]
        public async Task SavingsTargetIsNotNull_ServiceReturnsNull_AssignsId()
        {
            const long createdId = 4561341533;
            var idCreatorMock = A.Fake<IIdCreator>();
            A.CallTo(() => idCreatorMock.CreateId()).Returns(createdId);
            var fakeRepo = A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeRepo.GetById(A<long>._)).Returns((RepoModel) null);
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With(idCreatorMock)
                .With(fakeRepo)
                .Build();

            ServiceModel actual = await unitUnderTest.CreateOrUpdate(new ServiceModelWrite());

            Assert.Equal(createdId, actual.Id);
        }

        [Fact]
        public async Task SavingsTargetNotNull__CallsProducerWithSameData()
        {
            var data = AutoFaker.Generate<ServiceModelWrite>();
            var idCreatorMock = new Mock<IIdCreator>();

            var freeAssetsRepoMock = new Mock<IFreeAssetsRepository>();
            freeAssetsRepoMock.Setup(r => r.GetByAccountId(It.IsAny<long>())).ReturnsAsync(AutoFaker.Generate<FreeAssets>());

            const long createdId = 4561341533;
            idCreatorMock.Setup(i => i.CreateId()).Returns(createdId);
            (ApiSavingsTargetService unitUnderTest, MoqBuilder.MockBag mockBag) = new MoqBuilder.UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With<IIdCreator>(idCreatorMock.Object)
                .With<IFreeAssetsRepository>(freeAssetsRepoMock.Object)
                .AddTracingMocks()
                .BuildBoth();
            Mock<ISavingsTargetProducer> producerMock = mockBag.GetMock<ISavingsTargetProducer>();

            await unitUnderTest.CreateOrUpdate(data);

            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.Id == createdId)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.AccountId == data.AccountId)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.Name == data.Name)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.Description == data.Description)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.StartAmount == data.StartAmount)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.TargetAmount == data.TargetAmount)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.StartDate == data.StartDate)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(m => m.TargetDate == data.TargetDate)));
        }

        [Fact]
        public async Task ServiceReturnsData__CallsProducerWithDataFromService()
        {
            const long id = 123412341234;
            RepoModel repoData = new AutoFaker<RepoModel>().RuleFor(f => f.Id, id).Generate();
            ServiceModelWrite writeData = new AutoFaker<ServiceModelWrite>().RuleFor(f => f.Id, id).Generate();

            var repoMock = new Mock<ISavingsTargetRepository>();
            repoMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(repoData);

            var freeAssetsRepoMock = new Mock<IFreeAssetsRepository>();
            freeAssetsRepoMock.Setup(r => r.GetByAccountId(It.IsAny<long>())).ReturnsAsync(AutoFaker.Generate<FreeAssets>());

            (ApiSavingsTargetService unitUnderTest, MoqBuilder.MockBag mockBag) = new MoqBuilder.UnitUnderTestBuilder<ApiSavingsTargetService>()
                .AddTracingMocks()
                .With<ISavingsTargetRepository>(repoMock.Object)
                .With<IFreeAssetsRepository>(freeAssetsRepoMock.Object)
                .BuildBoth();
            Mock<ISavingsTargetProducer> producerMock = mockBag.GetMock<ISavingsTargetProducer>();

            await unitUnderTest.CreateOrUpdate(writeData);

            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.Id == repoData.Id)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.Name == writeData.Name)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.Description == writeData.Description)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.TargetAmount == writeData.TargetAmount)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.TargetDate == writeData.TargetDate)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.AccountId == repoData.AccountId)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.StartAmount == repoData.StartAmount)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.StartDate == repoData.StartDate)));
            producerMock.Verify(p => p.SavingsTargetUpdated(It.Is<ServiceModel>(e => e.Balance == repoData.Balance)));
        }

        [Fact]
        public async Task SavingsTargetNotNull__CallsGetByAccountId()
        {
            var accountId = AutoFaker.Generate<long>();
            ServiceModelWrite writeData = new AutoFaker<ServiceModelWrite>().RuleFor(f => f.AccountId, accountId).Generate();
            var builder = new UnitUnderTestBuilder<ApiSavingsTargetService>();
            var fakeFreeAssetsRepo = builder.GetMock<IFreeAssetsRepository>();
            var fakeSavingsTargetRepo = builder.GetMock<ISavingsTargetRepository>();
            A.CallTo(() => fakeSavingsTargetRepo.GetById(A<long>._)).Returns((RepoModel) null);
            ApiSavingsTargetService unitUnderTest = builder.Build();

            await unitUnderTest.CreateOrUpdate(writeData);

            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(accountId)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task AFreeAssetDoesNotExists__Throws()
        {
            ServiceModelWrite writeData = new AutoFaker<ServiceModelWrite>().Generate();
            var fakeRepo = A.Fake<IFreeAssetsRepository>();
            A.CallTo(() => fakeRepo.GetByAccountId(A<long>._)).Returns((FreeAssets) null);
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With(fakeRepo)
                .Build();

            await Assert.ThrowsAsync<NotFoundException>(() => unitUnderTest.CreateOrUpdate(writeData));
        }

        [Fact]
        public async Task SavingsTargetDoesNotExistYet__BalanceIsSetToStartAmount()
        {
            var serviceData = AutoFaker.Generate<ServiceModelWrite>();
            var fakeRepo = A.Fake<ISavingsTargetRepository>();
            var fakeProducer = A.Fake<ISavingsTargetProducer>();
            A.CallTo(() => fakeRepo.GetById(A<long>._)).Returns((RepoModel) null);
            ApiSavingsTargetService unitUnderTest = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With(fakeRepo)
                .With(fakeProducer)
                .Build();

            await unitUnderTest.CreateOrUpdate(serviceData);

            A.CallTo(() => 
                fakeProducer.SavingsTargetUpdated(
                    A<ServiceModel>.That.Matches(s => s.Balance == serviceData.StartAmount)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task SavingsTargetDoesExists__CallsGetByAccountIdWithIdFromExistingSavingsTarget()
        {
            var repoData = AutoFaker.Generate<RepoModel>();
            var fakeSavingsTargetRepository= A.Fake<ISavingsTargetRepository>();
            A.CallTo(() => fakeSavingsTargetRepository.GetById(A<long>._)).Returns(repoData);
            IUnitUnderTestBuilder<ApiSavingsTargetService> builder = new UnitUnderTestBuilder<ApiSavingsTargetService>()
                .With(fakeSavingsTargetRepository);
            var fakeFreeAssetsRepo = builder.GetMock<IFreeAssetsRepository>();
            ApiSavingsTargetService unitUnderTest = builder.Build();

            await unitUnderTest.CreateOrUpdate(AutoFaker.Generate<ServiceModelWrite>());

            A.CallTo(() => fakeFreeAssetsRepo.GetByAccountId(repoData.AccountId)).MustHaveHappenedOnceExactly();
        }
    }
}
