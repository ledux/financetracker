﻿using System;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using RepoModel = FinanceTracker.Apis.SavingsTarget.Repositories.Models.SavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.Models.AccountSavingsTargetTests
{
    public class CtorTests
    {
        [Fact]
        public void DbModelIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new AccountSavingsTarget(null));
        }

        [Fact]
        public void DbModelIsNotNull__CopiesValuesOver()
        {
            var dbModel = new RepoModel
            {
                Id = 1234123,
                AccountId = 9878797,
                Name = "Name",
                Description = "Description",
                TargetAmount = 1000,
                TargetDate = new DateTime(DateTime.Today.Year + 1, 3, 1),
                StartAmount = 100,
                Balance = 200,
                StartDate = new DateTime(DateTime.Today.Year - 1, 5, 1)
            };

            var actual = new AccountSavingsTarget(dbModel);

            Assert.Equal(dbModel.Id, actual.Id);
            Assert.Equal(dbModel.Name, actual.Name);
            Assert.Equal(dbModel.Description, actual.Description);
            Assert.Equal(dbModel.TargetAmount, actual.TargetAmount);
            Assert.Equal(dbModel.TargetDate, actual.TargetDate);
            Assert.Equal(dbModel.StartAmount, actual.StartAmount);
            Assert.Equal(dbModel.Balance, actual.Balance);
            Assert.Equal(dbModel.StartDate, actual.StartDate);
        }
    }
}
