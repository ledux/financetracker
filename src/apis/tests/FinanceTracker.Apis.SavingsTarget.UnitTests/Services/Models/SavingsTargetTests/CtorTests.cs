﻿using System;
using Xunit;
using ServiceModelWrite = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTargetWrite;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Services.Models.SavingsTargetTests
{
    public class CtorTests
    {
        private readonly ServiceModelWrite _writeModel;

        public CtorTests()
        {
            _writeModel = new ServiceModelWrite
            {
                Id = 12341234,
                AccountId = 99898989,
                Name = "name",
                Description = "Description of model",
                TargetDate = new DateTime(2021, 2, 15),
                StartDate = new DateTime(2020, 9, 20),
                TargetAmount = 2000,
                StartAmount = (decimal) 20.2
            };
        }

        [Fact]
        public void SavingsTargetWriteIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new ServiceModel(null));
        }

        [Fact]
        public void SavingsTargetWriteIsNotNull__CopiesAllData()
        {
            var actual = new ServiceModel(_writeModel);

            Assert.Equal(_writeModel.Id, actual.Id);
            Assert.Equal(_writeModel.AccountId, actual.AccountId);
            Assert.Equal(_writeModel.Name, actual.Name);
            Assert.Equal(_writeModel.Description, actual.Description);
            Assert.Equal(_writeModel.TargetDate, actual.TargetDate);
            Assert.Equal(_writeModel.StartDate, actual.StartDate);
            Assert.Equal(_writeModel.TargetAmount, actual.TargetAmount);
            Assert.Equal(_writeModel.StartAmount, actual.StartAmount);
        }

        [Fact]
        public void SavingsTargetWriteIsNotNull__PropertiesMissingOnWriteModelAreDefault()
        {
            var actual = new ServiceModel(_writeModel);

            Assert.Equal(default, actual.Balance);
        }
    }
}
