﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Controllers;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using ServiceWriteModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTargetWrite;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using WebModel = FinanceTracker.Apis.SavingsTarget.Controllers.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Controllers.SavingsTargetControllerTests

{
    public class GetByIdTests
    {
        private readonly ServiceModel _serviceData;
        private const long Id = 989891234;

        public GetByIdTests()
        {
            _serviceData = new ServiceModel
            {
                Id = 1234123,
                AccountId = 9898312,
                Name = "Name of the target",
                Description = "Description of the target",
                StartAmount = 200,
                TargetAmount = 1000,
                TargetDate = new DateTime(2022, 3, 13),
                Balance = 400,
                StartDate = new DateTime(2020, 5, 12)
            };
        }

        [Fact]
        public async Task ServiceReturnsNull_ReturnsNotFound()
        {
            var serviceMock = new Mock<ISavingsTargetService<ServiceWriteModel>>();
            serviceMock.Setup(s => s.GetById(It.IsAny<long>()))
                .ReturnsAsync((SavingsTarget.Services.Models.SavingsTarget) null);

            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<ServiceWriteModel>>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.GetById(Id);

            Assert.IsType<NotFoundResult>(actual);
        }

        [Fact]
        public async Task IdIsZero__ReturnBadRequest()
        {
            var serviceMock = new Mock<ISavingsTargetService<ServiceWriteModel>>();
            serviceMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(_serviceData);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<ServiceWriteModel>>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.GetById(0);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero__CallsServiceWithSameId()
        {
            (SavingsTargetController unitUnderTest, MockBag mockBag) = 
                new UnitUnderTestBuilder<SavingsTargetController>().BuildBoth();
            Mock<ISavingsTargetService<ServiceWriteModel>> serviceMock =
                mockBag.GetMock<ISavingsTargetService<ServiceWriteModel>>();

            await unitUnderTest.GetById(Id);

            serviceMock.Verify(s => s.GetById(Id));
        }

        [Fact]
        public async Task ServiceReturnsData__ReturnsSameData()
        {
            var serviceMock = new Mock<ISavingsTargetService<ServiceWriteModel>>();
            serviceMock.Setup(s => s.GetById(It.IsAny<long>())).ReturnsAsync(_serviceData);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<ServiceWriteModel>>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.GetById(Id);

            Assert.IsType<OkObjectResult>(actual);
            var result = (OkObjectResult) actual;
            Assert.IsType<WebModel>(result.Value);
            var data = (WebModel) result.Value;
            Assert.Equal(_serviceData.Id.ToString(), data.Id);
            Assert.Equal(_serviceData.AccountId.ToString(), data.AccountId);
            Assert.Equal(_serviceData.Name, data.Name);
            Assert.Equal(_serviceData.Description, data.Description);
            Assert.Equal(_serviceData.Balance, data.Balance);
            Assert.Equal(_serviceData.StartDate, data.StartDate);
            Assert.Equal(_serviceData.TargetDate, data.TargetDate);
            Assert.Equal(_serviceData.StartAmount, data.StartAmount);
            Assert.Equal(_serviceData.TargetAmount, data.TargetAmount);
        }
    }
}
