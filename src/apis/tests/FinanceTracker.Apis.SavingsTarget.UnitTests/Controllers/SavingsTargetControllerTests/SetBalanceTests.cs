﻿using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Controllers;
using FinanceTracker.Apis.SavingsTarget.Controllers.Models;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Testing.FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using WebSetBalance = FinanceTracker.Apis.SavingsTarget.Controllers.Models.SetBalance;
using ServiceSetBalance = FinanceTracker.Apis.SavingsTarget.Services.Models.SetBalance;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Controllers.SavingsTargetControllerTests
{
    public class SetBalanceTests
    {
        private const long Id = 234133551;

        [Fact]
        public async Task ServiceReturnsNull_ReturnsNotFound()
        {
            var fakeService = A.Fake<ISavingsTargetService<SavingsTargetWrite>>();
            A.CallTo(() => fakeService.SetBalance(A<ServiceSetBalance>._)).Returns((ServiceSavingsTarget) null);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<SavingsTargetWrite>>(fakeService)
                .Build();

            IActionResult actual = await unitUnderTest.SetBalance(Id, new WebSetBalance());

            Assert.IsType<NotFoundResult>(actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-1324)]
        [InlineData(-8823)]
        public async Task SavingsTargetIdIsZeroOrLes__ReturnsBadRequest(long savingsTargetId)
        {
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>().Build();
            
            IActionResult actual = await unitUnderTest.SetBalance(savingsTargetId, new WebSetBalance());

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Theory]
        [InlineData(1234452, 333)]
        [InlineData(881234, 8823.22)]
        public async Task SetBalanceIsNotNull_IdIsBiggerThanZero_CallsServiceWithSameData(long savingsTargetId, decimal balance)
        {
            var webData = new WebSetBalance { Balance = balance };
            var builder = new UnitUnderTestBuilder<SavingsTargetController>();
            var fakeService = builder.GetMock<ISavingsTargetService<SavingsTargetWrite>>();
            SavingsTargetController unitUnderTest = builder.Build();

            await unitUnderTest.SetBalance(savingsTargetId, webData);

            A.CallTo(() => 
                    fakeService.SetBalance(A<ServiceSetBalance>.That.Matches(s => s.SavingsTargetId == savingsTargetId)))
                .MustHaveHappenedOnceExactly();

            A.CallTo(() => 
                    fakeService.SetBalance(A<ServiceSetBalance>.That.Matches(s => s.Balance == balance)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task ServiceReturnsNotNull__ReturnsAccepted()
        {
            var fakeService = A.Fake<ISavingsTargetService<SavingsTargetWrite>>();
            var serviceData = new ServiceSavingsTarget();
            A.CallTo(() => fakeService.SetBalance(A<ServiceSetBalance>._)).Returns(serviceData);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<SavingsTargetWrite>>(fakeService)
                .Build();

            IActionResult actual = await unitUnderTest.SetBalance(Id, new WebSetBalance());

            Assert.IsType<NoContentResult>(actual);
        }
    }
}
