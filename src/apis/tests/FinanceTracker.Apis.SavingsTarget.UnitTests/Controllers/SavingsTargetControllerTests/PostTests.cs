﻿using System.Threading.Tasks;
using AutoBogus;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Controllers;
using FinanceTracker.Apis.SavingsTarget.Controllers.Models;
using FinanceTracker.Apis.SavingsTarget.Exceptions;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing.FakeItEasy;
using MoqBuilder = FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using ServiceModelWrite = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTargetWrite;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using WebModel = FinanceTracker.Apis.SavingsTarget.Controllers.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Controllers.SavingsTargetControllerTests
{
    public class PostTests
    {
        private readonly PostModel _data;

        public PostTests()
        {
            _data = new AutoFaker<PostModel>().RuleFor(m => m.AccountId, faker => faker.Random.Long().ToString());
        }

        [Fact]
        public async Task PostModelNotNull_CallsServiceWithIdZero()
        {
            (SavingsTargetController unitUnderTest, MoqBuilder.MockBag mockBag) = new MoqBuilder.UnitUnderTestBuilder<SavingsTargetController>()
                .BuildBoth();
            Mock<ISavingsTargetService<ServiceModelWrite>> serviceMock = mockBag.GetMock<ISavingsTargetService<ServiceModelWrite>>();
            serviceMock.Setup(s => s.CreateOrUpdate(It.IsAny<ServiceModelWrite>())).ReturnsAsync(new ServiceModel());

            await unitUnderTest.Post(new PostModel());

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.Id == 0)));
        }

        [Fact]
        public async Task PostModelNotNull_CallsServiceWithSameData()
        {
            (SavingsTargetController unitUnderTest, MoqBuilder.MockBag mockBag) = new MoqBuilder.UnitUnderTestBuilder<SavingsTargetController>()
                .BuildBoth();
            Mock<ISavingsTargetService<ServiceModelWrite>> serviceMock = mockBag.GetMock<ISavingsTargetService<ServiceModelWrite>>();
            serviceMock.Setup(s => s.CreateOrUpdate(It.IsAny<ServiceModelWrite>())).ReturnsAsync(new ServiceModel());

            await unitUnderTest.Post(_data);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.AccountId.ToString() == _data.AccountId)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.Name == _data.Name)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.Description == _data.Description)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.TargetDate == _data.TargetDate)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.StartDate == _data.StartDate)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.TargetAmount == _data.TargetAmount)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<ServiceModelWrite>(m => m.StartAmount == _data.StartAmount)));
        }

        [Fact]
        public async Task ServiceReturnsNotNull__Returns202WithData()
        {
            const long createdId = 12341234;
            ServiceModel returnValue = new AutoFaker<ServiceModel>().RuleFor(m => m.Id, createdId).Generate();
            var serviceMock = new Mock<ISavingsTargetService<ServiceModelWrite>>();
            serviceMock.Setup(s => s.CreateOrUpdate(It.IsAny<ServiceModelWrite>())).ReturnsAsync(returnValue);
            SavingsTargetController unitUnderTest = new MoqBuilder.UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<ServiceModelWrite>>(serviceMock.Object)
                .Build();

            IActionResult actionResult = await unitUnderTest.Post(_data);

            Assert.IsType<AcceptedAtActionResult>(actionResult);
            var acceptedResult = (AcceptedAtActionResult) actionResult;
            Assert.IsType<WebModel>(acceptedResult.Value);
            var acceptedData = (WebModel) acceptedResult.Value;
            Assert.Equal(createdId.ToString(), acceptedData.Id);
        }

        [Fact]
        public async Task ServiceThrowsNotFoundException__ReturnsBadRequest()
        {
            var fakeService = A.Fake<ISavingsTargetService<ServiceModelWrite>>();
            A.CallTo(() => fakeService.CreateOrUpdate(A<ServiceModelWrite>._)).ThrowsAsync(new NotFoundException());

            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With(fakeService)
                .Build();

            IActionResult actionResult = await unitUnderTest.Post(_data);

            Assert.IsType<BadRequestObjectResult>(actionResult);
        }
    }
}
