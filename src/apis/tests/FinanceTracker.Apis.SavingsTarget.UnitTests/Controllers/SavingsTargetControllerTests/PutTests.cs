﻿using System;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Controllers;
using FinanceTracker.Apis.SavingsTarget.Controllers.Models;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Controllers.SavingsTargetControllerTests
{
    public class PutTests
    {
        private readonly PutModel _putData;
        private const long Id = 45523243;

        public PutTests()
        {
            _putData = new PutModel
            {
                Name = "New name",
                Description = "New description",
                TargetAmount = 2000,
                TargetDate = new DateTime(2022, 5, 1)
            };
        }

        [Fact]
        public async Task IdIsZero_ReturnsBadRequest()
        {
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .Build();

            IActionResult actual = await unitUnderTest.Put(0, _putData);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero__CallsServiceWithModelWithSameId()
        {
            (SavingsTargetController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<SavingsTargetController>()
                .BuildBoth();
            Mock<ISavingsTargetService<SavingsTargetWrite>> serviceMock = mockBag.GetMock<ISavingsTargetService<SavingsTargetWrite>>();

            await unitUnderTest.Put(Id, _putData);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.Id == Id)));
        }

        [Fact]
        public async Task ServiceReturnsNull__ReturnsNotFound()
        {
            var serviceMock = new Mock<ISavingsTargetService<SavingsTargetWrite>>();
            serviceMock
                .Setup(s => s.CreateOrUpdate(It.IsAny<SavingsTargetWrite>()))
                .ReturnsAsync((ServiceModel) null);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<SavingsTargetWrite>>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.Put(Id, _putData);

            Assert.IsType<NotFoundObjectResult>(actual);
        }

        [Fact]
        public async Task ServiceReturnsData__ReturnsNoContent()
        {
            var serviceMock = new Mock<ISavingsTargetService<SavingsTargetWrite>>();
            serviceMock
                .Setup(s => s.CreateOrUpdate(It.IsAny<SavingsTargetWrite>()))
                .ReturnsAsync(new ServiceModel());
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<SavingsTargetWrite>>(serviceMock.Object)
                .Build();

            IActionResult actual = await unitUnderTest.Put(Id, _putData);

            Assert.IsType<NoContentResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero_DataIsNull_ReturnsBadRequest()
        {
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .Build();

            IActionResult actual = await unitUnderTest.Put(Id, null);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero_DataIsNotNull_SendsDataToService()
        {
            var serviceMock = new Mock<ISavingsTargetService<SavingsTargetWrite>>();
            serviceMock
                .Setup(s => s.CreateOrUpdate(It.IsAny<SavingsTargetWrite>()))
                .ReturnsAsync(new ServiceModel());
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With<ISavingsTargetService<SavingsTargetWrite>>(serviceMock.Object)
                .Build();

            await unitUnderTest.Put(Id, _putData);

            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.Description == _putData.Description)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.Name == _putData.Name)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.TargetAmount == _putData.TargetAmount)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.TargetDate == _putData.TargetDate)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.Id == Id)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.AccountId == default)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.StartDate == default)));
            serviceMock.Verify(s => s.CreateOrUpdate(It.Is<SavingsTargetWrite>(m => m.StartAmount == default)));
        }
    }
}
