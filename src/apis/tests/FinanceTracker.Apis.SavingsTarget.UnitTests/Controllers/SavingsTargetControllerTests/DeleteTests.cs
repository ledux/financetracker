﻿using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Controllers;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Testing;
using Microsoft.AspNetCore.Mvc;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Controllers.SavingsTargetControllerTests
{
    public class DeleteTests
    {
        private const long Id = 5552343;

        [Fact]
        public async Task IdIsZero_ReturnsBadRequest()
        {
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .Build();

            IActionResult actual = await unitUnderTest.Delete(0);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero__CallsServiceWithTheSameId()
        {
            (SavingsTargetController unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<SavingsTargetController>()
                .BuildBoth();
            Mock<ISavingsTargetService<SavingsTargetWrite>> serviceMock = mockBag.GetMock<ISavingsTargetService<SavingsTargetWrite>>();

            await unitUnderTest.Delete(Id);

            serviceMock.Verify(s => s.Delete(Id));
        }

        [Fact]
        public async Task IdIsNotZero__ReturnsNoContent()
        {
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .Build();

            IActionResult actual = await unitUnderTest.Delete(Id);

            Assert.IsType<NoContentResult>(actual);
        }
    }
}
