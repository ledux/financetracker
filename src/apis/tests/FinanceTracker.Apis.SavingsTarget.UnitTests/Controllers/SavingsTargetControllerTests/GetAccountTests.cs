﻿using System.Linq;
using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Controllers;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Testing.FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using ServiceSavingsTargetWrite = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTargetWrite;
using ServiceAccount = FinanceTracker.Apis.SavingsTarget.Services.Models.Account;
using ServiceAccountSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.AccountSavingsTarget;
using WebAccount = FinanceTracker.Apis.SavingsTarget.Controllers.Models.Account;
using WebAccountSavingsTarget = FinanceTracker.Apis.SavingsTarget.Controllers.Models.AccountSavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Controllers.SavingsTargetControllerTests
{
    public class GetAccountTests
    {
        private const long AccountId = 9891234;

        [Fact]
        public async Task AccountIdIsZero_ReturnsBadRequest()
        {
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>().Build();

            IActionResult actual = await unitUnderTest.GetAccount(0);

            Assert.IsType<BadRequestObjectResult>(actual);
        }

        [Fact]
        public async Task IdIsNotZero__CallsTheServiceWithSameId()
        {
            var builder = new UnitUnderTestBuilder<SavingsTargetController>();
            var fakeService = builder.GetMock<ISavingsTargetService<ServiceSavingsTargetWrite>>();
            SavingsTargetController unitUnderTest = builder.Build();

            await unitUnderTest.GetAccount(AccountId);

            A.CallTo(() => fakeService.GetByAccountId(AccountId)).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task IdIsNotZero_ServiceReturnsData_ReturnsSameData()
        {
            ServiceAccount serviceData = CreateTestData();
            var fakeService = A.Fake<ISavingsTargetService<ServiceSavingsTargetWrite>>();
            A.CallTo(() => fakeService.GetByAccountId(A<long>._)).Returns(serviceData);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With(fakeService)
                .Build();

            IActionResult actual = await unitUnderTest.GetAccount(AccountId);

            Assert.IsType<OkObjectResult>(actual);
            var result = (OkObjectResult)actual;
            Assert.IsType<WebAccount>(result.Value);
            var data = result.Value as WebAccount;

            Assert.NotNull(data);
            Assert.Equal(serviceData.FreeAssets, data.FreeAssets);
            Assert.Equal(serviceData.Id.ToString(), data.Id);
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.Id.ToString() == target.Id)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.Name == target.Name)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.Description == target.Description)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.Balance == target.Balance)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.StartAmount == target.StartAmount)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.TargetAmount == target.TargetAmount)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.StartDate == target.StartDate)));
            Assert.All(data.SavingsTargets, target => Assert.True(serviceData.SavingsTargets.Any(savingsTarget => savingsTarget.TargetDate == target.TargetDate)));
        }

        [Fact]
        public async Task IdIsNotZero_ServiceReturnsNull_ReturnsNotFound()
        {
            var fakeService = A.Fake<ISavingsTargetService<ServiceSavingsTargetWrite>>();
            A.CallTo(() => fakeService.GetByAccountId(A<long>._)).Returns((ServiceAccount)null);
            SavingsTargetController unitUnderTest = new UnitUnderTestBuilder<SavingsTargetController>()
                .With(fakeService)
                .Build();

            IActionResult actual = await unitUnderTest.GetAccount(AccountId);

            Assert.IsType<NotFoundResult>(actual);
        }

        private static ServiceAccount CreateTestData()
        {
            return new ServiceAccount
            {
                FreeAssets = 200,
                Id = 1234234,
                SavingsTargets = Enumerable.Range(0, 5).Select(id =>
                    new ServiceAccountSavingsTarget
                    {
                        Id = id,
                        Name = $"Name {id}",
                        Description = $"Description {id}",

                    }).ToArray()
            };
        }
    }
}
