﻿using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing.FakeItEasy;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.AccountingEntryUpdatedProjectionTests
{
    public class HandleTests
    {
        [Fact]
        public async Task EventDataNotNull_CallsUpdateWithAccountIdAndAmount()
        {
            var builder = new UnitUnderTestBuilder<AccountingEntryUpdatedProjection>();
            var fakeService = builder.GetMock<IFreeAssetsService>();
            var eventData = new AccountingEntryUpdated { AccountId = 1234123, Amount = (decimal) 33.2, Id = 9898123, };
            AccountingEntryUpdatedProjection unitUnderTest = builder.Build();

            await unitUnderTest.Handle(eventData);

            A.CallTo(() => fakeService.Update(eventData.AccountId, eventData.Amount)).MustHaveHappened();
        }
    }
}
