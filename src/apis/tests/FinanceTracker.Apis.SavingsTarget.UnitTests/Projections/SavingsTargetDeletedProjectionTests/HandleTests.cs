﻿using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using FakeItEasyBuilder = FinanceTracker.Common.Testing.FakeItEasy;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.SavingsTargetDeletedProjectionTests
{
    public class HandleTests
    {
        private const long Id = 22345323;

        [Fact]
        public async Task EventIsNotNull_CallsTheService()
        {
            (SavingsTargetDeletedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<SavingsTargetDeletedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<ISavingsTargetService<ServiceModel>> serviceMock = mockBag.GetMock<ISavingsTargetService<ServiceModel>>();
            var eventData = new SavingsTargetDeleted { SavingsTargetId = Id };

            await unitUnderTest.Handle(eventData);

            serviceMock.Verify(s => s.Delete(Id));
        }

        [Fact]
        public async Task EventIsNotNull__CallsUpdateOnFreeAssetsServiceWithBalanceZero()
        {
            (SavingsTargetDeletedProjection unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<SavingsTargetDeletedProjection>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IFreeAssetsService> serviceMock = mockBag.GetMock<IFreeAssetsService>();
            var eventData = new SavingsTargetDeleted { SavingsTargetId = Id };

            await unitUnderTest.Handle(eventData);

            serviceMock.Verify(s => s.Update(It.Is<SavingsTargetBalance>(t => t.SavingsTargetId == Id)));
            serviceMock.Verify(s => s.Update(It.Is<SavingsTargetBalance>(t => t.Balance == 0)));
        }

        [Fact]
        public async Task EventIsNotNull__CallsUpdateThenDelete()
        {
            var builder = new FakeItEasyBuilder.UnitUnderTestBuilder<SavingsTargetDeletedProjection>();
            var fakeFreeAssetsService = builder.GetMock<IFreeAssetsService>();
            var fakeSavingsTargetService = builder.GetMock<ISavingsTargetService<ServiceModel>>();
            SavingsTargetDeletedProjection unitUnderTest = builder.Build();
            var eventData = new SavingsTargetDeleted { SavingsTargetId = Id };

            await unitUnderTest.Handle(eventData);

            A.CallTo(() => fakeFreeAssetsService.Update(A<SavingsTargetBalance>._))
                .MustHaveHappenedOnceExactly()
                .Then(A.CallTo(() => fakeSavingsTargetService.Delete(A<long>._))
                    .MustHaveHappenedOnceExactly());
        }
    }
}
