﻿using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing.FakeItEasy;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.AccountUpdatedProjectionTests
{
    public class HandleTests
    {
        [Fact]
        public async Task EventIsNotNull_CallCreateWithIdAndBalance()
        {
            var eventData = new AccountUpdated { Id = 98921234, Balance = 2234 };
            var testBuilder = new UnitUnderTestBuilder<AccountUpdatedProjection>();
            var fakeFreeAssetsService = testBuilder.GetMock<IFreeAssetsService>();
            AccountUpdatedProjection unitUnderTest = testBuilder.Build();

            await unitUnderTest.Handle(eventData);

            A.CallTo(() => fakeFreeAssetsService.Create(A<FreeAssets>.That.Matches(f => f.AccountId == eventData.Id)))
                .MustHaveHappened();
            A.CallTo(() => fakeFreeAssetsService.Create(A<FreeAssets>.That.Matches(f => f.Amount == eventData.Balance)))
                .MustHaveHappened();
        }
    }
}
