﻿using System.Threading.Tasks;
using AutoBogus;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing.FakeItEasy;
using ServiceSavingsTarget = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.SavingsTargetBalanceSetProjectionTests
{
    public class HandleTests
    {
        [Fact]
        public async Task EventIsNotNull_SendsDataToTheService()
        {
            var eventData = new SavingsTargetBalanceSet { SavingsTargetId = 9812341, Balance = (decimal) 33.2 };
            var builder = new UnitUnderTestBuilder<SavingsTargetBalanceSetProjection>();
            var fakeService = builder.GetMock<ISavingsTargetService<ServiceSavingsTarget>>();
            SavingsTargetBalanceSetProjection unitUnderTest = builder.Build();

            await unitUnderTest.Handle(eventData);

            A.CallTo(
                    () => fakeService.SetBalance(
                        A<SetBalance>.That.Matches(s => s.Balance == eventData.Balance)))
                .MustHaveHappenedOnceExactly();
            A.CallTo(
                    () => fakeService.SetBalance(
                        A<SetBalance>.That.Matches(s => s.SavingsTargetId == eventData.SavingsTargetId)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task EventIsNotNull__SendsDataToFreeAssetsService()
        {
            var data = AutoFaker.Generate<SavingsTargetBalanceSet>();
            var builder = new UnitUnderTestBuilder<SavingsTargetBalanceSetProjection>();
            var fakeService = builder.GetMock<IFreeAssetsService>();
            SavingsTargetBalanceSetProjection unitUnderTest = builder.Build();

            await unitUnderTest.Handle(data);

            A.CallTo(() => fakeService.Update(
                    A<SavingsTargetBalance>.That.Matches(s => s.SavingsTargetId == data.SavingsTargetId)))
                .MustHaveHappenedOnceExactly();

            A.CallTo(() => fakeService.Update(
                    A<SavingsTargetBalance>.That.Matches(s => s.Balance == data.Balance)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public async Task EventIsNotNull_CallsFreeAssetsService_ThenCallsSavingsTargetService()
        {
            var data = AutoFaker.Generate<SavingsTargetBalanceSet>();
            var builder = new UnitUnderTestBuilder<SavingsTargetBalanceSetProjection>();
            var fakeFreeAssetsServiceService = builder.GetMock<IFreeAssetsService>();
            var fakeSavingsTargetServiceService = builder.GetMock<ISavingsTargetService<ServiceSavingsTarget>>();
            SavingsTargetBalanceSetProjection unitUnderTest = builder.Build();

            await unitUnderTest.Handle(data);

            A.CallTo(() => fakeFreeAssetsServiceService.Update(A<SavingsTargetBalance>._))
                .MustHaveHappenedOnceExactly()
                .Then(
                    A.CallTo(() => fakeSavingsTargetServiceService.SetBalance(A<SetBalance>._))
                        .MustHaveHappenedOnceExactly());
        }
    }
}
