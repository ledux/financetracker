﻿using System;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.Tracing.Abstractions;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.SavingsTargetBalanceSetProjectionTests
{
    public class CtorTests
    {
        [Fact]
        public void SavingsTargetServiceIsNull_Throws()
        {
            Assert.Throws<ArgumentNullException>(
                () => new SavingsTargetBalanceSetProjection(
                    null,
                    A.Fake<IFreeAssetsService>(),
                    A.Fake<ITracer>()));
        }

        [Fact]
        public void FreeAssetsServiceIsNull__Throws()
        {
            Assert.Throws<ArgumentNullException>(
                () => new SavingsTargetBalanceSetProjection(
                    A.Fake<ISavingsTargetService<SavingsTarget.Services.Models.SavingsTarget>>(),
                    null,
                    A.Fake<ITracer>()));
        }

        [Fact]
        public void TracerIsNull__Throws()
        {
            Assert.Throws<ArgumentNullException>(
                () => new SavingsTargetBalanceSetProjection(
                    A.Fake<ISavingsTargetService<SavingsTarget.Services.Models.SavingsTarget>>(),
                    A.Fake<IFreeAssetsService>(),
                    null));
        }
    }
}
