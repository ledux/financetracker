﻿using System.Threading.Tasks;
using AutoBogus;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing.FakeItEasy;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.SavingsTargetUpdatedProjectionTests
{
    public class HandleTests
    {
        [Fact]
        public async Task EventNotNull_SendsDataToService()
        {
            var eventData = AutoFaker.Generate<SavingsTargetUpdated>();

            var builder = new UnitUnderTestBuilder<SavingsTargetUpdatedProjection>();
            var serviceMock = builder.GetMock<ISavingsTargetService<ServiceModel>>();
            SavingsTargetUpdatedProjection unitUnderTest = builder.Build();

            await unitUnderTest.Handle(eventData);

            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.Id == eventData.Id))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.AccountId == eventData.AccountId))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.Name == eventData.Name))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.Description == eventData.Description))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.Balance == eventData.Balance))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.StartAmount == eventData.StartAmount))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.TargetAmount == eventData.TargetAmount))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.StartDate == eventData.StartDate))).MustHaveHappenedOnceExactly();
            A.CallTo(() => serviceMock.CreateOrUpdate(A<ServiceModel>.That.Matches(m => m.TargetDate == eventData.TargetDate))).MustHaveHappenedOnceExactly();
        }

        [Theory]
        [InlineData(10, -10)]
        [InlineData(2834, -2834)]
        [InlineData(-31321, 31321)]
        public async Task EventIsNotNull__CallsUpdateOnFreeAssetsServiceWithTheNegativeBalance(decimal balance, decimal negativeBalance)
        {
            SavingsTargetUpdated eventData = new AutoFaker<SavingsTargetUpdated>()
                .RuleFor(s => s.Balance, balance)
                .Generate();
            var builder = new UnitUnderTestBuilder<SavingsTargetUpdatedProjection>();
            var serviceMock = builder.GetMock<IFreeAssetsService>();
            SavingsTargetUpdatedProjection unitUnderTest = builder.Build();

            await unitUnderTest.Handle(eventData);

            A.CallTo(() => serviceMock.Update(eventData.AccountId, negativeBalance)).MustHaveHappenedOnceExactly();
        }
    }
}
