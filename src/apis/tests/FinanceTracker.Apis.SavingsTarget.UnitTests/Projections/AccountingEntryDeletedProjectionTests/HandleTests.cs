﻿using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Projections;
using FinanceTracker.Apis.SavingsTarget.Services;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing.FakeItEasy;
using Xunit;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Projections.AccountingEntryDeletedProjectionTests
{
    public class HandleTests
    {
        [Fact]
        public void EventIsNotNull_SendsNegativeAmountToService()
        {
            const long accountingEntryId = 1234;
            const long accountId = 798981234;
            const decimal amount = 934;
            const decimal negativeAmount = -934;
            var deletedEvent = new AccountingEntryDeleted
            {
                Id = accountingEntryId,
                Amount = amount,
                AccountId = accountId
            };
            var builder = new UnitUnderTestBuilder<AccountingEntryDeletedProjection>();
            var fakeService = builder.GetMock<IFreeAssetsService>();
            AccountingEntryDeletedProjection unitUnderTest = builder.Build();

            unitUnderTest.Handle(deletedEvent);

            A.CallTo(() => fakeService.Update(accountId, negativeAmount)).MustHaveHappenedOnceExactly();
        }
    }
}
