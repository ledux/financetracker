﻿
using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FakeItEasy;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Apis.SavingsTarget.Services.Models;
using FinanceTracker.Common.Testing.FakeItEasy;
using Xunit;
using BalanceSetEvent = FinanceTracker.Common.EventMessages.ApiEvents.SavingsTargetBalanceSet;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Producers.SavingsTargetProducerTests

{
    public class SavingsTargetBalanceSet
    {
        [Fact]
        public async Task SetBalanceIsNull_Throws()
        {
            SavingsTargetProducer unitUnderTest = new UnitUnderTestBuilder<SavingsTargetProducer>().Build();

            await Assert.ThrowsAsync<ArgumentNullException>(() => unitUnderTest.SavingsTargetBalanceSet(null));
        }

        [Fact]
        public async Task SetBalanceIsNotNull_SendsDataToTheClient()
        {
            var data = new SetBalance { SavingsTargetId = 123432, Balance = (decimal) 9922.2 };
            var builder = new UnitUnderTestBuilder<SavingsTargetProducer>();
            var fakeClient = builder.GetMock<IEventClient>();
            SavingsTargetProducer unitUnderTest = builder.Build();

            await unitUnderTest.SavingsTargetBalanceSet(data);

            A.CallTo(
                    () => fakeClient.Publish(
                        A<BalanceSetEvent>.That.Matches(b => b.SavingsTargetId == data.SavingsTargetId)))
                .MustHaveHappenedOnceExactly();
            A.CallTo(
                    () => fakeClient.Publish(
                        A<BalanceSetEvent>.That.Matches(b => b.Balance == data.Balance)))
                .MustHaveHappenedOnceExactly();
        }
    }
}
