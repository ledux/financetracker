﻿using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Xunit;
using Moq;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Producers.SavingsTargetProducerTests
{
    public class SavingsTargetDeletedTests
    {
        private const long Id = 3352342;

        [Fact]
        public async Task IdIsNotZero_PublishTheEventWithTheId()
        {
            (SavingsTargetProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<SavingsTargetProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> eventClientMock = mockBag.GetMock<IEventClient>();

            await unitUnderTest.SavingsTargetDeleted(Id);

            eventClientMock.Verify(c => c.Publish(It.Is<SavingsTargetDeleted>(e => e.SavingsTargetId == Id)));
        }
    }
}
