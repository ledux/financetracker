﻿using System;
using System.EventSourcing.Client;
using System.Threading.Tasks;
using FinanceTracker.Apis.SavingsTarget.Producers;
using FinanceTracker.Common.EventMessages.ApiEvents;
using FinanceTracker.Common.Testing;
using FinanceTracker.Common.Testing.Extensions;
using Moq;
using Xunit;
using ServiceModel = FinanceTracker.Apis.SavingsTarget.Services.Models.SavingsTarget;

namespace FinanceTracker.Apis.SavingsTarget.UnitTests.Producers.SavingsTargetProducerTests

{
    public class SavingsTargetUpdatedTests
    {
        public SavingsTargetUpdatedTests()
        {
        }

        [Fact]
        public async Task IdIsZero_Throws()
        {
            SavingsTargetProducer unitUnderTest = new UnitUnderTestBuilder<SavingsTargetProducer>()
                .AddTracingMocks()
                .Build();

            await Assert.ThrowsAsync<ArgumentException>(
                () => unitUnderTest.SavingsTargetUpdated(new ServiceModel { Id = 0 }));
        }

        [Fact]
        public async Task IdIsNotZero__CallsPublishWithTheSameData()
        {
            var targetData = new ServiceModel
            {
                Id = 12341234,
                AccountId = 9898982,
                Name = "Name of target",
                Description = "Description of target",
                Balance = (decimal) 234.33,
                StartAmount = (decimal) 1112.223,
                StartDate = new DateTime(2020, 9, 1),
                TargetAmount = 2000,
                TargetDate = new DateTime(2021, 1, 31)
            };
            (SavingsTargetProducer unitUnderTest, MockBag mockBag) = new UnitUnderTestBuilder<SavingsTargetProducer>()
                .AddTracingMocks()
                .BuildBoth();
            Mock<IEventClient> clientMock = mockBag.GetMock<IEventClient>();

            await unitUnderTest.SavingsTargetUpdated(targetData);

            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.Id == targetData.Id)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.AccountId == targetData.AccountId)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.Balance == targetData.Balance)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.StartDate == targetData.StartDate)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.StartAmount == targetData.StartAmount)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.TargetAmount == targetData.TargetAmount)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.TargetDate == targetData.TargetDate)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.Name == targetData.Name)));
            clientMock.Verify(c => c.Publish(It.Is<SavingsTargetUpdated>(st => st.Description == targetData.Description)));
        }
    }
}
