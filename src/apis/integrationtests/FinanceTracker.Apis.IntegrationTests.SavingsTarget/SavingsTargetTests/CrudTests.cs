﻿using System;
using System.Net;
using FinanceTracker.Apis.IntegrationTests.SavingsTarget.Models;
using FinanceTracker.Common.Testing.Integration.Attributes;
using FinanceTracker.Common.Testing.Integration.Kafka;
using FinanceTracker.Common.Testing.Integration.Mongo;
using MongoDB.Driver;
using RestSharp;
using Xunit;

namespace FinanceTracker.Apis.IntegrationTests.SavingsTarget.SavingsTargetTests

{
    [TestCaseOrderer("FinanceTracker.Common.Testing.Integration.Orderer.AttributeOrderer", "FinanceTracker.Common.Testing.Integration")]
    public class CrudTests
    {
        private static string _firstAccountId;
        private static string _firstSavingsTargetId;
        private const string AccountUrl = "http://localhost:5002/v1/account";
        private const string SavingsTargetUrl = "http://localhost:5004/v1/savingstarget";

        [Fact, TestOrder(10)]
        public void T10_CreateAccount_CreatesFreeAssets()
        {
            var postClient = new RestClient(AccountUrl);
            var firstRequest = new RestRequest(Method.POST);
            var firstAccount = new Account
            {
                Description = "First Account",
                Name = "First",
                Balance = 1000
            };
            firstRequest.AddJsonBody(firstAccount);
            IRestResponse<Account> restResponse = postClient.Execute<Account>(firstRequest);
            _firstAccountId = restResponse.Data.Id;
            
            var mongoClient = new MongoTestClient("financeTrackerIntegration");
            IMongoCollection<DbFreeAssets> collection = mongoClient.GetMongoCollection<DbFreeAssets>("savingsTarget.freeAssets");
            long accountIdAsLong = long.Parse(_firstAccountId);
            DbFreeAssets freeAssets = collection.FindSync(a => a.AccountId == accountIdAsLong).SingleOrDefault();

            Assert.NotNull(freeAssets);
            Assert.Equal(firstAccount.Balance, freeAssets.Amount);
        }

        [Fact, TestOrder(20)]
        public void T20_CreateSavingsTarget__ReturnsSameValueWithId()
        {
            var postClient = new RestClient(SavingsTargetUrl);
            var postRequest = new RestRequest(Method.POST);
            PostModel newSavingsTarget = CreateSavingsTarget();
            postRequest.AddJsonBody(newSavingsTarget);

            IRestResponse<Models.SavingsTarget> response = postClient.Execute<Models.SavingsTarget>(postRequest);
            Models.SavingsTarget returnData = response.Data;
            _firstSavingsTargetId = returnData.Id;

            Assert.False(string.IsNullOrEmpty(returnData.Id));
            Assert.Equal(newSavingsTarget.AccountId, returnData.AccountId);
            Assert.Equal(newSavingsTarget.Name, returnData.Name);
            Assert.Equal(newSavingsTarget.Description, returnData.Description);
            Assert.Equal(newSavingsTarget.StartAmount, returnData.StartAmount);
            Assert.Equal(newSavingsTarget.StartDate, returnData.StartDate);
            Assert.Equal(newSavingsTarget.TargetAmount, returnData.TargetAmount);
            Assert.Equal(newSavingsTarget.TargetDate, returnData.TargetDate);
        }

        [Fact, TestOrder(30)]
        public void T30_GetSavingsTargetById__ReturnsCreatedData()
        {
            var getClient = new RestClient($"{SavingsTargetUrl}/{_firstSavingsTargetId}");
            var getRequest = new RestRequest(Method.GET);
            PostModel expected = CreateSavingsTarget();

            IRestResponse<Models.SavingsTarget> response = getClient.Execute<Models.SavingsTarget>(getRequest);
            Models.SavingsTarget actual = response.Data;

            Assert.Equal(_firstSavingsTargetId, actual.Id);
            Assert.Equal(expected.AccountId, actual.AccountId);
            Assert.Equal(expected.Description, actual.Description);
            Assert.Equal(expected.Name, actual.Name);
            Assert.Equal(expected.StartAmount, actual.StartAmount);
            Assert.Equal(expected.StartDate, actual.StartDate);
            Assert.Equal(expected.TargetAmount, actual.TargetAmount);
            Assert.Equal(expected.TargetDate, actual.TargetDate);
            Assert.Equal(expected.StartAmount, actual.Balance);
        }

        [Fact, TestOrder(40)]
        public void T40_UpdateSavingsTarget__ValuesAreUpdated()
        {
            var restClient = new RestClient($"{SavingsTargetUrl}/{_firstSavingsTargetId}");
            var putRequest = new RestRequest(Method.PUT);
            var putData = new PutModel
            {
                Name = "New name",
                Description = "New Description",
                TargetAmount = 2000,
                TargetDate = DateTime.Today.AddMonths(6)
            };
            putRequest.AddJsonBody(putData);
            PostModel expected = CreateSavingsTarget();

            restClient.Execute(putRequest);

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<SavingsTargetEvent>();

            var getRequest = new RestRequest(Method.GET);
            IRestResponse<Models.SavingsTarget> response = restClient.Execute<Models.SavingsTarget>(getRequest);
            Models.SavingsTarget actual = response.Data;

            Assert.Equal(_firstSavingsTargetId, actual.Id);
            Assert.Equal(expected.AccountId, actual.AccountId);
            Assert.Equal(expected.StartAmount, actual.StartAmount);
            Assert.Equal(expected.StartDate, actual.StartDate);
            Assert.Equal(putData.TargetAmount, actual.TargetAmount);
            Assert.Equal(putData.TargetDate, actual.TargetDate);
            Assert.Equal(putData.Description, actual.Description);
            Assert.Equal(putData.Name, actual.Name);
            Assert.Equal(expected.StartAmount, actual.Balance);
        }

        [Fact, TestOrder(50)]
        public void T50_DeleteSavingsTarget__GetsNotFound()
        {
            var restClient = new RestClient($"{SavingsTargetUrl}/{_firstSavingsTargetId}");
            var deleteRequest = new RestRequest(Method.DELETE);
            var getRequest = new RestRequest(Method.GET);

            restClient.Execute(deleteRequest);

            IRestResponse response = restClient.Execute(getRequest);
            Assert.Equal(response.StatusCode, HttpStatusCode.NotFound);
        }

        private static PostModel CreateSavingsTarget()
        {
            return new PostModel
            {
                AccountId = _firstAccountId,
                Name = "name",
                Description = "description",
                StartAmount = 100,
                StartDate = DateTime.Today,
                TargetAmount = 1000,
                TargetDate = null
            };
        }
    }
}
