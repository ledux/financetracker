﻿using System;
using System.Linq;
using FinanceTracker.Apis.IntegrationTests.SavingsTarget.Models;
using FinanceTracker.Common.Testing.Integration.Attributes;
using RestSharp;
using Xunit;

namespace FinanceTracker.Apis.IntegrationTests.SavingsTarget.SavingsTargetTests
{
    [TestCaseOrderer("FinanceTracker.Common.Testing.Integration.Orderer.AttributeOrderer", "FinanceTracker.Common.Testing.Integration")]
    public class AccountingTests
    {
        private static string _accountId;
        private static Models.SavingsTarget _firstSavingsTarget;
        private const string AccountUrl = "http://localhost:5002/v1/account";
        private const string SavingsTargetUrl = "http://localhost:5004/v1/savingstarget";
        private const string SetBalanceUrl = "http://localhost:5004/v1/savingstarget/{0}/accounting";
        private const string AccountingUrl = "http://localhost:5000/v1/accounting";
        private const string SavingsTargetAccountUrl = "http://localhost:5004/v1/savingstarget/account";
        private const int FirstNewBalance = 200;
        private const int SecondNewBalance = 400;

        [Fact, TestOrder(0)]
        public void T00_SetupTests()
        {
            var accountClient = new RestClient(AccountUrl);
            var postAccountRequest = new RestRequest(Method.POST);
            var account = new Account
            {
                Description = "First Account",
                Name = "First",
                Balance = 1000
            };
            postAccountRequest.AddJsonBody(account);
            _accountId = accountClient.Execute<Account>(postAccountRequest).Data.Id;

            var savingsTargetClient = new RestClient(SavingsTargetUrl);
            var postSavingsTargetRequest = new RestRequest(Method.POST);
            PostModel newSavingsTarget = CreateSavingsTarget();
            postSavingsTargetRequest.AddJsonBody(newSavingsTarget);
            _firstSavingsTarget = savingsTargetClient.Execute<Models.SavingsTarget>(postSavingsTargetRequest).Data;

            Assert.False(string.IsNullOrEmpty(_accountId));
            Assert.NotNull(_firstSavingsTarget);
        }

        [Fact, TestOrder(10)]
        public void T10_SetBalance__ChangesTheBalance()
        {
            var setBalanceClient = new RestClient(string.Format(SetBalanceUrl, _firstSavingsTarget.Id));
            var putRequest = new RestRequest(Method.PUT);
            var changeBalance = new SetBalance { Balance = FirstNewBalance };
            putRequest.AddJsonBody(changeBalance);

            setBalanceClient.Execute(putRequest);

            var savingsTargetClient = new RestClient($"{SavingsTargetUrl}/{_firstSavingsTarget.Id}");
            var getRequest = new RestRequest(Method.GET);
            Models.SavingsTarget actual = savingsTargetClient.Execute<Models.SavingsTarget>(getRequest).Data;

            Assert.Equal(FirstNewBalance, actual.Balance);
        }

        [Fact, TestOrder(20)]
        public void T20_SetBalance_ChangesTheFreeAssets()
        {
            var setBalanceClient = new RestClient(string.Format(SetBalanceUrl, _firstSavingsTarget.Id));
            var putRequest = new RestRequest(Method.PUT);
            var changeBalance = new SetBalance { Balance = SecondNewBalance };
            putRequest.AddJsonBody(changeBalance);

            setBalanceClient.Execute(putRequest);
            
            var freeAssetsClient = new RestClient($"{SavingsTargetAccountUrl}/{_accountId}");
            var freeAssetsRequest = new RestRequest(Method.GET);
            FreeAssetsAccount account = freeAssetsClient.Execute<FreeAssetsAccount>(freeAssetsRequest).Data;

            Assert.Equal(600, account.FreeAssets);
        }

        [Fact, TestOrder(30)]
        public void T30_CreateSavingsTarget_GetFreeAssets_ReturnsBothSavingsTarget()
        {
            var savingsTargetClient = new RestClient(SavingsTargetUrl);
            var postSavingsTargetRequest = new RestRequest(Method.POST);
            PostModel newSavingsTarget = CreateSecondSavingsTarget();
            postSavingsTargetRequest.AddJsonBody(newSavingsTarget);
            Models.SavingsTarget secondSavingsTarget = savingsTargetClient.Execute<Models.SavingsTarget>(postSavingsTargetRequest).Data;

            var freeAssetsClient = new RestClient($"{SavingsTargetAccountUrl}/{_accountId}");
            var freeAssetsRequest = new RestRequest(Method.GET);
            FreeAssetsAccount account = freeAssetsClient.Execute<FreeAssetsAccount>(freeAssetsRequest).Data;
            Models.SavingsTarget firstAccountTarget = account.SavingsTargets.SingleOrDefault(s => s.Id == _firstSavingsTarget.Id);
            Models.SavingsTarget secondAccountTarget = account.SavingsTargets.SingleOrDefault(s => s.Id == secondSavingsTarget.Id);

            Assert.Equal(_firstSavingsTarget.AccountId, account.Id);
            Assert.NotNull(firstAccountTarget);
            Assert.Equal(SecondNewBalance, firstAccountTarget.Balance);
            Assert.Equal(_firstSavingsTarget.Description, firstAccountTarget.Description);
            Assert.Equal(_firstSavingsTarget.Name, firstAccountTarget.Name);
            Assert.Equal(_firstSavingsTarget.StartAmount, firstAccountTarget.StartAmount);
            Assert.Equal(_firstSavingsTarget.TargetAmount, firstAccountTarget.TargetAmount);
            Assert.Equal(_firstSavingsTarget.StartDate, firstAccountTarget.StartDate);
            Assert.Equal(_firstSavingsTarget.TargetDate, firstAccountTarget.TargetDate);

            Assert.NotNull(secondAccountTarget);
            Assert.Equal(secondSavingsTarget.Balance, secondAccountTarget.Balance);
            Assert.Equal(secondSavingsTarget.Description, secondAccountTarget.Description);
            Assert.Equal(secondSavingsTarget.Name, secondAccountTarget.Name);
            Assert.Equal(secondSavingsTarget.StartAmount, secondAccountTarget.StartAmount);
            Assert.Equal(secondSavingsTarget.TargetAmount, secondAccountTarget.TargetAmount);
            Assert.Equal(secondSavingsTarget.StartDate, secondAccountTarget.StartDate);
            Assert.Equal(secondSavingsTarget.TargetDate, secondAccountTarget.TargetDate);
        }

        [Fact, TestOrder(40)]
        public void T40_CreateAccountingEntry_FreeAssetsChanges()
        {
            const int accountingEntryAmount = 200;

            // balance of savings target 1:  400 (second set balance)
            // balance of savings target 2:  200 (start amount)
            // balance of account:          1200 (start + accountingEntryAmount)
            // free assets:                  600
            const int newFreeAssets = 600;
            var accountingEntry = new AccountingEntry
            {
                Amount = accountingEntryAmount,
                AccountId = _accountId,
                BookingDate = DateTime.Today,
            };
            var postClient = new RestClient(AccountingUrl);
            var postRequest = new RestRequest(Method.POST);
            postRequest.AddJsonBody(accountingEntry);
            postClient.Execute<AccountingEntry>(postRequest);

            var freeAssetsClient = new RestClient($"{SavingsTargetAccountUrl}/{_accountId}");
            var freeAssetsRequest = new RestRequest(Method.GET);
            FreeAssetsAccount account = freeAssetsClient.Execute<FreeAssetsAccount>(freeAssetsRequest).Data;

            Assert.Equal(newFreeAssets, account.FreeAssets);
        }

        private static PostModel CreateSecondSavingsTarget()
        {
            return new PostModel
            {
                AccountId = _accountId,
                Name = "other name",
                Description = "other description",
                StartAmount = 200,
                StartDate = DateTime.Today,
                TargetAmount = 2000,
                TargetDate = DateTime.Today.AddMonths(6)
            };
        }

        private static PostModel CreateSavingsTarget()
        {
            return new PostModel
            {
                AccountId = _accountId,
                Name = "name",
                Description = "description",
                StartAmount = 100,
                StartDate = DateTime.Today,
                TargetAmount = 1000,
                TargetDate = null
            };
        }
    }
}
    
