﻿namespace FinanceTracker.Apis.IntegrationTests.SavingsTarget.Models
{
    /// <summary>
    /// Represents a model for setting a new value to the balance of a savings target
    /// This is the model for web layer
    /// </summary>
    public class SetBalance
    {
        /// <summary>
        /// The new balance of the savings target
        /// </summary>
        public decimal Balance { get; set; }
    }
}
