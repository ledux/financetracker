﻿namespace FinanceTracker.Apis.IntegrationTests.SavingsTarget.Models
{
    internal class DbAccount
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Balance { get; set; }
        public bool IsDeleted { get; set; }
    }
}
