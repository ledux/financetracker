﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FinanceTracker.Apis.IntegrationTests.SavingsTarget.Models
{
    public class DbFreeAssets
    {
        [BsonId]
        public long AccountId { get; set; }
        [BsonRepresentation(BsonType.Decimal128)]
        public decimal Amount { get; set; }
    }
}
