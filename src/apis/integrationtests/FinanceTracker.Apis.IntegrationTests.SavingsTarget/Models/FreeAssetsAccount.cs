﻿using System.Collections.Generic;
using System.Linq;

namespace FinanceTracker.Apis.IntegrationTests.SavingsTarget.Models
{
    public class FreeAssetsAccount
    {
        /// <summary>
        /// The identification of the account
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The amount of money on this account which is not associated with a savings target
        /// It can be negative, but should be balanced out from one of the savings target
        /// </summary>
        public decimal FreeAssets { get; set; }
        /// <summary>
        /// The savings target of this account
        /// </summary>
        public IEnumerable<SavingsTarget> SavingsTargets { get; set; } = Enumerable.Empty<SavingsTarget>();
    }
}
