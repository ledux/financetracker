﻿using System;

namespace FinanceTracker.Apis.IntegrationTests.Account.Models
{
    internal class AccountingEntry
    {
        public string Id { get; set; }
        public decimal Amount { get; set; }
        public string AccountId { get; set; }
        public DateTimeOffset BookingDate { get; set; }
    }
}
