﻿namespace FinanceTracker.Apis.IntegrationTests.Account.Models
{
    public class Account
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Balance { get; set; }
        public bool IsDeleted { get; set; }
    }
}
