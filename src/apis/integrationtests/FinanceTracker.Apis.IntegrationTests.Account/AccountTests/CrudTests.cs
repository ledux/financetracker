﻿using System.Linq;
using System.Net;
using FinanceTracker.Apis.IntegrationTests.Account.Models;
using FinanceTracker.Common.Testing.Integration.Attributes;
using FinanceTracker.Common.Testing.Integration.Kafka;
using RestSharp;
using Xunit;

namespace FinanceTracker.Apis.IntegrationTests.Account.AccountTests
{
    [TestCaseOrderer("FinanceTracker.Common.Testing.Integration.Orderer.AttributeOrderer", "FinanceTracker.Common.Testing.Integration")]
    public class CrudTests
    {
        private const string AccountUrl = "http://localhost:5002/v1/account";
        private static string _accountUrl;
        private const decimal InitialAmount = (decimal)21234.22;

        [Fact, TestOrder(10)]
        public void CreateAnAccount_CanBeReadById()
        {
            var postClient = new RestClient(AccountUrl);
            var request = new RestRequest(Method.POST);
            const string description = "Account in integration";
            const string name = "Integration";
            var data = new Models.Account
            {
                Balance = InitialAmount,
                Description = description,
                Name = name
            };

            request.AddJsonBody(data);
            IRestResponse<Models.Account> restResponse = postClient.Execute<Models.Account>(request);
            Parameter locationParameter = restResponse.Headers.Single(h => h.Name == "Location");
            Models.Account createdAccount = restResponse.Data;
            _accountUrl = locationParameter.Value.ToString();

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient(_accountUrl);
            var getRequest = new RestRequest(Method.GET);
            IRestResponse<Models.Account> getResponse = getClient.Execute<Models.Account>(getRequest);
            Models.Account getAccount = getResponse.Data;

            Assert.Equal(createdAccount.Name, getAccount.Name);
            Assert.Equal(createdAccount.Id, getAccount.Id);
            Assert.Equal(createdAccount.Description, getAccount.Description);
            Assert.Equal(createdAccount.Balance, getAccount.Balance);
        }

        [Fact, TestOrder(20)]
        public void UpdateAccount__CanBeReadById()
        {
            var putClient = new RestClient(_accountUrl);
            var putRequest = new RestRequest(Method.PUT);
            const string description = "Description changed";
            const string name = "Name changed";
            var data = new Models.Account { Description = description, Name = name };

            putRequest.AddJsonBody(data);
            putClient.Execute(putRequest);

            var getClient = new RestClient(_accountUrl);
            var getRequest = new RestRequest(Method.GET);

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            IRestResponse<Models.Account> getResponse = getClient.Execute<Models.Account>(getRequest);
            Models.Account getAccount = getResponse.Data;

            Assert.Equal(description, getAccount.Description);
            Assert.Equal(name, getAccount.Name);
            Assert.Equal(InitialAmount, getAccount.Balance);
        }

        [Fact, TestOrder(30)]
        public void DeleteAccount__CannotBeGetByTheId()
        {
            var client = new RestClient(_accountUrl);
            var deleteRequest = new RestRequest(Method.DELETE);
            client.Execute(deleteRequest);

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getRequest = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(getRequest);

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
