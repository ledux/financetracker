﻿using System;
using System.Linq;
using System.Net;
using FinanceTracker.Apis.IntegrationTests.Account.Models;
using FinanceTracker.Common.Testing.Integration.Attributes;
using FinanceTracker.Common.Testing.Integration.Kafka;
using FinanceTracker.Common.Testing.Integration.Mongo;
using MongoDB.Driver;
using RestSharp;
using Xunit;

namespace FinanceTracker.Apis.IntegrationTests.Account.AccountTests
{
    [TestCaseOrderer("FinanceTracker.Common.Testing.Integration.Orderer.AttributeOrderer", "FinanceTracker.Common.Testing.Integration")]
    public class ChangeBalanceTests
    {
        private const string AccountUrl = "http://localhost:5002/v1/account";
        private static string _accountUrl;
        private static string _accountId;
        private static string _firstAccountingEntryId;
        private static string _secondAccountingEntryId;
        private const string AccountingEntryUrl = "http://localhost:5000/v1/accounting";
        private const decimal InitialAmount = (decimal)21234.22;

        [Fact, TestOrder(10)]
        public void CreateAccountingEntry_BalanceChanges()
        {
            SetupAccount();
            var accountingEntry = new AccountingEntry
            {
                Amount = (decimal) 200.11,
                AccountId = _accountId,
                BookingDate = DateTime.Today,
            };
            var postClient = new RestClient(AccountingEntryUrl);
            var postRequest = new RestRequest(Method.POST);
            postRequest.AddJsonBody(accountingEntry);
            IRestResponse<AccountingEntry> postResponse = postClient.Execute<AccountingEntry>(postRequest);
            _firstAccountingEntryId = postResponse.Data.Id;

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient(_accountUrl);
            var getRequest = new RestRequest(Method.GET);
            IRestResponse<Models.Account> getResponse = getClient.Execute<Models.Account>(getRequest);
            Models.Account getAccount = getResponse.Data;

            Assert.Equal((decimal) 21434.33, getAccount.Balance);
        }

        [Fact, TestOrder(20)]
        public void CreateAccountingEntry_NegativeAmount_BalanceDecreases()
        {
            var accountingEntry = new AccountingEntry
            {
                Amount = (decimal) -234.22,
                AccountId = _accountId,
                BookingDate = DateTime.Today,
            };
            var postClient = new RestClient(AccountingEntryUrl);
            var postRequest = new RestRequest(Method.POST);
            postRequest.AddJsonBody(accountingEntry);
            IRestResponse<AccountingEntry> postResponse = postClient.Execute<AccountingEntry>(postRequest);
            _secondAccountingEntryId = postResponse.Data.Id;

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient(_accountUrl);
            var getRequest = new RestRequest(Method.GET);
            IRestResponse<Models.Account> getResponse = getClient.Execute<Models.Account>(getRequest);
            Models.Account getAccount = getResponse.Data;

            Assert.Equal((decimal) 21200.11, getAccount.Balance);
        }

        [Fact, TestOrder(30)]
        public void UpdateAccountingEntry__BalanceChangesByDifference()
        {
            var data = new AccountingEntry
            {
                Id = _secondAccountingEntryId,
                Amount = (decimal) -250.22,
                AccountId = _accountId,
                BookingDate = DateTime.Today
            };
            var putClient = new RestClient($"{AccountingEntryUrl}/{_secondAccountingEntryId}");
            var putRequest = new RestRequest(Method.PUT);
            putRequest.AddJsonBody(data);
            putClient.Execute(putRequest);

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient(_accountUrl);
            var getRequest = new RestRequest(Method.GET);
            IRestResponse<Models.Account> getResponse = getClient.Execute<Models.Account>(getRequest);
            Models.Account getAccount = getResponse.Data;

            Assert.Equal((decimal) 21184.11, getAccount.Balance);
        }

        [Fact, TestOrder(40)]
        public void DeleteAccountingEntry__TheBalanceChangesByTheNegativeAmount()
        {
            var deleteClient = new RestClient($"{AccountingEntryUrl}/{_firstAccountingEntryId}");
            var deleteRequest = new RestRequest(Method.DELETE);
            deleteClient.Execute(deleteRequest);


            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient(_accountUrl);
            var getRequest = new RestRequest(Method.GET);
            IRestResponse<Models.Account> getResponse = getClient.Execute<Models.Account>(getRequest);
            Models.Account getAccount = getResponse.Data;

            Assert.Equal((decimal) 20984.00, getAccount.Balance);
        }

        [Fact, TestOrder(50)]
        public void DeleteAccount_HasStillAccountingEntries_NotAccessibleButInDatabase()
        {
            var client = new RestClient($"{AccountUrl}/{_accountId}");
            var deleteRequest = new RestRequest(Method.DELETE);
            client.Execute(deleteRequest);

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();

            var getRequest = new RestRequest(Method.GET);
            IRestResponse getResponse = client.Execute(getRequest);
            Assert.Equal(HttpStatusCode.NotFound, getResponse.StatusCode);

            var mongoClient = new MongoTestClient("financeTrackerIntegration");
            IMongoCollection<DbAccount> collection = mongoClient.GetMongoCollection<DbAccount>("account.accounts");
            long accountIdAsLong = long.Parse(_accountId);
            DbAccount account = collection.FindSync(a => a.Id == accountIdAsLong).SingleOrDefault();
            Assert.True(account.IsDeleted);
        }

        private static void SetupAccount()
        {
            var postClient = new RestClient(AccountUrl);
            var request = new RestRequest(Method.POST);
            const string description = "Account in integration";
            const string name = "Integration";
            var data = new Models.Account
            {
                Balance = InitialAmount,
                Description = description,
                Name = name
            };

            request.AddJsonBody(data);
            IRestResponse<Models.Account> restResponse = postClient.Execute<Models.Account>(request);
            Parameter locationParameter = restResponse.Headers.Single(h => h.Name == "Location");
            Models.Account createdAccount = restResponse.Data;
            _accountId = createdAccount.Id;
            _accountUrl = locationParameter.Value.ToString();
        }
    }
}
