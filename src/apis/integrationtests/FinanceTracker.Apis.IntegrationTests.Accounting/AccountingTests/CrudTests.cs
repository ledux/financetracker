﻿using System;
using System.Collections.Generic;
using System.Net;
using FinanceTracker.Apis.IntegrationTests.Accounting.Models;
using FinanceTracker.Common.Testing.Integration.Attributes;
using FinanceTracker.Common.Testing.Integration.Kafka;
using RestSharp;
using Xunit;

namespace FinanceTracker.Apis.IntegrationTests.Accounting.AccountingTests
{
    [TestCaseOrderer("FinanceTracker.Common.Testing.Integration.Orderer.AttributeOrderer", "FinanceTracker.Common.Testing.Integration")]
    public class CrudTests
    {
        private const string AccountUrl = "http://localhost:5002/v1/account";
        private const string AccountingUrl = "http://localhost:5000/v1/accounting";
        private const string ByAccountIdUrl = "http://localhost:5000/v1/accounting/account";
        private const decimal Amount = (decimal) 44.2;
        private const decimal UpdatedAmount = (decimal) 999.23;
        private readonly DateTime _bookingDate = DateTime.Today;

        private static string _firstAccountId;
        private static string _secondAccountId;
        private static string _firstAccountingEntryId;

        [Fact, TestOrder(0)]
        public void SetupTests()
        {
            var postClient = new RestClient(AccountUrl);
            var firstRequest = new RestRequest(Method.POST);
            var secondRequest = new RestRequest(Method.POST);
            var firstAccount = new Account
            {
                Description = "First Account",
                Name = "First"
            };
            var secondAccount = new Account
            {
                Name = "Second",
                Description = "Second Account"
            };

            firstRequest.AddJsonBody(firstAccount);
            IRestResponse<Account> restResponse = postClient.Execute<Account>(firstRequest);
            _firstAccountId = restResponse.Data.Id;

            secondRequest.AddJsonBody(secondAccount);
            IRestResponse<Account> response = postClient.Execute<Account>(secondRequest);
            _secondAccountId = response.Data.Id;

            Assert.False(string.IsNullOrWhiteSpace(_firstAccountId));
            Assert.False(string.IsNullOrWhiteSpace(_secondAccountId));
        }

        [Fact, TestOrder(10)]
        public void Create__ReturnsSameValueWithId()
        {
            var postClient = new RestClient(AccountingUrl);
            var request = new RestRequest(Method.POST);
            var data = new AccountingEntry
            {
                Amount = Amount,
                BookingDate = _bookingDate,
                AccountId = _firstAccountId,
            };

            request.AddJsonBody(data);
            IRestResponse<AccountingEntry> response = postClient.Execute<AccountingEntry>(request);
            AccountingEntry created = response.Data;
            _firstAccountingEntryId = created.Id;

            Assert.False(string.IsNullOrWhiteSpace(created.Id));
            Assert.Equal(Amount, created.Amount);
            Assert.Equal(_bookingDate, created.BookingDate);
            Assert.Equal(_firstAccountId, created.AccountId);
        }

        [Fact, TestOrder(20)]
        public void GetEntry__ReturnsTheExpectedData()
        {
            var getClient = new RestClient($"{AccountingUrl}/{_firstAccountingEntryId}");
            var request = new RestRequest(Method.GET);
            IRestResponse<AccountingEntry> response = getClient.Execute<AccountingEntry>(request);
            AccountingEntry data = response.Data;

            Assert.Equal(Amount, data.Amount);
            Assert.Equal(_bookingDate, data.BookingDate);
            Assert.Equal(_firstAccountId, data.AccountId);
            Assert.Equal(_firstAccountingEntryId, data.Id);
        }

        [Fact, TestOrder(30)]
        public void UpdateEntry__HasDataUpdated()
        {
            var putClient = new RestClient($"{AccountingUrl}/{_firstAccountingEntryId}");
            var putRequest = new RestRequest(Method.PUT);
            var data = new AccountingEntry
            {
                Amount = UpdatedAmount,
                BookingDate = _bookingDate,
                AccountId = _firstAccountId,
            };
            putRequest.AddJsonBody(data);
            putClient.Execute<AccountingEntry>(putRequest);
            
            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient($"{AccountingUrl}/{_firstAccountingEntryId}");
            var request = new RestRequest(Method.GET);
            IRestResponse<AccountingEntry> response = getClient.Execute<AccountingEntry>(request);
            AccountingEntry updatedData = response.Data;

            Assert.Equal(UpdatedAmount, updatedData.Amount);
            Assert.Equal(_bookingDate, updatedData.BookingDate);
            Assert.Equal(_firstAccountId, updatedData.AccountId);
            Assert.Equal(_firstAccountingEntryId, updatedData.Id);
        }

        [Fact, TestOrder(40)]
        public void DeleteEntry_DoesNotReturnItAnymore()
        {
            var deleteClient =new RestClient($"{AccountingUrl}/{_firstAccountingEntryId}");
            var deleteRequest = new RestRequest(Method.DELETE);
            deleteClient.Execute(deleteRequest);

            using var kafkaConsumer = new TestsKafkaConsumer("system.events");
            kafkaConsumer.Consume<AccountEvent>();
            var getClient = new RestClient($"{AccountingUrl}/{_firstAccountingEntryId}");
            var getRequest = new RestRequest(Method.GET);
            IRestResponse restResponse = getClient.Execute(getRequest);

            Assert.Equal(HttpStatusCode.NotFound, restResponse.StatusCode);
        }

        [Fact, TestOrder(50)]
        public void CreateEntriesOnAccounts_GetByAccount_FiltersCorrectly()
        {
            var firstEntry = new AccountingEntry { AccountId = _firstAccountId };
            var secondEntry = new AccountingEntry { AccountId = _secondAccountId };
            var postClient = new RestClient(AccountingUrl);
            var firstRequest = new RestRequest(Method.POST);
            var secondRequest = new RestRequest(Method.POST);
            firstRequest.AddJsonBody(firstEntry);
            secondRequest.AddJsonBody(secondEntry);

            IRestResponse<AccountingEntry> firstResponse = postClient.Execute<AccountingEntry>(firstRequest);
            IRestResponse<AccountingEntry> secondResponse = postClient.Execute<AccountingEntry>(secondRequest);

            string firstEntryId = firstResponse.Data.Id;
            string secondEntryId = secondResponse.Data.Id;

            var getClient = new RestClient();
            var firstGetRequest = new RestRequest(new Uri($"{ByAccountIdUrl}/{_firstAccountId}"), Method.GET);
            var secondGetRequest = new RestRequest(new Uri($"{ByAccountIdUrl}/{_secondAccountId}"), Method.GET);

            IRestResponse<IEnumerable<AccountingEntry>> firstGetResponse = getClient.Execute<IEnumerable<AccountingEntry>>(firstGetRequest);
            IRestResponse<IEnumerable<AccountingEntry>> secondGetResponse = getClient.Execute<IEnumerable<AccountingEntry>>(secondGetRequest);

            Assert.Single(firstGetResponse.Data);
            Assert.Single(secondGetResponse.Data);
            Assert.Collection(firstGetResponse.Data, entry => Assert.Equal(firstEntryId, entry.Id));
            Assert.Collection(secondGetResponse.Data, entry => Assert.Equal(secondEntryId, entry.Id));
        }
    }
}
