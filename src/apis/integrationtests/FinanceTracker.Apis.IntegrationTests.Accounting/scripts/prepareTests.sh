#!/bin/bash

# This script sets up an enviroment for running the autmomated integration tests for the account service
# It builds the necessary docker images and runs all services

echo 'building the account api docker container'
docker.exe build --file ../../../src/FinanceTracker.Apis.Account/Dockerfile --tag accountapi:integration ../../../..

echo 'building the accounting entry api docker container'
docker.exe build --file ../../../src/FinanceTracker.Apis.Accounting/Dockerfile --tag accountingapi:integration ../../../..

echo 'starting the test environment'
docker-compose.exe --file docker-compose.yaml up --detach
