import { Navigate } from 'react-router-dom'
import accountRoutes from 'app/views/accounts/AccountRoutes'
import accountingEntryRoutes from 'app/views/accountingEntries/accountingEntryRoutes'
import savingsTargetRoutes from 'app/views/savingsTarget/savingsTargetRoutes'

const redirectRoute = [
    {
        path: '/',
        exact: true,
        component: () => <Navigate to="/dashboard/default" />,
    },
]

const errorRoute = [
    {
        component: () => <Navigate to="/session/404" />,
    },
]

const routes = [...redirectRoute, ...errorRoute,  ...accountRoutes, ...accountingEntryRoutes, ...savingsTargetRoutes]

export default routes
