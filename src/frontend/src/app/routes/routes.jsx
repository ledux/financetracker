import AuthGuard from 'app/auth/AuthGuard'
import NotFound from 'app/views/sessions/NotFound'
import dashboardRoutes from 'app/views/dashboard/DashboardRoutes'
import sessionRoutes from 'app/views/sessions/SessionRoutes'
import rootRoutes from 'app/RootRoutes'
import MatxLayout from '../components/matx/MatxLayout/MatxLayout'

export const AllPages = () => {
    const all_routes = [
        {
            path: '/',
            element: (
                <AuthGuard>
                    <MatxLayout />
                </AuthGuard>
            ),
            children: [...dashboardRoutes, ...rootRoutes],
        },
        ...sessionRoutes,
        {
            path: '*',
            element: <NotFound />,
        },
    ]

    return all_routes
}

