import { IconButton, Table, TableBody, TableCell, TableHead, TableRow } from "@mui/material"
import AccountingEntry from "app/services/accountingEntries/AccountingEntry"
import DeleteForeverIcon from "@mui/icons-material/DeleteForever"
import EditIcon from "@mui/icons-material/Edit"
import { ReactElement } from "react"

const actionColumnStyle = { width: "65px" }

/** The props for the accounting entry list */
export interface IAccountingEntryListProps {
  /** The actual accounting entries to be rendered */
  accountingEntries: AccountingEntry[]
  /** The function which will be called, when the edit button is clicked */
  onEditClicked: (entry: AccountingEntry) => void
  /** The function which will be called, when the delete button is clicked */
  onDeleteClicked: (entry: AccountingEntry) => void
}

/**
 * Renders a list of accounting entries
 * @param props The props of the component
 */
function AccountingEntryList(props: IAccountingEntryListProps): ReactElement {
  return (
    <div className="w-100 overflow-auto">
      <Table>
        <TableHead>
          <TableRow>
            <TableCell className="px-0">Date</TableCell>
            <TableCell className="px-0">Description</TableCell>
            <TableCell className="px-0">Amount</TableCell>
            <TableCell style={actionColumnStyle} className="px-0">
              Edit
            </TableCell>
            <TableCell style={actionColumnStyle} className="px-0">
              Delete
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.accountingEntries.map((entry) => (
            <TableRow key={entry.id}>
              <TableCell>{entry.bookingDate.toDateString()}</TableCell>
              <TableCell>{entry.description}</TableCell>
              <TableCell>{entry.amount}</TableCell>
              <TableCell style={actionColumnStyle} className="px-0">
                <IconButton onClick={() => props.onEditClicked(entry)}>
                  <EditIcon color="primary" />
                </IconButton>
              </TableCell>
              <TableCell style={actionColumnStyle} className="px-0">
                <IconButton onClick={() => props.onDeleteClicked(entry)}>
                  <DeleteForeverIcon color="error" />
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  )
}

export default AccountingEntryList
