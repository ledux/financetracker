import { Fab, Icon } from "@mui/material"
import { SimpleCard } from "app/components/matx"
import { PropsWithChildren } from "react"

/** Props for the view of accounting entries */
export interface IAccountingEntriesProps {
  /** The function which will be called, when the add new entry button is clicked */
  onAddClick: () => void
}

/**
 * Renders a card which will show a header and an add button
 * @param props The props of the component
 */
function AccountingEntries(props: PropsWithChildren<IAccountingEntriesProps>) {
  return (
    <SimpleCard title="Accounting entries" subtitle={undefined} icon={undefined}>
      <Fab onClick={() => props.onAddClick()} color="primary" aria-label="Edit">
        <Icon>add</Icon>
      </Fab>
      {props.children}
    </SimpleCard>
  )
}

export default AccountingEntries
