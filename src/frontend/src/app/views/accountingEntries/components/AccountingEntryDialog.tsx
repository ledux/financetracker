import * as React from "react"

interface IAccountingEntryDialogProps {
  title: string
  onSaveClick: () => void
  onSaveContinueClick?: () => void
  onAbortClick: () => void
}

const AccountingEntryDialog: React.FunctionComponent<IAccountingEntryDialogProps> = (props) => <></>

export default AccountingEntryDialog
