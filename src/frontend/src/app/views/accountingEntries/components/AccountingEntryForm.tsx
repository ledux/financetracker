import AdapterDateFns from "@mui/lab/AdapterDateFns"
import { Button, FormControlLabel, Grid, Icon, Switch, TextField } from "@mui/material"
import { SimpleCard } from "app/components/matx"
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"
// import {
//     KeyboardDatePicker,
//     MuiPickersUtilsProvider,
// } from '@material-ui/pickers'
// import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date'
import FinAccount from "app/services/account/FinAccount"
import AccountingEntry from "app/services/accountingEntries/AccountingEntry"
// import { SimpleCard } from 'matx'
import * as React from "react"
import { DatePicker, LocalizationProvider } from "@mui/lab"
// import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator'

/** Props for the form to edit an accounting entry */
export interface IAccountingEntryFormProps {
  /** The account, to which the entry belongs */
  account: FinAccount
  /** The label for the button for saving */
  saveLabel: string
  /** The label for the button for saving with staying on the page */
  saveAndContinueLabel: string
  /**
   * Function which resolves the accouting entry to be edited
   * @param accountId The id
   * @returns A promise which resolves the accounting entry
   */
  getAccountingEntry: () => Promise<AccountingEntry>
  /**
   * The function which will be called when the submit button is called.
   * After the save the page will be navigated back
   * @param accountingEntry The accounting entry to be saved
   */
  onSubmit: (accountingEntry: AccountingEntry) => void
  /**
   * The function which will be called when the submit and continue button is called.
   * After the save the page will stay
   * @param accountingEntry The accounting entry to be saved
   */
  onSubmitContinue: (accountingEntry: AccountingEntry) => void
}

/**
 * Renders a form for editing an accounting entry
 */
export default class AccountingEntryForm extends React.Component<IAccountingEntryFormProps, AccountingEntry> {
  private submitButton = ""

  /**
   * Instantiates a new instance
   * @param props The props needed for the component
   */
  constructor(props: IAccountingEntryFormProps) {
    super(props)
    this.state = new AccountingEntry()
  }

  /** Called after the render. Loads the accounting entry into the form */
  componentDidMount() {
    this.props.getAccountingEntry().then((entry) => this.setState(entry))
  }

  private changeRecurring(checked: boolean): void {
    this.setState((prev) => ({ ...prev, recurring: checked }))
  }

  private changeDate(date: Date | null): void {
    const bookingDate = date ?? new Date()
    this.setState((prev) => ({ ...prev, bookingDate }))
  }

  private changeAmount(value: string): void {
    const amount = Number(value)
    this.setState((prev) => ({ ...prev, amount }))
  }

  private changeDescription(value: string): void {
    this.setState((prev) => ({ ...prev, description: value }))
  }

  private saveEntry() {
    this.setState((prev) => ({ ...prev, accountId: this.props.account.id }))
    if (this.submitButton === "submit") {
      this.props.onSubmit(this.state)
    } else {
      this.props.onSubmitContinue(this.state)
    }
  }

  public render() {
    return (
      <SimpleCard title="Create accounting entry" subtitle={`For account '${this.props.account.name}'`} icon="">
        <ValidatorForm onSubmit={() => this.saveEntry()}>
          <Grid container spacing={6}>
            <Grid item lg={8} md={8} sm={12} xs={12}>
              <TextValidator
                autoFocus
                className="mb-16 w-100"
                label="Description"
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.changeDescription(event.target.value)}
                type="text"
                name="description"
                value={this.state?.description}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
            </Grid>
            <Grid item lg={4} md={4} sm={6} xs={6}>
              <TextValidator
                className="mb-16 w-100"
                label="Amount"
                onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.changeAmount(event.target.value)}
                type="number"
                name="amount"
                value={this.state?.amount}
                validators={["required"]}
                errorMessages={["this field is required"]}
              />
            </Grid>
            <Grid item lg={2} md={3} sm={4} xs={8}>
              {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    className="mb-16 w-100"
                                    margin="none"
                                    id="mui-pickers-date"
                                    label="Date"
                                    inputVariant="standard"
                                    variant="inline"
                                    type="text"
                                    format="dd. MMM yyyy"
                                    autoOk
                                    value={this.state?.bookingDate}
                                    onChange={(date: MaterialUiPickersDate) =>
                                        this.changeDate(date)
                                    }
                                    KeyboardButtonProps={{
                                        'aria-label': 'change date',
                                    }}
                                />
                            </MuiPickersUtilsProvider> */}
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  value={this.state?.bookingDate}
                  onChange={(date) => this.changeDate(date)}
                  renderInput={(props) => <TextField {...props} label="Date" />}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item lg={2} md={2} sm={2} xs={2}>
              <FormControlLabel
                control={
                  <Switch
                    checked={this.state.recurring}
                    onChange={(event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => this.changeRecurring(checked)}
                  />
                }
                label="Recurring"
              />
            </Grid>
          </Grid>
          <Grid container spacing={6}>
            <Grid item lg={2} md={2} sm={6} xs={6}>
              <Button
                color="primary"
                variant="contained"
                type="submit"
                onClick={() => {
                  this.submitButton = "submit"
                }}
              >
                <Icon>send</Icon>
                <span className="pl-8 capitalize">{this.props.saveLabel}</span>
              </Button>
            </Grid>
            <Grid item xl={2} lg={3} md={4} sm={6} xs={6}>
              <Button
                color="primary"
                variant="contained"
                type="submit"
                onClick={() => {
                  this.submitButton = "continue"
                }}
              >
                <Icon>send</Icon>
                <span className="pl-8 capitalize">{this.props.saveAndContinueLabel}</span>
              </Button>
            </Grid>
          </Grid>
        </ValidatorForm>
      </SimpleCard>
    )
  }
}
