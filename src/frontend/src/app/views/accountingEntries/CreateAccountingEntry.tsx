import { ReactElement, useEffect, useState } from "react"
import { useParams, useNavigate } from "react-router-dom"
import Page, { RouteSegment } from "app/components/Page"
import accountRouteNames from "app/views/accounts/accountRouteNames"
import AccountingEntry from "app/services/accountingEntries/AccountingEntry"
import FinAccount from "app/services/account/FinAccount"
import { getAccountById } from "app/services/account/accountServices"
import { createAccountingEntry } from "app/services/accountingEntries/accountingEntryServices"
import AccountingEntryForm from "./components/AccountingEntryForm"

/** Renders the view for creating an new accounting entry */
function CreateAccountingEntry(): ReactElement {
  const { accountId } = useParams()
  const [account, setAccount] = useState(new FinAccount("0"))

  useEffect(() => {
    getAccountById(accountId ?? "").then((accountById) => setAccount(accountById))
  }, [])

  const navigate = useNavigate()

  const getEntry: () => Promise<AccountingEntry> = () =>
    new Promise<AccountingEntry>((resolve) => {
      const entry = new AccountingEntry()
      entry.bookingDate = new Date()
      resolve(entry)
    })

  const onSubmit: (accountingEntry: AccountingEntry) => void = (accountingEntry: AccountingEntry) => {
    createAccountingEntry(accountingEntry).then(() => navigate(-1))
  }

  const onSubmitContinue: (accountingEntry: AccountingEntry) => void = (accountingEntry: AccountingEntry) => {
    createAccountingEntry(accountingEntry)
  }

  const breadCrumbs = [
    new RouteSegment("Accounts", accountRouteNames.allAccounts),
    new RouteSegment(account.name ?? "", `${accountRouteNames.viewAccount}${accountId}`),
    new RouteSegment("Create Entry"),
  ]

  return (
    <Page breadCrumbRoutes={breadCrumbs}>
      <AccountingEntryForm
        account={account}
        getAccountingEntry={getEntry}
        onSubmit={onSubmit}
        saveLabel="Create"
        saveAndContinueLabel="Create another"
        onSubmitContinue={onSubmitContinue}
      />
    </Page>
  )
}

export default CreateAccountingEntry
