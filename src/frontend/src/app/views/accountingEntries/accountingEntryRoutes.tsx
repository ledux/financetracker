import Loadable from "app/components/matx/Loadable/Loadable"
import React, { lazy } from "react"

const CreateEntry = Loadable(lazy(() => import("./CreateAccountingEntry")))

const EditEntry = Loadable(lazy(() => import("./EditAccountingEntry")))

const routeNames = {
  createEntry: "/accounts/:accountId/entry/create",
  editEntry: "/accounts/:accountId/entry/edit/:entryId",
}

/**
 * Constructs an URL for the page of creating a new accounting entry for a certain account
 * @param accountId The id of the account, which ends up in the url
 */
const getCreateEntryRoute: (accountId: string) => string = (accountId: string) =>
  routeNames.createEntry.replace(":accountId", accountId)

/**
 * Constructs an URL for the edit page of an accounting entry for a certain account
 * @param accountId The id of the account
 * @param entryId The id of the accounting entry
 */
const getEditEntryRoute: (accountId: string, entryId: string) => string = (accountId: string, entryId: string) =>
  routeNames.editEntry.replace(":accountId", accountId).replace(":entryId", entryId)

const accountingEntryRoutes = [
  {
    path: routeNames.createEntry,
    element: <CreateEntry />,
  },
  {
    path: routeNames.editEntry,
    element: <EditEntry />,
  },
]

export default accountingEntryRoutes
export { routeNames, getCreateEntryRoute, getEditEntryRoute }
