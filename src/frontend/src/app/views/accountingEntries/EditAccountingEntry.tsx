import { ReactElement, useEffect, useState } from "react"
import { useParams, useNavigate } from "react-router-dom"
import Page, { RouteSegment } from "app/components/Page"
import accountRouteNames from "app/views/accounts/accountRouteNames"
import { getAccountById } from "app/services/account/accountServices"
import { getAcountingEntryById, updateAccountingEntry } from "app/services/accountingEntries/accountingEntryServices"
import FinAccount from "app/services/account/FinAccount"
import AccountingEntry from "app/services/accountingEntries/AccountingEntry"
import AccountingEntryForm, { IAccountingEntryFormProps } from "./components/AccountingEntryForm"

/** Renders the view for updating an existing accounting entry */
function EditAccountingEntry(): ReactElement {
  const { accountId, entryId } = useParams()
  const [account, setAccount] = useState(new FinAccount(""))
  const naviate = useNavigate()

  useEffect(() => {
    getAccountById(accountId ?? "").then((accountById) => setAccount(accountById))
  }, [])

  const getEntry: () => Promise<AccountingEntry> = () => getAcountingEntryById(entryId ?? "")

  const update: (entry: AccountingEntry) => void = (entry: AccountingEntry) => {
    updateAccountingEntry(entry).then(() => naviate(-1))
  }

  const updateAndContinue: (entry: AccountingEntry) => void = (entry: AccountingEntry) => {
    updateAccountingEntry(entry)
  }

  const props: IAccountingEntryFormProps = {
    account,
    getAccountingEntry: getEntry,
    onSubmit: (entry) => update(entry),
    onSubmitContinue: (entry) => updateAndContinue(entry),
    saveAndContinueLabel: "Save and continue",
    saveLabel: "Save",
  }

  const breadCrumbs: RouteSegment[] = [
    new RouteSegment("Accounts", accountRouteNames.allAccounts),
    new RouteSegment(account.name ?? "", `${accountRouteNames.viewAccount}${accountId}`),
    new RouteSegment("Edit Entry"),
  ]

  return (
    <Page breadCrumbRoutes={breadCrumbs}>
      <AccountingEntryForm
        account={props.account}
        saveLabel={props.saveLabel}
        saveAndContinueLabel={props.saveAndContinueLabel}
        getAccountingEntry={props.getAccountingEntry}
        onSubmit={props.onSubmit}
        onSubmitContinue={props.onSubmitContinue}
      />
    </Page>
  )
}

export default EditAccountingEntry
