import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import { render, fireEvent, RenderResult } from "@testing-library/react"
import each from "jest-each"
import FinAccount from "app/services/account/FinAccount"
import SavingsTargetForm, { ISavingsTargetFormProps } from "../components/SavingsTargetForm"
import { ThemeProvider } from "@emotion/react"
import { Theme } from "@mui/system"
import { createTheme } from "@mui/material"

const savingsTarget = new SavingsTarget("0", "name", "123412341234", 1000, "", new Date("2020-12-31"), undefined, 0)
let submittedSavingsTarget: SavingsTarget
const submitFunction = jest.fn((savingsTargetToSave: SavingsTarget) => (submittedSavingsTarget = savingsTargetToSave))
const props: ISavingsTargetFormProps = {
  account: new FinAccount("0"),
  freeAssets: 200,
  onSubmit: submitFunction,
  saveLabel: "save",
  getSavingsTarget: () => new Promise<SavingsTarget>((resolve) => resolve(savingsTarget)),
  amountLabel: "Amount",
  title: "Title",
  onChangeBalance: (changeBalance: { savingsTargetId: string; newBalance: number }): void => {},
}

const setup = (params: { theme: Theme }): RenderResult =>
  render(
    <ThemeProvider theme={params.theme}>
      <SavingsTargetForm {...props} />
    </ThemeProvider>
  )

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

describe("Values in inputs end up in the savings target", () => {
  each`
    nameAttribute | valueToSet
    ${"name"} | ${"Name of the savings target"}
    ${"description"} | ${"This is the description of the savings target"}
    ${"targetAmount"} | ${"1000"}
    ${"startAmount"} | ${"100"}
    `.test("that the value '$valueToSet' can be set to the input $nameAttribute", ({ nameAttribute, valueToSet }) => {
    const theme: Theme = createTheme()
    const form = setup({ theme: theme })
    const input = form.container.querySelector(`[name='${nameAttribute}']`) as HTMLInputElement
    fireEvent.change(input!, { target: { value: valueToSet } })
    expect(input!.value).toBe(valueToSet)
  })
})

describe("The free assets is shown with the currency", () => {
  test("that the value of free assets is shown with the currency CHF", () => {
    const theme: Theme = createTheme()
    const form = setup({ theme: theme })
    const freeAssets = form.getByTestId("freeAssets")
    expect(freeAssets.textContent).toBe(`Free Assets: ${props.freeAssets} CHF`)
  })
})
