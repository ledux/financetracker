import { getCreateSavingsTargetRoute, getEditSavingsTargetRoute } from "../savingsTargetRoutes"

describe("Test the getCreateSavingsTargetRoute", () => {
  test("that the accountId ends up in the url", () => {
    const accountId = "98123412"
    const actual = getCreateSavingsTargetRoute(accountId)
    expect(actual).toMatch(`/${accountId}/`)
  })
})

describe("Test the getEditSavingsTargetRoute", () => {
  test("that the accountId ends up in the url", () => {
    const accountId = "168234235"
    const actual = getEditSavingsTargetRoute(accountId, "")
    expect(actual).toMatch(`/${accountId}/`)
  })

  test("that the savingsTargetId ends up at the end of the url", () => {
    const savingsTargetId = "981152134"
    const actual = getEditSavingsTargetRoute("", savingsTargetId)
    const matcher = new RegExp(`/.*/${savingsTargetId}$`)
    expect(actual).toMatch(matcher)
  })
})
