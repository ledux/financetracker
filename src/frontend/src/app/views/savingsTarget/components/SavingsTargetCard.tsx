import { Grid, Button, Card } from "@mui/material"
import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import DeleteForeverIcon from "@mui/icons-material/DeleteForever"
import EditIcon from "@mui/icons-material/Edit"
import { Title } from "app/components/Elements"
import { useTheme } from "@mui/system"

/** The props for rendering a savings target in the list of the account */
export interface ISavingsTargetCardProps {
  /** The savings target, which should be rendered */
  savingsTarget: SavingsTarget
  /**
   *  Function which will called, when the edit button is clicked
   *  @param target The savings target, on which the button was clicked
   */
  onEditClicked: (target: SavingsTarget) => void
  /**
   *  Function which will called, when the delete button is clicked
   *  @param target The savings target, on which the button was clicked
   */
  onDeleteClicked: (target: SavingsTarget) => void
}

/**
 * Renders a savings target in the details tab of an account
 * @param props The necessary information for rendering
 */
function SavingsTargetCard(props: ISavingsTargetCardProps) {
  const theme = useTheme()
  const cardStyle = {
    maxWidth: "300px",
    marginTop: "15px",
    marginRight: "15px",
    padding: "0.5rem",
    backgroundColor: theme.palette.primary.dark,
  }

  return (
    <Card sx={{ ...cardStyle }} elevation={6} component="div">
      <Grid container spacing={1} className="p-10">
        <Grid item lg={12} xs={12}>
          <Title text={props.savingsTarget.name} />
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>Description</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>{props.savingsTarget.description}</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>Current balance</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>{props.savingsTarget.balance}</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>Target amount</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>{props.savingsTarget.targetAmount}</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>Target date</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <div>{props.savingsTarget.targetDate?.toDateString()}</div>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <Button
            className="x-center"
            color="primary"
            variant="contained"
            onClick={() => props.onEditClicked(props.savingsTarget)}
          >
            <EditIcon color="secondary" />
            <span className="pl-8 capitalize">Edit</span>
          </Button>
        </Grid>
        <Grid item lg={6} md={6} sm={6} xs={6}>
          <Button
            className="x-center"
            color="primary"
            variant="contained"
            onClick={() => props.onDeleteClicked(props.savingsTarget)}
          >
            <DeleteForeverIcon color="error" />
            <span className="pl-8 capitalize">Delete</span>
          </Button>
        </Grid>
      </Grid>
    </Card>
  )
}

export default SavingsTargetCard
