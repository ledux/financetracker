import { Fab, Icon } from "@mui/material"
import { PropsWithChildren, ReactElement } from "react"
import { styled } from "@mui/material/styles"
import { SimpleCard } from "app/components/matx"

/** The props for the container of the savings target */
export interface ISavingsTargetProps {
  /** Function which will be called when the add button is clicked */
  onAddClick: () => void
  /** How much money is not yet allocated to a savings target */
  freeAssets: number
}

const Title = styled("div")(() => ({
  justifyContent: "space-between",
}))

/**
 * Renders a container for the savings target of an account
 * @param props The props for this component
 * @returns A rendered container for the savings target of an account
 */
function SavingsTargets(props: PropsWithChildren<ISavingsTargetProps>): ReactElement {
  return (
    <SimpleCard title="Savings targets" subtitle={undefined} icon={undefined}>
      <Title>
        <Fab onClick={() => props.onAddClick()} color="primary" aria-label="Edit">
          <Icon>add</Icon>
        </Fab>
        <div className="card-title">Free Assets: {props.freeAssets}</div>
      </Title>
      {props.children}
    </SimpleCard>
  )
}

export default SavingsTargets
