import { SimpleCard } from "app/components/matx"
import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import * as React from "react"
import { ChangeEvent } from "react"
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator"
import FinAccount from "app/services/account/FinAccount"
import { Button, Grid, Icon, TextField } from "@mui/material"
import { DatePicker, LocalizationProvider } from "@mui/lab"
import AdapterDateFns from "@mui/lab/AdapterDateFns"
import IncreaseDecrease from "app/components/IncreaseDecrease"

/** The props for rendering a form for a savings target */
export interface ISavingsTargetFormProps {
  /** Function to get the savings target to be rendered */
  getSavingsTarget: () => Promise<SavingsTarget>
  /** The title of the page */
  title: string
  /** The account which the rendered savings target belongs */
  account: FinAccount
  /** The text on the save button */
  saveLabel: string
  /** The text on the amount input, start or current (balance) */
  amountLabel: string
  /** The functionality called, when the form is submitted  */
  onSubmit: (savingsTarget: SavingsTarget) => void
  /** The function called when one of the change balance button is clicked
   * @param savingsTargetId Id of the savings target
   * @param newBalance New balance of the savings target
   */
  onChangeBalance: (changeBalance: { savingsTargetId: string; newBalance: number }) => void
  /** How much money of the account is not yet associated with a savings target */
  freeAssets: number
}

/** Renders a form for editing and creating a savings target */
class SavingsTargetForm extends React.Component<ISavingsTargetFormProps, SavingsTarget> {
  componentDidMount() {
    this.props.getSavingsTarget().then((target) => this.setState({ ...target }))
  }

  changeStartAmount = (value: number): void => {
    this.setState((prevState) => ({ ...prevState, balance: value }))
  }

  changeStartDate(date: Date | null | undefined): void {
    this.setState((prevState) => ({ ...prevState, startDate: date ?? undefined }))
  }

  changeTargetDate(date: Date | null | undefined): void {
    this.setState((prevState) => ({ ...prevState, targetDate: date ?? undefined }))
  }

  changeTargetAmount(value: string): void {
    this.setState((prevState) => ({ ...prevState, targetAmount: Number(value) }))
  }

  changeDescription(value: string): void {
    this.setState((prevState) => ({ ...prevState, description: value }))
  }

  changeName(value: string): void {
    this.setState((prevState) => ({ ...prevState, name: value }))
  }

  public render() {
    return (
      <SimpleCard title={this.props.title} subtitle={`For account '${this.props.account.name}'`} icon="">
        {/* <ValidatorForm onSubmit={this.saveSavingsTarget}> */}
        <ValidatorForm onSubmit={() => this.props.onSubmit(this.state)}>
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12}>
              <TextValidator
                autoFocus
                className="mb16 w-100"
                label="Name"
                name="name"
                onChange={(event: ChangeEvent<HTMLInputElement>) => this.changeName(event.target.value)}
                type="text"
                value={this.state?.name || ""}
                validators={["required"]}
                errorMessages={["This field is required"]}
              />
            </Grid>
            <Grid item lg={6} md={6} sm={12}>
              <TextValidator
                className="mb16 w-100"
                label="Description"
                name="description"
                onChange={(event: ChangeEvent<HTMLInputElement>) => this.changeDescription(event.target.value)}
                type="text"
                value={this.state?.description || ""}
                multiline
                rows={3}
              />
            </Grid>
            <Grid item lg={6} md={6} sm={12}>
              <TextValidator
                className="mb16 w-100"
                label="Target Amount"
                name="targetAmount"
                onChange={(event: ChangeEvent<HTMLInputElement>) => this.changeTargetAmount(event.target.value)}
                type="number"
                value={this.state?.targetAmount || 0}
                validators={["required"]}
                errorMessages={["This field is required"]}
              />
            </Grid>
            <Grid item lg={6} md={6} sm={12}>
              {/* <>
                                <KeyboardDatePicker
                                    className="mb-16 w-100"
                                    margin="none"
                                    id="targetDatePicker"
                                    label="Target Date"
                                    inputVariant="standard"
                                    variant="inline"
                                    type="text"
                                    format="dd. MMM yyyy"
                                    autoOk
                                    value={this.state?.targetDate}
                                    onChange={(date: MaterialUiPickersDate) =>
                                        this.changeTargetDate(date)
                                    }
                                    KeyboardButtonProps={{
                                        'aria-label': 'change target date',
                                    }}
                                />
                            </MuiPickersUtilsProvider> */}
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  value={this.state?.targetDate}
                  onChange={(date) => this.changeTargetDate(date)}
                  renderInput={(props) => <TextField {...props} label="Target Date" />}
                />
              </LocalizationProvider>
            </Grid>
            <Grid item lg={6} md={6} sm={12}>
              <IncreaseDecrease
                intervalOne={10}
                intervalTwo={100}
                label={this.props.amountLabel}
                onChange={this.changeStartAmount}
                value={this.state?.balance || 0}
                onClick={(newValue) =>
                  this.props.onChangeBalance({ savingsTargetId: this.state.id as string, newBalance: newValue })
                }
              />
            </Grid>
            <Grid item lg={6} md={6} sm={12}>
              <div
                data-testid="freeAssets"
                className="font-size-18 mb16 w-100"
              >{`Free Assets: ${this.props.freeAssets} CHF`}</div>
            </Grid>
            <Grid item lg={6} md={6} sm={12}>
              {/* <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    className="mb-16 w-100"
                                    margin="none"
                                    id="startDatePicker"
                                    label="Start Date"
                                    inputVariant="standard"
                                    variant="inline"
                                    type="text"
                                    format="dd. MMM yyyy"
                                    autoOk
                                    value={this.state?.startDate}
                                    onChange={(date: MaterialUiPickersDate) =>
                                        this.changeStartDate(date)
                                    }
                                    KeyboardButtonProps={{
                                        'aria-label': 'change target date',
                                    }}
                                />
                            </MuiPickersUtilsProvider> */}
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  value={this.state?.startDate}
                  onChange={(date) => this.changeStartDate(date)}
                  renderInput={(props) => <TextField {...props} label="Start Date" />}
                />
              </LocalizationProvider>
            </Grid>
          </Grid>
          <Grid container spacing={6}>
            <Grid item lg={2} md={2} sm={6} xs={6}>
              <Button color="primary" variant="contained" type="submit">
                <Icon>send</Icon>
                <span className="pl-8 capitalize">{this.props.saveLabel}</span>
              </Button>
            </Grid>
          </Grid>
        </ValidatorForm>
      </SimpleCard>
    )
  }
}

export default SavingsTargetForm
