import { styled } from "@mui/system"
import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import SavingsTargetCard from "./SavingsTargetCard"

/** Props for rendering a list of savings targets */
export interface ISavingsTargetListProps {
  /** The savings targets which gets rendered */
  savingsTargets: SavingsTarget[]
  /**
   *  Function which will called, when the edit button is clicked
   *  @param target The savings target, on which the button was clicked
   */
  onEditClicked: (target: SavingsTarget) => void
  /**
   *  Function which will called, when the delete button is clicked
   *  @param target The savings target, on which the button was clicked
   */
  onDeleteClicked: (target: SavingsTarget) => void
}

const Card = styled("div")`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
`

/**
 * Renders multiple savings targets
 * @param props The props which are needed to be rendered
 */
function SavingsTargetList(props: ISavingsTargetListProps) {
  return (
    <Card className="flex">
      {props.savingsTargets.map((target) => (
        <SavingsTargetCard
          key={target.id}
          onDeleteClicked={props.onDeleteClicked}
          onEditClicked={props.onEditClicked}
          savingsTarget={target}
        />
      ))}
    </Card>
  )
}

export default SavingsTargetList
