import Page, { RouteSegment } from "app/components/Page"
import { getAccountById } from "app/services/account/accountServices"
import FinAccount from "app/services/account/FinAccount"
import {
  getSavingsTargetByAccountFactory,
  getSavingsTargetByIdFactory,
  updateSavingsTargetFactory,
} from "app/services/savingsTarget/factories"
import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import UpdateSavingsTarget from "app/services/savingsTarget/UpdateSavingsTarget"
import SetBalance from "app/services/savingsTarget/SetBalance"
import { ReactElement, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { setBalance } from "app/services/savingsTarget/savingsTargetsServices"
import SavingsTargetAccount from "app/services/savingsTarget/SavingsTargetAccount"
import accountRouteNames from "../accounts/accountRouteNames"
import SavingsTargetForm, { ISavingsTargetFormProps } from "./components/SavingsTargetForm"

function EditSavingsTarget(): ReactElement {
  const navigate = useNavigate()
  const { accountId, savingsTargetId } = useParams()
  const accId = accountId ?? ""
  const targetId = savingsTargetId ?? ""
  const [account, setAccount] = useState(new FinAccount("0"))
  const [targetAccount, setTargetAccount] = useState(new SavingsTargetAccount("", 0, []))
  const getSavingsTargetsByAccount = getSavingsTargetByAccountFactory()
  useEffect(() => {
    getAccountById(accId).then((accountById) => setAccount(accountById))
    getSavingsTargetsByAccount(accId).then((savingsTargetAccount) => setTargetAccount(savingsTargetAccount))
  }, [])

  const updateTarget = (savingsTarget: SavingsTarget): void => {
    updateSavingsTarget(savingsTarget).then(() => {
      navigate(-1)
    })
  }

  const updateSavingsTarget: (target: SavingsTarget) => Promise<UpdateSavingsTarget> = updateSavingsTargetFactory()

  const getSavingsTargetById = getSavingsTargetByIdFactory()

  const getTarget = (): Promise<SavingsTarget> => getSavingsTargetById(targetId)

  const props: ISavingsTargetFormProps = {
    freeAssets: targetAccount.freeAssets,
    account,
    onSubmit: updateTarget,
    getSavingsTarget: getTarget,
    saveLabel: "Update",
    amountLabel: "New Balance",
    title: "Edit Savings target",
    onChangeBalance: (changeBalance: { savingsTargetId: string; newBalance: number }): void => {
      setBalance(new SetBalance(changeBalance.newBalance, changeBalance.savingsTargetId))
    },
  }

  const breadCrumbs = [
    new RouteSegment("Accounts", accountRouteNames.allAccounts),
    new RouteSegment(account.name ?? "", `${accountRouteNames.viewAccount}${accountId}`),
    new RouteSegment("Update"),
  ]

  return (
    <Page breadCrumbRoutes={breadCrumbs}>
      <SavingsTargetForm {...props} />
    </Page>
  )
}

export default EditSavingsTarget
