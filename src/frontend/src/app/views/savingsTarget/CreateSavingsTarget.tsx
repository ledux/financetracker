import { ReactElement, useEffect, useState } from "react"
import { useParams, useNavigate } from "react-router-dom"
import moment from "moment"
import Page, { RouteSegment } from "app/components/Page"
import SavingsTargetForm, { ISavingsTargetFormProps } from "app/views/savingsTarget/components/SavingsTargetForm"
import accountRouteNames from "app/views/accounts/accountRouteNames"
import { getAccountById } from "app/services/account/accountServices"
import { getSavingsTargetByAccountFactory } from "app/services/savingsTarget/factories"
import { createSavingsTarget } from "app/services/savingsTarget/savingsTargetsServices"
import FinAccount from "app/services/account/FinAccount"
import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import SavingsTargetAccount from "app/services/savingsTarget/SavingsTargetAccount"

/**
 * Renders a page for creating a savings target
 * @param props The props for the creation of a savings target
 */
function CreateSavingsTarget(): ReactElement {
  const navigate = useNavigate()
  const { accountId } = useParams()
  const accId = accountId ?? ""
  const [account, setAccount] = useState(new FinAccount("0"))
  const [targetAccount, setTargetAccount] = useState(new SavingsTargetAccount("", 0, []))
  const getSavingsTargetsByAccount = getSavingsTargetByAccountFactory()

  useEffect(() => {
    getAccountById(accId).then((accountById) => setAccount(accountById))
    getSavingsTargetsByAccount(accId).then((savingsTargetAccount) => setTargetAccount(savingsTargetAccount))
  }, [])

  const getTarget: () => Promise<SavingsTarget> = () =>
    getAccountById(accId).then(
      (acc) => {
        const targetDate = moment().add(3, "M").endOf("M").toDate()
        const startDate = moment().startOf("M").toDate()
        return new SavingsTarget("", "", acc.id, 0, "", targetDate, startDate, 0)
      },
      (): SavingsTarget => new SavingsTarget("", "No account assigned", "0", 0, "", new Date(), new Date(), 0)
    )

  const submitTarget = (target: SavingsTarget): void => {
    createSavingsTarget(target).then(() => navigate(-1))
  }

  const formProps: ISavingsTargetFormProps = {
    account,
    onSubmit: submitTarget,
    getSavingsTarget: getTarget,
    saveLabel: "Save",
    title: "Create Savings target",
    amountLabel: "Start amount",
    freeAssets: targetAccount.freeAssets,
    onChangeBalance: (): void => {},
  }

  const breadCrumbs = [
    new RouteSegment("Accounts", accountRouteNames.allAccounts),
    new RouteSegment(account.name ?? "", `${accountRouteNames.viewAccount}${accountId}`),
    new RouteSegment("Create Savings Target"),
  ]

  return (
    <Page breadCrumbRoutes={breadCrumbs}>
      <SavingsTargetForm {...formProps} />
    </Page>
  )
}

export default CreateSavingsTarget
