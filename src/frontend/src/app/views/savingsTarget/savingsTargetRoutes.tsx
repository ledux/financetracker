import Loadable from "app/components/matx/Loadable/Loadable"
import React, { lazy } from "react"

const CreateSavingsTarget = Loadable(lazy(() => import("./CreateSavingsTarget")))
const EditSavingsTarget = Loadable(lazy(() => import("./EditSavingsTarget")))

// const EditSavingsTarget = MatxLoadable({
//   loader: () => import("./EditAccountingEntry"),
// })

const routeNames = {
  createSavingsTarget: "/accounts/:accountId/savingstarget/create",
  editSavingsTarget: "/accounts/:accountId/savingstarget/:savingsTargetId",
}

/**
 * Constructs an URL for the page of creating a new savings target for a certain account
 * @param accountId The id of the account, which ends up in the url
 */
const getCreateSavingsTargetRoute: (accountId: string) => string = (accountId: string) =>
  routeNames.createSavingsTarget.replace(":accountId", accountId)

/**
 * Constructs an URL for the edit page of an savings target for a certain account
 * @param accountId The id of the account
 * @param entryId The id of the savings target
 */
const getEditSavingsTargetRoute: (accountId: string, savingsTargetId: string) => string = (
  accountId: string,
  savingsTargetId: string
) => routeNames.editSavingsTarget.replace(":accountId", accountId).replace(":savingsTargetId", savingsTargetId)

const savingsTargetRoutes = [
  {
    path: routeNames.createSavingsTarget,
    element: <CreateSavingsTarget />,
  },
  {
    path: routeNames.editSavingsTarget,
    element: <EditSavingsTarget />,
  },
]

export default savingsTargetRoutes
export { routeNames, getCreateSavingsTargetRoute, getEditSavingsTargetRoute }
