import { useNavigate } from "react-router-dom"
import FinAccount from "app/services/account/FinAccount"
import { createAccount } from "app/services/account/accountServices"
import Page from "app/components/Page"
import AccountFormClass from "./components/AccountFormClass"
import routeNames from "./accountRouteNames"

/** Controls the view of the create account screen */
function CreateAccount() {
  const navigate = useNavigate()
  const addAccount: (account: FinAccount) => void = (account: FinAccount) => {
    createAccount(account).then(() => navigate(-1))
  }

  const fetchAccount: () => Promise<FinAccount> = () => Promise.resolve(new FinAccount("0"))

  return (
    <Page breadCrumbRoutes={[{ name: "Accounts", path: routeNames.allAccounts }, { name: "Create" }]}>
      <AccountFormClass getAccount={fetchAccount} saveAccount={addAccount} isEdit={false} title="Create Account" />
      {/* <AccountForm
        getAccount={() => {
          return new Promise<FinAccount>((resolve, _reject) =>
            resolve(new FinAccount(0))
          );
        }}
        saveAccount={(account: FinAccount) => createAccount(account)}
        isEdit={false}
      /> */}
    </Page>
  )
}

export default CreateAccount
