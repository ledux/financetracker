const routeNames = {
  allAccounts: "/accounts/all",
  createAccount: "/accounts/create",
  editAccount: "/accounts/edit/",
  viewAccount: "/accounts/view/",
}

export default routeNames
