import Loadable from "app/components/matx/Loadable/Loadable"
import { lazy } from "react"
import routeNames from "./accountRouteNames"

const Accounts = Loadable(lazy(() => import("./Accounts")))

const CreateAccounts = Loadable(lazy(() => import("./CreateAccount")))

const EditAccount = Loadable(lazy(() => import("./EditAccount")))

const ViewAccount = Loadable(lazy(() => import("./ViewAccount")))

const accountsRoutes = [
  {
    path: routeNames.allAccounts,
    element: <Accounts />,
  },
  {
    path: routeNames.createAccount,
    element: <CreateAccounts />,
  },
  {
    path: `${routeNames.editAccount}:id`,
    element: <EditAccount />,
  },
  {
    path: `${routeNames.viewAccount}:id`,
    element: <ViewAccount />,
  },
]

export default accountsRoutes
