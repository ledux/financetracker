import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import FinAccount from "app/services/account/FinAccount"
import Page from "app/components/Page"
import { getAllAccounts, deleteAccount } from "app/services/account/accountServices"
import { SimpleCard } from "app/components/matx"
import ConfirmDelete from "app/components/ConfirmDelete"
import AccountsList from "./components/AccountsList"
import routeNames from "./accountRouteNames"

/** Component for the view of all accounts */
function Accounts() {
  const [accounts, setAccounts] = useState(new Array<FinAccount>())
  const navigate = useNavigate()

  const editAccount: (account: FinAccount) => void = (account: FinAccount) => {
    navigate(`${routeNames.editAccount}${account.id}`)
  }

  const viewAccount: (account: FinAccount) => void = (account: FinAccount) => {
    navigate(`${routeNames.viewAccount}${account.id}`)
  }

  const handleDelete: (id: string) => void = (id: string) => {
    closeConfirmDialog()
    deleteAccount(id).then(() => fetchAccounts())
  }

  const [confirmProps, setConfirmProps] = useState({
    handleNo: () => closeConfirmDialog(),
    handleYes: () => {
      // placeholder. Will be set on open
    },
    isOpen: false,
    title: "",
    text: "",
  })

  const openConfirmDialog: (account: FinAccount) => void = (account: FinAccount) => {
    setConfirmProps({
      ...confirmProps,
      title: `Delete account "${account.name}?"`,
      text: `Do you want to delete the account  named "${account.name}"? This cannot be undone.`,
      handleYes: () => handleDelete(account.id),
      isOpen: true,
    })
  }

  const closeConfirmDialog: () => void = () => {
    setConfirmProps({
      ...confirmProps,
      isOpen: false,
    })
  }

  const fetchAccounts: () => void = () => {
    getAllAccounts().then((allAccounts) => {
      setAccounts(allAccounts)
    })
  }

  useEffect(() => fetchAccounts(), [])

  return (
    <Page breadCrumbRoutes={[{ name: "Accounts" }]}>
      <SimpleCard title="All accounts" subtitle="" icon="">
        <AccountsList accounts={accounts} deleteAccount={openConfirmDialog} editAccount={editAccount} viewAccount={viewAccount} />
      </SimpleCard>
      <ConfirmDelete {...confirmProps} />
    </Page>
  )
}

export default Accounts
