import { ReactElement } from "react"
import { TableCell, TableRow, Table, TableHead, TableBody, IconButton, Icon } from "@mui/material"
import FinAccount from "app/services/account/FinAccount"

/** Interface for the props of the list in the accounts screen */
export interface IAccountsListProps {
  /** The accounts which will be displayed in the list */
  accounts: FinAccount[]
  /**
   * The function which will be called when a account should be deleted
   * @param account: The account which should be deleted
   */
  deleteAccount: (account: FinAccount) => void
  /**
   * The function which will be called when an account should be edited
   * @param account: The data of the account, which will be edited
   */
  editAccount: (account: FinAccount) => void
  /**
   * The function which will be called when an account should be viewed
   * @param account: The data of the account, which will be viewed
   */
  viewAccount: (account: FinAccount) => void
}

const actionColumnStyle = { width: "65px" }

/**
 * Renders a list with all accounts
 * @param props The props of the component
 */
function AccountsList(props: IAccountsListProps): ReactElement {
  return (
    <div className="w-100 overflow-auto">
      <Table style={{ whiteSpace: "pre" }}>
        <TableHead>
          <TableRow>
            <TableCell className="px-0">Name</TableCell>
            <TableCell className="px-0">Description</TableCell>
            <TableCell className="px-0">Balance</TableCell>
            <TableCell style={actionColumnStyle} className="px-0">
              Edit
            </TableCell>
            <TableCell style={actionColumnStyle} className="px-0">
              Delete
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.accounts.map((account) => (
            <TableRow key={account.id} onClick={() => props.viewAccount(account)}>
              <TableCell className="px-0">{account.name}</TableCell>
              <TableCell className="px-0">{account.description}</TableCell>
              <TableCell className="px-0">{account.balance}</TableCell>
              <TableCell style={actionColumnStyle} className="px-0">
                <IconButton
                  onClick={(event) => {
                    event.stopPropagation()
                    props.editAccount(account)
                  }}
                >
                  <Icon color="primary">create</Icon>
                </IconButton>
              </TableCell>
              <TableCell style={actionColumnStyle} size="small" className="px-0">
                <IconButton
                  onClick={(event) => {
                    event.stopPropagation()
                    props.deleteAccount(account)
                  }}
                >
                  <Icon color="error">close</Icon>
                </IconButton>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  )
}

export default AccountsList
