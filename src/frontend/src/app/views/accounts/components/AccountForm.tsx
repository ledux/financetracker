import * as React from "react"
import { useState, useEffect } from "react"
import { ValidatorForm } from "react-material-ui-form-validator"
import { Button, Icon, Grid, TextField } from "@mui/material"
import FinAccount from "app/services/account/FinAccount"
import { SimpleCard } from "app/components/matx"

interface IAccountFormProps {
  getAccount: () => Promise<FinAccount>
  saveAccount: (account: FinAccount) => void
  isEdit: boolean
  title?: string
  saveButtonText?: string
}

const AccountForm: React.FunctionComponent<IAccountFormProps> = (props) => {
  const [account, setAccount] = useState<FinAccount>()

  useEffect(() => {
    props.getAccount().then((currentAccount) => setAccount(currentAccount))
  }, [])

  const submitAccount = (event: any) => {
    if (account) {
      props.saveAccount(account)
    }
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (account) {
      account.name = event.target.value
    }
    setAccount(account)
  }

  return (
    <SimpleCard title={props.title} subtitle="" icon="">
      <div>
        <ValidatorForm onSubmit={submitAccount}>
          <Grid container spacing={6}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextField
                className="mb-16 w-100"
                label="Name of the Account"
                onChange={handleChange}
                type="text"
                name="name"
                value={account?.name}
                // validators={["required", "minStringLength: 4"]}
                // errorMessages={["this field is required"]}
              />
              <TextField
                className="mb-16 w-100"
                label="Balance"
                onChange={handleChange}
                type="number"
                name="balance"
                value={account?.balance}
                disabled={props.isEdit}
              />
              <Button color="primary" variant="contained" type="submit">
                <Icon>send</Icon>
                <span className="pl-8 capitalize">Create</span>
              </Button>
            </Grid>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <TextField
                className="mb-16 w-100"
                label="Description"
                onChange={handleChange}
                type="text"
                name="description"
                value={account?.description}
              />
            </Grid>
          </Grid>
        </ValidatorForm>
      </div>
    </SimpleCard>
  )
}

export default AccountForm
