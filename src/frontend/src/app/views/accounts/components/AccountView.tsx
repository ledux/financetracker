import { ReactElement } from "react"
import FinAccount from "app/services/account/FinAccount"
import { Fab, Icon } from "@mui/material"
import { createTheme, styled } from "@mui/system"
// import { createTheme } from "@mui/lab/node_modules/@mui/system"
import { SimpleCard } from "app/components/matx"
import { H3 } from "app/components/matx/Typography"

/** Props for the account component in the view screen of an account */
interface IAccountViewProps {
  /** The account itself */
  account?: FinAccount
  /** The function, which will be called when the edit button is clicked */
  onEditClick: () => void
}

const theme = createTheme()

const Button = styled(Fab)(() => ({
  margin: theme.spacing(4),
}))

const DescriptionEdit = styled("div")(() => ({
  alignItems: "flex-start",
  justifyContent: "space-between",
}))

/**
 * Component for the account part of the view of an account
 * @param props The props of the component
 */
function AccountView(props: IAccountViewProps): ReactElement {
  return (
    <SimpleCard title={props.account?.name} subtitle={`${props.account?.description}`} icon="">
      <DescriptionEdit className="flex">
        <H3 className="" ellipsis="" textTransform="">{` ${props.account?.balance ?? "0"} CHF`}</H3>
        <div>
          <Button onClick={() => props.onEditClick()} color="primary" aria-label="Edit">
            <Icon>edit_icon</Icon>
          </Button>
        </div>
        <div className="card-title ">&nbsp;</div>
      </DescriptionEdit>
    </SimpleCard>
  )
}

AccountView.defaultProps = {
  account: new FinAccount("0"),
}

export default AccountView
