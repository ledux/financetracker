import React, { ReactElement } from "react"
import AccountingEntries, { IAccountingEntriesProps } from "app/views/accountingEntries/components/AccountingEntries"
import AccountingEntryList, { IAccountingEntryListProps } from "app/views/accountingEntries/components/AccountingEntryList"
import SavingsTargets, { ISavingsTargetProps } from "app/views/savingsTarget/components/SavingsTargets"
import SavingsTargetList, { ISavingsTargetListProps } from "app/views/savingsTarget/components/SavingsTargetList"
import { Tab, Tabs } from "@mui/material"
import TabPanel from "app/components/TabPanel"
import { SimpleCard } from "app/components/matx"

/** The interface of the props for the tabs */
export interface IDetailsTabProps {
  /** Holds the labels for the tabs selectors */
  labels: {
    /** The label for the tab selector for the accounting entries */
    accountingEntries: string
    /** The label for the tab selector for the savings target */
    savingsTarget: string
  }
  /** The props for the content of the tabs panels */
  tabProps: {
    /** The props for the container of the accounting entries */
    accountingEntries: IAccountingEntriesProps
    /** The props for the actual list of the accouting entries */
    accountingEntryList: IAccountingEntryListProps
    /** The props for the container of the savings target */
    savingsTargets: ISavingsTargetProps
    /** The props for the list of the savings targets */
    savingsTargetsList: ISavingsTargetListProps
  }
}

const tabNavProps: (index: number) => {
  id: string
  "aria-controls": string
} = (index: number) => ({
  id: `full-width-tab-${index}`,
  "aria-controls": `full-width-tabpanel-${index}`,
})

/**
 * Renders tabs with data associated with an account
 * @param props The props for the tabs and for the contents of the tabs
 */
function DetailsTab(props: IDetailsTabProps): ReactElement {
  const [value, setValue] = React.useState(0)

  const handleChange: (event: React.FormEvent<{}>, newValue: any) => void = (_event, newValue) => {
    setValue(newValue)
  }

  return (
    <SimpleCard title={undefined} subtitle={undefined} icon={undefined}>
      <Tabs value={value} onChange={handleChange} indicatorColor="primary" textColor="primary" variant="fullWidth">
        <Tab label={props.labels.accountingEntries} {...tabNavProps(0)} />
        <Tab label={props.labels.savingsTarget} {...tabNavProps(1)} />
      </Tabs>
      <TabPanel currentIndex={value} indexOfPanel={0}>
        <AccountingEntries {...props.tabProps.accountingEntries}>
          <AccountingEntryList {...props.tabProps.accountingEntryList} />
        </AccountingEntries>
      </TabPanel>
      <TabPanel currentIndex={value} indexOfPanel={1}>
        <SavingsTargets {...props.tabProps.savingsTargets}>
          <SavingsTargetList {...props.tabProps.savingsTargetsList} />
        </SavingsTargets>
      </TabPanel>
    </SimpleCard>
  )
}

export default DetailsTab
