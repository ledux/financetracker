import * as React from "react"
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator"
import { Button, Icon, Grid } from "@mui/material"
import FinAccount from "app/services/account/FinAccount"
import { SimpleCard } from "app/components/matx"
import { styled } from "@mui/system"

/** The interface for the props of the form for the create or update account screen */
export interface IAccountFormClassProps {
  /**
   * Function to get the data, which will be displayed in the form
   * @returns A promise which resolves the account data
   */
  getAccount: () => Promise<FinAccount>
  /**
   * The function which will save the account
   * @param account: The account data, which will be saved
   */
  saveAccount: (account: FinAccount) => void
  /** If the acount is edited. If true, the balance is not editable */
  isEdit: boolean
  /** The title of the form, shown at the top of the form */
  title?: string
}

const TextField = styled(TextValidator)(() => ({
  width: "100%",
  marginBottom: "16px",
}))

/** The form for editing and creating an account */
export default class AccountFormClass extends React.Component<IAccountFormClassProps, FinAccount> {
  componentDidMount() {
    this.props.getAccount().then((account) => {
      this.setState(account)
    })
  }

  private changeName = (name: string): void => {
    this.setState({ name })
  }

  private changeDescription = (description: string): void => {
    this.setState({ description })
  }

  private changeBalance = (balance: number): void => {
    this.setState({ balance })
  }

  private submitAccount = (): void => {
    this.props.saveAccount(this.state)
  }

  public render() {
    return (
      <SimpleCard title={this.props.title} subtitle="" icon="account_balance">
        <div>
          <ValidatorForm
            onSubmit={(event: React.FormEvent) => {
              event.preventDefault()
              this.submitAccount()
            }}
          >
            <Grid container spacing={6}>
              <Grid item lg={6} md={6} sm={12} xs={12} sx={{ mt: 2 }}>
                <TextField
                  label="Name of the Account"
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.changeName(event.target.value)}
                  type="text"
                  name="name"
                  value={this.state?.name}
                  validators={["required", "minStringLength: 4"]}
                  errorMessages={["this field is required"]}
                />
                <TextField
                  label={this.props.isEdit ? null : "Balance"}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.changeBalance(event.target.valueAsNumber)}
                  type="number"
                  name="balance"
                  value={this.state?.balance}
                  disabled={this.props.isEdit}
                />
                <Button color="primary" variant="contained" type="submit">
                  <Icon>send</Icon>
                  <span className="pl-8 capitalize">{this.props.isEdit ? "Save" : "Create"}</span>
                </Button>
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12} sx={{ mt: 2 }}>
                <TextField
                  label="Description"
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.changeDescription(event.target.value)}
                  type="text"
                  name="description"
                  value={this.state?.description}
                />
              </Grid>
            </Grid>
          </ValidatorForm>
        </div>
      </SimpleCard>
    )
  }
}
