import { useState, useEffect, ReactElement, Key } from "react"
import { MatxDivider } from "app/components/matx"
import FinAccount from "app/services/account/FinAccount"
import Page from "app/components/Page"
import { getAccountById } from "app/services/account/accountServices"
import { getAccountingEntriesByAccountId, deleteAccountingEntry } from "app/services/accountingEntries/accountingEntryServices"
import { useNavigate, useParams } from "react-router-dom"
import { getSavingsTargetByAccountFactory } from "app/services/savingsTarget/factories"
import { deleteSavingsTarget } from "app/services/savingsTarget/savingsTargetsServices"
import AccountingEntry from "app/services/accountingEntries/AccountingEntry"
import SavingsTargetAccount from "app/services/savingsTarget/SavingsTargetAccount"
import SavingsTarget from "app/services/savingsTarget/SavingsTarget"
import ConfirmDelete from "app/components/ConfirmDelete"
import DetailsTab, { IDetailsTabProps } from "./components/DetailsTab"
import { getCreateSavingsTargetRoute, getEditSavingsTargetRoute } from "../savingsTarget/savingsTargetRoutes"
import { ISavingsTargetListProps } from "../savingsTarget/components/SavingsTargetList"
import { getCreateEntryRoute, getEditEntryRoute } from "../accountingEntries/accountingEntryRoutes"
import routeNames from "./accountRouteNames"
import AccountView from "./components/AccountView"

/** Component for the view screen of an account  */
function ViewAccount(): ReactElement {
  const { id } = useParams()
  const accountId: string = id ?? ""
  const [account, setAccount] = useState<FinAccount>()
  const [entries, setEntries] = useState(new Array<AccountingEntry>())
  const [targetsAccount, setTargetsAccount] = useState(new SavingsTargetAccount("0", 0, []))
  const [confirmProps, setConfirmProps] = useState({
    handleNo: () => closeConfirmDialog(),
    handleYes: () => {
      // placeholder. Will be set on open
    },
    isOpen: false,
    text: "",
    title: "",
  })

  const navigate = useNavigate()
  const getSavingsTargetsByAccount = getSavingsTargetByAccountFactory()
  useEffect(() => {
    fetchData()
  }, [])

  const openDeleteAccountingEntryDialog = (entry: AccountingEntry): void => {
    setConfirmProps({
      ...confirmProps,
      title: `Delete ${entry.description}?`,
      text: `Do you want to delete the account entry "${entry.description}"? This cannot be undone.`,
      isOpen: true,
      handleYes: () => handleDeleteAccountingEntry(entry.id),
    })
  }

  const openDeleteSavingsTargetDialog = (target: SavingsTarget): void => {
    setConfirmProps({
      ...confirmProps,
      title: `Delete ${target.description}`,
      text: `Do you want to delete the savings target "${target.description}"? This cannot be undone.`,
      isOpen: true,
      handleYes: () => handleDeleteSavingsTarget(target.id),
    })
  }

  const closeConfirmDialog: () => void = () => {
    setConfirmProps({
      ...confirmProps,
      isOpen: false,
    })
  }

  const handleDeleteAccountingEntry: (entryId: string) => void = (entryId: string) => {
    closeConfirmDialog()
    deleteAccountingEntry(entryId).then(() => fetchData())
  }

  const handleDeleteSavingsTarget = (targetId: Key): void => {
    closeConfirmDialog()
    deleteSavingsTarget(targetId as string).then(() => fetchData())
  }

  const fetchData: () => void = () => {
    getAccountingEntriesByAccountId(accountId).then((entriesByAccount) => setEntries(entriesByAccount))
    getAccountById(accountId).then((accountById) => setAccount(accountById))
    getSavingsTargetsByAccount(accountId).then((savingsTargetAccount) => setTargetsAccount(savingsTargetAccount))
  }

  const editAccount: () => void = () => {
    navigate(`${routeNames.editAccount}${account?.id}`)
  }

  const editAccountingEntry: (entry: AccountingEntry) => void = (entry: AccountingEntry) => {
    const route = getEditEntryRoute(account?.id ?? "", entry.id)
    navigate(route)
  }

  const deleteEntry: (entry: AccountingEntry) => void = (entry: AccountingEntry) => {
    openDeleteAccountingEntryDialog(entry)
  }

  const goToCreateEntryPage: () => void = () => {
    const route = getCreateEntryRoute(account?.id ?? "")
    navigate(route)
  }

  const goToCreateSavingsTargetPage: () => void = () => {
    const route = getCreateSavingsTargetRoute(account?.id ?? "")
    navigate(route)
  }

  const goToEditSavingsTargetPage = (target: SavingsTarget): void => {
    const route = getEditSavingsTargetRoute(account?.id ?? "", target.id as string)
    navigate(route)
  }

  const savingsTargetCardProps: ISavingsTargetListProps = {
    savingsTargets: targetsAccount.savingsTargets,
    onDeleteClicked: openDeleteSavingsTargetDialog,
    onEditClicked: goToEditSavingsTargetPage,
  }

  const props: IDetailsTabProps = {
    labels: {
      accountingEntries: "accounting entries",
      savingsTarget: "Savings Targets",
    },
    tabProps: {
      accountingEntries: { onAddClick: () => goToCreateEntryPage() },
      accountingEntryList: {
        accountingEntries: entries,
        onDeleteClicked: deleteEntry,
        onEditClicked: editAccountingEntry,
      },
      savingsTargets: {
        onAddClick: goToCreateSavingsTargetPage,
        freeAssets: targetsAccount.freeAssets,
      },
      savingsTargetsList: savingsTargetCardProps,
    },
  }

  return (
    <Page breadCrumbRoutes={[{ name: "Accounts", path: routeNames.allAccounts }, { name: account?.name ?? "View Account" }]}>
      <AccountView account={account} onEditClick={editAccount} />
      <MatxDivider text="" sx={undefined} />
      <DetailsTab {...props} />
      <ConfirmDelete {...confirmProps} />
    </Page>
  )
}

export default ViewAccount
