import { ReactElement } from "react"
import { updateAccount, getAccountById } from "app/services/account/accountServices"
import { useNavigate, useParams } from "react-router-dom"
import FinAccount from "app/services/account/FinAccount"
import Page from "app/components/Page"
import AccountFormClass from "./components/AccountFormClass"
import routeNames from "./accountRouteNames"

/** Component for the edit view of an account  */
function EditAccount(): ReactElement {
  const { id } = useParams()
  const accountId = id ?? ""
  const navigate = useNavigate()

  const saveAccount: (account: FinAccount) => void = (account: FinAccount) => {
    updateAccount(account).then(() => navigate(-1))
  }

  return (
    <Page
      breadCrumbRoutes={[
        { name: "Accounts", path: routeNames.allAccounts },
        { name: "Account", path: `${routeNames.viewAccount}${id}` },
        { name: "Edit account" },
      ]}
    >
      <AccountFormClass
        getAccount={() => getAccountById(accountId)}
        saveAccount={(account) => saveAccount(account)}
        isEdit
        title="Edit Account"
      />
      {/* <AccountForm
        getAccount={() => {
          return new Promise<FinAccount>((resolve, _reject) =>
            getAccountById(id).then((account) => resolve(account))
          );
        }}
        saveAccount={(account: FinAccount) => saveAccount(account)}
        isEdit={false}
      /> */}
    </Page>
  )
}

export default EditAccount
