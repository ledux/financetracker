/**
 * Handles errorneous behaviour
 * @param message The message which describes the error
 * @param severity The severity of the error, i.e. info, debug, error
 */
function handleError(message: string, severity: string) {
  console.log(`${severity}: ${message}`)
}

export default handleError
