import axios from "axios"
import handleError from "app/services/utils/errorHandler"
import config from "app/config/AppConfig"
import AccountingEntry from "../accountingEntries/AccountingEntry"

const baseUrl = config.backend.accountingApiUrl

/**
 * Calls the backend to get all accounting entries by an account
 * @param accountId The account id
 * @returns A promise which resolves to an array of accounting entries
 */
const getByAccountId: (accountId: string) => Promise<AccountingEntry[]> = (accountId: string) => {
  const url = `${baseUrl}/account/${accountId}`
  const promise = new Promise<AccountingEntry[]>((resolve, reject) => {
    axios
      .get<AccountingEntry[]>(url)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Sends an accounting entry to the server via a POST request
 * @param entry The data sent to the server
 * @returns A promise with the created entry from the server
 */
const postAccountingEntry: (entry: AccountingEntry) => Promise<AccountingEntry> = (entry: AccountingEntry) => {
  const url = baseUrl
  const promise = new Promise<AccountingEntry>((resolve, reject) => {
    axios
      .post<AccountingEntry>(url, entry)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Gets an accounting entry from the server by its id
 * @param id The id of the requested accouting entry
 * @returns A promise which resolves the requested entry
 */
const getById: (id: string) => Promise<AccountingEntry> = (id: string) => {
  const url = `${baseUrl}/${id}`
  const promise = new Promise<AccountingEntry>((resolve, reject) => {
    axios
      .get<AccountingEntry>(url)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Sends an accounting entry to the server via a put request
 * @param entry The data, which gets put to the server
 * @returns A promise which resolves the same data as received
 */
const putEntry: (entry: AccountingEntry) => Promise<AccountingEntry> = (entry: AccountingEntry) => {
  const url = `${baseUrl}/${entry.id}`
  const promise = new Promise<AccountingEntry>((resolve, reject) => {
    axios
      .put(url, entry)
      .then(() => resolve(entry))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Deletes an entry from the server
 * @param entryId The id of the entry to be deleted
 * @returns A promise with the status text
 */
const deleteEntry: (entryId: string) => Promise<string> = (entryId: string) => {
  const url = `${baseUrl}/${entryId}`
  const promise = new Promise<string>((resolve, reject) => {
    axios
      .delete(url)
      .then((response) => resolve(response.statusText))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

export { getByAccountId, postAccountingEntry, getById, putEntry, deleteEntry }
