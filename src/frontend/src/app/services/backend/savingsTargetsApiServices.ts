import config from "app/config/AppConfig"
import axios from "axios"
import CreateSavingsTarget from "../savingsTarget/CreateSavingsTarget"
import SavingsTarget from "../savingsTarget/SavingsTarget"
import SavingsTargetAccount from "../savingsTarget/SavingsTargetAccount"
import SetBalance from "../savingsTarget/SetBalance"
import UpdateSavingsTarget from "../savingsTarget/UpdateSavingsTarget"
import handleError from "../utils/errorHandler"
import { deleteByUrl, get, put } from "./backend"

const baseUrl = config.backend.savingsTargetApiUrl

/**
 * Sends a post request to the savings target api
 * @param savingsTarget The savings target which should be posted to the server
 * @returns The response data of the post request
 */
const postSavingsTarget: (savingsTarget: CreateSavingsTarget) => Promise<CreateSavingsTarget> = (savingsTarget) => {
  const url = baseUrl
  const promise = new Promise<CreateSavingsTarget>((resolve, reject) => {
    axios
      .post<CreateSavingsTarget>(url, savingsTarget)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Calls the api to get the account with all savings target
 * @param accountId The id of tha account for which all savings targets are requested
 * @returns The account with all savings target
 */
const getByAccount: (accountId: string) => Promise<SavingsTargetAccount> = (accountId) => {
  const url = `${baseUrl}/account/${accountId}`
  return get(url)
}

/**
 * Calls the api to get the savings target with the provided id
 * @param savingsTargetId The identification of the savings target, which is requested
 * @returns The savings target from the backend
 */
const getById = (savingsTargetId: string): Promise<SavingsTarget> => {
  const url = `${baseUrl}/${savingsTargetId}`
  return get(url)
}

/**
 * Sends a put request to the server
 * @param savingsTarget The savings target to be sent to the backend
 * @returns The response data of the post request
 */
const putSavingsTarget = (savingsTarget: UpdateSavingsTarget): Promise<UpdateSavingsTarget> => {
  const url = `${baseUrl}/${savingsTarget.id}`
  return put(url, savingsTarget)
}

/**
 * Set a new value to the balance of a savings target
 * @param newBalance The new value of the balance of the savings target
 * @returns The new value of the savings target
 */
const putBalance = async (newBalance: SetBalance): Promise<SetBalance> => {
  const url = `${baseUrl}/${newBalance.savingsTargetId}/accounting`
  await put(url, { balance: newBalance.balance })
  return newBalance
}

/**
 * Deletes a savings target
 * @param id The identification of the savings target to be deleted
 * @returns The status text of the operation
 */
const deleteById = async (id: string): Promise<string> => {
  const url = `${baseUrl}/${id}`
  return deleteByUrl(url)
}

export { postSavingsTarget, getByAccount, getById, putSavingsTarget, putBalance, deleteById }
