import axios from "axios"
import FinAccount from "app/services/account/FinAccount"
import handleError from "app/services/utils/errorHandler"
import config from "app/config/AppConfig"

const baseUrl = config.backend.accountApiUrl

/**
 * Sends a post request to the account endpoint
 * @param account The account, which will be sent posted to the server
 * @returns A promise with the posted account from the server
 */
const postAccount: (account: FinAccount) => Promise<FinAccount> = (account: FinAccount) => {
  const url = baseUrl
  const promise = new Promise<FinAccount>((resolve, reject) => {
    axios
      .post<FinAccount>(url, account)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Calls the api to get all accounts
 * @returns A promise which resolves all accounts
 */
const getAccounts: () => Promise<FinAccount[]> = () => {
  const url = baseUrl
  const promise = new Promise<FinAccount[]>((resolve, reject) => {
    axios
      .get<FinAccount[]>(url)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Puts an existing account to the server
 * @param account The account which will be sent to the server
 * @return A promise, which will resolve the updated account
 */
const putAccount: (account: FinAccount) => Promise<FinAccount> = (account: FinAccount) => {
  const url = `${baseUrl}/${account.id}`
  const promise = new Promise<FinAccount>((resolve, reject) => {
    axios
      .put<FinAccount>(url, account)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Gets an account from the backend by its id
 * @param id The id of the requested account
 * @return A promise with the requested account or null
 */
const getById: (id: string) => Promise<FinAccount> = (id: string) => {
  const url = `${baseUrl}/${id}`

  const promise = new Promise<FinAccount>((resolve, reject) => {
    axios
      .get<FinAccount>(url)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}

/**
 * Calls the endpoint with a delete request and the given id
 * @param id The identification of the account which should be deleted
 * @returns A promise with the status
 */
const deleteById: (id: string) => Promise<string> = (id: string) => {
  const url = `${baseUrl}/${id}`
  const promise = new Promise<string>((resolve, reject) => {
    axios
      .delete(url)
      .then((response) => resolve(response.statusText))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })

  return promise
}
export { postAccount, getAccounts, putAccount, getById, deleteById }
