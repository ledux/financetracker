import axios from "axios"
import handleError from "../utils/errorHandler"

/**
 * Issues a get request to the backend
 * @param url The backend url from where the data should be fetched, including scheme and parameters
 * @returns A promise resolving to the requested data
 */
function get<T>(url: string): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    axios
      .get<T>(url)
      .then((response) => resolve(response.data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })
}

/**
 * Issues a put request with the data to the backend
 * @param url The backend url where the put request should be issued to, including scheme and parameters
 * @param data The payload, which should be sent in the body of the request
 * @returns A promise with the sent data
 */
function put<T>(url: string, data: T): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    axios
      .put(url, data)
      .then(() => resolve(data))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })
}

/**
 * Deletes a resource by its provided url
 * @param url the full url where the delete request should be sent to
 * @returns The status text of the response
 */
function deleteByUrl(url: string): Promise<string> {
  return new Promise((resolve, reject) => {
    axios
      .delete(url)
      .then((response) => resolve(response.statusText))
      .catch((error) => {
        handleError(error, "error")
        reject(error)
      })
  })
}

export { get, put, deleteByUrl }
