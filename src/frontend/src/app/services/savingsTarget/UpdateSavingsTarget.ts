import { Key } from "react"

/** Represents the data of a savings target which can be updated */
class UpdateSavingsTarget {
  /**
   * Initializes a new instance of an savings target
   * @param id Identifies the savings target
   * @param name The name of the savings target
   * @param targetAmount What the goal is of the savings target
   * @param description An additional description of the savings target
   * @param targetDate When the targetAmount should be
   */
  constructor(
    public id: Key = "",
    public name: string = "",
    public targetAmount: number = 0,
    public description?: string,
    public targetDate?: Date
  ) {}
}

export default UpdateSavingsTarget
