import { Key } from "react"

/** Represents a savings target */
class SavingsTarget {
  /**
   * Initializes a new instance of an savings target
   * @param id Identifies the savings target
   * @param name The name of the savings target
   * @param accountId The id of the account to which this savings target belongs
   * @param targetAmount What the goal is of the savings target
   * @param description An additional description of the savings target
   * @param targetDate When the targetAmount should be
   * @param startDate When the saving starts. Cannot be updated
   * @param balance How much money is currently allocated to this savings target
   */
  constructor(
    public id: Key = "",
    public name: string = "",
    public accountId: string = "0",
    public targetAmount: number = 0,
    public description?: string,
    public targetDate?: Date,
    public startDate?: Date,
    public balance?: number
  ) {}
}

export default SavingsTarget
