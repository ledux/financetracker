import { Key } from "react"

class CreateSavingsTarget {
  constructor(
    public startDate: Date,
    public id?: Key,
    public name: string = "",
    public accountId: string = "0",
    public targetAmount: number = 0,
    public description?: string,
    public targetDate?: Date,
    public startAmount?: number
  ) {}
}

export default CreateSavingsTarget
