import SavingsTarget from "./SavingsTarget"

class SavingsTargetAccount {
  constructor(public id: string, public freeAssets: number, public savingsTargets: SavingsTarget[]) {}
}

export default SavingsTargetAccount
