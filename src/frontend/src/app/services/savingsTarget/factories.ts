import { getById, getByAccount, putSavingsTarget, putBalance } from "../backend/savingsTargetsApiServices"
import SavingsTarget from "./SavingsTarget"
import SavingsTargetAccount from "./SavingsTargetAccount"
import {
  createSavingsTarget,
  getSavingsTargetById,
  getSavingsTargetsByAccount,
  updateSavingsTarget,
} from "./savingsTargetsServices"
import UpdateSavingsTarget from "./UpdateSavingsTarget"

/**
 * Factory function which resolves the backend dependency
 * @returns A function which gets all savings targets of an account
 */
function getSavingsTargetByAccountFactory(): (accountId: string) => Promise<SavingsTargetAccount> {
  return (id: string) => getSavingsTargetsByAccount(id, getByAccount)
}

/**
 * Factory function which resolves the backend dependency
 * @returns A function which gets a savings target by its id
 */
function getSavingsTargetByIdFactory(): (savingsTargetId: string) => Promise<SavingsTarget> {
  return (id: string) => getSavingsTargetById(id, getById)
}

function updateSavingsTargetFactory(): (savingsTarget: SavingsTarget) => Promise<UpdateSavingsTarget> {
  return (savingsTarget: SavingsTarget) => updateSavingsTarget(savingsTarget, putSavingsTarget, putBalance)
}

export { getSavingsTargetByAccountFactory, getSavingsTargetByIdFactory, updateSavingsTargetFactory, createSavingsTarget }
