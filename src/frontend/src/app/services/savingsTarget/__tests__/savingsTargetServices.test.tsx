import SavingsTargetAccount from "../SavingsTargetAccount"
import SavingsTarget from "../SavingsTarget"
import { createSavingsTarget, getSavingsTargetsByAccount, updateSavingsTarget } from "../savingsTargetsServices"
import { postSavingsTarget } from "../../backend/savingsTargetsApiServices"
import UpdateSavingsTarget from "../UpdateSavingsTarget"
import CreateSavingsTarget from "../CreateSavingsTarget"
jest.mock("../../backend/savingsTargetsApiServices")

describe("Test the getSavingsTargetAccount function", () => {
  const fakeBackend: (id: string) => Promise<SavingsTargetAccount> = (id: string) =>
    Promise.resolve(new SavingsTargetAccount(id, 1234, [new SavingsTarget(), new SavingsTarget(), new SavingsTarget()]))

  test("that the account id of the account ends up in all savings target", async () => {
    const accountId = "123456789"
    const savingsAccount: SavingsTargetAccount = await getSavingsTargetsByAccount(accountId, fakeBackend)
    expect(savingsAccount).not.toBeNull()
    expect(savingsAccount.savingsTargets).toHaveLength(3)
    expect(savingsAccount.savingsTargets).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ accountId: savingsAccount.id }),
        expect.objectContaining({ accountId: savingsAccount.id }),
        expect.objectContaining({ accountId: savingsAccount.id }),
      ])
    )
  })
})

describe("Test the updateSavingsTarget function", () => {
  const fakePutSavingsTarget = jest.fn((target) => Promise.resolve(target))
  const fakePutBalance = jest.fn((balance) => Promise.resolve(balance))
  const targetId = "12349923"
  const name = "name"
  const targetAmount = 1000
  const description = "description"
  const targetDate = new Date(2022, 1, 10)
  const newBalance = 400
  const target = new SavingsTarget(targetId, name, "123422", targetAmount, description, targetDate, undefined, newBalance)

  test("that both functions are called", () => {
    updateSavingsTarget(target, fakePutSavingsTarget, fakePutBalance)

    expect(fakePutSavingsTarget.mock.calls.length).toBe(1)
    expect(fakePutBalance.mock.calls.length).toBe(1)
  })

  test("that the function 'putBalance' is called with the balance", () => {
    updateSavingsTarget(target, fakePutSavingsTarget, fakePutBalance)

    expect(fakePutBalance.mock.calls[0][0]).toStrictEqual({ savingsTargetId: targetId, balance: newBalance })
  })

  test("that the function 'putSavingsTarget' is called with the data", () => {
    updateSavingsTarget(target, fakePutSavingsTarget, fakePutBalance)

    expect(fakePutSavingsTarget.mock.calls[0][0]).toEqual({ id: targetId, name, targetAmount, description, targetDate })
  })
})

describe("Test the createSavingsTarget function", () => {
  const balance = 100
  const testData = new SavingsTarget(
    undefined,
    "target",
    "12341234",
    1000,
    "description",
    new Date(2022, 11, 31),
    new Date(2022, 0, 1),
    balance
  )

  test("that the post function gets balance as start amount", async () => {
    await createSavingsTarget(testData)

    expect(
      (postSavingsTarget as unknown as jest.Mock<(savingsTarget: CreateSavingsTarget) => Promise<CreateSavingsTarget>>).mock
        .calls[0][0].startAmount
    ).toBe(balance)
  })
})
