import { debounce } from "lodash"
import { deleteById, postSavingsTarget, putBalance } from "../backend/savingsTargetsApiServices"
import SavingsTarget from "./SavingsTarget"
import SavingsTargetAccount from "./SavingsTargetAccount"
import SetBalance from "./SetBalance"
import UpdateSavingsTarget from "./UpdateSavingsTarget"

/**
 * Creates a new savings target
 * @param savingsTarget The new savings target which should be created
 * @returns A promise resolving to the response
 */
const createSavingsTarget = async (savingsTarget: SavingsTarget): Promise<SavingsTarget> => {
  await postSavingsTarget({
    ...savingsTarget,
    startAmount: savingsTarget.balance,
    startDate: savingsTarget.startDate ?? new Date(),
  })
  return savingsTarget
}

/**
 * Saves changes to an existing savings target
 * @param savingsTarget The savings target which should be updated
 * @param putSavingsTarget Function which updates the savings target
 * @param updateBalance Function which updates the balance of the savings target
 * @returns A promise with the updated values
 */
const updateSavingsTarget = async (
  savingsTarget: SavingsTarget,
  putSavingsTarget: (target: UpdateSavingsTarget) => Promise<UpdateSavingsTarget>,
  updateBalance: (newBalance: SetBalance) => Promise<SetBalance>
): Promise<UpdateSavingsTarget> => {
  const updateTarget = {
    id: savingsTarget.id,
    name: savingsTarget.name,
    targetAmount: savingsTarget.targetAmount,
    description: savingsTarget.description,
    targetDate: savingsTarget.targetDate,
  }
  const targetPromise = putSavingsTarget(updateTarget)
  const balancePromise = updateBalance({ savingsTargetId: savingsTarget.id as string, balance: savingsTarget.balance ?? 0 })
  await Promise.all([targetPromise, balancePromise])

  return savingsTarget
}

/**
 * Fetches all savings targets on an account. Accept the backend service as parameter for DI
 * @param accountId The identification of the account, for which all savings targets are requested
 * @param callApi Function for calling the data store to get the actual data
 * @returns The account with all savings target
 */
const getSavingsTargetsByAccount = async (
  accountId: string,
  callApi: (id: string) => Promise<SavingsTargetAccount>
): Promise<SavingsTargetAccount> => {
  const savingsAccount = await callApi(accountId)
  const targets = savingsAccount.savingsTargets.map((target) => updateTargetProps(savingsAccount.id, target))
  return { ...savingsAccount, savingsTargets: targets }
}

/**
 * Fetches a savings target by its id
 * @param targetId The identification of the savings target
 * @returns The requested savings target
 */
const getSavingsTargetById = async (
  targetId: string,
  callApi: (id: string) => Promise<SavingsTarget>
): Promise<SavingsTarget> => {
  const savingsTarget = await callApi(targetId)
  return updateTargetProps(savingsTarget.id as string, savingsTarget)
}

/**
 * Sets a new value to the balance of the savings target. Is debounced by 500ms
 * @param balance The new balance of the savings target
 * @returns The updated balance
 */
const setBalance = async (balance: SetBalance): Promise<SetBalance> => {
  debouncedSetBalance(balance)
  return balance
}

/**
 * Deletes a savings target
 * @param id The identification of the savings target to be deleted
 * @returns The status text of the operation
 */
const deleteSavingsTarget = (id: string): Promise<string> => {
  return deleteById(id)
}

const debouncedSetBalance = debounce((balance: SetBalance) => {
  putBalance(balance)
}, 500)

const updateTargetProps = (accountId: string, savingsTarget: SavingsTarget): SavingsTarget => {
  return {
    ...savingsTarget,
    accountId,
    startDate: new Date(savingsTarget.startDate ?? new Date()),
    targetDate: new Date(savingsTarget.targetDate ?? new Date()),
  }
}

export {
  createSavingsTarget,
  getSavingsTargetsByAccount,
  getSavingsTargetById,
  updateSavingsTarget,
  setBalance,
  deleteSavingsTarget,
}
