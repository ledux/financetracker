/** Data used to set the new balance to a savings target */
class SetBalance {
  /**
   * Instantiates a new instance of the object
   * @param balance The new value of the balance of the savings target
   * @param savingsTargetId The identification of the savings target
   */
  constructor(public balance: number, public savingsTargetId: string) {}
}

export default SetBalance
