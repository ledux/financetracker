/** Represents an account in the frontend */
class FinAccount {
  /**
   * Instantiates a new account
   * @param id The identification of the account
   * @param name The name of the account
   * @param description An optional description of the account
   * @param balance How much money lies on the account
   */
  constructor(public id: string, public name?: string, public description?: string, public balance?: number) {}
}

export default FinAccount
