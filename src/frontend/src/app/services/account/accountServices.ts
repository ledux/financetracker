import { postAccount, getAccounts, putAccount, getById, deleteById } from "app/services/backend/accountApiService"
import FinAccount from "./FinAccount"

/**
 * Creates a new account
 * @param account The account data which will be created.
 * @returns A promise with the newly created account
 */
const createAccount: (account: FinAccount) => Promise<FinAccount> = (account: FinAccount) => postAccount(account)

/**
 * Gets all accounts
 * @returns A promise, which resolves the accounts in an array
 */
const getAllAccounts: () => Promise<FinAccount[]> = () => getAccounts()

/**
 * Updates an account. Only name and description are updatable
 * @param account The account which will be updated
 * @returns A promise
 */
const updateAccount: (account: FinAccount) => Promise<FinAccount> = (account: FinAccount) => putAccount(account)

/**
 * Reads an account by its id
 * @param id The identification of the searched account
 * @returns A promise with the requested account or null
 */
const getAccountById: (id: string) => Promise<FinAccount> = (id: string) => getById(id)

/**
 * Deletes an account
 * @param id The identification of the account which should be deleted
 * @returns A promise with the status
 */
const deleteAccount: (id: string) => Promise<string> = (id: string) => deleteById(id)

export { createAccount, getAllAccounts, updateAccount, getAccountById, deleteAccount }
