import {
  getByAccountId,
  getById,
  postAccountingEntry,
  putEntry,
  deleteEntry,
} from "app/services/backend/accountingEntryApiServices"
import AccountingEntry from "./AccountingEntry"

/**
 * Gets all accounting entries of an account and sorts them by booking date descending
 * @param accountId The id of the account, for which the accounting entries are wanted
 * @returns A promise which resolves to a sorted array of accounting entries. At least an empty array
 */
const getAccountingEntriesByAccountId: (accountId: string) => Promise<AccountingEntry[]> = (accountId: string) =>
  getByAccountId(accountId)
    .then((entries) => {
      const typedEntries = entries.map((entry) => {
        return { ...entry, bookingDate: new Date(entry.bookingDate) }
      })

      return typedEntries.sort((first, second) => second.bookingDate.getTime() - first.bookingDate.getTime())
    })
    .catch(() => new Array<AccountingEntry>())

/**
 * Saves an accounting entry
 * @param entry The data of the accounting entry to be saved
 * @returns A promise with the saved accounting entry
 */
const createAccountingEntry: (entry: AccountingEntry) => Promise<AccountingEntry> = (entry: AccountingEntry) =>
  postAccountingEntry(entry)

/**
 * Reads an accouting entry by its id
 * @param id The id of the requested accounting entry
 * @returns A promise which resolves the requested entry
 */
const getAcountingEntryById: (id: string) => Promise<AccountingEntry> = (id: string) => getById(id)

/**
 * Updates an accounting entry
 * @param entry The date which will be updated
 * @returns A promise which resolves the updated entity
 */
const updateAccountingEntry: (entry: AccountingEntry) => Promise<AccountingEntry> = (entry: AccountingEntry) => putEntry(entry)

/**
 * Deletes an accounting entry
 * @param entryId The id of the accounting entry, which will be deleted
 * @returns A promise which resolves the status text
 */
const deleteAccountingEntry: (entryId: string) => Promise<string> = (entryId: string) => deleteEntry(entryId)

export {
  getAccountingEntriesByAccountId,
  createAccountingEntry,
  getAcountingEntryById,
  updateAccountingEntry,
  deleteAccountingEntry,
}
