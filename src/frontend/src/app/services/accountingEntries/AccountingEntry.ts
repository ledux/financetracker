const minDate: Date = new Date(1900, 0, 1)

/** Represents an accounting entry. It changes the balance of an account */
class AccountingEntry {
  /** The id of the entry */
  public id: string = "0"

  /** The amount by which the account changes through this entry */
  public amount: number = 0

  /** The id of the account, which balance gets changed by this entry */
  public accountId: string = "0"

  /** The description of the transaction */
  public description: string = ""

  /** If this transaction occurs every month */
  public recurring: boolean = false

  /** On which date this accounting entry happened */
  public bookingDate: Date = minDate

  /** Some tags */
  public tags: string[] = []

  /** To which category this entry belongs */
  public categoryId: string = "0"

  /** Which invoice is attached to this entry */
  public fileId: string = "0"
}

export default AccountingEntry
