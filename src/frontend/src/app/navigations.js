export const navigations = [
    {
        name: 'Accounts',
        icon: 'account_balance',
        children: [
            {
                name: 'All accounts',
                path: '/accounts/all',
                iconText: 'A',
            },
            {
                name: 'New Account',
                path: '/accounts/create',
                iconText: 'N',
            },
        ],
    },
]
