import { ReactElement } from "react"
import ConfirmationDialog, { IConfirmationDialogProps } from "app/components/ConfirmationDialog"

/** The props for the confirmation dialog for deleting an account */
export interface IConfirmDeleteProps {
  /** Set this to true, to open the dialog  */
  isOpen: boolean
  /** The function called when aborting the deletion  */
  handleNo: () => void
  /** The function called when confirming the deletion */
  handleYes: () => void
  /** The text which will appear on the confirmation dialog */
  text: string
  /** The title on the confirmation dialog */
  title: string
}

/**
 * Renders a confirmation dialog when deleting an entity
 * @param props The props for the dialog
 */
function ConfirmDelete(props: IConfirmDeleteProps): ReactElement {
  const confirmationProps: IConfirmationDialogProps = {
    abortText: "No, don't",
    confirmText: "Yes, please",
    isOpen: props.isOpen,
    onAbort: props.handleNo,
    onConfirm: props.handleYes,
    text: props.text,
    title: props.title,
  }

  return <ConfirmationDialog {...confirmationProps} />
}

export default ConfirmDelete
