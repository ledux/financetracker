import { styled, Theme, useTheme } from "@mui/system"

function Title(props: { text: string }) {
  const theme: Theme = useTheme()
  const TitleRoot = styled("div")(() => ({
    fontSize: "1.4rem",
    textTransform: "capitalize",
    fontWeight: "600",
    color: theme.palette.primary.light,
  }))

  return <TitleRoot>{props.text}</TitleRoot>
}

export { Title }
