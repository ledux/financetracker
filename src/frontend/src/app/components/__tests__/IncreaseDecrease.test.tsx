import { fireEvent, render, RenderResult } from "@testing-library/react"
import { ThemeProvider } from "@emotion/react"
import { Theme } from "@mui/system"
import { createTheme } from "@mui/material"
import IncreaseDecrease, { IIncreaseDecreaseProps } from "../IncreaseDecrease"
import each from "jest-each"
import { ValidatorForm } from "react-material-ui-form-validator"
import { values } from "lodash"

const defaultProps: IIncreaseDecreaseProps = {
  intervalOne: 10,
  intervalTwo: 100,
  label: "label",
  onChange: () => {},
  onClick: () => {},
  value: 100,
}
const setup = (params: { theme: Theme; props: IIncreaseDecreaseProps }): RenderResult =>
  render(
    <ThemeProvider theme={params.theme}>
      <ValidatorForm onSubmit={() => {}}>
        <IncreaseDecrease {...params.props} />
      </ValidatorForm>
    </ThemeProvider>
  )

describe("Interval values are displayed on button", () => {
  each`
        intervalOne | intervalTwo
        ${1} | ${2}
        ${10} | ${100}
        ${10} | ${1}
    `.test("that the values have prefixes", ({ intervalOne, intervalTwo }) => {
    const theme: Theme = createTheme()
    const props = { ...defaultProps, intervalOne: intervalOne, intervalTwo: intervalTwo }
    const input = setup({ theme: theme, props: props })

    const decOne = input.container.querySelector("[name='decOne']")
    const decTwo = input.container.querySelector("[name='decTwo']")
    const incOne = input.container.querySelector("[name='incOne']")
    const incTwo = input.container.querySelector("[name='incTwo']")

    expect(decOne?.innerHTML).toBe(`-${intervalOne}`)
    expect(decTwo?.innerHTML).toBe(`-${intervalTwo}`)
    expect(incOne?.innerHTML).toBe(`+${intervalOne}`)
    expect(incTwo?.innerHTML).toBe(`+${intervalTwo}`)
  })
})

describe("click on buttons", () => {
  each`
    buttonName | expectedValue | expectedBalance
    ${"decOne"} | ${-10} | ${90}
    ${"decTwo"} | ${-100} | ${0}
    ${"incOne"} | ${10} | ${110}
    ${"incTwo"} | ${100} | ${200}
  `.test("that the function is called with the correct value", ({ buttonName, expectedValue, expectedBalance }) => {
    const fakeOnClick = jest.fn()
    const theme: Theme = createTheme()
    const props = { ...defaultProps, onClick: fakeOnClick }
    const input = setup({ theme: theme, props: props })
    const button = input.container.querySelector(`[name='${buttonName}']`) as HTMLButtonElement

    fireEvent.click(button)

    expect(fakeOnClick.mock.calls[0][0]).toBe(expectedBalance)
    expect(fakeOnClick.mock.calls[0][1]).toBe(expectedValue)
  })
})

describe("change the input value", () => {
  test("should pass the value to the function", () => {
    const fakeOnChange = jest.fn()
    const theme: Theme = createTheme()
    const props = { ...defaultProps, onChange: fakeOnChange }
    const testee = setup({ theme: theme, props: props })
    const input = testee.container.querySelector("input") as HTMLInputElement
    const valueToSet = 200

    fireEvent.change(input, { target: { value: valueToSet } })

    expect(fakeOnChange.mock.calls[0][0]).toBe(valueToSet)
  })
})
