// import useSettings from 'app/hooks/useSettings'
import { CssBaseline, ThemeProvider, createTheme } from '@mui/material'

const MatxTheme = ({ children }) => {
    // const { settings } = useSettings()
    // let activeTheme = { ...settings.themes[settings.activeTheme] }
    const theme = createTheme({
        palette: {
            mode: "dark",
        }
    })
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            {children}
        </ThemeProvider>
    )
}

export default MatxTheme
