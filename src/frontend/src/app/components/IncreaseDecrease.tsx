import { styled } from "@mui/system"
import { ChangeEvent, ReactElement } from "react"
import { TextValidator } from "react-material-ui-form-validator"

/** Props for the input with increase and decrease values */
interface IIncreaseDecreaseProps {
  /** The lower interval by which the value in the input should be changed  */
  intervalOne: number
  /** The higher interval by which the value in the input should be changed  */
  intervalTwo: number
  /** The text on the input */
  label: string
  /** The value of the input */
  value: number
  /** The function which will be called when the value in the input changes.
   * Gets also called when one of the increase/decrease buttons are clicked
   * @param value The new value of the input
   */
  onChange: (value: number) => void
  /** The function which will be called when one of the increase/decrease buttons are clicked
   * @param newInputValue The new value of the input after the change
   * @param changeValue The new value by which the input was changed
   */
  onClick: (newInputValue: number, changeValue: number) => void
}

const Input = styled(TextValidator)(() => ({
  maxWidth: "100px",
  flexGrow: "6",
  margin: "1px",
}))

const Container = styled("div")(() => ({
  display: "flex",
  alignItems: "center",
}))

function IncDecButton(props: { changeValue: number; onClick: (value: number) => void; name: string }): ReactElement {
  const Button = styled("button")(({ theme }) => ({
    fontSize: "9px",
    padding: "4px",
    margin: "1px",
    boxShadow: (theme.shadows as any)[2],
    cursor: "pointer",
    border: `1px solid ${theme.palette.mode === "light" ? "rgba(0, 0, 0, 0.23)" : "rgba(255, 255, 255, 0.23)"}`,
    borderRadius: theme.shape.borderRadius,
    color: theme.palette.getContrastText(theme.palette.grey[300]),
    backgroundColor: theme.palette.primary.dark,
  }))

  const displayValue = (props.changeValue > 0 ? "+" : "") + props.changeValue

  return (
    <Button name={props.name} onClick={() => props.onClick(props.changeValue)}>
      {displayValue}
    </Button>
  )
}

/**
 * Renders an input component with increase/decrease buttons on the side
 * @param props The props used to render the component
 * @returns An input with increase/decrease buttons on the side
 */
function IncreaseDecrease(props: IIncreaseDecreaseProps): ReactElement {
  const inputValue = props.value
  const onChangeValueClick = (valueToChange: number): void => {
    const newValue = inputValue + valueToChange
    props.onChange(newValue)
    props.onClick(newValue, valueToChange)
  }

  return (
    <Container>
      <IncDecButton onClick={onChangeValueClick} changeValue={-props.intervalTwo} name="decTwo" />
      <IncDecButton onClick={onChangeValueClick} changeValue={-props.intervalOne} name="decOne" />
      <Input
        label={props.label}
        name="startAmount"
        onChange={(event: ChangeEvent<HTMLInputElement>) => props.onChange(Number(event.target.value))}
        type="number"
        value={props.value}
      />
      <IncDecButton onClick={onChangeValueClick} changeValue={props.intervalOne} name="incOne" />
      <IncDecButton onClick={onChangeValueClick} changeValue={props.intervalTwo} name="incTwo" />
    </Container>
  )
}

export default IncreaseDecrease
export type { IIncreaseDecreaseProps }
