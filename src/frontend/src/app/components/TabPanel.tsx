import { Box, Typography } from "@mui/material"
// import Typography from '@material-ui/core/Typography'
import React, { ReactElement } from "react"

/** The props for the panel  */
export interface ITabPanelProps {
  /** The index of the panel, which is shown at the moment  */
  currentIndex: number
  /** The index of this panel */
  indexOfPanel: number
}

/**
 * Renders content for tabs
 * @param props Props for the panel
 */
function TabPanel(props: React.PropsWithChildren<ITabPanelProps>): ReactElement {
  return (
    <div
      role="tabpanel"
      hidden={props.indexOfPanel !== props.currentIndex}
      id={`simple-tabpanel-${props.indexOfPanel}`}
      aria-labelledby={`simple-tab-${props.indexOfPanel}`}
    >
      {props.currentIndex === props.indexOfPanel && (
        <Box p={3}>
          <Typography>{props.children}</Typography>
        </Box>
      )}
    </div>
  )
}

export default TabPanel
