import { ReactElement } from "react"
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button } from "@mui/material"

/** Props for a general confirmation dialog */
export interface IConfirmationDialogProps {
  /** The title of the dialog */
  title: string
  /** More text for the dialog */
  text: string
  /** The text on the confirmation button */
  confirmText: string
  /** The text on the abort button */
  abortText: string
  /** If the dialog should open or not */
  isOpen: boolean
  /** The function to be called when the confirmation button is clicked */
  onConfirm: () => void
  /** The function to be called when the abort button is clicked */
  onAbort: () => void
}

/**
 * Renders a confirmation dialog
 * @param props The props for the confirmation dialog
 */
function ConfirmationDialog(props: IConfirmationDialogProps): ReactElement {
  return (
    <Dialog open={props.isOpen} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
      <DialogTitle id="alert-dialog-title">{props.title} </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">{props.text}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={props.onConfirm} color="primary" autoFocus>
          {props.confirmText}
        </Button>
        <Button onClick={props.onAbort} color="secondary">
          {props.abortText}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ConfirmationDialog
