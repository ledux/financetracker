import * as React from "react"
import { Breadcrumb } from "app/components/matx"
import { styled } from "@mui/system"
import { PropsWithChildren } from "react"

/** Routesegments for the breadcrumbs in the page header */
class RouteSegment {
  /**
   * Instantiates a new route segment for the breadcrumb
   * @param name The display name of the path
   * @param path The path, where the link leads to. If not set, no link will be rendered
   */
  constructor(public name: string, public path?: string) {}
}

/** The props for the basic page */
interface IPageProps {
  /** The segments, which will be rendered as breadcrumbs */
  breadCrumbRoutes: RouteSegment[]
}

const Container = styled("div")(({ theme }) => ({
  margin: "30px",
  [theme.breakpoints.down("sm")]: {
    margin: "16px",
  },
  "& .breadbrumb": {
    marginBottom: "30px",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "16px",
    },
  },
}))

/**
 * Renders a page with breadcrumbs at the top
 * @param props The props for the page
 */
function Page(props: PropsWithChildren<IPageProps>): React.ReactElement {
  return (
    <Container>
      <div className="breadcrumb">
        <Breadcrumb routeSegments={props.breadCrumbRoutes} />
      </div>
      {props.children}
    </Container>
  )
}

export default Page
export { RouteSegment }
export type { IPageProps }
