/** Values for configuration of the frontend application */
type AppConfig = {
  /** Configration to reach the backend */
  backend: {
    /** Base url for the account API */
    accountApiUrl: string
    /** Base url for the accounting API to deal with accounting entries */
    accountingApiUrl: string
    /** Base url for the savings target API */
    savingsTargetApiUrl: string
  }
}

const config: AppConfig = {
  backend: {
    accountApiUrl: process.env.REACT_APP_ACCCOUNTURL ?? "http://localhost:5002/v1/account",
    accountingApiUrl: process.env.REACT_APP_ACCCOUNTINGURL ?? "http://localhost:5000/v1/accounting",
    savingsTargetApiUrl: process.env.REACT_APP_SAVINGSTARGETURL ?? "http://localhost:5004/v1/savingstarget",
  },
}

export type { AppConfig }
export default config
