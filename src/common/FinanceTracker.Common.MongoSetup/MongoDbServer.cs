﻿namespace FinanceTracker.Common.MongoSetup
{
    /// <summary>
    /// Represents the connection details for a mongo db server
    /// </summary>
    public class MongoDbServer
    {
        /// <summary>
        /// The host name of the server
        /// </summary>
        public string Hostname { get; set; }

        /// <summary>
        /// The port on which the host is listening for incoming connections
        /// </summary>
        public int Port { get; set; }
    }
}