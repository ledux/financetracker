﻿namespace FinanceTracker.Common.MongoSetup
{
    /// <summary>
    /// The collection specific settings of a repo
    /// </summary>
    public class MongoDbCollectionSettings
    {
        /// <summary>
        /// The name of the repo, to which these settings belongs
        /// </summary>
        public string RepoName { get; set; }
        /// <summary>
        /// The name of the database to connect to 
        /// </summary>
        public string Database { get; set; }
        /// <summary>
        /// The name of the collection, which holds the requested data
        /// </summary>
        public string CollectionName { get; set; }
    }
}
