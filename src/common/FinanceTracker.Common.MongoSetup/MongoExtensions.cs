﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace FinanceTracker.Common.MongoSetup
{
    /// <summary>
    /// Extensions of mongo configurations and setups
    /// </summary>
    public static class MongoExtensions
    {
        /// <summary>
        /// Adds a configured <see cref="IMongoClient"/> to the service collection
        /// </summary>
        /// <param name="services">The service collection where the dependencies are added</param>
        /// <param name="settings">The settings for the mongo database</param>
        /// <param name="addFileBucket">If an additional <see cref="IGridFSBucket"/> should be added.
        /// This is used for accessing the files in the database</param>
        /// <returns>The same service collection with the added dependencies</returns>
        public static IServiceCollection AddMongo(this IServiceCollection services, MongoDbRepositorySettings settings, bool addFileBucket = false)
        {
            var mgoSettings = new MongoClientSettings
            {
                Server = new MongoServerAddress(settings.Host, settings.Port),
                ConnectTimeout = new TimeSpan(0, 0, 2),
                UseTls = settings.UseTls,
                RetryReads = true,
                RetryWrites = true
            };

            //if (!string.IsNullOrWhiteSpace(settings.Username) && !string.IsNullOrWhiteSpace(settings.Password))
            //{
            //    mgoSettings.Credential =
            //        MongoCredential.CreateCredential(settings.Db, settings.Username, settings.Password);
            //}

            services.AddSingleton<IMongoClient>(provider => new MongoClient(mgoSettings));

            //if (addFileBucket)
            //{
            //    services.AddSingleton<IGridFSBucket<long>>(
            //        provider => new GridFSBucket<long>(new MongoClient(mgoSettings).GetDatabase(settings.Db)));
            //}

            return services;
        }

        /// <summary>
        /// Reads settings and setups configurations of the mongo db access
        /// </summary>
        /// <param name="services">Services collection container</param>
        /// <param name="configuration">The configuration of the whole application</param>
        /// <param name="sectionName">The name of the section in the appsettings file, where the mongo is configured</param>
        /// <returns>The instance of the settings, read from the <paramref name="configuration"/></returns>
        public static MongoDbRepositorySettings ConfigureMongo(this IServiceCollection services, IConfiguration configuration, string sectionName = "persistence")
        {
            IConfigurationSection persistenceConfigs = configuration.GetSection(sectionName);
            services.Configure<MongoDbRepositorySettings>(persistenceConfigs);

            return persistenceConfigs.Get<MongoDbRepositorySettings>();
        }
    }

}
