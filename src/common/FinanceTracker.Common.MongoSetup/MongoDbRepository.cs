﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FinanceTracker.Common.MongoSetup
{
    /// <summary>
    /// Base repository for mongo repositories, which connect to a strictly typed collection
    /// </summary>
    /// <typeparam name="T">The schema of the collection</typeparam>
    public abstract class MongoDbRepository<T> : MongoDbRepositoryBase
    {
        /// <summary>
        /// The collection to access the data
        /// </summary>
        protected IMongoCollection<T> Collection;
        /// <summary>
        /// Gets the collection from the database
        /// </summary>
        /// <param name="client">The client which connects to the database</param>
        /// <param name="optionsAccessor">The configuration of the database</param>
        protected MongoDbRepository(IMongoClient client, IOptions<MongoDbRepositorySettings> optionsAccessor) 
            : base(client, optionsAccessor)
        {
            Collection = Database.GetCollection<T>(CollectionSettings.CollectionName);
        }
    }
}
