﻿using System;
using System.Runtime.Serialization;

namespace FinanceTracker.Common.MongoSetup.Exceptions
{
    [Serializable]
    public class MongoConfigurationException : Exception
    {
        /// <summary>
        /// <summary>Initializes a new instance of the <see cref="T:MongoConfigurationException"></see> class.</summary>
        /// </summary>
        public MongoConfigurationException() { }

        /// <summary>
        /// <summary>Initializes a new instance of the <see cref="T:MongoConfigurationException"></see> class.</summary>
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public MongoConfigurationException(string message) : base(message) { }

        /// <summary>
        /// <summary>Initializes a new instance of the <see cref="T:MongoConfigurationException"></see> class.</summary>
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="inner">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public MongoConfigurationException(string message, Exception inner) : base(message, inner) { }

        /// <summary>
        /// <summary>Initializes a new instance of the <see cref="T:MongoConfigurationException"></see> class.</summary>
        /// </summary>
        /// <param name="serializationInfo">The <see cref="T:System.Runtime.Serialization.SerializationInfo"></see> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="streamingContext">The <see cref="T:System.Runtime.Serialization.StreamingContext"></see> that contains contextual information about the source or destination.</param>
        protected MongoConfigurationException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext) { }
    }
}