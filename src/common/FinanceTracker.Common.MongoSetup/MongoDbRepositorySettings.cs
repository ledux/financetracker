﻿using System.Collections.Generic;

namespace FinanceTracker.Common.MongoSetup
{
    /// <summary>
    /// Settings for connecting to the mongo db server
    /// </summary>
    public class MongoDbRepositorySettings
    {
        /// <summary>
        /// A list with the possible servers to connect to
        /// </summary>
        public List<MongoDbServer> Servers { get; set; }

        /// <summary>
        /// The host name of the mongo db server
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// The port on which the host is listening for incoming connections
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Settings for the different repos and collections 
        /// </summary>
        public List<MongoDbCollectionSettings> Collections { get; set; }

        /// <summary>
        /// The username, with which the connection should be established
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The password to authenticate the user
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// If the connection should be ssl encrypted or not
        /// </summary>
        public bool UseTls { get; set; }
    }
}