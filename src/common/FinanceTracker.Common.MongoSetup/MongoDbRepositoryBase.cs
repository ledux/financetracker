﻿using System;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace FinanceTracker.Common.MongoSetup
{
    /// <summary>
    /// Abstract class for base setup of the
    /// common configurations of the mongo repository
    /// </summary>
    public abstract class MongoDbRepositoryBase
    {
        /// <summary>
        /// The name of the repo for accessing the config
        /// </summary>
        protected abstract string RepoName { get; }
        /// <summary>
        /// The database 
        /// </summary>
        protected IMongoDatabase Database { get; }
        /// <summary>
        /// The repo specific configuration
        /// </summary>
        protected MongoDbCollectionSettings CollectionSettings { get; }

        /// <summary>
        /// Gets the Database from the client
        /// </summary>
        /// <param name="client">The client which connects to the database</param>
        /// <param name="optionsAccessor">Settings to access to the mongo</param>
        protected MongoDbRepositoryBase(IMongoClient client, IOptions<MongoDbRepositorySettings> optionsAccessor)
        {
            if (optionsAccessor == null)
            {
                throw new ArgumentNullException(nameof(optionsAccessor));
            }

            CollectionSettings = optionsAccessor.Value.Collections.SingleOrDefault(
                c => c.RepoName.Equals(
                    RepoName,
                    StringComparison.InvariantCultureIgnoreCase));

            if (CollectionSettings == null)
            {
                throw new MongoConfigurationException(
                    $"There is no collection configuration for repo name '{RepoName}'");
            }

            if (string.IsNullOrWhiteSpace(CollectionSettings.Database))
            {
                throw new MongoConfigurationException("The database settings must be a non empty string");
            }

            Database = client.GetDatabase(CollectionSettings?.Database);
        }
    }

}