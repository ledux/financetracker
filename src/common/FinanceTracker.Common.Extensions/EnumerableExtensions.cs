﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FinanceTracker.Common.Extensions
{
    /// <summary>
    /// Extends the enumerables
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Enumerates a task with an <see cref="IEnumerable{T}"/> to an array
        /// </summary>
        /// <param name="enumerableTask">The task which resolves to an <see cref="IEnumerable{T}"/></param>
        /// <typeparam name="T">The type of the entries</typeparam>
        /// <returns>A task which resolves to an array</returns>
        public static async Task<T[]> ToArrayAsync<T>(this Task<IEnumerable<T>> enumerableTask)
        {
            return (await enumerableTask).ToArray();
        }
    }
}
