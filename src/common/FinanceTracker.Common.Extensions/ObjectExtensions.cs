﻿using System.Runtime.CompilerServices;

namespace FinanceTracker.Common.Extensions
{
    /// <summary>
    /// Extends the object
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Returns the fully qualified method name
        /// </summary>
        /// <param name="type">The class of the caller</param>
        /// <param name="caller">The method name, from where the method was called</param>
        /// <returns>The fully qualified method name of the caller</returns>
        public static string GetMethodName(this object type, [CallerMemberName] string caller = null)
        {
            return $"{type.GetType().FullName}.{caller}";
        }
    }
}
