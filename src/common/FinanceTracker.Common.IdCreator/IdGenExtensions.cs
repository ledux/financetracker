﻿using System;
using IdGen;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Common.IdCreator
{
    /// <summary>
    /// Class with extensions to register and manage 
    /// <see cref="IdGenerator"/> instance
    /// </summary>
    public static class IdGenExtensions
    {
        private const int Max14BitNumber = 0b11_1111_1111_1111;

        /// <summary>
        /// Register singleton instance of <see cref="IdGenerator"/>
        /// with the generator id as random value between 0 and Max 14 bit number
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddIdCreator(this IServiceCollection services)
        {
            var idStructure = new IdStructure(41, 14, 8);
            int randomId = new Random().Next(0, Max14BitNumber);
            services.AddSingleton<IIdCreator>(new IdCreator(randomId, idStructure));

            return services;
        }
    }
}
