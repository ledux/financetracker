﻿using System.Collections.Generic;

namespace FinanceTracker.Common.IdCreator
{
    /// <summary>
    /// Interface of a service which can provide
    /// a generation of <see cref="long"/> type Id.
    /// </summary>
    public interface IIdCreator : IEnumerable<long>
    {
        /// <summary>
        /// Creates an Id
        /// </summary>
        long CreateId();
    }
}