﻿using System.Collections;
using System.Collections.Generic;
using IdGen;

namespace FinanceTracker.Common.IdCreator
{
    /// <summary>
    /// A wrapper over <see cref="IdGenerator"/>
    /// to provide a dependency of <see cref="IdGenerator"/> to <see cref="IIdCreator"/>
    /// </summary>
    public class IdCreator : IIdCreator
    {
        private readonly IdGenerator _idGenerator;

        /// <summary>
        /// Default constructor. Received arguments are passed to
        /// <see cref="IdGenerator"/> constructor
        /// </summary>
        /// <param name="generatorId">The Id of the <see cref="IdGenerator"/> generator.</param>
        /// <param name="idStructure"><see cref="IdStructure"/></param>
        public IdCreator(int generatorId, IdStructure idStructure)
        {
            var options = new IdGeneratorOptions(idStructure, null, SequenceOverflowStrategy.SpinWait);
            _idGenerator = new IdGenerator(generatorId, options);
        }

        /// <summary>
        /// Returns <see cref="IdGenerator.CreateId()"/>
        /// </summary>
        public long CreateId()
        {
            return _idGenerator.CreateId();
        }

        /// <summary>
        /// Returns <see cref="IdGenerator.GetEnumerator()"/>
        /// </summary>
        public IEnumerator<long> GetEnumerator()
        {
            return _idGenerator.GetEnumerator();
        }

        /// <summary>
        /// Returns <see cref="IdGenerator.GetEnumerator()"/>
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _idGenerator.GetEnumerator();
        }
    }
}
