﻿using System;
using Confluent.Kafka;
using Newtonsoft.Json;

namespace FinanceTracker.Common.Testing.Integration.Kafka
{
    /// <summary>
    /// Kafka Consumer for integration tests
    /// </summary>
    public class TestsKafkaConsumer : IDisposable
    {
        private readonly IConsumer<string, string> _consumer;

        public TestsKafkaConsumer(string topic)
        {
            var conf = new ConsumerConfig
            {
                GroupId = "integration-test-consumer",
                BootstrapServers = "localhost:9092",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };

            IConsumer<string, string> consumer = new ConsumerBuilder<string, string>(conf).Build();

            consumer.Subscribe(topic);
            _consumer = consumer;
        }

        /// <summary>
        /// Consumes a single event with a certain key (if provided) and returns the payload of it
        /// </summary>
        /// <typeparam name="TEvent">The type, to which the payload will be deserialized</typeparam>
        /// <param name="key">If the key is not null or empty, events with a different key will be discarded</param>
        /// <returns>The payload of the event</returns>
        public TEvent Consume<TEvent>(string key = null)
        {
            try
            {
                ConsumeResult<string, string> consumeResult;

                if (!string.IsNullOrEmpty(key))
                {
                    do
                    {
                        consumeResult = _consumer.Consume(10000);
                    }
                    while (consumeResult?.Message?.Key != key);
                }
                else
                {
                    consumeResult = _consumer.Consume(10000);
                }
                var eventValue = JsonConvert.DeserializeObject<KafkaEvent>(consumeResult.Message.Value);
                var payload = eventValue.Content.ToObject<TEvent>();

                return payload;
            }
            finally
            {
                _consumer.Close();
            }

        }

        /// <summary>
        /// Disposes the consumer
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the consumer if disposing is true
        /// </summary>
        /// <param name="disposing">If set to true, then </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _consumer.Dispose();
            }
        }
    }

}
