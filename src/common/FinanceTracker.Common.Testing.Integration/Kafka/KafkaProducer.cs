﻿using System;
using Confluent.Kafka;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FinanceTracker.Common.Testing.Integration.Kafka
{
    /// <summary>
    /// Producer to send events to the event sourcing, used in integration tests
    /// </summary>
    public class TestsKafkaProducer : IDisposable
    {
        private readonly IProducer<string, string> _producer;
        private readonly string _topic;

        /// <summary>
        /// Instantiates a new instance of <see cref="TestsKafkaProducer"/>
        /// It instantiates a producer to produce to localhost
        /// </summary>
        /// <param name="topic">The topic, to which the data will be sent</param>
        public TestsKafkaProducer(string topic)
        {
            var conf = new ProducerConfig { BootstrapServers = "localhost:9092" };

            IProducer<string, string> producer = new ProducerBuilder<string, string>(conf).Build();

            _producer = producer;
            _topic = topic;
        }

        /// <summary>
        /// Sends an event with data to the event sourcing
        /// </summary>
        /// <typeparam name="TEvent">The type of the data, which will be sent to event sourcing</typeparam>
        /// <param name="eventKey">The key, used for the event</param>
        /// <param name="eventValue">The payload data of the event</param>
        public void Produce<TEvent>(string eventKey, TEvent eventValue)
        {
            try
            {
                var fileEvent = new KafkaEvent { Content = JObject.FromObject(eventValue) };

                string serializedEvent = JsonConvert.SerializeObject(fileEvent);
                _producer.Produce(_topic, new Message<string, string> { Key = eventKey, Value = serializedEvent });
            }
            catch
            {
                _producer.Dispose();
            }
        }

        /// <summary>
        /// Disposes the producer
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the producer
        /// </summary>
        /// <param name="disposing">If the managed resources should be disposed</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _producer.Dispose();
            }
        }
    }

}
