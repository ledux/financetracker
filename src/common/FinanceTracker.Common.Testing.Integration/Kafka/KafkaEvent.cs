﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FinanceTracker.Common.Testing.Integration.Kafka
{
    /// <summary>
    /// Represents an event, which will be sent to event sourcing
    /// </summary>
    public class KafkaEvent
    {
        /// <summary>
        /// Metadata in form of key value pairs
        /// </summary>
        [JsonProperty(PropertyName = "tags")]
        public IDictionary<string, string> Tags { get; set; }
        /// <summary>
        /// The actual payload of the event
        /// </summary>
        [JsonProperty(PropertyName = "cnt")]
        public JObject Content { get; set; }
    }
}
