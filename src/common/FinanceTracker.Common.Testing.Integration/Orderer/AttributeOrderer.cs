﻿using System;
using System.Collections.Generic;
using System.Linq;
using FinanceTracker.Common.Testing.Integration.Attributes;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace FinanceTracker.Common.Testing.Integration.Orderer
{
    /// <summary>
    /// Orders test cases by the Order value of <see cref="TestOrderAttribute"/>
    /// This order will be used for executing the tests.
    /// This is useful for integration test, when they build up on each other
    /// NEVER use this in unit tests. Unit tests must be independent.
    /// </summary>
    public class AttributeOrderer : ITestCaseOrderer
    {
        /// <summary>
        /// Orders the test cases first by the Order value, than by method name
        /// </summary>
        /// <typeparam name="TTestCase">The type of the test case</typeparam>
        /// <param name="testCases">The actual testcases to be ordered</param>
        /// <returns>An ordered list of the test cases, in which they should execute</returns>
        public IEnumerable<TTestCase> OrderTestCases<TTestCase>(IEnumerable<TTestCase> testCases) where TTestCase : ITestCase
        {
            var sortedMethods = new SortedDictionary<int, List<TTestCase>>();

            foreach (TTestCase testCase in testCases)
            {
                IAttributeInfo attributeInfo = testCase.TestMethod.Method
                    .GetCustomAttributes(typeof(TestOrderAttribute).AssemblyQualifiedName)
                    .SingleOrDefault();

                int priority = attributeInfo?.GetNamedArgument<int>(nameof(TestOrderAttribute.Order)) ?? 0;

                GetOrCreate(sortedMethods, priority).Add(testCase);
            }

            foreach (List<TTestCase> testCasesList in sortedMethods.Keys.Select(order => sortedMethods[order]))
            {
                testCasesList.Sort(
                    (first, second) => StringComparer.OrdinalIgnoreCase.Compare(
                        first.TestMethod.Method.Name,
                        second.TestMethod.Method.Name));

                foreach (TTestCase testCase in testCasesList) yield return testCase;
            }
        }

        private static TValue GetOrCreate<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TKey key) where TValue : new()
        {
            if (dictionary.TryGetValue(key, out TValue result)) return result;

            result = new TValue();
            dictionary[key] = result;

            return result;
        }
    }
}
