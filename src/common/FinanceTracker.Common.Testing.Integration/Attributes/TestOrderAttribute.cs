﻿using System;

namespace FinanceTracker.Common.Testing.Integration.Attributes
{
    /// <summary>
    /// Sets the order, with which a test will be executed.
    /// Lower order will be executed before higher order
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class TestOrderAttribute : Attribute
    {
        /// <summary>
        /// Instantiates a new instance of <see cref="TestOrderAttribute"/>
        /// </summary>
        /// <param name="order">The order number of the test</param>
        public TestOrderAttribute(int order)
        {
            Order = order;
        }

        /// <summary>
        /// The order number of the test.
        /// Higher numbers will be executed after lower numbers
        /// </summary>
        public int Order { get; }
    }
}
