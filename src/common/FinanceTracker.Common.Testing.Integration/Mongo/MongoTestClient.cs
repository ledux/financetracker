﻿using System;
using MongoDB.Driver;

namespace FinanceTracker.Common.Testing.Integration.Mongo
{
    /// <summary>
    /// Client to access a mongo database, used in integration tests
    /// </summary>
    public class MongoTestClient
    {
        private readonly IMongoDatabase _database;

        /// <summary>
        /// Instantiates a new <see cref="MongoTestClient"/>
        /// It configures and instantiates a <see cref="IMongoDatabase"/> to access on localhost and standard port 27017
        /// </summary>
        /// <param name="databaseName">The name of the database to connect to</param>
        public MongoTestClient(string databaseName)
        {
            var mongoSettings = new MongoClientSettings
            {
                Server = new MongoServerAddress("localhost", 27017),
                ConnectTimeout = new TimeSpan(0, 0, 2)
            };

            var mongoClient = new MongoClient(mongoSettings);

            _database = mongoClient.GetDatabase(databaseName);
        }

        /// <summary>
        /// Returns a collection in the configured database
        /// </summary>
        /// <typeparam name="T">The type of the documents, stored in the collection</typeparam>
        /// <param name="collectionName">The name of the collection, which should be returned</param>
        /// <returns>The collection in the database</returns>
        public IMongoCollection<T> GetMongoCollection<T>(string collectionName) => _database.GetCollection<T>(collectionName);
    }
}
