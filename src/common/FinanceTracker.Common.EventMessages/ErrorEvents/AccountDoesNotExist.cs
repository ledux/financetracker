﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ErrorEvents
{
    /// <summary>
    /// Event indicating that an account does not exist
    /// </summary>
    [Event("account", "missing")]
    public class AccountDoesNotExist
    {
        /// <summary>
        /// The id of the account, which was expected to exist but doesn't
        /// </summary>
        public long Id { get; set; }
    }
}
