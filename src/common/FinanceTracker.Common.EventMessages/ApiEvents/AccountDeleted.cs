﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Indicates that an account has been deleted
    /// </summary>
    [Event("account", "deleted")]
    public class AccountDeleted
    {
        /// <summary>
        /// The identification of the entry which should be deleted
        /// </summary>
        public long Id { get; set; }
    }
}
