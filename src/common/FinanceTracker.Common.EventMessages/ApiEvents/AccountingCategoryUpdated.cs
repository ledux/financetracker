﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Event gets issued when an accounting entry is created or updated
    /// </summary>
    [Event("accountingCategory", "updated")]
    public class AccountingCategoryUpdated
    {
        /// <summary>
        /// The identification of the category
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the category
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The identification of the parent. 0 if it does not have a parent.
        /// </summary>
        public long ParentId { get; set; }
        /// <summary>
        /// The name of the parent. Null, if it does not have a parent.
        /// </summary>
        public string ParentName { get; set; }
    }
}
