﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Indicates that an accounting entry has been deleted
    /// </summary>
    [Event("accountingEntry", "deleted")]
    public class AccountingEntryDeleted
    {
        /// <summary>
        /// The identification of the entry which should be deleted
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The amount of the deleted accounting entry
        /// </summary>
        public decimal Amount { get; set; }
        
        /// <summary>
        /// The id of the account to which this accounting entry belongs
        /// </summary>
        public long AccountId { get; set; }
    }
}
