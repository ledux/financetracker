﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Event indicating that a savings target is deleted
    /// </summary>
    [Event("savingsTarget", "deleted")]
    public class SavingsTargetDeleted
    {
        /// <summary>
        /// The Id of the deleted savings target
        /// </summary>
        public long SavingsTargetId { get; set; }
    }
}
