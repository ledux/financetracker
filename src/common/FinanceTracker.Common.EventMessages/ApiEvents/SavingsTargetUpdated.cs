﻿using System;
using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Represents an updated event of a savings target
    /// </summary>
    [Event("savingsTarget", "updated")]
    public class SavingsTargetUpdated
    {
        /// <summary>
        /// The identification of the target
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the savings target, required
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The optional description of the target
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The current balance of the target
        /// </summary>
        public decimal Balance { get; set; }
        /// <summary>
        /// On which account the money lies for this target
        /// </summary>
        public long AccountId { get; set; }
        /// <summary>
        /// Upon creation, what is the inital amount of the balance. This value cannot be changed anymore
        /// </summary>
        public decimal StartAmount { get; set; }
        /// <summary>
        /// How much wants the user save in order to meet the goal
        /// </summary>
        public decimal TargetAmount { get; set; }
        /// <summary>
        /// When has the user started saving for this goal. This value cannot be changed anymore
        /// </summary>
        public DateTimeOffset StartDate { get; set; }
        /// <summary>
        /// When does the user expect to reach this goal. This value is optional
        /// </summary>
        public DateTimeOffset? TargetDate { get; set; }
    }
}
