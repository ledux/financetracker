﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Indicates that an account has been updated
    /// </summary>
    [Event("account", "updated")]
    public class AccountUpdated
    {
        /// <summary>
        /// The id of the account
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// The name of the account
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The description of the account
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The balance of the account
        /// </summary>
        public decimal Balance { get; set; }
    }
}
