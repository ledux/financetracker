﻿using System.EventSourcing.Reflection;

namespace FinanceTracker.Common.EventMessages.ApiEvents
{
    /// <summary>
    /// Event indicating the balance of a savings target has been changed
    /// </summary>
    [Event("savingsTarget", "balanceSet")]
    public class SavingsTargetBalanceSet
    {
        /// <summary>
        /// The id of the savings target which balance has been changed
        /// </summary>
        public long SavingsTargetId { get; set; }
        /// <summary>
        /// The new Balance of the savings target
        /// </summary>
        public decimal Balance { get; set; }
    }
}
