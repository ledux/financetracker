﻿namespace FinanceTracker.Common.EventMessages.Constants
{
    /// <summary>
    /// Holds the keys for the attributes in tracing events
    /// </summary>
    public static class TracingKeys
    {
        public static readonly string AccountId = "accountId";
        public static readonly string AccountingEntryId = "accountingEntryId";
        public static readonly string CategoryId = "categoryId";
        public static readonly string SavingsTargetId = "savingsTargetId";
    }
}
