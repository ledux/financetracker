﻿using System;
using System.EventSourcing.Client;
using System.EventSourcing.Client.Kafka;
using System.EventSourcing.Client.Reflection;
using System.EventSourcing.Client.Serialization;
using System.EventSourcing.Hosting;
using System.EventSourcing.Hosting.Json;
using System.EventSourcing.Hosting.Kafka;
using System.EventSourcing.Hosting.Middleware;
using System.EventSourcing.Hosting.Projections;
using System.EventSourcing.Hosting.Projections.Reflection;
using System.EventSourcing.Hosting.Transformation;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FinanceTracker.Common.RetryPolicy;
using FinanceTracker.Common.Tracing.OpenTracing.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.Kafka;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using OpenTracing;
using FtTracer = FinanceTracker.Common.Tracing.Abstractions.ITracer;

namespace FinanceTracker.Common.KafkaHostBuilder
{
    /// <summary>
    /// Host builder which builds IHost and configures it
    /// to use kafka and produce/consume events from it
    /// </summary>
    public static class KafkaHostBuilder
    {
        private static KafkaConfiguration _kafkaConfiguration;

        /// <summary>
        /// Configures IHostBuilder to use and apply kafka configurations
        /// retrieved from appsettings files. 
        /// appsettings.ENVIRONMENT are read from CONFIG_DIR folder by environment type
        /// from ASPNETCORE_ENVIRONMENT environment variable
        /// </summary>
        public static IHostBuilder BuildKafkaHost(string kafkaSection = "kafka")
        {
            IHostBuilder builder = new HostBuilder()
                 .ConfigureAppConfiguration((hostContext, configurationBuilder) =>
                 {
                     string env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                     configurationBuilder
                         .SetBasePathFromEnv("CONFIG_DIR")
                         .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                         .AddJsonFile($"appsettings.{env}.json", optional: true, reloadOnChange: true)
                         .AddEnvironmentVariables();

                     IConfigurationRoot configuration = configurationBuilder.Build();
                     _kafkaConfiguration = configuration.GetSection(kafkaSection).Get<KafkaConfiguration>();
                 })
                 .ConfigureServices((hostBuilder, collection) =>
                 {

                     collection
                         .AddTracingCore(hostBuilder.Configuration)
                         .ConfigureKafka(_kafkaConfiguration)
                         .ConfigureKafkaRetry(hostBuilder.Configuration);
                 });

            return builder;
        }

        /// <summary>
        /// Adds the configuration for the confluent kafka to the service collection
        /// </summary>
        /// <param name="services">The collection where the configuration is added to</param>
        /// <param name="configuration">The configuration which is added</param>
        /// <returns>The same service collection to enable chaining</returns>
        public static IServiceCollection ConfigureKafka(this IServiceCollection services,
            KafkaConfiguration configuration)
        {
            services.Configure<KafkaListenerSettings>(settings =>
            {
                settings.Topics = configuration.ConsumerTopics;
                settings.BootstrapServers = configuration.BootstrapServers;
                settings.ConsumerGroup = configuration.ConsumerGroup;
                settings.AutoOffsetReset = configuration.AutoOffsetReset;
            })
            .Configure<KafkaConfiguration>(settings =>
            {
                settings.ConsumerTopics = configuration.ConsumerTopics;
                settings.BootstrapServers = configuration.BootstrapServers;
                settings.ConsumerGroup = configuration.ConsumerGroup;
                settings.AutoOffsetReset = configuration.AutoOffsetReset;
                settings.ProducerTopic = configuration.ProducerTopic;
            });

            return services;
        }

        /// <summary>
        /// Configures and adds the kafka client to the service collection.
        /// It creates the client using a retry policy when configred so.
        /// </summary>
        /// <param name="services">The service collection where the client is added to</param>
        /// <param name="configuration">The configuration of the client</param>
        /// <returns>The same service collection to enable chaining</returns>
        public static IServiceCollection AddKafkaClient(this IServiceCollection services, KafkaConfiguration configuration)
        {
            services.AddSingleton(provider => provider.BuildEventClient(configuration));

            return services;
        }

        /// <summary>
        /// Configures and adds a typed kafka client to the service collection.
        /// It can only handle a certain data type.
        /// </summary>
        /// <typeparam name="T">The type of the data, the client can handle</typeparam>
        /// <param name="collection">The service collection where the client is added to</param>
        /// <param name="configuration">The configuration for the client</param>
        /// <param name="keySelector">A function to determine which field of the event data should be used as the event key</param>
        /// <returns>The service collection with the added client</returns>
        public static IServiceCollection AddKafkaClient<T>(this IServiceCollection collection,
            KafkaConfiguration configuration, Func<T, string> keySelector)
        {
            collection.AddSingleton(provider => provider.BuildEventClient(keySelector, configuration));
            return collection;
        }


        /// <summary>
        /// Configures IHostBuilder to be a kafka messages consumer
        /// </summary>
        /// <param name="hostBuilder">IHostBuilder which is configured to use kafka</param>
        /// <param name="addProjections">Evens handling projections</param>
        /// <param name="addTransformators">Transformators for possible events convertation</param>
        public static IHostBuilder UseAsConsumer(this IHostBuilder hostBuilder,
            Func<IEventSourcingBuilder<IServiceCollection>, IEventSourcingBuilder<IServiceCollection>> addProjections,
            Func<ITransformationMiddlewareBuilder<StringJObjectContext>, ITransformationMiddlewareBuilder<StringJObjectContext>> addTransformators)
        {
            if (hostBuilder is null)
            {
                throw new ArgumentNullException(nameof(hostBuilder));
            }

            if (addTransformators is null)
            {
                throw new ArgumentNullException(nameof(addTransformators));
            }

            KafkaConfiguration kafkaConfiguration = null;

            hostBuilder
                .ConfigureServices(services =>
                {
                    kafkaConfiguration = services.BuildServiceProvider()
                        .GetService<IOptions<KafkaConfiguration>>()?.Value;
                })
                .UseKafka(
                    settings =>
                    {
                        settings.BootstrapServers = kafkaConfiguration.BootstrapServers;
                        settings.AutoOffsetReset = kafkaConfiguration.AutoOffsetReset;
                        settings.ConsumerGroup = kafkaConfiguration.ConsumerGroup;
                        settings.Topics = kafkaConfiguration.ConsumerTopics;
                    })
                .AddEventSourcing(builder =>
                {
                    builder
                        .FromKafka()
                        .Handle<string, JObject, StringJObjectContext>(
                        (k, v) => new StringJObjectContext { Content = v, Key = k },
                        (app) =>
                        {
                            app.UseProjections(projApp =>
                            {
                                projApp.FromServiceCollection(builder.Base);
                                projApp.AttributeKeyResolution();
                                projApp.KeyFrom(x => x.Key);
                                projApp.ContentFromJObject();
                            });

                            app.UseTransformations(subject => addTransformators(subject));
                        })
                        .RestoreSpanContext();

                    addProjections(builder);
                });

            return hostBuilder;
        }

        /// <summary>
        /// Configures IHostBuilder to be a kafka messages consumer
        /// by using DI instance of IEventClient which is configured 
        /// to push events into <see cref="KafkaConfiguration.ProducerTopic"/>
        /// </summary>
        /// <param name="hostBuilder">IHostBuilder which is configured to use kafka</param>
        /// <param name="configuration">The configuration for kafka</param>
        public static IHostBuilder UseAsProducer(this IHostBuilder hostBuilder, KafkaConfiguration configuration)
        {
            if (hostBuilder is null)
            {
                throw new ArgumentNullException(nameof(hostBuilder));
            }

            hostBuilder.ConfigureServices(collection => collection.AddKafkaClient(configuration));

            return hostBuilder;
        }

        /// <summary>
        /// Configures the host builder to consume messages
        /// </summary>
        /// <param name="hostBuilder">The builder, which builds the kafka host</param>
        /// <returns>The same builder to enable chaining</returns>
        public static IHostBuilder UseAsProducer(this IHostBuilder hostBuilder)
        {
            return hostBuilder.UseAsProducer(null);
        }

        /// <summary>
        /// Configures and creates an <see cref="IEventClient"/>. It configures it to use retry or not
        /// </summary>
        /// <param name="provider">The service provider used for getting the necessary service in order to build the client
        /// Nothing is added to it</param>
        /// <param name="configuration">The kafka configuration, used for configure the client.
        /// It takes precedent over the one in the <paramref name="provider"/></param>
        /// <returns>The same unchanged service provider</returns>
        internal static IEventClient BuildEventClient(this IServiceProvider provider, KafkaConfiguration configuration = null)
        {
            var openTracer = provider.GetService<ITracer>();
            var tracer = provider.GetService<FtTracer>();
            KafkaConfiguration kafkaConfiguration = configuration ?? provider.GetService<IOptions<KafkaConfiguration>>()?.Value;
            KafkaRetryConfiguration kafkaRetryConfiguration = provider.GetService<IOptions<KafkaRetryConfiguration>>()?.Value;

            if (kafkaConfiguration == null)
            {
                throw new InvalidOperationException("KafkaConfiguration is null. Call 'BuildKafkaHost' first");
            }

            string bootstrapServers = kafkaConfiguration.BootstrapServers.Aggregate((x, y) => $"{x},{y}");
            IEventClient client;

            if (kafkaRetryConfiguration != null && kafkaRetryConfiguration.IsRetryEnabled)
            {
                client = new EventClient()
                    .UseKafkaWithRetry(
                        kafkaConfiguration.ProducerTopic,
                        bootstrapServers,
                        kafkaRetryConfiguration,
                        tracer)
                    .UseSpanPropagation(openTracer)
                    .UseReflectionNameResolution()
                    .UseJsonSerialization();
            }
            else
            {
                client = new EventClient()
                    .UseKafka(kafkaConfiguration.ProducerTopic, bootstrapServers)
                    .UseSpanPropagation(openTracer)
                    .UseReflectionNameResolution()
                    .UseJsonSerialization();
            }

            return client;
        }

        internal static IEventClient BuildEventClient<T>(this IServiceProvider provider, Func<T, string> keySelector,
            KafkaConfiguration configuration = null)
        {
            var openTracer = provider.GetService<ITracer>();
            var tracer = provider.GetService<FtTracer>();
            KafkaConfiguration kafkaConfiguration = configuration ?? provider.GetService<IOptions<KafkaConfiguration>>()?.Value;
            KafkaRetryConfiguration kafkaRetryConfiguration = provider.GetService<IOptions<KafkaRetryConfiguration>>()?.Value;

            if (kafkaConfiguration == null)
            {
                throw new InvalidOperationException("KafkaConfiguration is null. Call 'BuildKafkaHost' first");
            }

            string bootstrapServers = kafkaConfiguration.BootstrapServers.Aggregate((x, y) => $"{x},{y}");
            IEventClient client;

            if (kafkaRetryConfiguration != null && kafkaRetryConfiguration.IsRetryEnabled)
            {
                client = new EventClient<T>()
                    .UseKafkaWithRetry(
                        kafkaConfiguration.ProducerTopic,
                        bootstrapServers,
                        kafkaRetryConfiguration,
                        tracer)
                    .UseSpanPropagation(openTracer)
                    .UseJsonSerialization();
            }
            else
            {
                client = new EventClient<T>()
                    .UseKafka(kafkaConfiguration.ProducerTopic, bootstrapServers)
                    .UseSpanPropagation(openTracer)
                    .UseJsonSerialization();
            }

            ((EventClient<T>)client).UseParser((data, newEvent) =>
            {
                newEvent.Name = keySelector(data);
                return Task.CompletedTask;
            });

            return client;
        }

        /// <summary>
        /// This method sets the base path to the value provided in the given environment variable.
        /// It can handle relative paths and will resolve them relativly to Directory.GetCurrentDirectory()
        /// </summary>
        /// <param name="builder">The configuration builder</param>
        /// <param name="environmentVariable">The name of the environment variable</param>
        /// <returns>the same configuration builder</returns>
        private static IConfigurationBuilder SetBasePathFromEnv(this IConfigurationBuilder builder, string environmentVariable)
        {
            string startupPath = Environment.GetEnvironmentVariable(environmentVariable)
                              ?? throw new ArgumentException($"Environment variable {environmentVariable} not found.");

            startupPath = Path.IsPathRooted(startupPath)
                ? startupPath
                : Path.Combine(Directory.GetCurrentDirectory(), startupPath);

            builder.SetBasePath(startupPath);

            return builder;
        }
    }
}
