﻿using System.Collections.Generic;

namespace FinanceTracker.Common.KafkaHostBuilder
{
    /// <summary>
    /// Konfiguration class for a consumer of the kafka event sourcing 
    /// </summary>
    public class KafkaConfiguration
    {
        private string _autoOffsetReset;

        /// <summary>
        /// All URLs for accessing the kafka brokers
        /// </summary>
        public IEnumerable<string> BootstrapServers { get; set; }

        /// <summary>
        /// The list of the topics, from which the consumer want to read events from
        /// Must be set when UseAsConsumer is used.
        /// </summary>
        public IEnumerable<string> ConsumerTopics { get; set; }

        /// <summary>
        /// A value for the topic which will be used
        /// in the EventClient to send its event to the kafka.
        /// Must be set when UseAsProducer is used.
        /// </summary>
        public string ProducerTopic { get; set; }

        /// <summary>
        /// The name of the consumer group this consumer belongs to
        /// Must be set when UseAsConsumer is used.
        /// </summary>
        public string ConsumerGroup { get; set; }

        /// <summary>
        /// Where the consumer wants to start to read, if there is no offset available for the consumer group
        /// Possible values are: earliest, latest.
        /// Converts value to start with Uppercase letter on set;
        /// </summary>
        public string AutoOffsetReset 
        {
            get => _autoOffsetReset;
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    value = value.ToLower();
                    _autoOffsetReset = value[0].ToString().ToUpper() + value.Substring(1);
                }
                else
                {
                    _autoOffsetReset = value;
                }
            }
        }
    }
}
