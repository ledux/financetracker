﻿using System;
using System.Collections.Generic;
using System.EventSourcing.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace FinanceTracker.Common.KafkaHostBuilder
{
    /// <summary>
    /// Resolves <see cref="IEventClient"/> depending on the topic they are producing to.
    /// </summary>
    /// <param name="topicName">The name of the producer topic</param>
    /// <returns>A fully configured <see cref="IEventClient"/> as a singleton</returns>
    public delegate IEventClient EventClientResolver(string topicName);

    /// <summary>
    /// Builds <see cref="IEventClient"/> with different producer topics
    /// </summary>
    public static class EventClientFactory
    {
        private static readonly Dictionary<string, IEventClient> Clients = new Dictionary<string, IEventClient>();

        /// <summary>
        /// Adds a <see cref="EventClientResolver"/> to the service collection
        /// </summary>
        /// <param name="serviceCollection">The service collection where the <see cref="EventClientResolver"/> is added to</param>
        /// <returns>The service collection with the <see cref="EventClientResolver"/> is added to</returns>
        public static IServiceCollection AddEventClients(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<EventClientResolver>(
                provider => topicName =>
                {
                    KafkaConfiguration kafkaConfiguration = provider.GetService<IOptions<KafkaConfiguration>>()?.Value;
                    if (kafkaConfiguration == null)
                    {
                        throw new InvalidOperationException("KafkaConfiguration is null. Call 'BuildKafkaHost' first");
                    }

                    if (!Clients.TryGetValue(topicName, out IEventClient client))
                    {
                        kafkaConfiguration.ProducerTopic = topicName;
                        client = provider.BuildEventClient(kafkaConfiguration);

                        Clients.Add(topicName, client);
                    }

                    return client;

                });
            return serviceCollection;

        }
    }
}
