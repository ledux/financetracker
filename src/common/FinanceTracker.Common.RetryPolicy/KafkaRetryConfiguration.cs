﻿namespace FinanceTracker.Common.RetryPolicy
{
    /// <summary>
    /// Represents the retry configuration for kafka event sourcing
    /// </summary>
    public class KafkaRetryConfiguration
    {
        /// <summary>
        /// Number of attempts before stop retrying.
        /// Indefinitely retry policy will be used in
        /// case if this value is 0
        /// </summary>
        public int RetryCount { get; set; }

        /// <summary>
        /// Timeout between retry attempts in milliseconds
        /// Exponential increases if this value is set to 0
        /// </summary>
        public int WaitBetweenRetriesInMilliseconds { get; set; }

        /// <summary>
        /// Defines if retry policy of kafka producer is enabled
        /// </summary>
        public bool IsRetryEnabled { get; set; }
    }
}