﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FinanceTracker.Common.RetryPolicy
{
    /// <summary>
    /// Extends the service collection with the retry policy for the kafka builder
    /// </summary>
    public static class KafkaRetryExtensions
    {
        /// <summary>
        /// Adds the configuration of the retry policy of kafka to the service collection
        /// </summary>
        /// <param name="serviceCollection">The service collection to which the configuration should be added</param>
        /// <param name="configuration">The configuration root, where the config is read from</param>
        /// <returns>The service collection to enable chaining</returns>
        public static IServiceCollection ConfigureKafkaRetry(this IServiceCollection serviceCollection,
            IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            var retryConfigs = configuration
                .GetSection("kafka.retry")
                .Get<KafkaRetryConfiguration>();

            serviceCollection.Configure<KafkaRetryConfiguration>(settings =>
            {
                settings.WaitBetweenRetriesInMilliseconds = retryConfigs.WaitBetweenRetriesInMilliseconds;
                settings.RetryCount = retryConfigs.RetryCount;
                settings.IsRetryEnabled = retryConfigs.IsRetryEnabled;
            });

            return serviceCollection;
        }
    }
}
