﻿using System;
using System.EventSourcing.Client;
using System.EventSourcing.Kafka;
using System.Threading.Tasks;
using Confluent.Kafka;
using FinanceTracker.Common.Tracing.Abstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Polly;
using Polly.Retry;

namespace FinanceTracker.Common.RetryPolicy
{
    public static class KafkaClientWithRetry
    {
        private static int _waitBetweenRetriesInMilliseconds;
        private static ITracingSpan _tracingSpan;

        /// <summary>
        /// Submitts messages to kafka on a given topic and using the given bootstrap servers.
        /// This method will create a new producer for every message submitted.
        /// This extions uses <see cref="Polly"/> retry policies
        /// </summary>
        /// <param name="subject">The <c>EventClient</c> to extend</param>
        /// <param name="topic">The topic to produce messages to</param>
        /// <param name="bootstrapServers">the boostrap servers</param>
        /// <param name="kafkaRetryConfiguration">
        /// configures retry policies. Check the purpose of each setting.
        /// </param>
        /// <param name="tracer">Used to log the retry messages</param>
        /// <returns>the modified event client.</returns>
        public static IEventClient UseKafkaWithRetry(this IEventClient subject,
            string topic,
            string bootstrapServers,
            KafkaRetryConfiguration kafkaRetryConfiguration,
            ITracer tracer)
        {
            _waitBetweenRetriesInMilliseconds = kafkaRetryConfiguration.WaitBetweenRetriesInMilliseconds;
            _tracingSpan = tracer.BuildSpan("kafka-retryclient");

            var config = new ProducerConfig { BootstrapServers = bootstrapServers };
            var builder = new ProducerBuilder<string, string>(config);

            AsyncRetryPolicy<DeliveryResult<string, string>> retry;

            if (kafkaRetryConfiguration.RetryCount != 0)
            {
                retry = Policy
                    .Handle<ProduceException<string, string>>()
                    .OrResult<DeliveryResult<string, string>>(r => r.Status == PersistenceStatus.NotPersisted)
                    .WaitAndRetryAsync(kafkaRetryConfiguration.RetryCount, GetRetryTimeout, OnRetryAction);
            }
            else
            {
                retry = Policy
                    .Handle<ProduceException<string, string>>()
                    .OrResult<DeliveryResult<string, string>>(r => r.Status == PersistenceStatus.NotPersisted)
                    .WaitAndRetryForeverAsync(GetRetryTimeout, OnRetryAction);
            }


            subject.UseHandle(
                async evnt =>
                {
                    using IProducer<string, string> producer = builder.Build();
                    try
                    {
                        var kafkaEvent = new KafkaEvent { Tags = evnt.Tags, Content = JObject.Parse(evnt.Content) };
                        string strContent = await Task.Run(() => JsonConvert.SerializeObject(kafkaEvent));
                        await retry.ExecuteAsync(() => 
                            producer.ProduceAsync(topic, new Message<string, string> { Key = evnt.Name, Value = strContent }));
                    }
                    catch (ProduceException<string, string> e)
                    {
                        _tracingSpan.LogError(e, "Publish to kafka throws an exception after all retries");
                        throw;
                    }
                });

            return subject;
        }

        private static TimeSpan GetRetryTimeout(int retryAttempt)
        {
            if (_waitBetweenRetriesInMilliseconds == 0)
            {
                _waitBetweenRetriesInMilliseconds = (int)(Math.Pow(2, retryAttempt) > 3600 ? 3600 : Math.Pow(2, retryAttempt)) * 1000;
            }

            return TimeSpan.FromMilliseconds(_waitBetweenRetriesInMilliseconds);
        }

        private static void OnRetryAction(DelegateResult<DeliveryResult<string, string>> deliveryResult, TimeSpan span, int count, Context _)
        {
            OnRetryAction(deliveryResult, count, span);
        }

        private static void OnRetryAction(DelegateResult<DeliveryResult<string, string>> deliveryResult, int count, TimeSpan span)
        {
            if (deliveryResult.Exception != null)
            {
                _tracingSpan.LogError(deliveryResult.Exception, $"Publish to kafka throws an exception. Retry: {count}, Timeout: {span.TotalSeconds} sec.");
            }
            else if (deliveryResult.Result != null)
            {
                _tracingSpan.LogError(@$"Publish to kafka failed with delivery status {deliveryResult.Result.Status}. 
                                                Retry: {count}, Timeout: {span.TotalSeconds} sec.");
            }
        }
    }
}
