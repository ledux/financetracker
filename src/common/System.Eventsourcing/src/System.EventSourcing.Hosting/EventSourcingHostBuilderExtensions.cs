﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace System.EventSourcing.Hosting
{
    public static class EventSourcingHostBuilderExtensions
    {
        public static IEventSourcingBuilder<IServiceCollection> AddProjection<TProjection>(this IEventSourcingBuilder<IServiceCollection> builder)
        {
            var isProjection = typeof(TProjection)
                .GetInterfaces()
                .Any(x =>
                  x.IsGenericType &&
                  x.GetGenericTypeDefinition() == typeof(IProjection<>));

            if (!isProjection)
            {
                throw new ArgumentException($"Type {typeof(TProjection).Name} cannot be registered because it does not implement IProjection<TEvent>");
            }

            builder.Base.AddTransient(typeof(TProjection));
            return builder;
        }
    }
}
