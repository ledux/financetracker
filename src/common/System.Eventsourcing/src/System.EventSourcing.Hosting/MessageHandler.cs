using System.Threading.Tasks;

namespace System.EventSourcing.Hosting
{
    public delegate Task MessageHandler<TKey, TContent>(TKey name, TContent content);
}
