using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace System.EventSourcing.Hosting.Middleware
{
    public static class MiddlewareHostBuilderExtensions
    {
        public static IEventSourcingBuilder<IServiceCollection> Handle<TKey, TContent, TContext>(
            this IEventSourcingBuilder<IServiceCollection> builder,
            Func<TKey, TContent, TContext> transform,
            Action<IMiddlewareAppBuilder<TContext, IServiceCollection>> middlewareFactory)
            where TContext : IContext
        {
            builder.Configure(() =>
            {
                var appBuilder = new MiddlewareAppBuilder<TContext, IServiceCollection> { Base = builder };
                middlewareFactory(appBuilder);

                Middleware<TContext> middleware = appBuilder.Build();

                builder.Base.AddScoped(sp =>
                {
                    Task Handler(TKey key, TContent content)
                    {
                        TContext context = transform(key, content);
                        context.Services = sp;
                        return middleware(context);
                    }

                    return (MessageHandler<TKey, TContent>) Handler;
                });
            });

            return builder;
        }
    }
}
