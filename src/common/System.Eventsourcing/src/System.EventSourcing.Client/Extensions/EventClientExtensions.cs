﻿using System.Threading.Tasks;

namespace System.EventSourcing.Client.Extensions
{
    public static class EventClientExtensions
    {
        public static IEventClient UseNullKey(this IEventClient eventClient)
        {
            eventClient.UseParser((data, type, evnt) =>
            {
                evnt.Name = null;
                return Task.CompletedTask;
            });

            return eventClient;
        }
    }
}
