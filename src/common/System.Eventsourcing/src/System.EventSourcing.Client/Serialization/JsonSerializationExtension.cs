﻿using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace System.EventSourcing.Client.Serialization
{
    public static class JsonSerializationExtension
    {
        public static IEventClient UseJsonSerialization(this IEventClient subject)
        {
            subject.UseParser(
                async (evt, type, evnt) =>
                {
                    using (var memstream = new MemoryStream())
                    {
                        string strContent = string.Empty;
                        await Task.Run(() => strContent = JsonConvert.SerializeObject(evt));
                        evnt.Content = strContent;
                    }
                });

            return subject;
        }
    }
}
