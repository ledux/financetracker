﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace System.EventSourcing.Client
{
    /// <summary>
    /// Event client which handles typed events and accepts event specific middleware.
    /// </summary>
    /// <typeparam name="T">The type of the event this client can handle</typeparam>
    public class EventClient<T> : EventClient
    {
        private readonly IList<EventMiddleware<T>> _typedParsers = new List<EventMiddleware<T>>();

        /// <summary>
        /// Registers a parser for a typed event
        /// The order of registering is the order they will be applied to the <see cref="Event"/>
        /// </summary>
        /// <param name="parser">The functionality for changing the event depending on the event data</param>
        public void UseParser(EventMiddleware<T> parser)
        {
            _typedParsers.Add(parser);
        }

        /// <summary>
        /// Applies all parsers to the <see cref="Event"/> and calls all handlers with the result
        /// </summary>
        /// <param name="eventData">The data for the event</param>
        /// <returns></returns>
        public async Task Publish(T eventData)
        {
            var newEvent = new Event();
                Type dataType = typeof(T);

            foreach (EventMiddleware<T> middleware in _typedParsers)
            {
                await middleware(eventData, newEvent);
            }

            foreach (EventMiddleware eventMiddleware in Parsers)
            {
                await eventMiddleware(eventData, dataType, newEvent);
            }

            foreach (EventHandle handler in Handlers)
            {
                await handler(newEvent);
            }
        }
    }
}
