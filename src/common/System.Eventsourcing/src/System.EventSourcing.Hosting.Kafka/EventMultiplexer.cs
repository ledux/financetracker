﻿using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting.Kafka;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.EventSourcing.Kafka;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.EventSourcing.Hosting.Kafka
{
    internal class EventMultiplexer : IMessageHandler<string, byte[]>
    {
        private readonly IServiceProvider _serviceProvider;

        public EventMultiplexer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task Handle(string key, byte[] value)
        {
            using (IServiceScope scope = _serviceProvider.CreateScope())
            {
                var @event = JsonConvert.DeserializeObject<KafkaEvent>(Encoding.UTF8.GetString(value));


                if (@event.Tags != null && @event.Tags.Any())
                {
                    var context = scope.ServiceProvider.GetService<IEventContext>();

                    foreach (KeyValuePair<string, string> tag in @event.Tags)
                    {
                        context.Tags.Add(tag.Key, tag.Value);
                    }
                }

                var handler = scope.ServiceProvider.GetService<MessageHandler<string, JObject>>();
                return handler(key, @event.Content);
            }
        }
    }
}