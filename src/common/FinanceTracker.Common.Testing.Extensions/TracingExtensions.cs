﻿using FinanceTracker.Common.Tracing.Abstractions;
using Moq;

namespace FinanceTracker.Common.Testing.Extensions
{
    /// <summary>
    /// Extends the <see cref="IUnitUnderTestBuilder{T}"/> with dependencies for the <see cref="ITracer"/>
    /// </summary>
    public static class TracingExtensions
    {
        /// <summary>
        /// Adds the mocks necessary to test methods which use <see cref="ITracer"/>
        /// </summary>
        /// <typeparam name="T">The type of the unit under test</typeparam>
        /// <param name="builder">The Builder, which gets extended</param>
        /// <returns>The same builder to enable chaining</returns>
        public static IUnitUnderTestBuilder<T> AddTracingMocks<T>(this IUnitUnderTestBuilder<T> builder) where T : class
        {
            var tracerMock = new Mock<ITracer>();
            var spanMock = new Mock<ITracingSpan>();
            spanMock.SetupAllProperties();
            var context = Mock.Of<ITracingSpanContext>();
            spanMock.Setup(s => s.Context).Returns(context);
            tracerMock.Setup(t => t.ActiveSpan).Returns(spanMock.Object);
            tracerMock.Setup(t => t.BuildSpan(It.IsAny<string>())).Returns(spanMock.Object);

            builder.With<ITracer>(tracerMock.Object);

            return builder;
        }
    }
}
