﻿using System.Threading;
using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using Moq;

namespace FinanceTracker.Common.Testing.Extensions
{
    public static class UnitUnderTestBuilderExtensions
    {
        public static IUnitUnderTestBuilder<TRepository> SetupMongo<TRepository>(
            this IUnitUnderTestBuilder<TRepository> builder) where TRepository : MongoDbRepositoryBase
        {
            Mock<IMongoClient> clientMock = new Mock<IMongoClient>().SetupDatabase();
            return AddDependencies(builder, clientMock);
        }

        public static IUnitUnderTestBuilder<TRepository> SetupMongo<TRepository, TCollection>(
            this IUnitUnderTestBuilder<TRepository> builder, IMongoCollection<TCollection> collection)
            where TRepository : MongoDbRepository<TCollection>
        {
            Mock<IMongoClient> clientMock = new Mock<IMongoClient>().SetupDatabase(collection);
            return AddDependencies(builder, clientMock);
        }

        public static IUnitUnderTestBuilder<TRepository> SetupGridFSBucket<TRepository>(
            this IUnitUnderTestBuilder<TRepository> builder, byte[] fileBytes = null) where TRepository : MongoDbRepositoryBase
        {
            var bucketMock = new Mock<IGridFSBucket<long>>();
            bucketMock.Setup(
                b => b.DownloadAsBytesAsync(
                    It.IsAny<long>(),
                    It.IsAny<GridFSDownloadOptions>(),
                    It.IsAny<CancellationToken>())).ReturnsAsync(fileBytes);

            return builder.With<IGridFSBucket<long>>(bucketMock.Object);
        }

        private static IUnitUnderTestBuilder<TRepository> AddDependencies<TRepository>(IUnitUnderTestBuilder<TRepository> builder, IMock<IMongoClient> clientMock)
            where TRepository : MongoDbRepositoryBase
        {
            Mock<IOptions<MongoDbRepositorySettings>> optionsMock =
                new Mock<IOptions<MongoDbRepositorySettings>>().SetupSettings();
            builder
                .With<IMongoClient>(clientMock.Object)
                .With<IOptions<MongoDbRepositorySettings>>(optionsMock.Object);

            return builder;
        }
    }

}
