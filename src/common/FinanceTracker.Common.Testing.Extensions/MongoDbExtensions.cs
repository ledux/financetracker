﻿using FinanceTracker.Common.MongoSetup;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Moq;

namespace FinanceTracker.Common.Testing.Extensions
{
    internal static class MockExtensions 
    {
        internal static Mock<IMongoClient> SetupDatabase(this Mock<IMongoClient> clientMock)
        {
            var databaseMock = new Mock<IMongoDatabase>();
            clientMock
                .Setup(
                    c => c.GetDatabase(
                        It.IsAny<string>(),
                        It.IsAny<MongoDatabaseSettings>()))
                .Returns(databaseMock.Object);

            return clientMock;
        }

        internal static Mock<IMongoClient> SetupDatabase<TCollection>(this Mock<IMongoClient> clientMock, IMongoCollection<TCollection> collection)
        {
            var databaseMock = new Mock<IMongoDatabase>();
            databaseMock.Setup(
                    d => d.GetCollection<TCollection>(
                        It.IsAny<string>(),
                        It.IsAny<MongoCollectionSettings>()))
                .Returns(collection);


            clientMock
                .Setup(
                    c => c.GetDatabase(
                        It.IsAny<string>(),
                        It.IsAny<MongoDatabaseSettings>()))
                .Returns(databaseMock.Object);

            return clientMock;
        }

        internal static Mock<IOptions<MongoDbRepositorySettings>> SetupSettings(
            this Mock<IOptions<MongoDbRepositorySettings>> settingsMock)
        {
            var settings = new MongoDbRepositorySettings();
            settingsMock.SetupGet(s => s.Value).Returns(settings);
            return settingsMock;
        }
    }
}
