﻿using System;
using Moq;

namespace FinanceTracker.Common.Testing
{
    /// <summary>
    /// Builds classes which can be tested in unit tests
    /// </summary>
    /// <typeparam name="T">The type of the class which will be built</typeparam>
    public interface IUnitUnderTestBuilder<T> where T : class
    {
        /// <summary>
        /// Instantiates the class with the provided dependencies, or respectively, with mock objects
        /// </summary>
        /// <returns>An instance to be used in unit tests</returns>
        T Build();

        /// <summary>
        /// Instantiates the unit under test with the provided dependencies, or respectively, with mock objects. 
        /// Along with said class, a <see cref="MockBag"/> object is created, holding the implicitly instantiated
        /// <see cref= "Mock{T}" /> objects
        /// </summary>
        /// <returns>A tuple of the unit under test and a <see cref="MockBag"/> object holding the implicitly
        /// instantiated <see cref="Mock"/> objects</returns>
        (T, MockBag) BuildBoth();

        /// <summary>
        /// Adds a dependency to inject during instantiation of the unit under test
        /// </summary>
        /// <typeparam name="TDependency">The interface type of the constructor parameter for which an instance is provided</typeparam>
        /// <param name="dependency">The instance used in the constructor. Most likely a mocked dependency</param>
        /// <returns>This instance of <see cref="IUnitUnderTestBuilder{T}"/> for a fluent construction</returns>
        /// <exception cref="ArgumentException">Thrown if the dependency does not implement the interface provided by the type parameter</exception>
        IUnitUnderTestBuilder<T> With<TDependency>(object dependency);
    }
}