﻿using System;
using System.Collections.Generic;
using Moq;

namespace FinanceTracker.Common.Testing
{
    /// <summary>
    /// Object holding <see cref="Mock"/> objects
    /// </summary>
    public class MockBag
    {
        private readonly Dictionary<Type, object> _dependencies;

        /// <summary>
        /// Internal constructor with the dependencies
        /// </summary>
        /// <param name="dependencies">The dependencies as mock objects</param>
        internal MockBag(Dictionary<Type, object> dependencies)
        {
            _dependencies = dependencies ?? throw new ArgumentNullException(nameof(dependencies));
        }

        /// <summary>
        /// Gets the mocked object of the dependencies' type. Can be used to perform validation on them.
        /// </summary>
        /// <typeparam name="T">The interface type to return a mock object of</typeparam>
        /// <returns>A mock object of the type T, null if there is no such mock</returns>
        public Mock<T> GetMock<T>() where T : class
        {
            if (_dependencies.ContainsKey(typeof(T)))
            {
                return (Mock<T>) _dependencies[typeof(T)];
            }

            return null;
        }

        /// <summary>
        /// Add a new mock to the mock bag
        /// </summary>
        /// <param name="type">The type of the mocked object</param>
        /// <param name="dependency">The mocked object</param>
        public void Add(Type type, object dependency)
        {
            _dependencies[type] = dependency;
        }
    }
}
