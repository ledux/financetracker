﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Moq;

namespace FinanceTracker.Common.Testing
{
    /// <summary>
    /// Builds classes which can be tested in unit tests
    /// <code>
    /// var unitUnderTest = new UnitUnderTest().With&lt;ServiceA&gt;().With&lt;ServiceB&gt;(serviceBMock.Object).Build();
    /// </code>
    /// </summary>
    /// <typeparam name="T">The type of the class which will be built</typeparam>
    public class UnitUnderTestBuilder<T> : IUnitUnderTestBuilder<T> where T : class
    {
        private readonly Dictionary<Type, object> _dependencies = new Dictionary<Type, object>();
        private readonly Dictionary<Type, object> _mockDependencies = new Dictionary<Type, object>();

        /// <summary>
        /// Instantiates the class with the provided dependencies, or respectively, with mock objects
        /// </summary>
        /// <returns>An instance to be used in unit tests</returns>
        public T Build()
        {
            Type type = typeof(T);
            ConstructorInfo constructorInfo = type
                .GetConstructors()
                .OrderByDescending(c => c.GetParameters().Length)
                .First();

            object[] parameters = constructorInfo
                .GetParameters()
                .Select(GetInstanceOrCreateMock)
                .ToArray();

            return (T)constructorInfo.Invoke(parameters);
        }

        /// <summary>
        /// Instantiates the unit under test with the provided dependencies, or respectively, with mock objects. 
        /// Along with said class, a <see cref="MockBag"/> object is created, holding the implicitly instantiated
        /// <see cref= "Mock{T}" /> objects
        /// </summary>
        /// <returns>A tuple of the unit under test and a <see cref="MockBag"/> object holding the implicitly
        /// instantiated <see cref="Mock{T}"/> objects</returns>
        public (T, MockBag) BuildBoth()
        {
            T unitUnderTest = Build();
            var mockBag = new MockBag(_mockDependencies);

            return (unitUnderTest, mockBag);
        }

        /// <summary>
        /// Adds a dependency to inject during instantiation of the unit under test
        /// </summary>
        /// <typeparam name="TDependency">The interface type of the constructor parameter for which an instance is provided</typeparam>
        /// <param name="dependency">The instance used in the constructor. Most likely a mocked dependency</param>
        /// <returns>This instance of <see cref="IUnitUnderTestBuilder{T}"/> for a fluent construction</returns>
        /// <exception cref="ArgumentException">Thrown if the dependency does not implement the interface provided by the type parameter</exception>
        public IUnitUnderTestBuilder<T> With<TDependency>(object dependency)
        {
            if (!(dependency is TDependency))
            {
                throw new ArgumentException($"{nameof(dependency)} does not implement {typeof(TDependency)}");
            }

            _dependencies[typeof(TDependency)] = dependency;

            return this;
        }

        private object GetInstanceOrCreateMock(ParameterInfo parameter)
        {
            if (_dependencies.ContainsKey(parameter.ParameterType))
            {
                return _dependencies[parameter.ParameterType];
            }

            Type genericMock = typeof(Mock<>).MakeGenericType(parameter.ParameterType);
            var mock = (Mock)Activator.CreateInstance(genericMock);
            _mockDependencies.Add(parameter.ParameterType, mock);

            return mock.Object;
        }
    }
}