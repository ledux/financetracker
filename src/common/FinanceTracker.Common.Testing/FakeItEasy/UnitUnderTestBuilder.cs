﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FakeItEasy.Sdk;

namespace FinanceTracker.Common.Testing.FakeItEasy
{
    public class UnitUnderTestBuilder<T> : IUnitUnderTestBuilder<T> where T : class
    {
        private readonly Dictionary<Type, object> _dependencies;
        private readonly ConstructorInfo _constructorInfo;

        public UnitUnderTestBuilder()
        {
            Type type = typeof(T);
            _constructorInfo = type
                .GetConstructors()
                .OrderByDescending(c => c.GetParameters().Length)
                .First();

            IEnumerable<KeyValuePair<Type, object>> keyValuePairs = _constructorInfo
                .GetParameters()
                .Select(info => new KeyValuePair<Type, object>(info.ParameterType,  Create.Fake(info.ParameterType)));

            _dependencies = keyValuePairs.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }

        public T Build()
        {
            object[] parameters = _constructorInfo
                .GetParameters()
                .Select(info => _dependencies[info.ParameterType])
                .ToArray();
            return (T)_constructorInfo.Invoke(parameters);
        }

        public IUnitUnderTestBuilder<T> With<TDependency>(TDependency dependency)
        {
            if (dependency == null)
            {
                throw new ArgumentNullException(nameof(dependency));
            }

            _dependencies[typeof(TDependency)] = dependency;

            return this;
        }

        public TMock GetMock<TMock>() where TMock : class
        {
            return (TMock) _dependencies[typeof(TMock)];
        }
    }
}