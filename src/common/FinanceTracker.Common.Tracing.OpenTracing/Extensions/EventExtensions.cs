﻿using System.Collections.Generic;
using FinanceTracker.Common.Tracing.Models;

namespace FinanceTracker.Common.Tracing.OpenTracing.Extensions
{
    /// <summary>
    /// Extends the functionality of the <see cref="TracingEvent"/> for OpenTracing
    /// </summary>
    public static class EventExtensions
    {
        private const string MessageKey = "message";

        /// <summary>
        /// Adds the name of the event as message to the attributes and returns them for logging to the OpenTracing span
        /// </summary>
        /// <param name="tracingEvent">The event which gets extended</param>
        /// <returns>Key/Value Pairs for logging</returns>
        public static IEnumerable<KeyValuePair<string, object>> ToOpenTracingLog(this TracingEvent tracingEvent)
        {
            var dict = new Dictionary<string, object>(tracingEvent.Attributes);

            if (!dict.TryAdd(MessageKey, tracingEvent.Name))
            {
                dict[MessageKey] = tracingEvent.Name;
            }
          
            return dict;
        }
    }
}
