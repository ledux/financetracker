﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.Extensions.Primitives;

namespace FinanceTracker.Common.Tracing.OpenTracing.Extensions
{
    /// <summary>
    /// Contains extension methods to IOpenTracingBuilder
    /// </summary>
    public static class OpenTracingBuilderExtensions
    {
        /// <summary>
        /// Logs all header accept "Authorization" to tracer.
        /// Also logs user id and user roles if present
        /// </summary>
        /// <param name="openTracingBuilder">Instance of IOpenTracingBuilder</param>
        public static IOpenTracingBuilder AddRequestHeadersLogger(this IOpenTracingBuilder openTracingBuilder)
        {
            openTracingBuilder.ConfigureAspNetCore(o => o.Hosting.OnRequest = (span, context) =>
            {
                StringValues token = context.Request.Headers.SingleOrDefault(h => h.Key == "Authorization").Value;

                if (!string.IsNullOrEmpty(token))
                {
                    var handler = new JwtSecurityTokenHandler();
                    string jwt = token.ToString().Replace("Bearer ", string.Empty);
                    if (handler.CanReadToken(jwt))
                    {
                        var jwtToken = handler.ReadToken(jwt) as JwtSecurityToken;
                        string userId = jwtToken?.Claims.FirstOrDefault(claim => claim.Type == "userId")?.Value;
                        string userRole = jwtToken?.Claims.FirstOrDefault(claim => claim.Type == "userRole")?.Value;

                        span.Log(new Dictionary<string, object>()
                        {
                            { "userId", userId ?? "n/a" },
                            { "userRole", userRole ?? "n/a" }
                        });

                        span.SetTag("userId", userId);
                    }
                }

                span.Log(context.Request.Headers
                    .Where(h => h.Key != "Authorization")
                    .Select(h => new KeyValuePair<string, object>(h.Key, h.Value)));
            });

            return openTracingBuilder;
        }
    }
}
