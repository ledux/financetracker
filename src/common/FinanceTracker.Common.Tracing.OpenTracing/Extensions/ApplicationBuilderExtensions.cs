﻿using FinanceTracker.Common.Tracing.OpenTracing.Implementations;
using Microsoft.AspNetCore.Builder;

namespace FinanceTracker.Common.Tracing.OpenTracing.Extensions
{
    /// <summary>
    /// Extends the functionality of the <see cref="IApplicationBuilder"/>
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Adds the middleware to add the tracing ID to the response.
        /// </summary>
        /// <param name="builder">The application builder</param>
        /// <returns>The same application builder to allow chaining</returns>
        public static IApplicationBuilder UseTracingSpanPropagation(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<OpenTracingSpanPropagation>();
        }
    }
}
