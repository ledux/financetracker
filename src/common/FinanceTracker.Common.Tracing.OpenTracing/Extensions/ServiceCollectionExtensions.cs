﻿using System;
using System.Linq;
using System.Net.Sockets;
using FinanceTracker.Common.Tracing.Models;
using FinanceTracker.Common.Tracing.OpenTracing.Implementations;
using Jaeger;
using Jaeger.Metrics;
using Jaeger.Reporters;
using Jaeger.Samplers;
using Jaeger.Senders.Thrift;
using Jaeger.Thrift.Senders.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenTracing;

namespace FinanceTracker.Common.Tracing.OpenTracing.Extensions
{
        /// <summary>
    /// Extends the service collection to add libraries for tracing and configures them
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds the tracer and configures it. Used for services without API
        /// </summary>
        /// <param name="serviceCollection">The service collection where the tracer should be added</param>
        /// <param name="configuration">The configuration for configure the tracer</param>
        /// <returns></returns>
        public static IServiceCollection AddTracingCore(this IServiceCollection serviceCollection,
            IConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            var tracingConfiguration = configuration
                .GetSection("tracing")
                .Get<TracingConfiguration>();

            serviceCollection.AddSingleton<ITracer>(
                provider =>
                {
                    RemoteReporter reporter = null;
                    try
                    {
                        reporter = new RemoteReporter.Builder()
                            .WithSender(new UdpSender(tracingConfiguration.Host, tracingConfiguration.Port, ThriftUdpClientTransport.MaxPacketSize))
                            .WithMetrics(new MetricsImpl(NoopMetricsFactory.Instance))
                            .Build();
                    }
                    catch (SocketException e)
                    {
                        Console.WriteLine(e);
                    }

                    string applicationName = AppDomain.CurrentDomain.FriendlyName
                        .Split('.')
                        .LastOrDefault();

                    return new Tracer.Builder(applicationName)
                        .WithSampler(new ConstSampler(true))
                        .WithReporter(reporter)
                        .Build();
                });

            serviceCollection.AddSingleton<Abstractions.ITracer, OpenTracingTracer>();

            return serviceCollection;
        }

        /// <summary>
        /// Adds the Open tracing libraries for creating spans upon APIs calls and configures and adds the tracer.
        /// Should only be used in web APIs
        /// </summary>
        /// <param name="serviceCollection">The collection to add the tracing libraries to.</param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddTracing(this IServiceCollection serviceCollection,
            IConfiguration configuration)
        {
            serviceCollection.AddOpenTracing(bu => bu.AddRequestHeadersLogger());
            serviceCollection.AddTracingCore(configuration);

            return serviceCollection;
        }
    }
}
