﻿using System.Collections.Generic;
using System.EventSourcing.Client;
using System.EventSourcing.Hosting;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;
using OpenTracing;
using OpenTracing.Propagation;

namespace FinanceTracker.Common.Tracing.OpenTracing.Extensions
{
        /// <summary>
    /// Extensions for extending the tracing span over event sourcing
    /// </summary>
    public static class KafkaExtensions
    {
        /// <summary>
        /// Configures kafka to read the span id from the tags and create a new span out of it
        /// </summary>
        /// <param name="builder">The builder, which builds the es functionality</param>
        /// <returns>The builder to enable chaining</returns>
        public static IEventSourcingBuilder<IServiceCollection> RestoreSpanContext(
            this IEventSourcingBuilder<IServiceCollection> builder)
        {
            ServiceDescriptor innerMessageHandlerServiceDescriptor;
            builder.Configure(() =>
            {
                innerMessageHandlerServiceDescriptor = builder.Base.FirstOrDefault(
                    s =>
                        s.ServiceType == typeof(MessageHandler<string, JObject>));

                builder.Base.Remove(innerMessageHandlerServiceDescriptor);

                builder.Base.AddScoped<MessageHandler<string, JObject>>(
                    sp =>
                    {
                        return (name, obj) =>
                        {
                            var eventContext = sp.GetRequiredService<IEventContext>();
                            var tracer = sp.GetRequiredService<ITracer>();
                            ISpanContext spanContext = tracer.Extract(
                                BuiltinFormats.TextMap,
                                new TextMapExtractAdapter(eventContext.Tags));
                            using IScope _ = tracer.BuildSpan(name)
                                .AsChildOf(spanContext)
                                .WithTag("checkpoint", "read-from-es")
                                .StartActive(true);
                            var innerMessageHandler =
                                innerMessageHandlerServiceDescriptor?.ImplementationFactory(sp) as
                                    MessageHandler<string, JObject>;

                            return innerMessageHandler?.Invoke(name, obj);
                        };
                    });
                });

            return builder;
        }

        /// <summary>
        /// Configures kafka to add the span id to the tags of the message.
        /// This allows to span tracing over event sourcing.
        /// </summary>
        /// <param name="eventClient">The event client which sends the message to kafka</param>
        /// <param name="tracer">The tracer, which is used to trace the request.</param>
        /// <returns>The event client to enable chaining.</returns>
        public static IEventClient UseSpanPropagation(this IEventClient eventClient, ITracer tracer)
        {
            if (tracer == null)
            {
                return eventClient;
            }

            eventClient.UseParser((obj, type, evnt) =>
            {
                if (tracer.ActiveSpan?.Context != null)
                {
                    evnt.Tags ??= new Dictionary<string, string>();
                    tracer.Inject(tracer.ActiveSpan.Context, BuiltinFormats.TextMap, new TextMapInjectAdapter(evnt.Tags));
                }

                return Task.CompletedTask;
            });

            return eventClient;
        }
    }

}
