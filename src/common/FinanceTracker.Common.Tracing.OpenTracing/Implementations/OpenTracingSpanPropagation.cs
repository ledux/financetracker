﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OpenTracing;
using OpenTracing.Propagation;

namespace FinanceTracker.Common.Tracing.OpenTracing.Implementations
{
    /// <summary>
    /// Middleware to include the tracing id to the http header
    /// </summary>
    public class OpenTracingSpanPropagation
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="next">The next middleware to be calledn</param>
        public OpenTracingSpanPropagation(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// The invocation of the middleware. Adds the context to the http headers
        /// </summary>
        /// <param name="httpContext">The context in which the http calls</param>
        /// <param name="tracer">The open tracing tracer, from which the trace id will be taken</param>
        /// <returns>Task for async</returns>
        public async Task InvokeAsync(HttpContext httpContext, ITracer tracer)
        {
            var spanContextCarrier = new Dictionary<string, string>();
            tracer.Inject(tracer.ActiveSpan.Context, BuiltinFormats.TextMap, new TextMapInjectAdapter(spanContextCarrier));
            foreach ((string key, string value) in spanContextCarrier)
            {
                httpContext.Response.Headers.Add(key, value);
            }

            await _next(httpContext);
        }
    }

}