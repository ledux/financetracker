﻿using System;
using System.Collections.Generic;
using FinanceTracker.Common.Tracing.Abstractions;
using FinanceTracker.Common.Tracing.Models;
using OpenTracing;

namespace FinanceTracker.Common.Tracing.OpenTracing.Implementations
{
    /// <summary>
    /// A span which traces a request throught the application
    /// Opentracing implementation of a tracing span. Wraps an <see cref="ISpan"/>
    /// </summary>
    public sealed class OpenTracingSpan : ITracingSpan
    {
        #region fields
        private readonly IScope _scope;
        private readonly ISpan _span;
        #endregion

        #region constructors
        /// <summary>
        /// Creates a new context with the span
        /// </summary>
        /// <param name="span">The opentracing span</param>
        public OpenTracingSpan(ISpan span)
        {
            _span = span;
            Context = new OpenTracingContext(_span.Context);
        }

        /// <summary>
        /// Creates a span from a opentracing scope
        /// </summary>
        /// <param name="scope">The opentracing scope</param>
        public OpenTracingSpan(IScope scope) : this(scope.Span)
        {
            _scope = scope;
        }
        #endregion

        #region properties
        /// <summary>
        /// The context the span should surround
        /// </summary>
        public ITracingSpanContext Context { get; }
        /// <summary>
        /// The status of the span
        /// </summary>
        public TracingStatus Status { get; set; }
        #endregion

        #region public methods
        /// <summary>
        /// Sets an attribute to the span. Can be searched and filtered by the key
        /// </summary>
        /// <param name="key">The key of the attribute</param>
        /// <param name="value">The value of the attribute</param>
        public void SetAttribute(string key, string value)
        {
            _span.SetTag(key, value);
        }

        /// <summary>
        /// Sets an attribute to the span. Can be searched and filtered by the key
        /// </summary>
        /// <param name="key">The key of the attribute</param>
        /// <param name="value">The value of the attribute</param>
        public void SetAttribute(string key, object value)
        {
            _span.SetTag(key, value.ToString());
        }

        /// <summary>
        /// Sets an attribute to the span. Can be searched and filtered by the key
        /// </summary>
        /// <param name="key">The key of the attribute</param>
        /// <param name="value">The value of the attribute</param>
        public void SetAttribute(string key, int value)
        {
            _span.SetTag(key, value);
        }

        /// <summary>
        /// Sets an attribute to the span. Can be searched and filtered by the key
        /// </summary>
        /// <param name="key">The key of the attribute</param>
        /// <param name="value">The value of the attribute</param>
        public void SetAttribute(string key, long value)
        {
            _span.SetTag(key, value.ToString());
        }

        /// <summary>
        /// Sets an attribute to the span. Can be searched and filtered by the key
        /// </summary>
        /// <param name="key">The key of the attribute</param>
        /// <param name="value">The value of the attribute</param>
        public void SetAttribute(string key, double value)
        {
            _span.SetTag(key, value);
        }

        /// <summary>
        /// Sets an attribute to the span. Can be searched and filtered by the key
        /// </summary>
        /// <param name="key">The key of the attribute</param>
        /// <param name="value">The value of the attribute</param>
        public void SetAttribute(string key, bool value)
        {
            _span.SetTag(key, value);
        }

        /// <summary>
        /// Sets multiple attributes to the span. Can be searched and filtered by the key
        /// It tries to figure out the data type in order to use the correct method.
        /// </summary>
        /// <param name="attributes">The dictionary of all attributes</param>
        public void SetAttributes(IDictionary<string, object> attributes)
        {
            foreach ((string key, object value) in attributes)
            {
                switch (value)
                {
                    case bool boolValue:
                        _span.SetTag(key, boolValue);
                        break;
                    case long longValue:
                        _span.SetTag(key, longValue.ToString());
                        break;
                    case int intValue:
                        _span.SetTag(key, intValue);
                        break;
                    case double doubleValue:
                        _span.SetTag(key, doubleValue);
                        break;
                    default:
                        _span.SetTag(key, value.ToString());
                        break;
                }
            }
        }

        /// <summary>
        /// Logs the attributes of the event.
        /// Logs cannot be searched
        /// </summary>
        /// <param name="tracingEvent">The event, which attributes get logged to the span</param>
        public void LogEvent(TracingEvent tracingEvent)
        {
            _span.Log(tracingEvent.Attributes);
        }

        /// <summary>
        /// Logs a message to the span
        /// Logs cannot be searched
        /// </summary>
        /// <param name="message">The message which gets logged to the span</param>
        public void LogEvent(string message)
        {
            _span.Log(message);
        }

        /// <summary>
        /// Logs a exception to the span
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception, which gets logged to the span</param>
        /// <param name="message">The message which gets logged to the span</param>
        public void LogError(Exception exception, string message)
        {
            LogException(exception, message);
        }

        /// <summary>
        /// Logs a exception to the span
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception, which gets logged to the span</param>
        public void LogError(Exception exception)
        {
            LogException(exception);
        }

        /// <summary>
        /// Logs a exception to the span
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception, which gets logged to the span</param>
        /// <param name="status">The status gets set as the span status</param>
        public void LogError(Exception exception, TracingStatus status)
        {
            LogException(exception, status);
        }

        /// <summary>
        /// Logs a exception to the span
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception, which gets logged to the span</param>
        /// <param name="status">The status gets set as the span status</param>
        /// <param name="message">The message which gets logged to the span</param>
        public void LogError(Exception exception, TracingStatus status, string message)
        {
            LogException(exception, status, message);
        }

        /// <summary>
        /// Logs a exception to the span
        /// Logs cannot be searched
        /// </summary>
        /// <param name="message">The message gets logged with key "error" and the status <see cref="TracingStatus.Unknown"/></param>
        public void LogError(string message)
        {
            _span.Log(new Dictionary<string, object> { { "error", message } });
            Status = TracingStatus.Unknown.WithDescription(message);
        }

        /// <summary>
        /// Logs an error. Sets the Status of the span to <paramref name="status"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="message">The message to be logged</param>
        /// <param name="status">The status of the error</param>
        public void LogError(string message, TracingStatus status)
        {
            _span.Log(new Dictionary<string, object> { { "error", message } });
            Status = status;
        }
        #endregion

        #region private methods
        private void LogException(Exception exception, string message)
        {
            LogException(exception, TracingStatus.Unknown.WithDescription(message), message);
        }

        private void LogException(Exception exception)
        {
            LogException(exception, TracingStatus.Unknown.WithDescription(exception.Message));
        }

        private void LogException(Exception exception, TracingStatus status, string message = null)
        {
            Dictionary<string, object> exceptionLogs = CreateExceptionLogs(exception, message);
            _span.Log(exceptionLogs);
            Status = status;
        }

        private static Dictionary<string, object> CreateExceptionLogs(Exception exception, string message)
        {
            Dictionary<string, object> exceptionLogs = CreateExceptionLogs(exception);
            if (!string.IsNullOrWhiteSpace(message))
            {
                exceptionLogs.Add("message", message);
            }

            return exceptionLogs;
        }

        private static Dictionary<string, object> CreateExceptionLogs(Exception exception)
        {
            return new Dictionary<string, object> { { "exception", exception } };
        }

        public void Dispose()
        {
            if (!Status.IsOk)
            {
                _span.SetTag("error", true);
            }

            _span.SetTag("status", Status.CanonicalCode.ToString());
            _span.SetTag("status-description", Status.Description);

            _scope?.Dispose();
        }
        #endregion
    }
}
