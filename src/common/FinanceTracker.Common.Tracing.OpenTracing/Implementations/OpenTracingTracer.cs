﻿using System;
using System.Collections.Generic;
using FinanceTracker.Common.Tracing.Abstractions;
using OpenTracing;
using OpenTracing.Propagation;
using ITracer = FinanceTracker.Common.Tracing.Abstractions.ITracer;

namespace FinanceTracker.Common.Tracing.OpenTracing.Implementations
{
    /// <summary>
    /// Opentracing implementation of the <see cref="ITracer"/>
    /// Basically a wrapper around <see cref="OpenTracing.ITracer"/>
    /// </summary>
    public class OpenTracingTracer : ITracer
    {
        private readonly global::OpenTracing.ITracer _tracer;
        private ITracingSpan _activeSpan;
        private IScope _scope;

        /// <summary>
        /// Instantiates an new instance of the <see cref="OpenTracingTracer"/>
        /// </summary>
        /// <param name="tracer">The opentracing implementation</param>
        public OpenTracingTracer(global::OpenTracing.ITracer tracer)
        {
            _tracer = tracer ?? throw new ArgumentNullException(nameof(tracer));
        }

        /// <summary>
        /// Returns the current span
        /// </summary>
        public ITracingSpan ActiveSpan
        {
            get
            {
                if (_tracer.ActiveSpan.Context.SpanId != _activeSpan?.Context?.SpanId)
                {
                    _activeSpan = new OpenTracingSpan(_tracer.ActiveSpan);
                }

                return _activeSpan;
            }
        }

        /// <summary>
        /// Creates a new span as a child of the current span.
        /// </summary>
        /// <param name="operationName">The name of the span</param>
        /// <returns>The newly created span</returns>
        public ITracingSpan BuildSpan(string operationName)
        {
            return CreateSpan(operationName);
        }

        /// <summary>
        /// Creates a new span as a child of the given span.
        /// </summary>
        /// <param name="operationName">The name of the span</param>
        /// <param name="parent">The span which should act as the parent</param>
        /// <returns>The newly created span</returns>
        public ITracingSpan BuildSpan(string operationName, ITracingSpan parent)
        {
            var spanContextCarrier = new Dictionary<string, string>();
            _tracer.Inject(_tracer.ActiveSpan.Context, BuiltinFormats.TextMap, new TextMapInjectAdapter(spanContextCarrier));
            ISpanContext spanContext = _tracer.Extract(
                BuiltinFormats.TextMap,
                new TextMapExtractAdapter(spanContextCarrier));
            return CreateSpan(operationName, spanContext);
        }

        private ITracingSpan CreateSpan(string operationName)
        {
            _scope = _tracer.BuildSpan(operationName).StartActive(true);
            return CreateSpan();
        }

        private ITracingSpan CreateSpan(string operationName, ISpanContext spanContext)
        {
            _scope = _tracer.BuildSpan(operationName).AsChildOf(spanContext).StartActive(true);
            return CreateSpan();
        }

        private ITracingSpan CreateSpan()
        {
            _activeSpan = new OpenTracingSpan(_scope);
            return ActiveSpan;
        }
    }

}