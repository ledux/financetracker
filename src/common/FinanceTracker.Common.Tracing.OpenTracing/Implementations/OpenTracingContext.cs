﻿using System;
using FinanceTracker.Common.Tracing.Abstractions;
using OpenTracing;

namespace FinanceTracker.Common.Tracing.OpenTracing.Implementations
{
    /// <summary>
    /// Wrapper class for the <see cref="ISpanContext"/> of OpenTracing
    /// </summary>
    public class OpenTracingContext : ITracingSpanContext
    {
        private readonly ISpanContext _spanContext;

        /// <summary>
        /// Instantiates an instance with the <see cref="ISpanContext"/>
        /// </summary>
        /// <param name="spanContext">The concrete instance of a <see cref="ISpanContext"/> of OpenTracing</param>
        public OpenTracingContext(ISpanContext spanContext)
        {
            _spanContext = spanContext ?? throw new ArgumentNullException(nameof(spanContext));
        }

        /// <summary>
        /// The identification of the trace, to which the span belongs.
        /// Maps to <see cref="ISpanContext.TraceId"/>
        /// </summary>
        public string TraceId => _spanContext.TraceId;

        /// <summary>
        /// The identification of the span.
        /// Maps to <see cref="ISpanContext.SpanId"/>
        /// </summary>
        public string SpanId => _spanContext.SpanId;
    }
}
