﻿namespace FinanceTracker.Common.Tracing.Abstractions
{
    /// <summary>
    /// The context of a span
    /// </summary>
    public interface ITracingSpanContext
    {
        /// <summary>
        /// The identification of the trace, to which the span belongs.
        /// </summary>
        string TraceId { get; }
        /// <summary>
        /// The identification of the span.
        /// </summary>
        string SpanId { get; }
    }
}