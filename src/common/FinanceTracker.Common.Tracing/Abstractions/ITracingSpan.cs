﻿using System;
using System.Collections.Generic;
using FinanceTracker.Common.Tracing.Models;

namespace FinanceTracker.Common.Tracing.Abstractions
{
    /// <summary>
    /// Represents a span in a trace
    /// </summary>
    public interface ITracingSpan : IDisposable
    {
                /// <summary>
        /// The context in which this span exists
        /// </summary>
        ITracingSpanContext Context { get; }
        /// <summary>
        /// The current status of the span
        /// </summary>
        TracingStatus Status { get; set; }
        /// <summary>
        /// Sets a tag with a string value.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="key">The key of the tag</param>
        /// <param name="value">The value of the tag</param>
        void SetAttribute(string key, string value);
        /// <summary>
        /// Sets a tag with an object value.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="key">The key of the tag</param>
        /// <param name="value">The value of the tag</param>
        void SetAttribute(string key, object value);
        /// <summary>
        /// Sets a tag with an int value.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="key">The key of the tag</param>
        /// <param name="value">The value of the tag</param>
        void SetAttribute(string key, int value);
        /// <summary>
        /// Sets a tag with a long value.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="key">The key of the tag</param>
        /// <param name="value">The value of the tag</param>
        void SetAttribute(string key, long value);
        /// <summary>
        /// Sets a tag with a double value.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="key">The key of the tag</param>
        /// <param name="value">The value of the tag</param>
        void SetAttribute(string key, double value);
        /// <summary>
        /// Sets a tag with a boolean value.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="key">The key of the tag</param>
        /// <param name="value">The value of the tag</param>
        void SetAttribute(string key, bool value);
        /// <summary>
        /// Sets multiple tags at once.
        /// This is an attribute in Opentelemetry and a Tag in Opentracing
        /// </summary>
        /// <param name="attributes">The key value pairs, which should be added</param>
        void SetAttributes(IDictionary<string, object> attributes);
        /// <summary>
        /// Adds an event to the span.
        /// Logs the attributes of the event.
        /// Logs cannot be searched
        /// </summary>
        /// <param name="tracingEvent">The event which will be added</param>
        void LogEvent(TracingEvent tracingEvent);
        /// <summary>
        /// Logs an message as an event
        /// Logs cannot be searched
        /// </summary>
        /// <param name="message">The message which will be logged</param>
        void LogEvent(string message);
        /// <summary>
        /// Logs an exception. Sets the Status of the span to <see cref="StatusCanonicalCode.Unknown"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception which will be logged</param>
        /// <param name="message">An addtional message</param>
        void LogError(Exception exception, string message);
        /// <summary>
        /// Logs an exception. Sets the Status of the span to <see cref="StatusCanonicalCode.Unknown"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception which will be logged</param>
        void LogError(Exception exception);
        /// <summary>
        /// Logs an exception. Sets the Status of the span to <paramref name="status"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception which will be logged</param>
        /// <param name="status">The status to which the span will be set</param>
        void LogError(Exception exception, TracingStatus status);
        /// <summary>
        /// Logs an exception. Sets the Status of the span to <paramref name="status"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="exception">The exception which will be logged</param>
        /// <param name="status">The status to which the span will be set</param>
        /// <param name="message">An addtional message</param>
        void LogError(Exception exception, TracingStatus status, string message);
        /// <summary>
        /// Logs an error. Sets the Status of the span to <see cref="StatusCanonicalCode.Unknown"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="message">error message</param>
        void LogError(string message);
        /// <summary>
        /// Logs an error. Sets the Status of the span to <paramref name="status"/>
        /// Logs cannot be searched
        /// </summary>
        /// <param name="message">The message to be logged</param>
        /// <param name="status">The status of the error</param>
        void LogError(string message, TracingStatus status);
    }
}