﻿namespace FinanceTracker.Common.Tracing.Abstractions
{
    /// <summary>
    /// Provides functionality to create tracing spans
    /// </summary>
    public interface ITracer
    {
        /// <summary>
        /// Reference to the active span
        /// </summary>
        ITracingSpan ActiveSpan { get; }
        /// <summary>
        /// Creates a new tracing span.
        /// </summary>
        /// <param name="operationName">A descriptive name of the created span </param>
        /// <returns>The newly created span</returns>
        ITracingSpan BuildSpan(string operationName);
        /// <summary>
        /// Creates a new tracing span as a child of an existing span
        /// </summary>
        /// <param name="operationName">A descriptive name of the created span </param>
        /// <param name="parent">The span which acts as a parent</param>
        /// <returns>The newly created span</returns>
        ITracingSpan BuildSpan(string operationName, ITracingSpan parent);
        
    }
}