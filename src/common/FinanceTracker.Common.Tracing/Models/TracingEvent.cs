﻿using System;
using System.Collections.Generic;

namespace FinanceTracker.Common.Tracing.Models
{
    /// <summary>
    /// Indicates an event in the the trace/span. It gets logged as key/value pairs.
    /// </summary>
    public class TracingEvent
    {
        private static readonly Dictionary<string, object> EmptyAttributes = new Dictionary<string, object>();

        /// <summary>
        /// Gets the name of the event. Will be logged as message.
        /// </summary>
        public string Name { get; }
        /// <summary>
        /// Gets the time at which the event occured
        /// </summary>
        public DateTime TimeStamp { get; }
        /// <summary>
        /// Gets the attributes of the event
        /// </summary>
        public IDictionary<string, object> Attributes { get; }

        /// <summary>
        /// Instantiates a new event with UtcNow and empty attributes
        /// </summary>
        /// <param name="name">The name of the event</param>
        public TracingEvent(string name) : this(name, DateTime.UtcNow, EmptyAttributes) { }

        /// <summary>
        /// Instantiates a new event with UtcNow 
        /// </summary>
        /// <param name="name">The name of the event</param>
        /// <param name="attributes">Key/value pairs of the event</param>
        public TracingEvent(string name, IDictionary<string, object> attributes) : this(name, DateTime.UtcNow, attributes) { }

        /// <summary>
        /// Instantiates a new event with UtcNow 
        /// </summary>
        /// <param name="name">The name of the event</param>
        /// <param name="attributes">Key/value pairs of the event</param>
        /// <param name="timeStamp">Event timestamp. Timestamp MUST only be used for the events that happened in the past, not at the moment of this call.</param>
        public TracingEvent(string name, in DateTime timeStamp, IDictionary<string, object> attributes)
        {
            Name = name ?? string.Empty;
            TimeStamp = timeStamp != default ? timeStamp : DateTime.UtcNow;
            Attributes = attributes ?? EmptyAttributes;
            Attributes["name"] = Name;
        }
    }

}