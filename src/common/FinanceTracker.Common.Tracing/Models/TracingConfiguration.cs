﻿namespace FinanceTracker.Common.Tracing.Models
{
    /// <summary>
    /// Model of the Tracing configuration
    /// </summary>
    public class TracingConfiguration
    {
        /// <summary>
        /// Host of the tracing agent 
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Port of the tracing agent 
        /// </summary>
        public int Port { get; set; }
    }
}
